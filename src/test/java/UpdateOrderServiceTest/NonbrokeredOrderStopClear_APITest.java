package UpdateOrderServiceTest;

import BaseTest.BaseTest;
import apiManager.APIManager;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pojoClasses.requestPojo.UpdateOrderMicroservice.StopClearNonbrokered.StopClear;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import requestHelper.RequestMethod;
import requestPayloadProvider.UpdateOrderMicroService.StopClearNonbrokered.StopClearNonbrokeredRequestPayloadProvider;
import testConfiguration.TestConfig;

public class NonbrokeredOrderStopClear_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST") + TestConfig.get("UPDATE_ORDER_SERVICE_KONG_URL")).
                basePath(TestConfig.get("STOP_CLEAR_NONBROKERED_ORDER_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @BeforeClass(alwaysRun = true)
    public void createTestData(ITestContext context) throws Exception {
        String orderID = APIManager.getNewOrderAPIInstance().createFlatbedFTLOrder();
        context.setAttribute("orderID", orderID);
        APIManager.getDTTAssignmentAPIInstance().
                assignDriverTractorTrailerToOrder(orderID,"ATTEST","ATTRAC1","ATTRAIL1");
    }

    @Test(groups = {"HealthCheck"})
    public void non_brokered_order_stop_clear_API_health_check(ITestContext context) throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        StopClearNonbrokeredRequestPayloadProvider stopClearNonbrokeredRequestPayloadProvider = new StopClearNonbrokeredRequestPayloadProvider();
        StopClear reqPayload = stopClearNonbrokeredRequestPayloadProvider.
                getNonbrokeredStopClearPayloadForStop((String) context.getAttribute("orderID"),1);

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST_DEBUG(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }
}
