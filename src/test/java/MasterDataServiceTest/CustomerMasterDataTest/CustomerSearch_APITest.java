/**
 *
 */
package MasterDataServiceTest.CustomerMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestPayloadProvider.MasterData.Customer.CustomerSearchAPIRequestPayload;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.*;
import pojoClasses.responsePojo.MasterData.Customer.Customer;
import testConfiguration.TestConfig;

import java.util.List;

/**
 * @author Manoj
 *
 */
public class CustomerSearch_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("CUSTOMER_SEARCH_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void customer_search_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setUserGrop("FLAT");
        reqPayload.setIsActive("Y");

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_customer_list_returned_IsNot_MoreThan_100() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("NULL Response object. \n Response body: \n"
                    + response.asPrettyString());
        }
        if (customerList.size() != 0) {
            Assert.assertTrue(customerList.size() <= 100,
                    "Customer list size expected: <=100 but API returned size: " + customerList.size());
        } else {
            throw new Exception("Unexpected response.Response object is empty for customer list \n Response body: \n"
                    + response.asPrettyString());
        }
    }

    @Test
    public void verify_customer_list_entries_are_from_TMS() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("NULL Response object. \n Response body: \n"
                    + response.asPrettyString());
        }
        SoftAssert softAssert = new SoftAssert();
        if (customerList.size() != 0) {
            for (Customer c : customerList) {
                String companyID = (c.getCompanyId() == null) ? "NULL" : c.getCompanyId().trim();
                softAssert.assertEquals(companyID, "T100", "CustomerID: " + c.getId() + " expected companyID: T100"
                        + " but API returned companyID: " + c.getCompanyId());
            }
            softAssert.assertAll();
        } else {
            throw new Exception("Unexpected response!!! Response object is empty for customer list \n Response body: \n"
                    + response.asPrettyString());
        }
    }

    @Test
    public void verify_customer_details_returned() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerId("1000092");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        Customer customer;
        try {
            customer = response.jsonPath().getObject("response[0]", Customer.class);
        } catch (Exception e) {
            throw new Exception("Response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        String expectedCustomerID = "1000092 ";
        String expectedCustomerName = "A&T STAINLESS";
        String expectedCity = "MIDLAND                                           ";
        String expectedState = "PA";
        String expectedZip = "15059     ";
        String expectedLocationAddress = "ATTN ACCTS PAYABLE";
        String expectedCompanyId = "TMS ";
        Double expectedCreditLimit = (double) 5000;
        String expectedIsActive = "Y";

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(customer.getId(), expectedCustomerID,
                "Expected Customer ID: " + expectedCustomerID + " but API returned " + customer.getId());
        softAssert.assertEquals(customer.getName(), expectedCustomerName,
                "Expected Customer name: " + expectedCustomerName + " but API returned " + customer.getName());
        softAssert.assertEquals(customer.getCity(), expectedCity,
                "Expected Customer city: " + expectedCity + " but API returned " + customer.getCity());
        softAssert.assertEquals(customer.getState(), expectedState,
                "Expected Customer state: " + expectedState + " but API returned " + customer.getCity());
        softAssert.assertEquals(customer.getZipCode(), expectedZip,
                "Expected Customer zip: " + expectedZip + " but API returned " + customer.getCity());
        softAssert.assertEquals(customer.getLocationAddress(), expectedLocationAddress,
                "Expected Customer location address: " + expectedLocationAddress + " but API returned "
                        + customer.getLocationAddress());
        softAssert.assertEquals(customer.getCompanyId(), expectedCompanyId,
                "Expected Customer company id: " + expectedCompanyId + " but API returned " + customer.getCompanyId());
        softAssert.assertEquals(customer.getCreditLimit(), expectedCreditLimit, "Expected Customer credit limit: "
                + expectedCreditLimit + " but API returned " + customer.getCreditLimit());
        softAssert.assertEquals(customer.getIsActive(), expectedIsActive, "Expected Customer IsActive state: "
                + expectedIsActive + " but API returned " + customer.getIsActive());
        softAssert.assertAll();
    }

    @Test
    public void verify_customer_1000020_is_InActive() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerId("1000020");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        Customer customer;
        try {
            customer = response.jsonPath().getObject("response[0]", Customer.class);
        } catch (Exception e) {
            throw new Exception("Response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }
        Assert.assertEquals(customer.getIsActive(), "N",
                "Expected Customer-1000020 IsActive state: N " + " but API returned " + customer.getIsActive());
    }

    @Test
    public void verify_invalid_customerID_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data supplier needed
        reqPayload.setCustomerId("123009");
        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        int customerListSize = response.jsonPath().getList("response", Customer.class).size();
        int expected_CustomerListSize = 0;
        Assert.assertEquals(customerListSize, expected_CustomerListSize,
                "Expected Customer list size-" + expected_CustomerListSize + " but API returned " + customerListSize);
    }

    @Test(groups = { "Sanity" })
    public void verify_user_search_by_customerID_1007777() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setUserGrop("FLAT");
        reqPayload.setIsActive("Y");
        reqPayload.setCustomerId("1007777 ");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customer = parseResponseToObjectList(response,Customer.class);

        String expectedCustomerID = "1007777 ";
        String retrievedCustomerID = customer.get(0).getId();
        Assert.assertEquals(retrievedCustomerID, expectedCustomerID,
                "Expected Customer ID: [" + expectedCustomerID + "] but API returned [" + retrievedCustomerID + "]");
    }

    @Test
    public void verify_user_search_by_partialCustomerID_286_returns_customerID_that_contains_286() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerId("286");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customer.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }
        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customer) {
            softAssert.assertTrue(c.getId().contains("286"),
                    "CustomerId: " + c.getId() + "does not contain pattern 286");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_customerName_TCW_INC() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerName("TCW INC");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        Customer customer;
        try {
            customer = response.jsonPath().getObject("response[0]", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        String expectedCustomerName = "TCW INC";
        Assert.assertEquals(customer.getName(), expectedCustomerName,
                "Expected Customer Name-" + expectedCustomerName + " but API returned " + customer.getName());
    }

    @Test
    public void verify_user_search_by_partial_CustomerName_WALM_returns_customerNames_that_contains_WALM() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerName("WALM");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customer.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customer) {
            softAssert.assertTrue(c.getName().toLowerCase().contains("walm"),
                    "CustomerID: " + c.getId() + "does not contain pattern WALM in its name");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_CustomerName_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data suplier needed
        reqPayload.setCustomerName("xyz");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }

        int customerListSize = customer.size();
        int expected_CustomerListSize = 0;

        Assert.assertEquals(customerListSize, expected_CustomerListSize,
                "Expected Customer list size-" + expected_CustomerListSize + " but API returned " + customerListSize);
    }

    @Test
    public void verify_user_search_by_customerAddress_319_COMMERCE_WAY() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setAddress("319 COMMERCE WAY");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        Customer customer;
        try {
            customer = response.jsonPath().getObject("response[0]", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        String expectedCustomerAddress = "319 COMMERCE WAY";

        Assert.assertEquals(customer.getLocationAddress().toUpperCase(), expectedCustomerAddress,
                "Expected Customer Address-" + expectedCustomerAddress + " but API returned "
                        + customer.getLocationAddress() + " for CustomerID: " + customer.getId());
    }

    @Test
    public void verify_user_searchBy_partial_CustomerAddress_DEERE_returns_customerNames_that_contains_DEERE() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerName("DEERE");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customer.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customer) {
            softAssert.assertTrue(c.getName().toLowerCase().contains("deere"),
                    "CustomerID: " + c.getId() + "does not contain pattern DEERE in its address");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_customerAddress_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data suplier needed
        reqPayload.setAddress("XYZ");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }

        int customerListSize = customer.size();
        int expectedCustomerListSize = 0;

        Assert.assertEquals(customerListSize, expectedCustomerListSize,
                "Expected Customer list size-" + expectedCustomerListSize + " but API returned " + customerListSize);
    }

    @Test
    public void verify_user_searchBy_city_LYNBROOK() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCity("LYNBROOK");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }
        String expectedCity = "LYNBROOK";

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customerList) {
            softAssert.assertEquals(c.getCity().trim(), expectedCity, "Expected Customer city-" + expectedCity
                    + " but API returned " + c.getCity().trim() + " for CustomerID: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test(enabled = false)
    public void verify_User_SearchBy_PartialCity_Returns_CustomerNames_That_Contains_partial() {

    }

    @Test
    public void verify_invalid_customerCity_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data suplier needed
        reqPayload.setCity("9");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customer;
        try {
            customer = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }

        int customerListSize = customer.size();
        int expectedCustomerListSize = 0;

        Assert.assertEquals(customerListSize, expectedCustomerListSize,
                "Expected Customer list size-" + expectedCustomerListSize + " but API returned " + customerListSize);
    }

    @Test
    public void verify_user_searchBy_ZIP_60440() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setZip("60440");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }
        String expectedZip = "60440";

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customerList) {
            softAssert.assertTrue(c.getZipCode().trim().contains(expectedZip), "Expected Customer zip- " + expectedZip
                    + " but API returned " + c.getZipCode().trim() + " for CustomerID: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_searchBy_partial_customerAddress_604_returns_customerNames_that_Contains_604() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setZip("604");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customerList) {
            softAssert.assertTrue(c.getZipCode().toLowerCase().contains("604"),
                    "CustomerID: " + c.getId() + "does not contain pattern 604 in its zip");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_CustomerZip_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data suplier needed
        reqPayload.setZip("000000");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }

        int customerListSize = customerList.size();
        int expectedCustomerListSize = 0;

        Assert.assertEquals(customerListSize, expectedCustomerListSize,
                "Expected Customer list size-" + expectedCustomerListSize + " but API returned " + customerListSize);
    }

    @Test
    public void verify_user_searchBy_state_NY() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setState("NY");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }
        String expectedState = "NY";

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customerList) {
            softAssert.assertTrue(c.getState().trim().equals(expectedState), "Expected Customer State- " + expectedState
                    + " but API returned " + c.getState().trim() + " for CustomerID: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_searchBy_partial_StateCode_N_returns_stateCose_that_contains_N() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setState("N");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (Customer c : customerList) {
            softAssert.assertTrue(c.getState().toLowerCase().contains("n"),
                    "CustomerID: " + c.getId() + "does not contain pattern 'n' in its state code");
        }
        softAssert.assertAll();

    }

    @Test
    public void verify_invalid_customerState_returns_emptyList() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        // data suplier needed
        reqPayload.setState("XYZ");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception(
                    "Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }

        int customerListSize = customerList.size();
        int expectedCustomerListSize = 0;

        Assert.assertEquals(customerListSize, expectedCustomerListSize,
                "Expected Customer list size-" + expectedCustomerListSize + " but API returned " + customerListSize);
    }

    @Test
    public void verify_user_searchBy_ALL_search_criteria() throws Exception {
        CustomerSearchAPIRequestPayload reqPayload = new CustomerSearchAPIRequestPayload();
        reqPayload.setAgencyId("1001");
        reqPayload.setUserType("AGFT");
        reqPayload.setCustomerId("1000028");
        reqPayload.setCustomerName("7014 C T S");
        reqPayload.setAddress("2515 BRUNSWICK AVE BLDG B");
        reqPayload.setCity("LINDEN");
        reqPayload.setZip("7036");
        reqPayload.setState("NJ");

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<Customer> customerList;
        try {
            customerList = response.jsonPath().getList("response", Customer.class);
        } catch (Exception e) {
            throw new Exception("Unexpected!! Response object is null \n Response payload: \n" + response.asPrettyString());
        }
        if (customerList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
        }

        String expectedCustomerID = "1000028 ";
        Assert.assertEquals(customerList.get(0).getId(), expectedCustomerID);
    }
}
