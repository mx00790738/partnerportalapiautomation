/**
 * 
 */
package MasterDataServiceTest.CustomerMasterDataTest;

import java.util.List;

import BaseTest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestPayloadProvider.MasterData.Customer.CustomerDirectAPIRequestPayloadProvider;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.MasterData.Customer.DirectCustomerDetails;
import testConfiguration.TestConfig;

/**
 * @author Manoj
 *good to go
 */
public class CustomerDirect_APITest extends BaseTest {
	private RequestSpecification requestSpecification;

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
				.basePath(TestConfig.get("CUSTOMER_DIRECT_API_PATH"))
				.header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
						TestConfig.get("PASSWORD")))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}

	@Test(groups = { "HealthCheck"})
	public void customer_direct_API_health_check() throws Exception {
		CustomerDirectAPIRequestPayloadProvider reqPayload = new CustomerDirectAPIRequestPayloadProvider();
		reqPayload.setDivision("FLAT");

		RequestSpecification requestSpecification = this.requestSpecification;
		requestSpecification.body(reqPayload);

		Response response = RequestMethod.POST(requestSpecification);
		BusinessResponse responsePayload = response.as(BusinessResponse.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(response.getStatusCode(), 200,
				"Expected status code: 200 instead API returned status code: " + response.getStatusCode());

		softAssert.assertEquals(responsePayload.getCode(), 200,
				"Expected code: 200 in response payload instead API returned status code: "
						+ responsePayload.getCode());

		String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
		softAssert.assertEquals(status, "SUCCESS",
				"Expected status: SUCCESS in response payload instead API returned status: "
						+ responsePayload.getStatus());

		ResponseStatus responseStatus = responsePayload.getResponseStatus();
		if(responseStatus != null) {
			String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseCode();
			softAssert.assertEquals(responseCode, "000",
					"Expected responseCode: 000 in response payload instead API returned responseCode: "
							+ responseCode);
			String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseDesc();
			softAssert.assertEquals(responseDesc, "SUCCESS",
					"Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
							+ responseDesc);
		} else{
			softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
		}
		softAssert.assertAll();
	}

	@Test(groups = { "Sanity" })
	public void verify_active_direct_customers_1008728_details() throws Exception {
		CustomerDirectAPIRequestPayloadProvider reqPayload = new CustomerDirectAPIRequestPayloadProvider();
		reqPayload.setDivision("FLAT");

		RequestSpecification requestSpecification = this.requestSpecification;
		requestSpecification.body(reqPayload);

		Response response = RequestMethod.POST(requestSpecification);
		List<DirectCustomerDetails> directCustomerList = parseResponseToObjectList(response,DirectCustomerDetails.class);

		DirectCustomerDetails retrievedDirectCustomer =
				retrieveObjectFromObjectList(directCustomerList,"getId","1008728 ");

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(retrievedDirectCustomer.getCity().trim(), "LOCUST",
				"Expected city: LOCUST but API returned: " + retrievedDirectCustomer.getCity());

		softAssert.assertEquals(retrievedDirectCustomer.getCompanyId().trim(), "T100",
				"Expected company id: T100 but API returned: " + retrievedDirectCustomer.getCompanyId());

		softAssert.assertEquals(retrievedDirectCustomer.getCreditLimit(), 66665.0,
				"Expected credit limit: 66665.0 but API returned: " + retrievedDirectCustomer.getCreditLimit());

		softAssert.assertEquals(retrievedDirectCustomer.getId(), "1008728 ",
				"Expected customer id: \"1008728 \" but API returned: " + retrievedDirectCustomer.getId());

		softAssert.assertEquals(retrievedDirectCustomer.getIsActive(), "Y",
				"Expected IsActive status: \"Y\" but API returned: " + retrievedDirectCustomer.getIsActive());

		softAssert.assertEquals(retrievedDirectCustomer.getLocationAddress(), "421 BROWNS HILL ROAD",
				"Expected loc address status: \"421 BROWNS HILL ROAD\" but API returned: "
						+ retrievedDirectCustomer.getLocationAddress());

		softAssert.assertEquals(retrievedDirectCustomer.getName(), "CHICAGO TUBE & IRON - LOCUST DC",
				"Expected IsActive status: \"CHICAGO TUBE & IRON - LOCUST DC\" but API returned: " + retrievedDirectCustomer.getName());

		softAssert.assertEquals(retrievedDirectCustomer.getState(), "NC",
				"Expected IsActive status: \"NC\" but API returned: " + retrievedDirectCustomer.getState());

		softAssert.assertEquals(retrievedDirectCustomer.getZipCode().trim(), "28097",
				"Expected IsActive status: \"28097\" but API returned: " + retrievedDirectCustomer.getZipCode());

		softAssert.assertAll();
	}

	@Test
	public void verify_inActive_direct_customers_1004996_details() throws Exception {
		CustomerDirectAPIRequestPayloadProvider reqPayload = new CustomerDirectAPIRequestPayloadProvider();
		RequestSpecification requestSpecification = this.requestSpecification;
		requestSpecification.body(reqPayload);

		Response response = RequestMethod.POST(requestSpecification);
		List<DirectCustomerDetails> directCustomerList = parseResponseToObjectList(response,DirectCustomerDetails.class);

		DirectCustomerDetails retrievedDirectCustomer =
				retrieveObjectFromObjectList(directCustomerList,"getId","1004996 ");

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(retrievedDirectCustomer.getCity().trim(), "BOISE",
				"Expected city: BOISE but API returned: " + retrievedDirectCustomer.getCity());

		softAssert.assertEquals(retrievedDirectCustomer.getCompanyId().trim(), "TMS",
				"Expected company id: TMS but API returned: " + retrievedDirectCustomer.getCompanyId());

		softAssert.assertEquals(retrievedDirectCustomer.getCreditLimit(), 5000.0,
				"Expected credit limit: 5000 but API returned: " + retrievedDirectCustomer.getCreditLimit());

		softAssert.assertEquals(retrievedDirectCustomer.getId(), "1004996 ",
				"Expected customer id: \"1004996\" but API returned: " + retrievedDirectCustomer.getId());

		softAssert.assertEquals(retrievedDirectCustomer.getIsActive(), "N",
				"Expected IsActive status: \"Y\" but API returned: " + retrievedDirectCustomer.getIsActive());

		softAssert.assertEquals(retrievedDirectCustomer.getLocationAddress(), "ACCTS PAYABLE",
				"Expected loc address status: \"ACCTS PAYABLE\" but API returned: "
						+ retrievedDirectCustomer.getLocationAddress());

		softAssert.assertEquals(retrievedDirectCustomer.getName(), "MOTIVE POWER INC",
				"Expected IsActive status: \"MOTIVE POWER INC\" but API returned: " + retrievedDirectCustomer.getName());

		softAssert.assertEquals(retrievedDirectCustomer.getState(), "ID",
				"Expected IsActive status: \"ID\" but API returned: " + retrievedDirectCustomer.getState());

		softAssert.assertEquals(retrievedDirectCustomer.getZipCode().trim(), "83716",
				"Expected IsActive status: \"83716\" but API returned: " + retrievedDirectCustomer.getZipCode());

		softAssert.assertAll();
	}
}
