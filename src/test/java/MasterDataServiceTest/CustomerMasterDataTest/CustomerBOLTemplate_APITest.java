package MasterDataServiceTest.CustomerMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.*;
import testConfiguration.TestConfig;

import java.util.List;

public class CustomerBOLTemplate_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
                .header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD")))
                .headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void customer_bol_template_API_health_check() throws Exception {
        String path = TestConfig.get("CUSTOMER_BOL_TEMPLATE_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000003"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_customer_1006521_returns_BOL_template_7722() throws Exception {
        String path = TestConfig.get("CUSTOMER_BOL_TEMPLATE_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1006521"));

        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        List<Integer> retrievedList = parseResponseToObjectList(response, Integer.class);

        if(retrievedList == null){
            throw new Exception("Response returned as NULL \n"+response.prettyPrint());
        }
        Assert.assertEquals(retrievedList.get(0),(Integer)7722,
                "Expected BOL Template: 7722 but API Returned: "+retrievedList.get(0));
    }

    @Test
    public void verify_customer_1000037_returns_BOL_template_7723() throws Exception {
        String path = TestConfig.get("CUSTOMER_BOL_TEMPLATE_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1006263"));

        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        List<Integer> retrievedList = parseResponseToObjectList(response, Integer.class);

        if(retrievedList == null){
            throw new Exception("Response returned as NULL \n"+response.prettyPrint());
        }
        Assert.assertEquals(retrievedList.get(0),(Integer)7721,
                "Expected BOL Template: 7721 but API Returned: "+retrievedList.get(0));
    }

    @Test
    public void verify_customer_1000220_returns_BOL_template_7700() throws Exception {
        String path = TestConfig.get("CUSTOMER_BOL_TEMPLATE_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000220"));

        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        List<Integer> retrievedList = parseResponseToObjectList(response, Integer.class);

        if(retrievedList == null){
            throw new Exception("Response returned as NULL \n"+response.prettyPrint());
        }
        Assert.assertEquals(retrievedList.get(0),(Integer)7700,
                "Expected BOL Template: 7700 but API Returned: "+retrievedList.get(0));
    }

    @Test
    public void verify_invalid_customer_returns_empty_response() throws Exception {
        String path = TestConfig.get("CUSTOMER_BOL_TEMPLATE_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "123"));

        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        List<Integer> retrievedList = parseResponseToObjectList(response, Integer.class);

        if(retrievedList == null){
            throw new Exception("Response returned as NULL \n"+response.prettyPrint());
        }
        Assert.assertEquals(retrievedList.size(),0,
                "Expected empty bol list response but API returned bol list size: "+retrievedList.size());
    }
}
