/**
 *
 */
package MasterDataServiceTest.CustomerMasterDataTest;

import static io.restassured.RestAssured.given;

import BaseTest.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.MasterData.Customer.CustomerContact;
import testConfiguration.TestConfig;

import java.util.List;

/**
 * @author Manoj
 *
 */
public class CustomerContact_APITest extends BaseTest {

    private static RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
                .header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD")))
                .headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void customer_contact_API_health_check() throws Exception {
        String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000112"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_no_of_contact_record_retrieved_is_3_for_CustomerID_1001438() throws Exception {
        String path = TestConfig.get("CUSTOMER_CONTACTS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1001438"));

        Response response = RequestMethod.GET(requestSpecification);
        List<CustomerContact> retrievedContact = parseResponseToObjectList(response, CustomerContact.class);

        int retrievedListSize = retrievedContact.size();
        Assert.assertEquals(retrievedListSize, 3,
                "Expected no. of contact records: 3 instead API returned no. of contact record: "
                        + retrievedListSize);

    }

    @Test
    public void verify_1st_contact_record_details_of_CustomerID_1000110() throws Exception {
        String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000110"));

        Response response = RequestMethod.GET(requestSpecification);
        List<CustomerContact> retrievedContactList = parseResponseToObjectList(response, CustomerContact.class);
        CustomerContact retrievedContact =
                retrieveObjectFromObjectList(retrievedContactList, "getId", "1000110");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(retrievedContact.getActive(), "Y",
                "Contact name MARK expected contact status: Y instead API returned contact status: "
                        + retrievedContact.getActive());

        softAssert.assertEquals(retrievedContact.getComapnyId(), "TMS ",
                "Contact name MARK expected company id: TMS instead API returned contact status: "
                        + retrievedContact.getComapnyId());

        softAssert.assertEquals(retrievedContact.getDetentionContact(), "N",
                "Contact name MARK expected detention contact: \"N\" instead API returned contact status: "
                        + retrievedContact.getDetentionContact());

        softAssert.assertEquals(retrievedContact.getEmail(), "mark001@gmail.com",
                "Contact name MARK expected email address: \"mark001@gmail.com\" instead API returned email address: "
                        + retrievedContact.getEmail());

        softAssert.assertEquals(retrievedContact.getFax(), "212-555-1234        ",
                "Contact name MARK expected Fax number: \"212-555-1234\" instead API returned Fax number: "
                        + retrievedContact.getFax());

        softAssert.assertEquals(retrievedContact.getMobilePhone(), "214-367-8803        ",
                "Contact name MARK expected mobile number: \"214-367-8803\" instead API returned mobile number: "
                        + retrievedContact.getMobilePhone());

        softAssert.assertEquals(retrievedContact.getName(), "MARK",
                "Expected contact name: \"MARK\" instead API returned contact name: " + retrievedContact.getName());

        softAssert.assertEquals(retrievedContact.getParentrowId(), "1000110 ",
                "Contact name MARK expected parent row id: \"1000110\" instead API returned parent row id: "
                        + retrievedContact.getParentrowId());

        softAssert.assertEquals(retrievedContact.getParentrowType(), "C",
                "Contact name MARK expected parent row type: \"C\" instead API returned parent row type: "
                        + retrievedContact.getParentrowType());

        softAssert.assertEquals(retrievedContact.getPayableContact(), "Y",
                "Contact name MARK expected payable contact flag: \"Y\" instead API returned payable contact flag: "
                        + retrievedContact.getPayableContact());

        softAssert.assertEquals(retrievedContact.getPhone(), "650-513-0154        ",
                "Contact name MARK expected phone number: \"650-513-0154\" instead API returned phone number: "
                        + retrievedContact.getPhone());

        softAssert.assertEquals(retrievedContact.getRapidAlertNotify(), "E",
                "Contact name MARK expected rapid alrent notify via: \"E\" instead API returned rapid alert notify via: "
                        + retrievedContact.getRapidAlertNotify());

        softAssert.assertEquals(retrievedContact.getTermsContact(), "N",
                "Contact name MARK expected terms contact flag: \"N\" instead API returned terms contact flag: "
                        + retrievedContact.getTermsContact());

        softAssert.assertEquals(retrievedContact.getTitle(), "MAIN CONTACT",
                "Contact name MARK expected contact title: \"N\" instead API returned contact title: "
                        + retrievedContact.getTitle());

        softAssert.assertEquals(retrievedContact.getWebAccess(), "Y",
                "Contact name MARK expected web access flag: \"Y\" instead API returned web access flag: "
                        + retrievedContact.getWebAccess());

        softAssert.assertAll();
    }

    @Test
    public void verify_2nd_contact_record_details_of_CustomerID_1000110() throws Exception {

        String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000110"));

        Response response = RequestMethod.GET(requestSpecification);
        List<CustomerContact> retrievedContactList = parseResponseToObjectList(response, CustomerContact.class);
        CustomerContact retrievedContact =
                retrieveObjectFromObjectList(retrievedContactList, "getId", "1000110");


        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(retrievedContact.getActive(), "N",
                "Contact name ROSALIN expected contact status: Y instead API returned contact status: "
                        + retrievedContact.getActive());

        softAssert.assertEquals(retrievedContact.getComapnyId(), "TMS ",
                "Contact name ROSALIN expected company id: TMS instead API returned contact status: "
                        + retrievedContact.getComapnyId());

        softAssert.assertEquals(retrievedContact.getDetentionContact(), "Y",
                "Contact name ROSALIN expected detention contact: \"Y\" instead API returned contact status: "
                        + retrievedContact.getDetentionContact());

        softAssert.assertEquals(retrievedContact.getEmail(), "rosalin.park@gmail.com",
                "Contact name ROSALIN expected email address: \"rosalin.park@gmail.com\" instead API returned email address: "
                        + retrievedContact.getEmail());

        softAssert.assertEquals(retrievedContact.getName(), "ROSALIN",
                "Expected contact name: \"ROSALIN\" instead API returned contact name: " + retrievedContact.getName());

        softAssert.assertEquals(retrievedContact.getParentrowId(), "1000110 ",
                "Contact name ROSALIN expected parent row id: \"1000110\" instead API returned parent row id: "
                        + retrievedContact.getParentrowId());

        softAssert.assertEquals(retrievedContact.getParentrowType(), "C",
                "Contact name ROSALIN expected parent row type: \"C\" instead API returned parent row type: "
                        + retrievedContact.getParentrowType());

        softAssert.assertEquals(retrievedContact.getPayableContact(), "N",
                "Contact name ROSALIN expected payable contact flag: \"N\" instead API returned payable contact flag: "
                        + retrievedContact.getPayableContact());

        softAssert.assertEquals(retrievedContact.getTermsContact(), "N",
                "Contact name ROSALIN expected terms contact flag: \"N\" instead API returned terms contact flag: "
                        + retrievedContact.getTermsContact());

        softAssert.assertEquals(retrievedContact.getTitle(), "DETENTION POC",
                "Contact name ROSALIN expected contact title: \"DETENTION POC\" instead API returned contact title: "
                        + retrievedContact.getTitle());

        softAssert.assertEquals(retrievedContact.getWebAccess(), "N",
                "Contact name ROSALIN expected web access flag: \"N\" instead API returned web access flag: "
                        + retrievedContact.getWebAccess());

        softAssert.assertAll();
    }

    @Test
    public void verify_customer_contact_details_if_not_available_returned_as_NULL() throws Exception {
        String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000110"));

        Response response = RequestMethod.GET(requestSpecification);
        List<CustomerContact> retrievedContactList = parseResponseToObjectList(response, CustomerContact.class);
        CustomerContact retrievedContact =
                retrieveObjectFromObjectList(retrievedContactList, "getId", "1000110");


        SoftAssert softAssert = new SoftAssert();

        softAssert.assertNull(retrievedContact.getEmail(),
                "Contact name MARK expected email address value: null instead API returned email address: "
                        + retrievedContact.getEmail());

        softAssert.assertNull(retrievedContact.getFax(),
                "Contact name MARK expected Fax number value: null instead API returned Fax number: "
                        + retrievedContact.getFax());

        softAssert.assertNull(retrievedContact.getMobilePhone(),
                "Contact name MARK expected mobile number: null instead API returned mobile number: "
                        + retrievedContact.getMobilePhone());

        softAssert.assertNull(retrievedContact.getPhone(),
                "Contact name MARK expected phone number: null instead API returned phone number: "
                        + retrievedContact.getPhone());

        softAssert.assertNull(retrievedContact.getRapidAlertNotify(),
                "Contact name MARK expected rapid alrent notify via: null instead API returned rapid alert notify via: "
                        + retrievedContact.getRapidAlertNotify());

        softAssert.assertNull(retrievedContact.getTitle(),
                "Contact name MARK expected contact title: null instead API returned contact title: "
                        + retrievedContact.getTitle());

        softAssert.assertAll();
    }

    @Test
    public void verify_customer_contact_response_return_empty_object_if_no_contact_records_ispresent() throws Exception {

        String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000058"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerContact> retrievedContactList = parseResponseToObjectList(response, CustomerContact.class);

		Assert.assertTrue(retrievedContactList.isEmpty(), "Expected contact records:0 instead API returned "
                + retrievedContactList.size() + " contact record");

    }
}
