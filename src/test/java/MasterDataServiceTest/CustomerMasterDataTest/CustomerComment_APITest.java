/**
 * 
 */
package MasterDataServiceTest.CustomerMasterDataTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import BaseTest.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.MasterData.Customer.CustomerComment;
import pojoClasses.responsePojo.CustomerCommentsResponsePOJO;
import testConfiguration.TestConfig;

/**
 * @author Manoj
 *
 */
public class CustomerComment_APITest extends BaseTest {
	private static RequestSpecification requestSpecification;

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
				.header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
						TestConfig.get("PASSWORD")))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}

	@Test(groups = { "HealthCheck"})
	public void customer_comments_API_health_check() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000088"));

		Response response = RequestMethod.GET(requestSpecification);
		BusinessResponse responsePayload = response.as(BusinessResponse.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(response.getStatusCode(), 200,
				"Expected status code: 200 instead API returned status code: " + response.getStatusCode());

		softAssert.assertEquals(responsePayload.getCode(), 200,
				"Expected code: 200 in response payload instead API returned status code: "
						+ responsePayload.getCode());

		String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
		softAssert.assertEquals(status, "SUCCESS",
				"Expected status: SUCCESS in response payload instead API returned status: "
						+ responsePayload.getStatus());

		ResponseStatus responseStatus = responsePayload.getResponseStatus();
		if(responseStatus != null) {
			String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseCode();
			softAssert.assertEquals(responseCode, "000",
					"Expected responseCode: 000 in response payload instead API returned responseCode: "
							+ responseCode);
			String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseDesc();
			softAssert.assertEquals(responseDesc, "SUCCESS",
					"Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
							+ responseDesc);
		} else{
			softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
		}
		softAssert.assertAll();
	}

	@Test(groups = { "Sanity" })
	public void verify_no_of_comment_record_retrived_is_3_for_CustomerID_1001399() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1001399"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		int retrievedListSize = retrievedCommentList.size();

		Assert.assertEquals(retrievedListSize, 3,
				"Expected no. of contact records: 3 instead API returned no. of contact record: "
						+ retrievedListSize);

	}

	@Test
	public void verify_all_type_of_comment_record_are_retrieved() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000089"));

		ArrayList<String> expectedCommentTypeID = new ArrayList<String>();
		expectedCommentTypeID.add("CBI");
		expectedCommentTypeID.add("CCO");
		expectedCommentTypeID.add("CONTACT");
		expectedCommentTypeID.add("CREDIT");
		expectedCommentTypeID.add("DAU");
		expectedCommentTypeID.add("DCA");
		expectedCommentTypeID.add("DCO");
		expectedCommentTypeID.add("DCR");
		expectedCommentTypeID.add("DGI");
		expectedCommentTypeID.add("DID");
		expectedCommentTypeID.add("DLO");
		expectedCommentTypeID.add("DPA");
		expectedCommentTypeID.add("DRC");
		expectedCommentTypeID.add("DRE");
		expectedCommentTypeID.add("DRF");
		expectedCommentTypeID.add("DRI");
		expectedCommentTypeID.add("DSA");
		expectedCommentTypeID.add("DSV");
		expectedCommentTypeID.add("DSW");
		expectedCommentTypeID.add("DTE");
		expectedCommentTypeID.add("DTR");
		expectedCommentTypeID.add("DVP");
		expectedCommentTypeID.add("DWC");
		expectedCommentTypeID.add("DWT");
		expectedCommentTypeID.add("ELOG");
		expectedCommentTypeID.add("FINAL_CO");
		expectedCommentTypeID.add("FINAL_ST");
		expectedCommentTypeID.add("IRE");
		expectedCommentTypeID.add("KUDO1");
		expectedCommentTypeID.add("KUDO2");
		expectedCommentTypeID.add("KUDOS");
		expectedCommentTypeID.add("LEIS");
		expectedCommentTypeID.add("LSC");
		expectedCommentTypeID.add("LSCA");
		expectedCommentTypeID.add("PDE");
		expectedCommentTypeID.add("REPLACE");
		expectedCommentTypeID.add("ROU");
		expectedCommentTypeID.add("SDR");
		expectedCommentTypeID.add("TRE");
		expectedCommentTypeID.add("WRITEOFF");
		Collections.sort(expectedCommentTypeID);

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		ArrayList<String> retrievedCommentTypeID = new ArrayList<String>();
		for (CustomerComment comment : retrievedCommentList) {
			retrievedCommentTypeID.add(comment.getCommentTypeId().trim());
		}
		Collections.sort(retrievedCommentTypeID);

		SoftAssert softAssert = new SoftAssert();
		for (String s : expectedCommentTypeID) {
			softAssert.assertTrue(retrievedCommentTypeID.contains(s),
					"Comment type: " + s + " is not retrieved by the API");
		}
		softAssert.assertAll();
	}

	@Test
	public void verify_dispatch_comment_are_retrieved() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		ArrayList<String> retrievedOrderCommentTypeID = new ArrayList<String>();
		for (CustomerComment comment : retrievedCommentList) {
			retrievedOrderCommentTypeID.add(comment.getOrderCommentType().trim());
		}

		Assert.assertTrue(retrievedOrderCommentTypeID.contains("DC"), "Dispatch comment is not retrieved by the API");
	}

	@Test
	public void verify_billing_comment_are_retrived() throws Exception {

		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		ArrayList<String> retrievedOrderCommentTypeID = new ArrayList<String>();
		for (CustomerComment comment : retrievedCommentList) {
			retrievedOrderCommentTypeID.add(comment.getOrderCommentType().trim());
		}

		Assert.assertTrue(retrievedOrderCommentTypeID.contains("BC"), "Billing is not retrived by the API");

	}

	@Test
	public void verify_other_comment_are_retrieved() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		ArrayList<String> retrievedOrderCommentTypeID = new ArrayList<String>();
		for (CustomerComment comment : retrievedCommentList) {
			retrievedOrderCommentTypeID.add(comment.getOrderCommentType().trim());
		}

		Assert.assertTrue(retrievedOrderCommentTypeID.contains("OC"), "Other is not retrived by the API");
	}

	@Test
	public void verify_hot_comment_are_retrieved() throws Exception {

		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerComment> retrievedCommentList = parseResponseToObjectList(response, CustomerComment.class);

		ArrayList<String> retrievedOrderCommentTypeID = new ArrayList<String>();
		for (CustomerComment comment : retrievedCommentList) {
			retrievedOrderCommentTypeID.add(comment.getOrderCommentType().trim());
		}

		Assert.assertTrue(retrievedOrderCommentTypeID.contains("HC"), "Hot is not retrived by the API");
	}

	@Test
	public void verify_comments_details_for_a_retrieved_billing_comment() throws Exception {

		String path = PATH.MASTER_DATA_CUSTOMER_COMMENTS;
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		CustomerComment retrievedBillingComment = null;
		for (CustomerComment comment : commentList) {
			if (comment.getOrderCommentType().equals("BC")) {
				retrievedBillingComment = comment;
			}
		}

		if (retrievedBillingComment != null) {
			SoftAssert softAssert = new SoftAssert();

			softAssert.assertNull(retrievedBillingComment.getAttachFilename(),
					"Expected attach file name: null instead API returned attach file name: "
							+ retrievedBillingComment.getAttachFilename());
			softAssert.assertEquals(retrievedBillingComment.getCommentTypeId(), "CBI     ",
					"Expected comment_type_id: CBI instead API returned comment_type_id: "
							+ retrievedBillingComment.getCommentTypeId());

			softAssert.assertEquals(retrievedBillingComment.getComments(), "due pending on sep10",
					"Expected comment: \"due pending on sep10\" instead API returned : "
							+ retrievedBillingComment.getComments());

			softAssert.assertEquals(retrievedBillingComment.getCompanyId(), "TMS ",
					"Expected companyId: TMS instead API returned companyId: " + retrievedBillingComment.getCompanyId());

			softAssert.assertEquals(retrievedBillingComment.getCopyToOrder(), "Y",
					"Expected copy_to_order status: \"Y\" instead API returned copy_to_order status: "
							+ retrievedBillingComment.getCopyToOrder());

			softAssert.assertEquals(retrievedBillingComment.getDisplayAtdispatch(), "N",
					"Expected display_atdispatch status: \"N\" instead API returned display_atdispatch status: "
							+ retrievedBillingComment.getDisplayAtdispatch());

			softAssert.assertEquals(retrievedBillingComment.getEnteredDate(), "2021-09-23T13:38:00.000Z",
					"Expected entered_date: \"2021-09-23T13:38:00.000Z\" instead API returned entered_date: "
							+ retrievedBillingComment.getEnteredDate());

			softAssert.assertEquals(retrievedBillingComment.getEnteredUserId(), "manoj.than",
					"Expected entered_user_id : \"manoj.than\" instead API returned : "
							+ retrievedBillingComment.getEnteredUserId());

			softAssert.assertEquals(retrievedBillingComment.getOnboardLock(), "N",
					"Expected onboard_lock: \"N\" instead API returned onboard_lock: "
							+ retrievedBillingComment.getOnboardLock());

			softAssert.assertEquals(retrievedBillingComment.getParentRowId(), "1000189                         ",
					"Expected parent_row_id: \"1000189\" instead API returned parent_row_id: "
							+ retrievedBillingComment.getParentRowId());

			softAssert.assertEquals(retrievedBillingComment.getParentRowType(), "C",
					"Expected parent_row_type: \"C\" instead API returned parent_row_type: "
							+ retrievedBillingComment.getParentRowType());

			softAssert.assertAll();
		} else {
			throw new Exception(
					"Test data has been modified. Check billing comment is added for customer 1000189 in masterfile");
		}
	}

	@Test
	public void verify_comments_details_for_a_retrieved_others_comment() throws Exception {

		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		CustomerComment retrievedBillingComment = null;
		for (CustomerComment comment : commentList) {
			if (comment.getOrderCommentType().equals("OC")) {
				retrievedBillingComment = comment;
			}
		}

		if (retrievedBillingComment != null) {
			SoftAssert softAssert = new SoftAssert();

			softAssert.assertNull(retrievedBillingComment.getAttachFilename(),
					"Expected attach file name: null instead API returned attach file name: "
							+ retrievedBillingComment.getAttachFilename());
			softAssert.assertEquals(retrievedBillingComment.getCommentTypeId(), "DGI     ",
					"Expected comment_type_id: DGI instead API returned comment_type_id: "
							+ retrievedBillingComment.getCommentTypeId());

			softAssert.assertEquals(retrievedBillingComment.getComments(), "driver name john",
					"Expected comment: \"due pending on sep10\" instead API returned : "
							+ retrievedBillingComment.getComments());

			softAssert.assertEquals(retrievedBillingComment.getCompanyId(), "TMS ",
					"Expected companyId: TMS instead API returned companyId: " + retrievedBillingComment.getCompanyId());

			softAssert.assertEquals(retrievedBillingComment.getCopyToOrder(), "N",
					"Expected copy_to_order status: \"Y\" instead API returned copy_to_order status: "
							+ retrievedBillingComment.getCopyToOrder());

			softAssert.assertEquals(retrievedBillingComment.getDisplayAtdispatch(), "N",
					"Expected display_atdispatch status: \"N\" instead API returned display_atdispatch status: "
							+ retrievedBillingComment.getDisplayAtdispatch());

			softAssert.assertEquals(retrievedBillingComment.getEnteredDate(), "2021-09-23T13:40:00.000Z",
					"Expected entered_date: \"2021-09-23T13:40:00.000Z\" instead API returned entered_date: "
							+ retrievedBillingComment.getEnteredDate());

			softAssert.assertEquals(retrievedBillingComment.getEnteredUserId(), "manoj.than",
					"Expected entered_user_id : \"manoj.than\" instead API returned : "
							+ retrievedBillingComment.getEnteredUserId());

			softAssert.assertEquals(retrievedBillingComment.getOnboardLock(), "N",
					"Expected onboard_lock: \"N\" instead API returned onboard_lock: "
							+ retrievedBillingComment.getOnboardLock());

			softAssert.assertEquals(retrievedBillingComment.getParentRowId(), "1000189                         ",
					"Expected parent_row_id: \"1000189\" instead API returned parent_row_id: "
							+ retrievedBillingComment.getParentRowId());

			softAssert.assertEquals(retrievedBillingComment.getParentRowType(), "C",
					"Expected parent_row_type: \"C\" instead API returned parent_row_type: "
							+ retrievedBillingComment.getParentRowType());

			softAssert.assertAll();
		} else {
			throw new Exception(
					"Test data has been modified. Check other comment is added for customer 1000189 in masterfile");
		}

	}

	@Test
	public void verify_comments_details_for_a_retrieved_dispatch_comment() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));
		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		CustomerComment retrievedBillingComment = null;
		for (CustomerComment comment : commentList) {
			if (comment.getOrderCommentType().equals("DC")) {
				retrievedBillingComment = comment;
			}
		}

		if (retrievedBillingComment != null) {
			SoftAssert softAssert = new SoftAssert();

			softAssert.assertNull(retrievedBillingComment.getAttachFilename(),
					"Expected attach file name: null instead API returned attach file name: "
							+ retrievedBillingComment.getAttachFilename());
			softAssert.assertEquals(retrievedBillingComment.getCommentTypeId(), "DCO     ",
					"Expected comment_type_id: DCO instead API returned comment_type_id: "
							+ retrievedBillingComment.getCommentTypeId());

			softAssert.assertEquals(retrievedBillingComment.getComments(), "driver is under supervision",
					"Expected comment: \"due pending on sep10\" instead API returned : "
							+ retrievedBillingComment.getComments());

			softAssert.assertEquals(retrievedBillingComment.getCompanyId(), "TMS ",
					"Expected companyId: TMS instead API returned companyId: " + retrievedBillingComment.getCompanyId());

			softAssert.assertEquals(retrievedBillingComment.getCopyToOrder(), "Y",
					"Expected copy_to_order status: \"Y\" instead API returned copy_to_order status: "
							+ retrievedBillingComment.getCopyToOrder());

			softAssert.assertEquals(retrievedBillingComment.getDisplayAtdispatch(), "N",
					"Expected display_atdispatch status: \"N\" instead API returned display_atdispatch status: "
							+ retrievedBillingComment.getDisplayAtdispatch());

			softAssert.assertEquals(retrievedBillingComment.getEnteredDate(), "2021-09-23T13:40:00.000Z",
					"Expected entered_date: \"2021-09-23T13:40:00.000Z\" instead API returned entered_date: "
							+ retrievedBillingComment.getEnteredDate());

			softAssert.assertEquals(retrievedBillingComment.getEnteredUserId(), "manoj.than",
					"Expected entered_user_id : \"manoj.than\" instead API returned : "
							+ retrievedBillingComment.getEnteredUserId());

			softAssert.assertEquals(retrievedBillingComment.getOnboardLock(), "N",
					"Expected onboard_lock: \"N\" instead API returned onboard_lock: "
							+ retrievedBillingComment.getOnboardLock());

			softAssert.assertEquals(retrievedBillingComment.getParentRowId(), "1000189                         ",
					"Expected parent_row_id: \"1000189\" instead API returned parent_row_id: "
							+ retrievedBillingComment.getParentRowId());

			softAssert.assertEquals(retrievedBillingComment.getParentRowType(), "C",
					"Expected parent_row_type: \"C\" instead API returned parent_row_type: "
							+ retrievedBillingComment.getParentRowType());

			softAssert.assertAll();
		} else {
			throw new Exception(
					"Test data has been modified. Check dispatch comment is added for customer 1000189 in masterfile");
		}
	}

	@Test
	public void verify_comments_details_for_a_retrieved_hot_comment() throws Exception {

		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000189"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		CustomerComment retrievedBillingComment = null;
		for (CustomerComment comment : commentList) {
			if (comment.getOrderCommentType().equals("HC")) {
				retrievedBillingComment = comment;
			}
		}

		if (retrievedBillingComment != null) {
			SoftAssert softAssert = new SoftAssert();

			softAssert.assertNull(retrievedBillingComment.getAttachFilename(),
					"Expected attach file name: null instead API returned attach file name: "
							+ retrievedBillingComment.getAttachFilename());
			softAssert.assertEquals(retrievedBillingComment.getCommentTypeId(), "LSC     ",
					"Expected comment_type_id: LSC instead API returned comment_type_id: "
							+ retrievedBillingComment.getCommentTypeId());

			softAssert.assertEquals(retrievedBillingComment.getComments(), "high priority order",
					"Expected comment: \"due pending on sep10\" instead API returned : "
							+ retrievedBillingComment.getComments());

			softAssert.assertEquals(retrievedBillingComment.getCompanyId(), "TMS ",
					"Expected companyId: TMS instead API returned companyId: " + retrievedBillingComment.getCompanyId());

			softAssert.assertEquals(retrievedBillingComment.getCopyToOrder(), "Y",
					"Expected copy_to_order status: \"Y\" instead API returned copy_to_order status: "
							+ retrievedBillingComment.getCopyToOrder());

			softAssert.assertEquals(retrievedBillingComment.getDisplayAtdispatch(), "N",
					"Expected display_atdispatch status: \"N\" instead API returned display_atdispatch status: "
							+ retrievedBillingComment.getDisplayAtdispatch());

			softAssert.assertEquals(retrievedBillingComment.getEnteredDate(), "2021-09-23T13:41:00.000Z",
					"Expected entered_date: \"2021-09-23T13:41:00.000Z\" instead API returned entered_date: "
							+ retrievedBillingComment.getEnteredDate());

			softAssert.assertEquals(retrievedBillingComment.getEnteredUserId(), "manoj.than",
					"Expected entered_user_id : \"manoj.than\" instead API returned : "
							+ retrievedBillingComment.getEnteredUserId());

			softAssert.assertEquals(retrievedBillingComment.getOnboardLock(), "N",
					"Expected onboard_lock: \"N\" instead API returned onboard_lock: "
							+ retrievedBillingComment.getOnboardLock());

			softAssert.assertEquals(retrievedBillingComment.getParentRowId(), "1000189                         ",
					"Expected parent_row_id: \"1000189\" instead API returned parent_row_id: "
							+ retrievedBillingComment.getParentRowId());

			softAssert.assertEquals(retrievedBillingComment.getParentRowType(), "C",
					"Expected parent_row_type: \"C\" instead API returned parent_row_type: "
							+ retrievedBillingComment.getParentRowType());

			softAssert.assertAll();
		} else {
			throw new Exception(
					"Test data has been modified. Check other comment is added for customer 1000189 in masterfile");
		}
	}

	@Test
	public void verify_null_is_returned_when_comment_type_is_not_specified() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000111"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		try {
			CustomerComment retrievedComment = commentList.get(0);
			SoftAssert softAssert = new SoftAssert();

			softAssert.assertNull(retrievedComment.getAttachFilename(),
					"Expected attach file name: null instead API returned attach file name: "
							+ retrievedComment.getAttachFilename());
			softAssert.assertNull(retrievedComment.getCommentTypeId(),
					"Expected comment_type_id: null instead API returned comment_type_id: "
							+ retrievedComment.getCommentTypeId());
			softAssert.assertNull(retrievedComment.getOrderCommentType(),
					"Expected order_comment_type: null instead API returned order_comment_type: "
							+ retrievedComment.getOrderCommentType());
			softAssert.assertAll();
		} catch (Exception e) {
			throw new Exception(
					"Test data has been modified. Check comment is added for customer 1000189 in masterfile");
		}
	}

	@Test
	public void verify_empty_list_is_returned_when_no_comment_is_added_for_the_customer() throws Exception {
		String path = TestConfig.get("CUSTOMER_COMMENTS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000112"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerCommentsResponsePOJO responsePayload = response.as(CustomerCommentsResponsePOJO.class);

		List<CustomerComment> commentList = responsePayload.getResponse();
		
		Assert.assertEquals(commentList.size(), 0,
				"Expected retrieved comment no: 0 instead API returned: " + commentList.size());

	}
}
