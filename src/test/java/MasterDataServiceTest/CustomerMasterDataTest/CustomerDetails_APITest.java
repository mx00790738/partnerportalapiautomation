/**
 * 
 */
package MasterDataServiceTest.CustomerMasterDataTest;

import static io.restassured.RestAssured.given;

import BaseTest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Customer.CustomerDetails;
import pojoClasses.responsePojo.ResponseStatus;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import testConfiguration.TestConfig;

/**
 * @author Manoj
 *
 */
public class CustomerDetails_APITest extends BaseTest {

	private static RequestSpecification requestSpecification;

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
				.header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
						TestConfig.get("PASSWORD")))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}

	@Test(groups = { "HealthCheck"})
	public void customer_details_API_health_check() throws Exception {
		String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000088"));

		Response response = RequestMethod.GET(requestSpecification);
		BusinessResponse responsePayload = response.as(BusinessResponse.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(response.getStatusCode(), 200,
				"Expected status code: 200 instead API returned status code: " + response.getStatusCode());

		softAssert.assertEquals(responsePayload.getCode(), 200,
				"Expected code: 200 in response payload instead API returned status code: "
						+ responsePayload.getCode());

		String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
		softAssert.assertEquals(status, "SUCCESS",
				"Expected status: SUCCESS in response payload instead API returned status: "
						+ responsePayload.getStatus());

		ResponseStatus responseStatus = responsePayload.getResponseStatus();
		if(responseStatus != null) {
			String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseCode();
			softAssert.assertEquals(responseCode, "000",
					"Expected responseCode: 000 in response payload instead API returned responseCode: "
							+ responseCode);
			String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseDesc();
			softAssert.assertEquals(responseDesc, "SUCCESS",
					"Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
							+ responseDesc);
		} else{
			softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
		}
		softAssert.assertAll();
	}

	@Test(groups = { "Sanity" })
	public void verify_customer_details1_for_1000112() throws Exception {
		String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000112"));

		Response response = given(requestSpecification).when().get();
		CustomerDetails retrievedDetails = parseResponseToObject(response, CustomerDetails.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(retrievedDetails.getId(), "1000112",
				"Expected customer id: 1000112 instead API returned customer id: "
						+ retrievedDetails.getId());

		softAssert.assertEquals(retrievedDetails.getIsActive(), "Y",
				"Expected Is Active status: Y instead API returned IsActive status: "
						+ retrievedDetails.getIsActive());

		softAssert.assertEquals(retrievedDetails.getName(), "ABBOTT LABORATORIES",
				"Expected customer name: \"ABBOTT LABORATORIES\" instead API returned customer name: "
						+ retrievedDetails.getName());

		softAssert.assertEquals(retrievedDetails.getAddress1(), "100 ABBOTT PARK RD",
				"Expected customer address1: \"100 ABBOTT PARK RD\" instead API returned customer address1: "
						+ retrievedDetails.getAddress1());

		softAssert.assertEquals(retrievedDetails.getCity(), "ABBOTT PARK",
				"Expected customer city: \"ABBOTT PARK\" instead API returned customer city: "
						+ retrievedDetails.getCity());

		softAssert.assertEquals(retrievedDetails.getStateId(), "IL",
				"Expected customer state: \"IL\" instead API returned customer state: "
						+ retrievedDetails.getStateId());

		softAssert.assertEquals(retrievedDetails.getZipCode(), "60064-3500",
				"Expected customer zip code: \"60064-3500\" instead API returned customer zip code: "
						+ retrievedDetails.getZipCode());

		softAssert.assertAll();
	}

	@Test
	public void verify_customer_finance_details_for_1000112() throws Exception {
		String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000112"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerDetails retrievedDetails = parseResponseToObject(response, CustomerDetails.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(retrievedDetails.getCreditLimit(), 50000.0,
				"Expected customer credit limit: 50000 instead API returned customer credit limit: "
						+ retrievedDetails.getCreditLimit());

		softAssert.assertEquals(retrievedDetails.getBalance(), "54317.73",
				"Expected customer balance: \"54317.73\" instead API returned customer balance: "
						+ retrievedDetails.getBalance());

		softAssert.assertEquals(retrievedDetails.getHighBalance(), "67615.86",
				"Expected customer high balance: \"67615.86\" instead API returned customer name: "
						+ retrievedDetails.getHighBalance());

		softAssert.assertEquals(retrievedDetails.getPastDue(), "73619.41",
				"Expected customer total past due: \"73619.41\" instead API returned customer total past due: "
						+ retrievedDetails.getPastDue());

		softAssert.assertEquals(retrievedDetails.getLastBillDate(), "20210803000000-0500",
				"Expected last bill date: \"20210803000000-0500\" instead API returned last bill date: "
						+ retrievedDetails.getLastBillDate());

		softAssert.assertEquals(retrievedDetails.getLastShipDate(), "20210727000000-0500",
				"Expected last ship date: \"20210727000000-0500\" instead API returned last ship date: "
						+ retrievedDetails.getLastShipDate());

		softAssert.assertEquals(retrievedDetails.getLastPayDate(), "20210714000000-0500",
				"Expected last pay date: \"20210714000000-0500\" instead API returned last pay date: "
						+ retrievedDetails.getLastPayDate());

		softAssert.assertEquals(retrievedDetails.getBilledLoads(), "55",
				"Expected orders billed: \"55\" instead API returned orders billed: "
						+ retrievedDetails.getBilledLoads());

		softAssert.assertEquals(retrievedDetails.getPaidLoads(), "7",
				"Expected orders paid: \"7\" instead API returned orders paid: "
						+ retrievedDetails.getPaidLoads());

		softAssert.assertEquals(retrievedDetails.getAverageBill(), "1382.33",
				"Expected Average bill: \"1382.33\" instead API returned Average bill: "
						+ retrievedDetails.getAverageBill());

		softAssert.assertEquals(retrievedDetails.getAveragePayDays(), "27",
				"Expected Average pay day: \"27\" instead API returned Average pay day: "
						+ retrievedDetails.getAveragePayDays());

		softAssert.assertAll();
	}

	@Test
	public void verify_customer_other_details_for_1000088() throws Exception {
		String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000088"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerDetails retrievedDetails = parseResponseToObject(response, CustomerDetails.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(retrievedDetails.getMotorCarrierId(), "BL56",
				"Expected customer motor carrier id: BL56 instead API returned customer motor carrier id: "
						+ retrievedDetails.getMotorCarrierId());

		softAssert.assertEquals(retrievedDetails.getDotNumber(), "338749984",
				"Expected DOT No.: \"338749984\" instead API returned DOT No. : "
						+ retrievedDetails.getDotNumber());

		softAssert.assertEquals(retrievedDetails.getPrevCode(), "2345",
				"Expected prev code: \"2345\" instead API returned prev code: "
						+ retrievedDetails.getPrevCode());

		softAssert.assertEquals(retrievedDetails.getCategory(), "INT",
				"Expected category: \"INT\" instead API returned category: "
						+ retrievedDetails.getCategory());

		softAssert.assertEquals(retrievedDetails.getBridgeId(), "1000088",
				"Expected bridge Acct: \"1000088\" instead API returned bridge Acct: "
						+ retrievedDetails.getBridgeId());

		softAssert.assertEquals(retrievedDetails.getCertOfInsDate(), "20210901000000-0500",
				"Expected certificate of insurence date: \"20210901000000-0500\" instead API returned certificate of insurence date: "
						+ retrievedDetails.getCertOfInsDate());

		softAssert.assertEquals(retrievedDetails.getEnteredDate(), "20190420000000-0500",
				"Expected date added: \"20190420000000-0500\" instead API returned date added: "
						+ retrievedDetails.getEnteredDate());

		softAssert.assertAll();
	}

	@Test
	public void verify_customer_details_if_not_available_returned_as_NULL() throws Exception {

		String path = TestConfig.get("CUSTOMER_DETAILS_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000115"));

		Response response = RequestMethod.GET(requestSpecification);
		CustomerDetails retrievedDetails = parseResponseToObject(response, CustomerDetails.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertNull(retrievedDetails.getAddress2(),
				"Expected customer address2: null instead API returned customer address2: "
						+ retrievedDetails.getAddress2());

		softAssert.assertNull(retrievedDetails.getBalance(),
				"Expected customer balance: null instead API returned customer balance: "
						+ retrievedDetails.getBalance());

		softAssert.assertNull(retrievedDetails.getHighBalance(),
				"Expected customer high balance: null instead API returned customer name: "
						+ retrievedDetails.getHighBalance());

		softAssert.assertNull(retrievedDetails.getLastBillDate(),
				"Expected last bill date: null instead API returned last bill date: "
						+ retrievedDetails.getLastBillDate());

		softAssert.assertNull(retrievedDetails.getLastShipDate(),
				"Expected last ship date: null instead API returned last ship date: "
						+ retrievedDetails.getLastShipDate());

		softAssert.assertNull(retrievedDetails.getLastPayDate(),
				"Expected last pay date: null instead API returned last pay date: "
						+ retrievedDetails.getLastPayDate());

		softAssert.assertNull(retrievedDetails.getBilledLoads(),
				"Expected orders billed: null instead API returned orders billed: "
						+ retrievedDetails.getBilledLoads());

		softAssert.assertNull(retrievedDetails.getPaidLoads(),
				"Expected orders paid: null instead API returned orders paid: "
						+ retrievedDetails.getPaidLoads());

		softAssert.assertNull(retrievedDetails.getAverageBill(),
				"Expected Average bill: null instead API returned Average bill: "
						+ retrievedDetails.getAverageBill());

		softAssert.assertNull(retrievedDetails.getAveragePayDays(),
				"Expected Average pay day: null instead API returned Average pay day: "
						+ retrievedDetails.getAveragePayDays());

		softAssert.assertNull(retrievedDetails.getMotorCarrierId(),
				"Expected customer motor carrier id: null instead API returned customer motor carrier id: "
						+ retrievedDetails.getMotorCarrierId());

		softAssert.assertNull(retrievedDetails.getDotNumber(),
				"Expected DOT No.: null instead API returned DOT No. : "
						+ retrievedDetails.getDotNumber());

		softAssert.assertNull(retrievedDetails.getPrevCode(),
				"Expected prev code: null instead API returned prev code: "
						+ retrievedDetails.getPrevCode());

		softAssert.assertNull(retrievedDetails.getCategory(),
				"Expected category: null instead API returned category: "
						+ retrievedDetails.getCategory());

		softAssert.assertNull(retrievedDetails.getBridgeId(),
				"Expected bridge Acct: null instead API returned bridge Acct: "
						+ retrievedDetails.getBridgeId());

		softAssert.assertNull(retrievedDetails.getCertOfInsDate(),
				"Expected certificate of insurence date: null instead API returned certificate of insurence date: "
						+ retrievedDetails.getCertOfInsDate());

		softAssert.assertAll();
	}

	@Test(enabled = false)
	public void verify_customer_1000114_is_inactive() {

	}
}
