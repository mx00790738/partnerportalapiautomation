package MasterDataServiceTest.CommodityMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Commodity.FreightClass;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class CommodityFreightClass_APITest extends BaseTest {
    private static RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
                .header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD")))
                .headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void commodity_freight_class_API_health_check() throws Exception {
        String path = TestConfig.get("COMMODITY_FREIGHT_CLASS_API_PATH");
        requestSpecification = requestSpecification.basePath(path);

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else{
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_freight_class_100_retrieved_details() throws Exception {
        String expectedID = "100";
        String expectedValue = "100";

        String path = TestConfig.get("COMMODITY_FREIGHT_CLASS_API_PATH");
        requestSpecification = requestSpecification.basePath(path);

        Response response = RequestMethod.GET(requestSpecification);
        List<FreightClass> retrievedList = parseResponseToObjectList(response, FreightClass.class);

        FreightClass retrievedFreightClass =
                retrieveObjectFromObjectList(retrievedList, "getId",expectedID);

        SoftAssert softAssert = new SoftAssert();

        String retrievedID = retrievedFreightClass.getId();
        softAssert.assertEquals(retrievedID, expectedID,
                "Expected Freight class Id: ["+expectedID+"] instead API returned : ["
                        + retrievedID + "]");

        String retrievedValue = retrievedFreightClass.getValue();
        softAssert.assertEquals(retrievedValue, expectedValue,
                "Expected Freight class value: ["+expectedValue+"] instead API returned : ["
                        + retrievedValue + "]");

        softAssert.assertAll();
    }
}
