/*

 */
package MasterDataServiceTest.CommodityMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Commodity.CustomerCommodity;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Manoj
 */
public class CustomerCommodity_APITest extends BaseTest {
    private static RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.
                given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void customer_commodity_API_health_check() throws Exception {
        String path = TestConfig.get("CUSTOMER_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "DUMDATGA"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_no_of_commodity_record_retrived_is_15_for_CustomerID_1000111() throws Exception {
        int expectedCommentListSize = 15;
        String path = TestConfig.get("COMMODITY_MASTER_DATA_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000045"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerCommodity> commodityList = parseResponseToObjectList(response, CustomerCommodity.class);
		int retrievedCommodityListSize = commodityList.size();

        Assert.assertEquals(retrievedCommodityListSize, expectedCommentListSize,
                "Expected no. of commodity records: " + expectedCommentListSize
                        + " instead API returned no. of commodity record: "
                        + retrievedCommodityListSize);
    }

    @Test
    public void verify_all_type_of_commodity_record_are_retrieved() throws Exception {

        String path = TestConfig.get("COMMODITY_MASTER_DATA_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000111"));

        ArrayList<String> expectedCommodityList = new ArrayList<>();
        expectedCommodityList.add("CLOTHING");
        expectedCommodityList.add("SERR");
        expectedCommodityList.add("UHV");
        expectedCommodityList.add("911");
        expectedCommodityList.add("BROKER");
        expectedCommodityList.add("BOXES");
        expectedCommodityList.add("ENTRPRISE");
        expectedCommodityList.add("GAURANTEE");
        expectedCommodityList.add("HIGHRISK");
        expectedCommodityList.add("NEWCUST");
        expectedCommodityList.add("PAIDDH");
        expectedCommodityList.add("HAZ");
        expectedCommodityList.add("POWERONLY");
        expectedCommodityList.add("SVCFOCUS");
        expectedCommodityList.add("STORE");
        Collections.sort(expectedCommodityList);

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerCommodity> commodityList = parseResponseToObjectList(response, CustomerCommodity.class);

        ArrayList<String> retrievedCommunityID = new ArrayList<>();
        for (CustomerCommodity commodity : commodityList) {
            retrievedCommunityID.add(commodity.getCommodityId().trim());
        }
        Collections.sort(retrievedCommunityID);
        SoftAssert softAssert = new SoftAssert();
        for (String s : retrievedCommunityID) {
            softAssert.assertTrue(expectedCommodityList.contains(s),
                    "Commodity type: " + s + " is not retrieved" +
                            " by the API");
        }
        softAssert.assertAll();
    }

	@Test(groups = { "Sanity" })
    public void verify_ADHESIVES_commodity_retrieved_for_Customer_DUMDATGA() throws Exception {
        String path = TestConfig.get("CUSTOMER_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "DUMDATGA"));

        Response response = RequestMethod.GET(requestSpecification);
        List<CustomerCommodity> commodityList = parseResponseToObjectList(response, CustomerCommodity.class);
        CustomerCommodity retrievedCommodity =
				retrieveObjectFromObjectList(commodityList, "getCommodityId", "ADHES    ");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(retrievedCommodity.getCommodityId(), "ADHES    ",
                "Expected commodityId: CLOTHING instead API returned commodityId: "
                        + retrievedCommodity.getCommodityId());

        softAssert.assertEquals(retrievedCommodity.getDescription(), "ADHESIVES",
                "Expected commodity description: \"CLOTHING\" instead API returned : "
                        + retrievedCommodity.getDescription());

        softAssert.assertEquals(retrievedCommodity.getHazmat(), "N",
                "Expected Hazmat status: TMS instead API returned Hazmat status: " + retrievedCommodity.getHazmat());

        softAssert.assertAll();
    }

    @Test
    public void verify_hazmat_commodity_data_retrived_for_Customer_1000111() throws Exception {

        String path = TestConfig.get("COMMODITY_MASTER_DATA_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000111"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerCommodity> commodityList = parseResponseToObjectList(response, CustomerCommodity.class);
		CustomerCommodity retrievedCommodity =
				retrieveObjectFromObjectList(commodityList, "getCommodityId", "HAZ      ");

            SoftAssert softAssert = new SoftAssert();

            softAssert.assertEquals(retrievedCommodity.getCommodityId(), "HAZ      ",
                    "Expected commodityId: CLOTHING instead API returned commodityId: "
                            + retrievedCommodity.getCommodityId());

            softAssert.assertEquals(retrievedCommodity.getDescription(), "Hazardous Materials",
                    "Expected commodity description: \"CLOTHING\" instead API returned : "
                            + retrievedCommodity.getDescription());

            softAssert.assertEquals(retrievedCommodity.getHazmat(), "Y",
                    "Expected Hazmat status: TMS instead API returned Hazmat status: " + retrievedCommodity.getHazmat());

            softAssert.assertAll();
    }

    @Test
    public void verify_empty_list_is_returned_when_no_commodity_is_added_for_the_customer() throws Exception {
        String path = TestConfig.get("COMMODITY_MASTER_DATA_COMMODITY_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000112"));

		Response response = RequestMethod.GET(requestSpecification);
		List<CustomerCommodity> commodityList = parseResponseToObjectList(response, CustomerCommodity.class);

        int retrievedListSize = commodityList.size();
        Assert.assertEquals(retrievedListSize, 0,
                "Expected retrieved commodity no: 0 instead API returned: " + retrievedListSize);

    }
}
