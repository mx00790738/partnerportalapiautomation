package MasterDataServiceTest.CommodityMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Commodity.HazmatCode;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class CommodityHazmatCodes_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("COMMODITY_HAZMAT_CODES_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void commodity_hazmat_codes_API_health_check() throws Exception {
        String reqPayload = "{\"id\":\"\",\"description\":\"\",\"companyId\":\"T100\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_hazmat_code_NA1057_details_retrieved() throws Exception {
        String expectedCode = "NA1057";
        String expectedDesc = "Lighters, non-pressurized, containing fl";
        String expectedhazmatClass = "3";
        String expectedhazmatErgNumber = "115";
        String expectedhazmatProperShipname = "Lighters, non-pressurized, containing flammable liquid";
        String expectedpackageGroup = "II";

        String reqPayload = "{\"id\":\"\",\"description\":\"\",\"companyId\":\"T100\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<HazmatCode> retrievedList = parseResponseToObjectList(response, HazmatCode.class);

        HazmatCode retrievedHazmatCode =
                retrieveObjectFromObjectList(retrievedList, "getCode",expectedCode);

        SoftAssert softAssert = new SoftAssert();

        String retrievedCode = retrievedHazmatCode.getCode();
        softAssert.assertEquals(retrievedCode, expectedCode,
                "Expected hazmat code: ["+expectedCode+"] instead API returned : ["
                        + retrievedCode + "]");

        String retrievedDesc = retrievedHazmatCode.getDescription();
        softAssert.assertEquals(retrievedDesc, expectedDesc,
                "Expected hazmat Desc: ["+expectedDesc+"] instead API returned : ["
                        + retrievedDesc + "]");

        String retrievedhazmatClass = retrievedHazmatCode.getHazmatClass();
        softAssert.assertEquals(retrievedhazmatClass, expectedhazmatClass,
                "Expected hazmatClass: ["+expectedhazmatClass+"] instead API returned : ["
                        + retrievedhazmatClass + "]");

        String retrievedhazmatErgNumber = retrievedHazmatCode.getHazmatErgNumber();
        softAssert.assertEquals(retrievedhazmatErgNumber, expectedhazmatErgNumber,
                "Expected hazmatErgNumber: ["+expectedhazmatErgNumber+"] instead API returned : ["
                        + retrievedhazmatErgNumber + "]");

        String retrievedhazmatProperShipname = retrievedHazmatCode.getHazmatProperShipname();
        softAssert.assertEquals(retrievedhazmatProperShipname, expectedhazmatProperShipname,
                "Expected hazmatProperShipname: ["+expectedhazmatProperShipname+"] instead API returned : ["
                        + retrievedhazmatProperShipname + "]");

        String retrievedpackageGroup = retrievedHazmatCode.getPackageGroup();
        softAssert.assertEquals(retrievedpackageGroup, expectedpackageGroup,
                "Expected packageGroup: ["+expectedpackageGroup+"] instead API returned : ["
                        + retrievedpackageGroup + "]");

        softAssert.assertAll();
    }
}
