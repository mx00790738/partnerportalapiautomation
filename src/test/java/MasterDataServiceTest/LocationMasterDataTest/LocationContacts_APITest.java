package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.LocationContact;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class LocationContacts_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_contacts_API_health_check() throws Exception {
        String path= TestConfig.get("LOCATION_CONTACTS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","8638RAMO"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_contact_is_NULL_for_locationID_84LURIVA() throws Exception {
        String locId = "84LURIVA";
        String path= TestConfig.get("LOCATION_CONTACTS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse<List<LocationContact>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "LocationID: ["+locId+"] contact list is not null");
    }

    @Test
    public void verify_contact_list_size_is_2_for_locationId_8638RAMO() throws Exception {
        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_CONTACTS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        List<LocationContact> contactList = parseResponseToObjectList(response, LocationContact.class);

        Integer retrievedContactListSize = contactList.size();
        Integer expectedContactListSize = 2;
        Assert.assertEquals(retrievedContactListSize, expectedContactListSize ,
                "Expected contact list size: ["+expectedContactListSize+"] " +
                        "but API returned list size ["+retrievedContactListSize+"]");
    }

    @Test(groups = { "Sanity" })
    public void verify_contact_record_with_all_contact_details_given_in_masterfiles() throws Exception {
        String expected_company_id = "T100";
        String expected_email = "jafl@gmail.com";
        String expected_fax = "800-456-345";
        Boolean expected_is_active = true;
        String expected_mobile_phone = "998-868-5317";
        String expected_name = "JOON";
        String expected_parent_row_id = "8638RAMO";
        String expected_parent_row_type = "L";
        String expected_phone = "238286";
        String expected_title = "MAIN POC";
        String expected_type = "contact";

        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_CONTACTS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);

        List<LocationContact> contactList = parseResponseToObjectList(response, LocationContact.class);

        if(contactList.isEmpty()){
            throw new Exception("Unexpected response!! contact is returned null");
        }
        LocationContact retrievedContact = null;

        for(LocationContact c: contactList){
            if(c.getName().equals(expected_name)){
                retrievedContact = c;
            }
        }

        if(retrievedContact == null){
            throw new Exception(
                    "Test data has been modified. Check contact record with name [" +expected_name+"] added for loc ["+locId+"] in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_company_id= (retrievedContact.getCompanyId() == null) ? "NULL" : retrievedContact.getCompanyId();
        softAssert.assertEquals(retrieved_company_id, expected_company_id,
                "Expected Company ID: [" + expected_company_id + "] but API returned [" + retrieved_company_id + "]");

        String retrieved_email= (retrievedContact.getEmail() == null) ? "NULL" : retrievedContact.getEmail();
        softAssert.assertEquals(retrieved_email, expected_email,
                "Expected email: [" + expected_email + "] but API returned [" + retrieved_email + "]");

        String retrieved_fax= (retrievedContact.getFax() == null) ? "NULL" : retrievedContact.getFax();
        softAssert.assertEquals(retrieved_fax, expected_fax,
                "Expected fax: [" + expected_fax + "] but API returned [" + retrieved_fax + "]");

        Boolean retrieved_is_active= retrievedContact.getIsActive();
        softAssert.assertEquals(retrieved_is_active, expected_is_active,
                "Expected is_active: [" + expected_is_active + "] but API returned [" + retrieved_is_active + "]");

        String retrieved_mobile_phone= (retrievedContact.getMobilePhone()== null) ? "NULL" : retrievedContact.getMobilePhone();
        softAssert.assertEquals(retrieved_mobile_phone, expected_mobile_phone,
                "Expected mobile_phone: [" + expected_mobile_phone + "] but API returned [" + retrieved_mobile_phone + "]");

        String retrieved_parent_row_id= (retrievedContact.getParentRowId()== null) ? "NULL" : retrievedContact.getParentRowId();
        softAssert.assertEquals(retrieved_parent_row_id, expected_parent_row_id,
                "Expected parent_row_id: [" + expected_parent_row_id + "] but API returned [" + retrieved_parent_row_id + "]");

        String retrieved_parent_row_type= (retrievedContact.getParentRowType()== null) ? "NULL" : retrievedContact.getParentRowType();
        softAssert.assertEquals(retrieved_parent_row_type, expected_parent_row_type,
                "Expected parent_row_type: [" + expected_parent_row_type + "] but API returned [" + retrieved_parent_row_type + "]");

        String retrieved_phone= (retrievedContact.getPhone()== null) ? "NULL" : retrievedContact.getPhone();
        softAssert.assertEquals(retrieved_phone, expected_phone,
                "Expected phone: [" + expected_phone + "] but API returned [" + retrieved_phone + "]");

        String retrieved_title= (retrievedContact.getTitle()== null) ? "NULL" : retrievedContact.getTitle();
        softAssert.assertEquals(retrieved_title, expected_title,
                "Expected title: [" + expected_title + "] but API returned [" + retrieved_title + "]");

        String retrieved_type= (retrievedContact.getType()== null) ? "NULL" : retrievedContact.getType();
        softAssert.assertEquals(retrieved_type, expected_type,
                "Expected type: [" + expected_type + "] but API returned [" + retrieved_type + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_null_is_returned_for_missing_contact_details_in_masterfiles() throws Exception {
        String expected_email = null;
        String expected_fax = null;
        String expected_mobile_phone = null;
        String expected_name = "GEORGE";
        String expected_phone = null;
        String expected_title = null;

        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_CONTACTS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        List<LocationContact> retrievedContactList = parseResponseToObjectList(response, LocationContact.class);

        if(retrievedContactList.isEmpty()){
            throw new Exception("Unexpected response!! contact is returned null");
        }
        LocationContact retrievedContact = null;

        for(LocationContact c: retrievedContactList){
            if(c.getName().equals(expected_name)){
                retrievedContact = c;
            }
        }

        if(retrievedContact == null){
            throw new Exception(
                    "Test data has been modified. Check contact record with name [" +expected_name+"] added for loc ["+locId+"] in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_email= retrievedContact.getEmail();
        softAssert.assertNull(retrieved_email,
                "Expected email: [" + expected_email + "] but API returned [" + retrieved_email + "]");

        String retrieved_fax= retrievedContact.getFax();
        softAssert.assertNull(retrieved_fax,
                "Expected fax: [" + expected_fax + "] but API returned [" + retrieved_fax + "]");

        String retrieved_mobile_phone= retrievedContact.getMobilePhone();
        softAssert.assertNull(retrieved_mobile_phone,
                "Expected mobile_phone: [" + expected_mobile_phone + "] but API returned [" + retrieved_mobile_phone + "]");

        String retrieved_phone= retrievedContact.getPhone();
        softAssert.assertNull(retrieved_phone,
                "Expected phone: [" + expected_phone + "] but API returned [" + retrieved_phone + "]");

        String retrieved_title= retrievedContact.getTitle();
        softAssert.assertNull(retrieved_title,
                "Expected title: [" + expected_title + "] but API returned [" + retrieved_title + "]");

        softAssert.assertAll();
    }
}
