package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.CityStateZipCountyCountry;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class City_State_Zip_County_Country_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_CITY_STATE_ZIP_COUNTY_COUNTRY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void citystatezipcountycountry_API_health_check() throws Exception {
        String reqPayload = "{\"postalCode\":\"60707\",\"returnTypeCode\":2}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_locations_with_postalcode_60707_are_returned_when_search_by_postalcode_60707() throws Exception {
        String reqPayload = "{\"postalCode\":\"60707\",\"returnTypeCode\":2}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountry> locationList = parseResponseToObjectList(response, CityStateZipCountyCountry.class);

        int listSize = locationList.size();
        if(listSize == 0){
            throw new Exception("Unexpected!! returned location list size is 0");
        }

        String expectedPostalCode = "60707";

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i< listSize; i++){
            String retrieved_postalcode= (locationList.get(i).getPostalCode() == null) ?
                    "NULL" : locationList.get(i).getPostalCode();
            softAssert.assertEquals(retrieved_postalcode, expectedPostalCode,
                    "Expected postal code: [" + expectedPostalCode + "] but API returned [" + retrieved_postalcode + "] " +
                            "at List Index: ["+i+"]");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_locations_with_city_DALLAS_are_returned_when_search_by_city_DALLAS() throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"DALLAS\",\"state\":\"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountry> locationList = parseResponseToObjectList(response, CityStateZipCountyCountry.class);

        int listSize = locationList.size();
        if(listSize == 0){
            throw new Exception("Unexpected!! returned location list size is 0");
        }

        String expectedCity = "DALLAS";

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i< listSize; i++){
            String retrieved_city= (locationList.get(i).getCity() == null) ?
                    "NULL" : locationList.get(i).getCity();
            softAssert.assertTrue(retrieved_city.contains(expectedCity),
                    "Expected postal code: [" + expectedCity + "] but API returned [" + retrieved_city + "] " +
                            "at List Index: ["+i+"]");
        }
        softAssert.assertAll();
    }
}
