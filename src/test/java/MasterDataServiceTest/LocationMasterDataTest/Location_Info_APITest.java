package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.LocationInfo;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class Location_Info_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_info_API_health_check() throws Exception {
        String path= TestConfig.get("LOCATION_INFO_API_PATH");
        requestSpecification.basePath(path.replace("{id}","145DSAGA"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_location_info_of_145DSAGA() throws Exception {
        String expected_id = "145DSAGA";
        String expected_addressLine1 = "145 Distribution Dr";
        String expected_city = "SAVANNAH";
        Integer expected_cityId = 309416;
        String expected_locationCode = "145DSAGA";
        String expected_name = "145 Distribution Dr";
        String expected_state = "GA";
        String expected_postalcode = "31408";
        String expected_locationType = "Un-defined";
        String expected_locationName = "145 Distribution Dr";
        Boolean expected_apptRequired = false;
        Boolean expected_active = false;

        String path= TestConfig.get("LOCATION_INFO_API_PATH");
        requestSpecification.basePath(path.replace("{id}","145DSAGA"));

        Response response = RequestMethod.GET(requestSpecification);

        LocationInfo retrievedLocInfo = parseResponseToObject(response,LocationInfo.class);
        if(retrievedLocInfo == null){
            throw new Exception("Unexpected response!! Location info is returned null");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_id= (retrievedLocInfo.getId() == null) ? "NULL" : retrievedLocInfo.getId();
        softAssert.assertEquals(retrieved_id, expected_id,
                "Expected ID: [" + expected_id + "] but API returned [" + retrieved_id + "]");

        String retrieved_addressLine1= (retrievedLocInfo.getAddressLine1() == null) ? "NULL" : retrievedLocInfo.getAddressLine1();
        softAssert.assertEquals(retrieved_addressLine1, expected_addressLine1,
                "Expected addressLine1: [" + expected_addressLine1 + "] but API returned [" + retrieved_addressLine1 + "]");

        String retrieved_city= (retrievedLocInfo.getCity() == null) ? "NULL" : retrievedLocInfo.getCompanyId();
        softAssert.assertEquals(retrieved_city, expected_city,
                "Expected city: [" + expected_city + "] but API returned [" + retrieved_city + "]");

        Integer retrieved_cityId= (retrievedLocInfo.getCityId() == null) ? 0 : retrievedLocInfo.getCityId();
        softAssert.assertEquals(retrieved_cityId, expected_cityId,
                "Expected cityId: [" + expected_cityId + "] but API returned [" + (retrieved_cityId == 0 ? "NULL":retrieved_cityId)+ "]");

        String retrieved_locationCode= (retrievedLocInfo.getLocationCode() == null) ? "NULL" : retrievedLocInfo.getLocationCode();
        softAssert.assertEquals(retrieved_locationCode, expected_locationCode,
                "Expected locationCode: [" + expected_locationCode + "] but API returned [" + retrieved_locationCode + "]");

        String retrieved_name= (retrievedLocInfo.getName() == null) ? "NULL" : retrievedLocInfo.getName();
        softAssert.assertEquals(retrieved_name, expected_name,
                "Expected name: [" + expected_name + "] but API returned [" + retrieved_name + "]");

        String retrieved_postalcode= (retrievedLocInfo.getPostalCode() == null) ? "NULL" : retrievedLocInfo.getPostalCode();
        softAssert.assertEquals(retrieved_postalcode, expected_postalcode,
                "Expected postalcode: [" + expected_postalcode + "] but API returned [" + retrieved_postalcode + "]");

        String retrieved_state= (retrievedLocInfo.getState() == null) ? "NULL" : retrievedLocInfo.getState();
        softAssert.assertEquals(retrieved_state, expected_state,
                "Expected state: [" + expected_state + "] but API returned [" + retrieved_state + "]");

        String retrieved_locationType= (retrievedLocInfo.getLocationType() == null) ? "NULL" : retrievedLocInfo.getLocationType();
        softAssert.assertEquals(retrieved_locationType, expected_locationType,
                "Expected locationType: [" + expected_locationType + "] but API returned [" + retrieved_locationType + "]");

        String retrieved_locationName= (retrievedLocInfo.getLocationName() == null) ? "NULL" : retrievedLocInfo.getLocationName();
        softAssert.assertEquals(retrieved_locationName, expected_locationName,
                "Expected locationName: [" + expected_locationName + "] but API returned [" + retrieved_locationName + "]");

        Boolean retrieved_apptRequired= retrievedLocInfo.getApptRequired();
        softAssert.assertEquals(retrieved_apptRequired, expected_apptRequired,
                "Expected ID: [" + expected_apptRequired + "] but API returned [" + retrieved_apptRequired + "]");

        Boolean retrieved_active= retrievedLocInfo.getActive();
        softAssert.assertEquals(retrieved_active, expected_active,
                "Expected ID: [" + expected_active + "] but API returned [" + retrieved_active + "]");

    }
}
