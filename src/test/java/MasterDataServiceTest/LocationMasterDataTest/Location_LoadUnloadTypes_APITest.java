package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.LoadUnloadType;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.HashMap;
import java.util.List;

public class Location_LoadUnloadTypes_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_LOAD_UNLOAD_TYPES_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_loadunloadtypes_API_health_check() throws Exception {
        String reqPayload = "{\"id\":\"\",\"descr\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="LoadUnloadtypesDetailsDataProvider",groups = { "Sanity" })
    public void verify_loadunloadtypes_details_retrieved(String expectedType, HashMap<String, Object> expectedTypeDetails) throws Exception {
        String reqPayload = "{\"id\":\"\",\"descr\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LoadUnloadType> retrievedList = parseResponseToObjectList(response, LoadUnloadType.class);

        LoadUnloadType retrievedRecord = null;
        for(LoadUnloadType e:retrievedList) {
            if (e.getId().trim().equals(expectedType)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with expected LoadUnload Type: ["+expectedType+"] not found");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_company_id= (retrievedRecord.getCompanyId()== null) ? "NULL" : retrievedRecord.getCompanyId();
        softAssert.assertEquals(retrieved_company_id, expectedTypeDetails.get("company_id"),
                "Expected company_id: [" + expectedTypeDetails.get("company_id") + "] " +
                        "but API returned [" + retrieved_company_id + "]");

        String retrieved_descr= (retrievedRecord.getDescr()== null) ? "NULL" : retrievedRecord.getDescr();
        softAssert.assertEquals(retrieved_descr, expectedTypeDetails.get("descr"),
                "Expected descr: [" + expectedTypeDetails.get("descr") + "] " +
                        "but API returned [" + retrieved_descr + "]");

        String retrieved_id= (retrievedRecord.getId()== null) ? "NULL" : retrievedRecord.getId();
        softAssert.assertEquals(retrieved_id, expectedTypeDetails.get("id"),
                "Expected id: [" + expectedTypeDetails.get("id") + "] " +
                        "but API returned [" + retrieved_id + "]");

        String retrieved_is_active= (retrievedRecord.getIsActive()== null) ? "NULL" :
                (retrievedRecord.getIsActive())? "true":"false";
        softAssert.assertEquals(retrieved_is_active, expectedTypeDetails.get("is_active"),
                "Expected is_active: [" + expectedTypeDetails.get("is_active") + "] " +
                        "but API returned [" + retrieved_is_active + "]");

        String retrieved_type= (retrievedRecord.getType()== null) ? "NULL" : retrievedRecord.getType();
        softAssert.assertEquals(retrieved_type, expectedTypeDetails.get("__type"),
                "Expected __type: [" + expectedTypeDetails.get("__type") + "] " +
                        "but API returned [" + retrieved_type + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_loadunloadtypes_list_size_is_14() throws Exception {
        String reqPayload = "{\"id\":\"\",\"descr\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LoadUnloadType> retrievedList = parseResponseToObjectList(response, LoadUnloadType.class);

        int expectedListSize = 14;
        int retrievedListSize = retrievedList.size();
        Assert.assertEquals(retrievedListSize, expectedListSize,"Expected loadunloadtypes list size ["+expectedListSize+"] but API returned ["+
                retrievedListSize +"]");
    }

    @Test
    public void verify_BOBTAIL_loadunloadtype_is_retrieved_when_user_search_by_id_BOBTAIL() throws Exception {
        String expectedLoadUnloadType = "BOBTAIL";
        String reqPayload = "{\"id\":\""+expectedLoadUnloadType+"\",\"descr\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LoadUnloadType> retrievedList = parseResponseToObjectList(response, LoadUnloadType.class);

        String retrievedLoadUnloadType = retrievedList.get(0).getId();
        Assert.assertEquals(retrievedLoadUnloadType,expectedLoadUnloadType,
                "Expected LoadUnloadType ["+expectedLoadUnloadType+"] but API returned ["+retrievedLoadUnloadType+"]");
    }

    @Test
    public void verify_Live_load_loadunloadtype_is_retrieved_when_user_search_by_descr_Live_load() throws Exception {
        String expectedLoadUnloadType = "Live load";
        String reqPayload = "{\"id\":\"\",\"descr\":\""+expectedLoadUnloadType+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LoadUnloadType> retrievedList = parseResponseToObjectList(response, LoadUnloadType.class);

        String retrievedLoadUnloadType = retrievedList.get(0).getDescr();
        Assert.assertEquals(retrievedLoadUnloadType,expectedLoadUnloadType,
                "Expected LoadUnloadType ["+expectedLoadUnloadType+"] but API returned ["+retrievedLoadUnloadType+"]");
    }

    @DataProvider(name="LoadUnloadtypesDetailsDataProvider")
    public Object[][] getLoadUnloadtypesDetailsForValidation(){
        HashMap<String, Object> BOBTAIL = new HashMap<String, Object>();
        BOBTAIL.put("company_id","T100");
        BOBTAIL.put("descr","NO TRAILER NEEDED");
        BOBTAIL.put("id","BOBTAIL");
        BOBTAIL.put("is_active","true");
        BOBTAIL.put("__type","load_unload");

        HashMap<String, Object> DL = new HashMap<String, Object>();
        DL.put("company_id","T100");
        DL.put("descr","Driver load");
        DL.put("id","DL");
        DL.put("is_active","true");
        DL.put("__type","load_unload");

        HashMap<String, Object> DROP = new HashMap<String, Object>();
        DROP.put("company_id","T100");
        DROP.put("descr","DROP TRAILER");
        DROP.put("id","DROP");
        DROP.put("is_active","true");
        DROP.put("__type","load_unload");

        HashMap<String, Object> DROPHOOK = new HashMap<String, Object>();
        DROPHOOK.put("company_id","T100");
        DROPHOOK.put("descr","Drop and Hook Trailer");
        DROPHOOK.put("id","DROPHOOK");
        DROPHOOK.put("is_active","true");
        DROPHOOK.put("__type","load_unload");

        HashMap<String, Object> DU = new HashMap<String, Object>();
        DU.put("company_id","T100");
        DU.put("descr","Driver unload");
        DU.put("id","DU");
        DU.put("is_active","true");
        DU.put("__type","load_unload");

        HashMap<String, Object> EMPTY = new HashMap<String, Object>();
        EMPTY.put("company_id","T100");
        EMPTY.put("descr","EMPTY");
        EMPTY.put("id","EMPTY");
        EMPTY.put("is_active","true");
        EMPTY.put("__type","load_unload");

        HashMap<String, Object> FLOOR = new HashMap<String, Object>();
        FLOOR.put("company_id","T100");
        FLOOR.put("descr","Floor loaded");
        FLOOR.put("id","FLOOR");
        FLOOR.put("is_active","true");
        FLOOR.put("__type","load_unload");

        HashMap<String, Object> LL = new HashMap<String, Object>();
        LL.put("company_id","T100");
        LL.put("descr","Live load");
        LL.put("id","LL");
        LL.put("is_active","true");
        LL.put("__type","load_unload");

        HashMap<String, Object> LU = new HashMap<String, Object>();
        LU.put("company_id","T100");
        LU.put("descr","Live unload");
        LU.put("id","LU");
        LU.put("is_active","true");
        LU.put("__type","load_unload");

        HashMap<String, Object> N = new HashMap<String, Object>();
        N.put("company_id","T100");
        N.put("descr","No driver loading or unload");
        N.put("id","N");
        N.put("is_active","true");
        N.put("__type","load_unload");

        HashMap<String, Object> NT = new HashMap<String, Object>();
        NT.put("company_id","T100");
        NT.put("descr","No Touch");
        NT.put("id","NT");
        NT.put("is_active","true");
        NT.put("__type","load_unload");

        HashMap<String, Object> PLTEXCH = new HashMap<String, Object>();
        PLTEXCH.put("company_id","T100");
        PLTEXCH.put("descr","Pallet Exchange");
        PLTEXCH.put("id","PLTEXCH");
        PLTEXCH.put("is_active","true");
        PLTEXCH.put("__type","load_unload");

        HashMap<String, Object> SLIP = new HashMap<String, Object>();
        SLIP.put("company_id","T100");
        SLIP.put("descr","Slip sheets");
        SLIP.put("id","SLIP");
        SLIP.put("is_active","true");
        SLIP.put("__type","load_unload");

        HashMap<String, Object> Y = new HashMap<String, Object>();
        Y.put("company_id","T100");
        Y.put("descr","Yes, driver will load/unl");
        Y.put("id","Y");
        Y.put("is_active","true");
        Y.put("__type","load_unload");

        return new Object[][]
                {
                        {"BOBTAIL",BOBTAIL},
                        {"DL",DL},
                        {"DROP",DROP},
                        {"DROPHOOK",DROPHOOK},
                        {"DU",DU},
                        {"EMPTY",EMPTY},
                        {"FLOOR",FLOOR},
                        {"LL",LL},
                        {"LU",LU},
                        {"N",N},
                        {"NT",NT},
                        {"PLTEXCH",PLTEXCH},
                        {"SLIP",SLIP},
                        {"Y",Y}
                };
    }
}
