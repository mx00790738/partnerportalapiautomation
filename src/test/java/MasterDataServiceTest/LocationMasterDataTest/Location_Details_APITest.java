package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import javafx.util.Pair;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.*;
import pojoClasses.responsePojo.MasterData.Location.*;
import testConfiguration.TestConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Location_Details_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_details_API_health_check() throws Exception {
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","6000HECA"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse<LocationDetails> responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_location_address_details_of_location_id_801EGRTX() throws Exception {
        String expectedActiveStatus = "true";
        String expectedAddressLine1 = "801 E Avenue K";
        String expectedAddressLine2 = "NORTH STREET";
        String expectedApptRequired = "false";
        String expected_bseg = "N";
        String expectedCity = "GRAND PRAIRIE";
        Integer expectedCityID = 151988;
        String expectedCountry = "USA";
        String expectedID= "801EGRTX";
        String expectedName= "BIMBO BAKING";
        String expectedPostalCode= "75050";
        String expectedState= "TX";
        String expectedGoogleId = "ChIJ2TgCSlKjOIgR-7XLi5hrVlo";

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","801EGRTX"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        SoftAssert softAssert = new SoftAssert();

        String retrievedActiveStatus = (locationDetails.getActive() == null) ? "NULL" : locationDetails.getActive().toString();
        softAssert.assertEquals(retrievedActiveStatus, expectedActiveStatus,
                "Expected Active status: " + expectedActiveStatus + " but API returned " + retrievedActiveStatus);

        String retrievedAddressLine1 = (locationDetails.getAddressLine1() == null) ? "NULL" : locationDetails.getAddressLine1().trim();
        softAssert.assertEquals(retrievedAddressLine1, expectedAddressLine1,
                "Expected Address line 1: " + expectedAddressLine1 + " but API returned " + retrievedAddressLine1);

        String retrievedAddressLine2 = (locationDetails.getAddressLine2() == null) ? "NULL" : locationDetails.getAddressLine2().trim();
        softAssert.assertEquals(retrievedAddressLine2, expectedAddressLine2,
                "Expected Address line 2: " + expectedAddressLine2 + " but API returned " + retrievedAddressLine2);

        String retrievedApptRequired = (locationDetails.getApptRequired()== null) ? "NULL" : locationDetails.getApptRequired().toString();
        softAssert.assertEquals(retrievedApptRequired, expectedApptRequired,
                "Expected Appt required: " + expectedApptRequired + " but API returned " + expectedApptRequired);

        String retrieved_bseg = (locationDetails.getBseg()== null) ? "NULL" : locationDetails.getBseg();
        softAssert.assertEquals(retrieved_bseg, expected_bseg,
                "Expected bseg: " + expected_bseg + " but API returned " + retrieved_bseg);

        String retrievedCity = (locationDetails.getCity()== null) ? "NULL" : locationDetails.getCity();
        softAssert.assertEquals(retrievedCity, expectedCity,
                "Expected city: " + expectedCity + " but API returned " + retrievedCity);

        Integer retrievedCityID = (locationDetails.getCityId()== null) ? Integer.valueOf("NULL") : locationDetails.getCityId();
        softAssert.assertEquals(retrievedCityID, expectedCityID,
                "Expected city id: " + expectedCityID + " but API returned " + retrievedCityID);

        String retrievedCountry = (locationDetails.getCountry()== null) ? "NULL" : locationDetails.getCountry();
        softAssert.assertEquals(retrievedCountry, expectedCountry,
                "Expected country: " + expectedCountry + " but API returned " + retrievedCountry);

        String retrievedId = (locationDetails.getId()== null) ? "NULL" : locationDetails.getId();
        softAssert.assertEquals(retrievedId, expectedID,
                "Expected location id: " + expectedID + " but API returned " + retrievedId);

        String retrievedLocName = (locationDetails.getName()== null) ? "NULL" : locationDetails.getName();
        softAssert.assertEquals(retrievedLocName, expectedName,
                "Expected location name: " + expectedName + " but API returned " + retrievedLocName);

        String retrievedPostalCode = (locationDetails.getPostalCode()== null) ? "NULL" : locationDetails.getPostalCode();
        softAssert.assertEquals(retrievedPostalCode, expectedPostalCode,
                "Expected postal code: " + expectedPostalCode + " but API returned " + retrievedPostalCode);

        String retrievedState = (locationDetails.getState()== null) ? "NULL" : locationDetails.getState();
        softAssert.assertEquals(retrievedState, expectedState,
                "Expected state: " + expectedState + " but API returned " + retrievedState);

        String retrievedGoogleID = (locationDetails.getGoogleId()== null) ? "NULL" : locationDetails.getGoogleId();
        softAssert.assertEquals(retrievedGoogleID, expectedGoogleId,
                "Expected state: " + expectedGoogleId + " but API returned " + retrievedGoogleID);

        softAssert.assertAll();
    }

    @Test
    public void verify_dayOpenCloseMap_for_location_id_1690TRCA() throws Exception {
        String expectedMondayStartTime = "04:00";
        String expectedMondayCloseTime = "02:00";
        String expectedTuesdayStartTime = "04:00";
        String expectedTuesdayCloseTime = "02:00";
        String expectedWednesdayStartTime = "04:00";
        String expectedWednesdayCloseTime = "02:00";
        String expectedThursdayStartTime = "04:00";
        String expectedThursdayCloseTime = "02:00";
        String expectedFridayStartTime = "04:00";
        String expectedFridayCloseTime = "02:00";
        String expectedSaturdayStartTime = "04:00";
        String expectedSaturdayCloseTime = "02:00";
        String expectedSundayStartTime = "04:00";
        String expectedSundayCloseTime = "02:00";

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","1690TRCA"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        DayOpenCloseMap dayOpenCloseMap = locationDetails.getDayOpenCloseMap();
        if(dayOpenCloseMap==null){
            throw new Exception("Unexpected response!! dayOpenCloseMap is returned null");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrievedMondayStartTime = (dayOpenCloseMap.getMonday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getMonday().getStartTime();
        softAssert.assertEquals(retrievedMondayStartTime, expectedMondayStartTime,
                "Expected monday start time: " + expectedMondayStartTime + " but API returned " + retrievedMondayStartTime);

        String retrievedMondayCloseTime = (dayOpenCloseMap.getMonday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getMonday().getCloseTime();
        softAssert.assertEquals(retrievedMondayCloseTime, expectedMondayCloseTime,
                "Expected monday close time: " + expectedMondayCloseTime + " but API returned " + retrievedMondayCloseTime);

        String retrievedTuesdayStartTime = (dayOpenCloseMap.getTuesday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getTuesday().getStartTime();
        softAssert.assertEquals(retrievedTuesdayStartTime, expectedTuesdayStartTime,
                "Expected Tuesday start time: " + expectedTuesdayStartTime + " but API returned " + retrievedTuesdayStartTime);

        String retrievedTuesdayCloseTime = (dayOpenCloseMap.getTuesday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getTuesday().getCloseTime();
        softAssert.assertEquals(retrievedTuesdayCloseTime, expectedTuesdayCloseTime,
                "Expected Tuesday close time: " + expectedTuesdayCloseTime + " but API returned " + retrievedTuesdayCloseTime);

        String retrievedWednesdayStartTime = (dayOpenCloseMap.getWednesday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getWednesday().getStartTime();
        softAssert.assertEquals(retrievedWednesdayStartTime, expectedWednesdayStartTime,
                "Expected Wednesday start time: " + expectedWednesdayStartTime + " but API returned " + retrievedWednesdayStartTime);

        String retrievedWednesdayCloseTime = (dayOpenCloseMap.getWednesday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getWednesday().getCloseTime();
        softAssert.assertEquals(retrievedWednesdayCloseTime, expectedWednesdayCloseTime,
                "Expected Wednesday close time: " + expectedWednesdayCloseTime + " but API returned " + retrievedWednesdayCloseTime);

        String retrievedThursdayStartTime = (dayOpenCloseMap.getThursday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getThursday().getStartTime();
        softAssert.assertEquals(retrievedThursdayStartTime, expectedThursdayStartTime,
                "Expected Thursday start time: " + expectedThursdayStartTime + " but API returned " + retrievedThursdayStartTime);

        String retrievedThursdayCloseTime = (dayOpenCloseMap.getThursday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getThursday().getCloseTime();
        softAssert.assertEquals(retrievedThursdayCloseTime, expectedThursdayCloseTime,
                "Expected Thursday close time: " + expectedThursdayCloseTime + " but API returned " + retrievedThursdayCloseTime);

        String retrievedFridayStartTime = (dayOpenCloseMap.getFriday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getFriday().getStartTime();
        softAssert.assertEquals(retrievedFridayStartTime, expectedFridayStartTime,
                "Expected Friday start time: " + expectedFridayStartTime + " but API returned " + retrievedFridayStartTime);

        String retrievedFridayCloseTime = (dayOpenCloseMap.getFriday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getFriday().getCloseTime();
        softAssert.assertEquals(retrievedFridayCloseTime, expectedFridayCloseTime,
                "Expected Friday close time: " + expectedFridayCloseTime + " but API returned " + retrievedFridayCloseTime);

        String retrievedSaturdayStartTime = (dayOpenCloseMap.getSaturday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getSaturday().getStartTime();
        softAssert.assertEquals(retrievedSaturdayStartTime, expectedSaturdayStartTime,
                "Expected Saturday start time: " + expectedSaturdayStartTime + " but API returned " + retrievedSaturdayStartTime);

        String retrievedSaturdayCloseTime = (dayOpenCloseMap.getSaturday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getSaturday().getCloseTime();
        softAssert.assertEquals(retrievedSaturdayCloseTime, expectedSaturdayCloseTime,
                "Expected Saturday close time: " + expectedSaturdayCloseTime + " but API returned " + retrievedSaturdayCloseTime);

        String retrievedSundayStartTime = (dayOpenCloseMap.getSunday().getStartTime()== null) ? "NULL" : dayOpenCloseMap.getSunday().getStartTime();
        softAssert.assertEquals(retrievedSundayStartTime, expectedSundayStartTime,
                "Expected Sunday start time: " + expectedSundayStartTime + " but API returned " + retrievedSundayStartTime);

        String retrievedSundayCloseTime = (dayOpenCloseMap.getSunday().getCloseTime()== null) ? "NULL" : dayOpenCloseMap.getSunday().getCloseTime();
        softAssert.assertEquals(retrievedSundayCloseTime, expectedSundayCloseTime,
                "Expected Sunday close time: " + expectedSundayCloseTime + " but API returned " + retrievedSundayCloseTime);

        softAssert.assertAll();
    }

    @Test
    public void verify_dayOpenCloseMap_values_are_null_for_location_id_801EGRTX() throws Exception {
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","801EGRTX"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        DayOpenCloseMap dayOpenCloseMap = locationDetails.getDayOpenCloseMap();
        if(dayOpenCloseMap==null){
            throw new Exception("Unexpected response!! dayOpenCloseMap is returned null");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrievedMondayStartTime = dayOpenCloseMap.getMonday().getStartTime();
        softAssert.assertNull(retrievedMondayStartTime,
                "Expected monday start time: null but API returned " + retrievedMondayStartTime);

        String retrievedMondayCloseTime = dayOpenCloseMap.getMonday().getCloseTime();
        softAssert.assertNull(retrievedMondayCloseTime,
                "Expected monday close time: null but API returned " + retrievedMondayCloseTime);

        String retrievedTuesdayStartTime = dayOpenCloseMap.getTuesday().getStartTime();
        softAssert.assertNull(retrievedTuesdayStartTime,
                "Expected Tuesday start time: null but API returned " + retrievedTuesdayStartTime);

        String retrievedTuesdayCloseTime = dayOpenCloseMap.getTuesday().getCloseTime();
        softAssert.assertNull(retrievedTuesdayCloseTime,
                "Expected Tuesday close time: null but API returned " + retrievedTuesdayCloseTime);

        String retrievedWednesdayStartTime = dayOpenCloseMap.getWednesday().getStartTime();
        softAssert.assertNull(retrievedWednesdayStartTime,
                "Expected Wednesday start time: null but API returned " + retrievedWednesdayStartTime);

        String retrievedWednesdayCloseTime = dayOpenCloseMap.getWednesday().getCloseTime();
        softAssert.assertNull(retrievedWednesdayCloseTime,
                "Expected Wednesday close time: null but API returned " + retrievedWednesdayCloseTime);

        String retrievedThursdayStartTime = dayOpenCloseMap.getThursday().getStartTime();
        softAssert.assertNull(retrievedThursdayStartTime,
                "Expected Thursday start time: null but API returned " + retrievedThursdayStartTime);

        String retrievedThursdayCloseTime = dayOpenCloseMap.getThursday().getCloseTime();
        softAssert.assertNull(retrievedThursdayCloseTime,
                "Expected Thursday close time: null but API returned " + retrievedThursdayCloseTime);

        String retrievedFridayStartTime = dayOpenCloseMap.getFriday().getStartTime();
        softAssert.assertNull(retrievedFridayStartTime,
                "Expected Friday start time: null but API returned " + retrievedFridayStartTime);

        String retrievedFridayCloseTime = dayOpenCloseMap.getFriday().getCloseTime();
        softAssert.assertNull(retrievedFridayCloseTime,
                "Expected Friday close time: null but API returned " + retrievedFridayCloseTime);

        String retrievedSaturdayStartTime = dayOpenCloseMap.getSaturday().getStartTime();
        softAssert.assertNull(retrievedSaturdayStartTime,
                "Expected Saturday start time: null but API returned " + retrievedSaturdayStartTime);

        String retrievedSaturdayCloseTime = dayOpenCloseMap.getSaturday().getCloseTime();
        softAssert.assertNull(retrievedSaturdayCloseTime,
                "Expected Saturday close time: null but API returned " + retrievedSaturdayCloseTime);

        String retrievedSundayStartTime = dayOpenCloseMap.getSunday().getStartTime();
        softAssert.assertNull(retrievedSundayStartTime,
                "Expected Sunday start time: null but API returned " + retrievedSundayStartTime);

        String retrievedSundayCloseTime = dayOpenCloseMap.getSunday().getCloseTime();
        softAssert.assertNull(retrievedSundayCloseTime,
                "Expected Sunday close time: null but API returned " + retrievedSundayCloseTime);

        softAssert.assertAll();
    }

    @Test(dataProvider="LocationIDDataProvider")
    public void verify_driver_LoadID_and_UnLoadID_values_returned(String locID, Pair<String, String> expectedValue) throws Exception {
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locID));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String expectedDriverLoadID = expectedValue.getKey();
        String expectedDriverUnLoadID = expectedValue.getValue();

        SoftAssert softAssert = new SoftAssert();
        String retrievedDriverLoadID = (locationDetails.getDriverLoadId()== null) ? "NULL" : locationDetails.getDriverLoadId();
        softAssert.assertEquals(retrievedDriverLoadID, expectedDriverLoadID,
                "Expected Driver Load ID: " + expectedDriverLoadID + " but API returned " + retrievedDriverLoadID);

        String retrievedDriverUnLoadID = (locationDetails.getDriverUnloadId()== null) ? "NULL" : locationDetails.getDriverUnloadId();
        softAssert.assertEquals(retrievedDriverUnLoadID, expectedDriverUnLoadID,
                "Expected Driver UnLoad ID: " + expectedDriverUnLoadID + " but API returned " + retrievedDriverUnLoadID);

        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_locQualifierMap_values_for_location_id_801EGRTX() throws Exception {
        boolean expectedConsignee = false;
        boolean expectedCustomer = false;
        boolean expectedCustomsBroker = false;
        boolean expectedDistCenter = false;
        boolean expectedDropYard = false;
        boolean expectedGeocoded = true;
        boolean expectedOutsideTerminal = false;
        boolean expectedProspect = false;
        boolean expectedShipper = false;
        boolean expectedSteamship = false;
        boolean expectedTerminal = false;
        boolean expectedTrailerPool = false;
        boolean expectedTrailerWash = false;
        boolean expectedTruckstop = false;

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","801EGRTX"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        LocQualifierMap locQualifierMap = locationDetails.getLocQualifierMap();
        if(locQualifierMap==null){
            throw new Exception("Unexpected response!! locQualifierMap is returned null");
        }

        SoftAssert softAssert = new SoftAssert();
        boolean retrievedConsignee = locQualifierMap.getConsignee();
        softAssert.assertFalse(retrievedConsignee,
                "Expected Consignee value in locQualifierMap: "+expectedConsignee+" but API returned " + retrievedConsignee);

        boolean retrievedCustomer = locQualifierMap.getCustomer();
        softAssert.assertFalse(retrievedCustomer,
                "Expected Customer value in locQualifierMap: "+expectedCustomer+" but API returned " +retrievedCustomer );

        boolean retrievedCustomsBroker = locQualifierMap.getCustomsBroker();
        softAssert.assertFalse(retrievedCustomsBroker,
                "Expected CustomsBroker value in locQualifierMap: "+expectedCustomsBroker+" but API returned " + retrievedCustomsBroker);

        boolean retrievedDistCenter = locQualifierMap.getDistCenter();
        softAssert.assertFalse(retrievedDistCenter,
                "Expected DistCenter value in locQualifierMap: "+expectedDistCenter+" but API returned " + retrievedDistCenter);

        boolean retrievedDropYard = locQualifierMap.getDropYard();
        softAssert.assertFalse(retrievedDropYard,
                "Expected DropYard value in locQualifierMap: "+expectedDropYard+" but API returned " + retrievedDropYard);

        boolean retrievedGeocoded = locQualifierMap.getGeocoded();
        softAssert.assertTrue(retrievedGeocoded,
                "Expected Geocoded value in locQualifierMap: "+expectedGeocoded+" but API returned " + retrievedGeocoded);

        boolean retrievedOutsideTerminal = locQualifierMap.getOutsideTerminal();
        softAssert.assertTrue(retrievedOutsideTerminal,
                "Expected OutsideTerminal value in locQualifierMap: "+expectedOutsideTerminal+" but API returned " + retrievedOutsideTerminal);

        boolean retrievedProspect = locQualifierMap.getProspect();
        softAssert.assertFalse(retrievedProspect,
                "Expected Prospect value in locQualifierMap: "+expectedProspect+" but API returned " + retrievedProspect);

        boolean retrievedShipper = locQualifierMap.getShipper();
        softAssert.assertFalse(retrievedShipper,
                "Expected Shipper value in locQualifierMap: "+expectedShipper+" but API returned " + retrievedShipper);

        boolean retrievedSteamship = locQualifierMap.getSteamship();
        softAssert.assertFalse(retrievedSteamship,
                "Expected Steamship value in locQualifierMap: "+expectedSteamship+" but API returned " + retrievedSteamship);

        boolean retrievedTerminal = locQualifierMap.getTerminal();
        softAssert.assertFalse(retrievedTerminal,
                "Expected Terminal value in locQualifierMap: "+expectedTerminal+" but API returned " + retrievedTerminal);

        boolean retrievedTrailerPool = locQualifierMap.getTrailerPool();
        softAssert.assertFalse(retrievedTrailerPool,
                "Expected TrailerPool value in locQualifierMap: "+expectedTrailerPool+" but API returned " + retrievedTrailerPool);

        boolean retrievedTrailerWash = locQualifierMap.getTrailerWash();
        softAssert.assertFalse(retrievedTrailerWash,
                "Expected TrailerWash value in locQualifierMap: "+expectedTrailerWash+" but API returned " + retrievedTrailerWash);

        boolean retrievedTruckstop = locQualifierMap.getTruckstop();
        softAssert.assertFalse(retrievedTruckstop,
                "Expected Truckstop value in locQualifierMap: "+expectedTruckstop+" but API returned " + retrievedTruckstop);

        softAssert.assertAll();
    }

    @Test
    public void verify_locQualifierMap_values_for_location_id_84LUMEFL() throws Exception {
        boolean expectedConsignee = true;
        boolean expectedCustomer = true;
        boolean expectedCustomsBroker = true;
        boolean expectedDistCenter = true;
        boolean expectedDropYard = true;
        boolean expectedGeocoded = false;
        boolean expectedOutsideTerminal = false;
        boolean expectedProspect = true;
        boolean expectedShipper = true;
        boolean expectedSteamship = true;
        boolean expectedTerminal = true;
        boolean expectedTrailerPool = true;
        boolean expectedTrailerWash = false;
        boolean expectedTruckstop = true;

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","84LUMEFL"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        LocQualifierMap locQualifierMap = locationDetails.getLocQualifierMap();
        if(locQualifierMap==null){
            throw new Exception("Unexpected response!! locQualifierMap is returned null");
        }

        SoftAssert softAssert = new SoftAssert();

        boolean retrievedConsignee = locQualifierMap.getConsignee();
        softAssert.assertTrue(retrievedConsignee,
                "Expected Consignee value in locQualifierMap: "+expectedConsignee+" but API returned " + retrievedConsignee);

        boolean retrievedCustomer = locQualifierMap.getCustomer();
        softAssert.assertTrue(retrievedCustomer,
                "Expected Customer value in locQualifierMap: "+expectedCustomer+" but API returned " +retrievedCustomer );

        boolean retrievedCustomsBroker = locQualifierMap.getCustomsBroker();
        softAssert.assertTrue(retrievedCustomsBroker,
                "Expected CustomsBroker value in locQualifierMap: "+expectedCustomsBroker+" but API returned " + retrievedCustomsBroker);

        boolean retrievedDistCenter = locQualifierMap.getDistCenter();
        softAssert.assertTrue(retrievedDistCenter,
                "Expected DistCenter value in locQualifierMap: "+expectedDistCenter+" but API returned " + retrievedDistCenter);

        boolean retrievedDropYard = locQualifierMap.getDropYard();
        softAssert.assertTrue(retrievedDropYard,
                "Expected DropYard value in locQualifierMap: "+expectedDropYard+" but API returned " + retrievedDropYard);

        boolean retrievedGeocoded = locQualifierMap.getGeocoded();
        softAssert.assertFalse(retrievedGeocoded,
                "Expected Geocoded value in locQualifierMap: "+expectedGeocoded+" but API returned " + retrievedGeocoded);

        boolean retrievedOutsideTerminal = locQualifierMap.getOutsideTerminal();
        softAssert.assertFalse(retrievedOutsideTerminal,
                "Expected OutsideTerminal value in locQualifierMap: "+expectedOutsideTerminal+" but API returned " + retrievedOutsideTerminal);

        boolean retrievedProspect = locQualifierMap.getProspect();
        softAssert.assertTrue(retrievedProspect,
                "Expected Prospect value in locQualifierMap: "+expectedProspect+" but API returned " + retrievedProspect);

        boolean retrievedShipper = locQualifierMap.getShipper();
        softAssert.assertTrue(retrievedShipper,
                "Expected Shipper value in locQualifierMap: "+expectedShipper+" but API returned " + retrievedShipper);

        boolean retrievedSteamship = locQualifierMap.getSteamship();
        softAssert.assertTrue(retrievedSteamship,
                "Expected Steamship value in locQualifierMap: "+expectedSteamship+" but API returned " + retrievedSteamship);

        boolean retrievedTerminal = locQualifierMap.getTerminal();
        softAssert.assertTrue(retrievedTerminal,
                "Expected Terminal value in locQualifierMap: "+expectedTerminal+" but API returned " + retrievedTerminal);

        boolean retrievedTrailerPool = locQualifierMap.getTrailerPool();
        softAssert.assertTrue(retrievedTrailerPool,
                "Expected TrailerPool value in locQualifierMap: "+expectedTrailerPool+" but API returned " + retrievedTrailerPool);

        boolean retrievedTrailerWash = locQualifierMap.getTrailerWash();
        softAssert.assertFalse(retrievedTrailerWash,
                "Expected TrailerWash value in locQualifierMap: "+expectedTrailerWash+" but API returned " + retrievedTrailerWash);

        boolean retrievedTruckstop = locQualifierMap.getTruckstop();
        softAssert.assertTrue(retrievedTruckstop,
                "Expected Truckstop value in locQualifierMap: "+expectedTruckstop+" but API returned " + retrievedTruckstop);

        softAssert.assertAll();
    }

    @Test
    public void verify_direction_details_for_location_id_84LUORFL() throws Exception {
        String expectedDirections = "FROM I-94W.  TAKE IL-137/BUCKELY RD EXIT.  USE RIGHT TWO LANES TO TURN RIGHT ONTO IL-137.  TAKE THE FIRST RIGHT YOU COME TO ABOUT A 1/4 MILE DOWN ONTO ABBOTT PARK RD.  THEY WILL BE JUST UNDER A HALF MILE DOWN. VEAR RIGHT ON ABBOT PARK RD, AP 5 SHOULD BE ON THE LEFT HAND SIDE OF ABBOT PARK ROAD. AP 5 IS ON THE BACK SIDE OF ABBOT LABORATORIES CREDIT UNION";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","84LUORFL"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String retrievedDirections = locationDetails.getDirections();
        if(retrievedDirections==null){
            throw new Exception("Unexpected response!! directions is returned null");
        }

        Assert.assertEquals(retrievedDirections, expectedDirections, "Expected directions: ["+expectedDirections+"] but API returned: "+retrievedDirections);
    }

    @Test
    public void verify_pickupInstruction_details_for_location_id_84LUORFL() throws Exception {
        String expectedPickupInstruction = "DRIVERS GO TO GATE 3\nGO TO AP 5";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","84LUORFL"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String retrievedPickupInstruction = locationDetails.getPickupInstruction();
        if(retrievedPickupInstruction==null){
            throw new Exception("Unexpected response!! pickupInstruction is returned null");
        }

        Assert.assertEquals(retrievedPickupInstruction, expectedPickupInstruction, "Expected directions: ["+expectedPickupInstruction
                +"] but API returned: "+retrievedPickupInstruction);
    }

    @Test
    public void verify_dropInstruction_details_for_location_id_84LUORFL() throws Exception {
        String expectedDropInstruction = "DO NOT USE GPS DIRECTIONS - WILL TAKE YOU TO A DIFFERENT PLACE, USE THESE DIRECTIONS";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","84LUORFL"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String retrievedDropInstruction = locationDetails.getDropInstruction();
        if(retrievedDropInstruction==null){
            throw new Exception("Unexpected response!! DropInstruction is returned null");
        }

        Assert.assertEquals(retrievedDropInstruction, expectedDropInstruction, "Expected directions: ["+expectedDropInstruction
                +"] but API returned: "+retrievedDropInstruction);
    }

    @Test
    public void verify_loading_time_for_location_id_8201FOAR() throws Exception {
        String expectedLoadingTime = "0";

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","8201FOAR"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String retrievedLoadingTime = locationDetails.getLoadingTime();
        if(retrievedLoadingTime==null){
            throw new Exception("Unexpected response!! LoadingTime is returned null");
        }

        Assert.assertEquals(retrievedLoadingTime, expectedLoadingTime, "Expected LoadingTime: ["+expectedLoadingTime
                +"] but API returned: ["+retrievedLoadingTime+"]");
    }

    @Test
    public void verify_Unloading_time_for_location_id_8201FOAR() throws Exception {
        String expectedUnloadingTime = "0";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","8201FOAR"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        String retrievedUnloadingTime = locationDetails.getUnloadingTime();
        if(retrievedUnloadingTime==null){
            throw new Exception("Unexpected response!! UnloadingTime is returned null");
        }

        Assert.assertEquals(retrievedUnloadingTime, expectedUnloadingTime, "Expected UnloadingTime: ["+expectedUnloadingTime
                +"] but API returned: ["+retrievedUnloadingTime+"]");
    }

    //add missing details null tc
    //119SMETX all cmt added for tc

    @Test
    public void verify_all_type_of_comment_record_are_retrieved_for_location() throws Exception {
        ArrayList<String> expectedCommentTypeID = new ArrayList<String>();
        expectedCommentTypeID.add("CBI");
        expectedCommentTypeID.add("CCO");
        expectedCommentTypeID.add("CONTACT");
        expectedCommentTypeID.add("CREDIT");
        expectedCommentTypeID.add("CSC");
        expectedCommentTypeID.add("DAU");
        expectedCommentTypeID.add("DCA");
        expectedCommentTypeID.add("DCO");
        expectedCommentTypeID.add("DCR");
        expectedCommentTypeID.add("DGI");
        expectedCommentTypeID.add("DID");
        expectedCommentTypeID.add("DISP");
        expectedCommentTypeID.add("DLO");
        expectedCommentTypeID.add("DPA");
        expectedCommentTypeID.add("DRE");
        expectedCommentTypeID.add("DRF");
        expectedCommentTypeID.add("DRI");
        expectedCommentTypeID.add("DSA");
        expectedCommentTypeID.add("DSV");
        expectedCommentTypeID.add("DSW");
        expectedCommentTypeID.add("DTE");
        expectedCommentTypeID.add("DTR");
        expectedCommentTypeID.add("DVP");
        expectedCommentTypeID.add("DWC");
        expectedCommentTypeID.add("DWT");
        expectedCommentTypeID.add("ELOG");
        expectedCommentTypeID.add("EQUIPDOC");
        expectedCommentTypeID.add("FINAL_CO");
        expectedCommentTypeID.add("FINAL_ST");
        expectedCommentTypeID.add("FLOLOGIX");
        expectedCommentTypeID.add("GEN");
        expectedCommentTypeID.add("IMPORTED");
        expectedCommentTypeID.add("INACTIVE");
        expectedCommentTypeID.add("INT");
        expectedCommentTypeID.add("IRE");
        expectedCommentTypeID.add("KUDO1");
        expectedCommentTypeID.add("KUDO2");
        expectedCommentTypeID.add("KUDOS");
        expectedCommentTypeID.add("LEIS");
        expectedCommentTypeID.add("LSC");
        expectedCommentTypeID.add("LSCA");
        expectedCommentTypeID.add("PDE");
        expectedCommentTypeID.add("REPLACE");
        expectedCommentTypeID.add("ROU");
        expectedCommentTypeID.add("SDR");
        expectedCommentTypeID.add("SETTLGAC");
        expectedCommentTypeID.add("SHOP-UPD");
        expectedCommentTypeID.add("TERM");
        expectedCommentTypeID.add("TIS");
        expectedCommentTypeID.add("TNOT");
        expectedCommentTypeID.add("TRE");
        expectedCommentTypeID.add("TVI");
        expectedCommentTypeID.add("WRITEOFF");

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","28WEWYMI"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationComment> retrievedCommentList = locationDetails.getComment();

        if(retrievedCommentList==null){
            throw new Exception("Unexpected response!! comment is returned null");
        }

        ArrayList<String> retrievedCommentTypeID = new ArrayList<String>();
        for(LocationComment c: retrievedCommentList){
            retrievedCommentTypeID.add(c.getCommentTypeId());
        }

        SoftAssert softAssert = new SoftAssert();
        for (String s : expectedCommentTypeID) {
            System.out.println("id: "+s+" comp: "+retrievedCommentTypeID.contains(s));
            softAssert.assertTrue(retrievedCommentTypeID.contains(s),
                    "Comment type: " + s + " is not retrieved by the API");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="expectedCommentDetails")
    public void verify_comment_details_retrieved_for_locationId_28WEWYMI(String commentType,
                                                                        HashMap<String, Object> expectedData) throws Exception {
        String expectedCompanyID = (String) expectedData.get("company_id");
        String expectedComments = (String) expectedData.get("comments");
        String expectedEnteredDate = (String) expectedData.get("entered_date");
        String expectedEnteredUserId = (String) expectedData.get("entered_user_id");
        String expectedParentRowID = (String) expectedData.get("parent_row_id");
        String expectedParentRowType = (String) expectedData.get("parent_row_type");
        String expectedType = (String) expectedData.get("__type");
        String commentTypeDescr_expectedCompanyID = (String) expectedData.get("NESTED_company_id");
        String commentTypeDescr_expectedDesc = (String) expectedData.get("NESTED_descr");
        String commentTypeDescr_expectedID = (String) expectedData.get("NESTED_id");
        Boolean commentTypeDescr_expectedIsActive = (Boolean) expectedData.get("NESTED_is_active");

        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","28WEWYMI"));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationComment> retrievedCommentList = locationDetails.getComment();
        if(retrievedCommentList==null){
            throw new Exception("Unexpected response!! comment is returned null");
        }
        LocationComment retrievedComment = null;
        for(LocationComment c: retrievedCommentList){
            if(c.getCommentTypeId().equals(commentType)){
                retrievedComment = c;
            }
        }

        if(retrievedComment == null){
            throw new Exception(
                    "Test data has been modified. Check comment of type "+commentType+" is added for loc 119SMETX in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrievedCompanyID= (retrievedComment.getCompanyId() == null) ? "NULL" : retrievedComment.getCompanyId();
        softAssert.assertEquals(retrievedCompanyID, expectedCompanyID,
                "Expected Company ID: [" + expectedCompanyID + "] but API returned [" + retrievedCompanyID + "]");

        String retrievedComments= (retrievedComment.getComments() == null) ? "NULL" : retrievedComment.getComments();
        softAssert.assertEquals(retrievedComments, expectedComments,
                "Expected Comments: [" + expectedComments + "] but API returned [" + retrievedComments + "]");

        String retrievedDescr= (retrievedComment.getEnteredDate() == null) ? "NULL" : retrievedComment.getEnteredDate();
        softAssert.assertEquals(retrievedDescr, expectedEnteredDate,
                "Expected desc: [" + expectedEnteredDate + "] but API returned [" + retrievedDescr + "]");

        String retrievedEnteredUserId= (retrievedComment.getEnteredUserId()== null) ? "NULL" : retrievedComment.getEnteredUserId();
        softAssert.assertEquals(retrievedEnteredUserId, expectedEnteredUserId,
                "Expected entered user id: [" + expectedEnteredUserId + "] but API returned [" + retrievedEnteredUserId + "]");

        String retrievedParentRowID= (retrievedComment.getParentRowId()== null) ? "NULL" : retrievedComment.getParentRowId();
        softAssert.assertEquals(retrievedParentRowID, expectedParentRowID,
                "Expected parent row id: [" + expectedParentRowID + "] but API returned [" + retrievedParentRowID + "]");

        String retrievedParentRowType= (retrievedComment.getParentRowType()== null) ? "NULL" : retrievedComment.getParentRowType();
        softAssert.assertEquals(retrievedParentRowType, expectedParentRowType,
                "Expected parent row type: [" + expectedParentRowType + "] but API returned [" + retrievedParentRowType + "]");

        String retrievedType= (retrievedComment.getType()== null) ? "NULL" : retrievedComment.getType();
        softAssert.assertEquals(retrievedType, expectedType,
                "Expected __type: [" + expectedType + "] but API returned [" + retrievedType + "]");

        String commentTypeDescr_retrievedCompanyID= (retrievedComment.getCommentTypeDescr().getCompanyId() == null) ? "NULL" :
                retrievedComment.getCommentTypeDescr().getCompanyId();
        softAssert.assertEquals(commentTypeDescr_retrievedCompanyID, commentTypeDescr_expectedCompanyID,
                "Expected commentTypeDescr.company_id: [" + commentTypeDescr_expectedCompanyID + "] but API returned [" + commentTypeDescr_retrievedCompanyID + "]");

        String commentTypeDescr_retrievedDesc= (retrievedComment.getCommentTypeDescr().getDescr()== null) ? "NULL" :
                retrievedComment.getCommentTypeDescr().getDescr();
        softAssert.assertEquals(commentTypeDescr_retrievedDesc, commentTypeDescr_expectedDesc,
                "Expected commentTypeDescr.descr: [" + commentTypeDescr_expectedDesc + "] but API returned [" + commentTypeDescr_retrievedDesc + "]");

        String commentTypeDescr_retrievedID= (retrievedComment.getCommentTypeDescr().getId() == null) ? "NULL" :
                retrievedComment.getCommentTypeDescr().getId();
        softAssert.assertEquals(commentTypeDescr_retrievedID, commentTypeDescr_expectedID,
                "Expected commentTypeDescr.id: [" + commentTypeDescr_expectedID + "] but API returned [" + commentTypeDescr_retrievedID + "]");

        Boolean commentTypeDescr_retrievedIsActive= (retrievedComment.getCommentTypeDescr().getIsActive() == null) ? Boolean.valueOf("NULL") :
                retrievedComment.getCommentTypeDescr().getIsActive();
        softAssert.assertEquals(commentTypeDescr_retrievedIsActive, commentTypeDescr_expectedIsActive,
                "Expected commentTypeDescr.is_active: [" + commentTypeDescr_expectedIsActive + "] but API returned [" + commentTypeDescr_retrievedIsActive + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_comment_list_is_returned_NULL_for_locationId_8638RAMO() throws Exception {
        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationComment> retrievedCommentList = locationDetails.getComment();
        Assert.assertNull(retrievedCommentList, "Comment for location ID: ["+locId+"] is not [NULL]");
    }

    @Test
    public void verify_comment_list_size_is_3_for_locationId_8638RAMO() throws Exception {
        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationComment> commentList = locationDetails.getComment();
        if(commentList == null){
            throw new Exception("Comment list is returned null \n Response payload: \n" + response.asPrettyString());
        }
        Integer retrievedCommentListSize = commentList.size();
        Integer expectedCommentListSize = 3;
        Assert.assertEquals(retrievedCommentListSize, expectedCommentListSize ,
                "Expected comment list size: ["+expectedCommentListSize+"] but API returned list size ["+retrievedCommentListSize+"]");
    }

    @Test
    public void verify_comment_type_id_and_commentTypeDescr_is_NULL_for_comment_without_type() throws Exception {
        String locId = "84LURIVA";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationComment> retrievedCommentList = locationDetails.getComment();
        if(retrievedCommentList==null){
            throw new Exception("Unexpected response!! comment is returned null");
        }
        LocationComment retrievedComment = retrievedCommentList.get(0);

        if(retrievedComment == null){
            throw new Exception(
                    "Test data has been modified. Check comment record without type is added for loc ["+locId+"] in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNull(retrievedComment.getCommentTypeDescr(), "commentTypeDescr is not null");
        softAssert.assertNull(retrievedComment.getCommentTypeId(), "comment_type_id is not null");
        softAssert.assertAll();
    }

    //recheck the test we r not using asserNull for null check
    @Test
    public void verify_contact_is_NULL_for_locationID_84LURIVA() throws Exception {
        String locId = "84LURIVA";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationContact> contactList = locationDetails.getContact();
        if(contactList == null){
            throw new Exception(" Contact list is returned null \n Response payload: \n" + response.asPrettyString());
        }
        int contactListSize = contactList.size();
        Assert.assertEquals(contactListSize, 0, "LocationID: ["+locId+"] contact list is not empty");
    }

    @Test
    public void verify_contact_list_size_is_2_for_locationId_8638RAMO() throws Exception {
        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationContact> contactList = locationDetails.getContact();

        Integer retrievedContactListSize = contactList.size();
        Integer expectedContactListSize = 2;
        Assert.assertEquals(retrievedContactListSize, expectedContactListSize ,
                "Expected comment list size: ["+expectedContactListSize+"] but API returned list size ["+retrievedContactListSize+"]");
    }

    @Test
    public void verify_contact_record_with_all_contact_details_given_in_masterfiles() throws Exception {
        String expected_company_id = "T100";
        String expected_email = "jafl@gmail.com";
        String expected_fax = "800-456-345";
        Boolean expected_is_active = true;
        String expected_mobile_phone = "998-868-5317";
        String expected_name = "JOON";
        String expected_parent_row_id = "8638RAMO";
        String expected_parent_row_type = "L";
        String expected_phone = "238286";
        String expected_title = "MAIN POC";
        String expected_type = "contact";

        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationContact> retrievedContactList = locationDetails.getContact();
        if(retrievedContactList==null){
            throw new Exception("Unexpected response!! contact is returned null");
        }
        LocationContact retrievedContact = null;

        for(LocationContact c: retrievedContactList){
            if(c.getName().equals(expected_name)){
                retrievedContact = c;
            }
        }

        if(retrievedContact == null){
            throw new Exception(
                    "Test data has been modified. Check contact record with name [" +expected_name+"] added for loc ["+locId+"] in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_company_id= (retrievedContact.getCompanyId() == null) ? "NULL" : retrievedContact.getCompanyId();
        softAssert.assertEquals(retrieved_company_id, expected_company_id,
                "Expected Company ID: [" + expected_company_id + "] but API returned [" + retrieved_company_id + "]");

        String retrieved_email= (retrievedContact.getEmail() == null) ? "NULL" : retrievedContact.getEmail();
        softAssert.assertEquals(retrieved_email, expected_email,
                "Expected email: [" + expected_email + "] but API returned [" + retrieved_email + "]");

        String retrieved_fax= (retrievedContact.getFax() == null) ? "NULL" : retrievedContact.getFax();
        softAssert.assertEquals(retrieved_fax, expected_fax,
                "Expected fax: [" + expected_fax + "] but API returned [" + retrieved_fax + "]");

        Boolean retrieved_is_active= retrievedContact.getIsActive();
        softAssert.assertEquals(retrieved_is_active, expected_is_active,
                "Expected is_active: [" + expected_is_active + "] but API returned [" + retrieved_is_active + "]");

        String retrieved_mobile_phone= (retrievedContact.getMobilePhone()== null) ? "NULL" : retrievedContact.getMobilePhone();
        softAssert.assertEquals(retrieved_mobile_phone, expected_mobile_phone,
                "Expected mobile_phone: [" + expected_mobile_phone + "] but API returned [" + retrieved_mobile_phone + "]");

        String retrieved_parent_row_id= (retrievedContact.getParentRowId()== null) ? "NULL" : retrievedContact.getParentRowId();
        softAssert.assertEquals(retrieved_parent_row_id, expected_parent_row_id,
                "Expected parent_row_id: [" + expected_parent_row_id + "] but API returned [" + retrieved_parent_row_id + "]");

        String retrieved_parent_row_type= (retrievedContact.getParentRowType()== null) ? "NULL" : retrievedContact.getParentRowType();
        softAssert.assertEquals(retrieved_parent_row_type, expected_parent_row_type,
                "Expected parent_row_type: [" + expected_parent_row_type + "] but API returned [" + retrieved_parent_row_type + "]");

        String retrieved_phone= (retrievedContact.getPhone()== null) ? "NULL" : retrievedContact.getPhone();
        softAssert.assertEquals(retrieved_phone, expected_phone,
                "Expected phone: [" + expected_phone + "] but API returned [" + retrieved_phone + "]");

        String retrieved_title= (retrievedContact.getTitle()== null) ? "NULL" : retrievedContact.getTitle();
        softAssert.assertEquals(retrieved_title, expected_title,
                "Expected title: [" + expected_title + "] but API returned [" + retrieved_title + "]");

        String retrieved_type= (retrievedContact.getType()== null) ? "NULL" : retrievedContact.getType();
        softAssert.assertEquals(retrieved_type, expected_type,
                "Expected type: [" + expected_type + "] but API returned [" + retrieved_type + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_null_is_returned_for_missing_contact_details_in_masterfiles() throws Exception {
        String expected_email = null;
        String expected_fax = null;
        String expected_mobile_phone = null;
        String expected_name = "GEORGE";
        String expected_phone = null;
        String expected_title = null;

        String locId = "8638RAMO";
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locId));

        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locationDetails = parseResponseToObject(response, LocationDetails.class);

        List<LocationContact> retrievedContactList = locationDetails.getContact();
        if(retrievedContactList==null){
            throw new Exception("Unexpected response!! contact is returned null");
        }
        LocationContact retrievedContact = null;

        for(LocationContact c: retrievedContactList){
            if(c.getName().equals(expected_name)){
                retrievedContact = c;
            }
        }

        if(retrievedContact == null){
            throw new Exception(
                    "Test data has been modified. Check contact record with name [" +expected_name+"] added for loc ["+locId+"] in masterfile");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_email= retrievedContact.getEmail();
        softAssert.assertNull(retrieved_email,
                "Expected email: [" + expected_email + "] but API returned [" + retrieved_email + "]");

        String retrieved_fax= retrievedContact.getFax();
        softAssert.assertNull(retrieved_fax,
                "Expected fax: [" + expected_fax + "] but API returned [" + retrieved_fax + "]");

        String retrieved_mobile_phone= retrievedContact.getMobilePhone();
        softAssert.assertNull(retrieved_mobile_phone,
                "Expected mobile_phone: [" + expected_mobile_phone + "] but API returned [" + retrieved_mobile_phone + "]");

        String retrieved_phone= retrievedContact.getPhone();
        softAssert.assertNull(retrieved_phone,
                "Expected phone: [" + expected_phone + "] but API returned [" + retrieved_phone + "]");

        String retrieved_title= retrievedContact.getTitle();
        softAssert.assertNull(retrieved_title,
                "Expected title: [" + expected_title + "] but API returned [" + retrieved_title + "]");

        softAssert.assertAll();
    }

    @DataProvider(name="expectedCommentDetails")
    public Object[][] getLocationIDDataProvider(){
        HashMap<String, Object> ROU = new HashMap<String, Object>();
        ROU.put("comment_type_id","ROU");
        ROU.put("comments","*** THIS IS TEST COMMENT 123 ***");
        ROU.put("company_id","T100");
        ROU.put("entered_date","20211102115400-0500");
        ROU.put("entered_user_id","manoj.than");
        ROU.put("parent_row_id","28WEWYMI");
        ROU.put("parent_row_type","L");
        ROU.put("__type","comments");
        ROU.put("NESTED_id","ROU");
        ROU.put("NESTED_is_active",true);
        ROU.put("NESTED_company_id","T100");
        ROU.put("NESTED_descr","DRIVER ROUTING INFO");

        HashMap<String, Object> SDR = new HashMap<String, Object>();
        SDR.put("comment_type_id","SDR");
        SDR.put("comments","*** THIS IS TEST COMMENT 123 ***");
        SDR.put("company_id","T100");
        SDR.put("entered_date","20211102115400-0500");
        SDR.put("entered_user_id","manoj.than");
        SDR.put("parent_row_id","28WEWYMI");
        SDR.put("parent_row_type","L");
        SDR.put("__type","comments");
        SDR.put("NESTED_id","SDR");
        SDR.put("NESTED_is_active",true);
        SDR.put("NESTED_company_id","T100");
        SDR.put("NESTED_descr","SETTLEMENTS DRVR SETUP");

        HashMap<String, Object> TRE = new HashMap<String, Object>();
        TRE.put("comment_type_id","TRE");
        TRE.put("comments","*** THIS IS TEST COMMENT 123 ***");
        TRE.put("company_id","T100");
        TRE.put("entered_date","20211102115500-0500");
        TRE.put("entered_user_id","manoj.than");
        TRE.put("parent_row_id","28WEWYMI");
        TRE.put("parent_row_type","L");
        TRE.put("__type","comments");
        TRE.put("NESTED_id","TRE");
        TRE.put("NESTED_is_active",true);
        TRE.put("NESTED_company_id","T100");
        TRE.put("NESTED_descr","RESERVED");

        HashMap<String, Object> WRITEOFF = new HashMap<String, Object>();
        WRITEOFF.put("comment_type_id","WRITEOFF");
        WRITEOFF.put("comments","*** THIS IS TEST COMMENT 123 ***");
        WRITEOFF.put("company_id","T100");
        WRITEOFF.put("entered_date","20211102115600-0500");
        WRITEOFF.put("entered_user_id","manoj.than");
        WRITEOFF.put("parent_row_id","28WEWYMI");
        WRITEOFF.put("parent_row_type","L");
        WRITEOFF.put("__type","comments");
        WRITEOFF.put("NESTED_id","WRITEOFF");
        WRITEOFF.put("NESTED_is_active",true);
        WRITEOFF.put("NESTED_company_id","T100");
        WRITEOFF.put("NESTED_descr","WRITE OFF");

        HashMap<String, Object> LSCA = new HashMap<String, Object>();
        LSCA.put("comment_type_id","LSCA");
        LSCA.put("comments","*** THIS IS TEST COMMENT 123 ***");
        LSCA.put("company_id","T100");
        LSCA.put("entered_date","20211102115300-0500");
        LSCA.put("entered_user_id","manoj.than");
        LSCA.put("parent_row_id","28WEWYMI");
        LSCA.put("parent_row_type","L");
        LSCA.put("__type","comments");
        LSCA.put("NESTED_id","LSCA");
        LSCA.put("NESTED_is_active",true);
        LSCA.put("NESTED_company_id","T100");
        LSCA.put("NESTED_descr","STUDENT PAPERWORK");

        HashMap<String, Object> PDE = new HashMap<String, Object>();
        PDE.put("comment_type_id","PDE");
        PDE.put("comments","*** THIS IS TEST COMMENT 123 ***");
        PDE.put("company_id","T100");
        PDE.put("entered_date","20211102115300-0500");
        PDE.put("entered_user_id","manoj.than");
        PDE.put("parent_row_id","28WEWYMI");
        PDE.put("parent_row_type","L");
        PDE.put("__type","comments");
        PDE.put("NESTED_id","PDE");
        PDE.put("NESTED_is_active",true);
        PDE.put("NESTED_company_id","T100");
        PDE.put("NESTED_descr","POTENTIAL DETENTION");

        HashMap<String, Object> REPLACE = new HashMap<String, Object>();
        REPLACE.put("comment_type_id","REPLACE");
        REPLACE.put("comments","*** THIS IS TEST COMMENT 123 ***");
        REPLACE.put("company_id","T100");
        REPLACE.put("entered_date","20211102115300-0500");
        REPLACE.put("entered_user_id","manoj.than");
        REPLACE.put("parent_row_id","28WEWYMI");
        REPLACE.put("parent_row_type","L");
        REPLACE.put("__type","comments");
        REPLACE.put("NESTED_id","REPLACE");
        REPLACE.put("NESTED_is_active",true);
        REPLACE.put("NESTED_company_id","T100");
        REPLACE.put("NESTED_descr","Replace master code");

        HashMap<String, Object> KUDOS = new HashMap<String, Object>();
        KUDOS.put("comment_type_id","KUDOS");
        KUDOS.put("comments","*** THIS IS TEST COMMENT 123 ***");
        KUDOS.put("company_id","T100");
        KUDOS.put("entered_date","20211102115200-0500");
        KUDOS.put("entered_user_id","manoj.than");
        KUDOS.put("parent_row_id","28WEWYMI");
        KUDOS.put("parent_row_type","L");
        KUDOS.put("__type","comments");
        KUDOS.put("NESTED_id","KUDOS");
        KUDOS.put("NESTED_is_active",true);
        KUDOS.put("NESTED_company_id","T100");
        KUDOS.put("NESTED_descr","Driver On Time Delivery");

        return new Object[][] { { "ROU", ROU },{ "SDR", SDR },{ "TRE", TRE }, { "REPLACE", REPLACE },{ "WRITEOFF", WRITEOFF },
                { "LSCA", LSCA },{ "PDE", PDE },{ "KUDOS", KUDOS }};
    }

    @DataProvider(name="LocationIDDataProvider")
    public Object[][] getExpectedCommentDetails(){
        return new Object[][]
                {
                        {"8600PLMO",new Pair<>("BOBTAIL","BOBTAIL")},
                        {"8604HYMD",new Pair<>("N","N")},
                        {"8638RAMO", new Pair<>("DL","DL")},
                        {"8800TOAZ", new Pair<>("DROP","DROP")},
                        {"89DELONY", new Pair<>("DROPHOOK","DROPHOOK")},
                        {"850SLOCO", new Pair<>("DU","DU")},
                        {"84LUFOFL", new Pair<>("EMPTY","EMPTY")},
                        {"84LUMEFL", new Pair<>("FLOOR","FLOOR")},
                        {"84LUORFL", new Pair<>("LL","LL")},
                        {"84LURIVA", new Pair<>("LU","LU")},
                        {"84LUWIFL", new Pair<>("NT","NT")},
                        {"8330BAFL", new Pair<>("PLTEXCH","PLTEXCH")},
                        {"8201FOAR", new Pair<>("SLIP","SLIP")},
                        {"820WWIIL", new Pair<>("Y","Y")},
                };
    }
}
