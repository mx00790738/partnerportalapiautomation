package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import javafx.util.Pair;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import pojoClasses.responsePojo.MasterData.Location.WorkingHours;
import testConfiguration.TestConfig;

public class Location_WorkingHoursAPITest extends BaseTest {

    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_working_hours_API_health_check() throws Exception {
        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","9440OAI1").
                replace("{state_id}","IL"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_working_hours_for_location_id_1690TRCA() throws Exception {
        String expectedMondayStartTime = "04:00";
        String expectedMondayCloseTime = "02:00";
        String expectedTuesdayStartTime = "04:00";
        String expectedTuesdayCloseTime = "02:00";
        String expectedWednesdayStartTime = "04:00";
        String expectedWednesdayCloseTime = "02:00";
        String expectedThursdayStartTime = "04:00";
        String expectedThursdayCloseTime = "02:00";
        String expectedFridayStartTime = "04:00";
        String expectedFridayCloseTime = "02:00";
        String expectedSaturdayStartTime = "04:00";
        String expectedSaturdayCloseTime = "02:00";
        String expectedSundayStartTime = "04:00";
        String expectedSundayCloseTime = "02:00";

        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","1690TRCA").replace("{state_id}","CA"));

        Response response = RequestMethod.GET(requestSpecification);
        WorkingHours workingHours = parseResponseToObject(response, WorkingHours.class);

        SoftAssert softAssert = new SoftAssert();

        String retrievedMondayStartTime = (workingHours.getTime().getMonday().getStartTime()== null) ? "NULL" : workingHours.getTime().getMonday().getStartTime();
        softAssert.assertEquals(retrievedMondayStartTime, expectedMondayStartTime,
                "Expected monday start time: [" + expectedMondayStartTime + "] but API returned [" + retrievedMondayStartTime + "]");

        String retrievedMondayCloseTime = (workingHours.getTime().getMonday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getMonday().getCloseTime();
        softAssert.assertEquals(retrievedMondayCloseTime, expectedMondayCloseTime,
                "Expected monday close time: [" + expectedMondayCloseTime + "] but API returned [" + retrievedMondayCloseTime + "]");

        String retrievedTuesdayStartTime = (workingHours.getTime().getTuesday().getStartTime()== null) ? "NULL" : workingHours.getTime().getTuesday().getStartTime();
        softAssert.assertEquals(retrievedTuesdayStartTime, expectedTuesdayStartTime,
                "Expected Tuesday start time: [" + expectedTuesdayStartTime + "] but API returned [" + retrievedTuesdayStartTime + "]");

        String retrievedTuesdayCloseTime = (workingHours.getTime().getTuesday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getTuesday().getCloseTime();
        softAssert.assertEquals(retrievedTuesdayCloseTime, expectedTuesdayCloseTime,
                "Expected Tuesday close time: [" + expectedTuesdayCloseTime + "] but API returned [" + retrievedTuesdayCloseTime + "]");

        String retrievedWednesdayStartTime = (workingHours.getTime().getWednesday().getStartTime()== null) ? "NULL" : workingHours.getTime().getWednesday().getStartTime();
        softAssert.assertEquals(retrievedWednesdayStartTime, expectedWednesdayStartTime,
                "Expected Wednesday start time: [" + expectedWednesdayStartTime + "] but API returned [" + retrievedWednesdayStartTime + "]");

        String retrievedWednesdayCloseTime = (workingHours.getTime().getWednesday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getWednesday().getCloseTime();
        softAssert.assertEquals(retrievedWednesdayCloseTime, expectedWednesdayCloseTime,
                "Expected Wednesday close time: [" + expectedWednesdayCloseTime + "] but API returned [" + retrievedWednesdayCloseTime + "]");

        String retrievedThursdayStartTime = (workingHours.getTime().getThursday().getStartTime()== null) ? "NULL" : workingHours.getTime().getThursday().getStartTime();
        softAssert.assertEquals(retrievedThursdayStartTime, expectedThursdayStartTime,
                "Expected Thursday start time: [" + expectedThursdayStartTime + "] but API returned [" + retrievedThursdayStartTime + "]");

        String retrievedThursdayCloseTime = (workingHours.getTime().getThursday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getThursday().getCloseTime();
        softAssert.assertEquals(retrievedThursdayCloseTime, expectedThursdayCloseTime,
                "Expected Thursday close time: [" + expectedThursdayCloseTime + "] but API returned [" + retrievedThursdayCloseTime + "]");

        String retrievedFridayStartTime = (workingHours.getTime().getFriday().getStartTime()== null) ? "NULL" : workingHours.getTime().getFriday().getStartTime();
        softAssert.assertEquals(retrievedFridayStartTime, expectedFridayStartTime,
                "Expected Friday start time: [" + expectedFridayStartTime + "] but API returned [" + retrievedFridayStartTime + "]");

        String retrievedFridayCloseTime = (workingHours.getTime().getFriday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getFriday().getCloseTime();
        softAssert.assertEquals(retrievedFridayCloseTime, expectedFridayCloseTime,
                "Expected Friday close time: [" + expectedFridayCloseTime + "] but API returned [" + retrievedFridayCloseTime + "]");

        String retrievedSaturdayStartTime = (workingHours.getTime().getSaturday().getStartTime()== null) ? "NULL" : workingHours.getTime().getSaturday().getStartTime();
        softAssert.assertEquals(retrievedSaturdayStartTime, expectedSaturdayStartTime,
                "Expected Saturday start time: [" + expectedSaturdayStartTime + "] but API returned [" + retrievedSaturdayStartTime + "]");

        String retrievedSaturdayCloseTime = (workingHours.getTime().getSaturday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getSaturday().getCloseTime();
        softAssert.assertEquals(retrievedSaturdayCloseTime, expectedSaturdayCloseTime,
                "Expected Saturday close time: [" + expectedSaturdayCloseTime + "] but API returned [" + retrievedSaturdayCloseTime + "]");

        String retrievedSundayStartTime = (workingHours.getTime().getSunday().getStartTime()== null) ? "NULL" : workingHours.getTime().getSunday().getStartTime();
        softAssert.assertEquals(retrievedSundayStartTime, expectedSundayStartTime,
                "Expected Sunday start time: [" + expectedSundayStartTime + "] but API returned [" + retrievedSundayStartTime + "]");

        String retrievedSundayCloseTime = (workingHours.getTime().getSunday().getCloseTime()== null) ? "NULL" : workingHours.getTime().getSunday().getCloseTime();
        softAssert.assertEquals(retrievedSundayCloseTime, expectedSundayCloseTime,
                "Expected Sunday close time: [" + expectedSundayCloseTime + "] but API returned [" + retrievedSundayCloseTime + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_dayOpenCloseMap_values_are_null_for_location_id_801EGRTX() throws Exception {
        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","801EGRTX").replace("{state_id}","CA"));

        Response response = RequestMethod.GET(requestSpecification);
        WorkingHours workingHours = parseResponseToObject(response, WorkingHours.class);

        SoftAssert softAssert = new SoftAssert();

        String retrievedMondayStartTime = workingHours.getTime().getMonday().getStartTime();
        softAssert.assertNull(retrievedMondayStartTime,
                "Expected monday start time: [null] but API returned [" + retrievedMondayStartTime +"]");

        String retrievedMondayCloseTime = workingHours.getTime().getMonday().getCloseTime();
        softAssert.assertNull(retrievedMondayCloseTime,
                "Expected monday close time: [null] but API returned [" + retrievedMondayCloseTime +"]");

        String retrievedTuesdayStartTime = workingHours.getTime().getTuesday().getStartTime();
        softAssert.assertNull(retrievedTuesdayStartTime,
                "Expected Tuesday start time: [null] but API returned [" + retrievedTuesdayStartTime +"]");

        String retrievedTuesdayCloseTime = workingHours.getTime().getTuesday().getCloseTime();
        softAssert.assertNull(retrievedTuesdayCloseTime,
                "Expected Tuesday close time: [null] but API returned [" + retrievedTuesdayCloseTime +"]");

        String retrievedWednesdayStartTime = workingHours.getTime().getWednesday().getStartTime();
        softAssert.assertNull(retrievedWednesdayStartTime,
                "Expected Wednesday start time: [null] but API returned [" + retrievedWednesdayStartTime +"]");

        String retrievedWednesdayCloseTime = workingHours.getTime().getWednesday().getCloseTime();
        softAssert.assertNull(retrievedWednesdayCloseTime,
                "Expected Wednesday close time: [null] but API returned [" + retrievedWednesdayCloseTime +"]");

        String retrievedThursdayStartTime = workingHours.getTime().getThursday().getStartTime();
        softAssert.assertNull(retrievedThursdayStartTime,
                "Expected Thursday start time: [null] but API returned [" + retrievedThursdayStartTime +"]");

        String retrievedThursdayCloseTime = workingHours.getTime().getThursday().getCloseTime();
        softAssert.assertNull(retrievedThursdayCloseTime,
                "Expected Thursday close time: [null] but API returned [" + retrievedThursdayCloseTime +"]");

        String retrievedFridayStartTime = workingHours.getTime().getFriday().getStartTime();
        softAssert.assertNull(retrievedFridayStartTime,
                "Expected Friday start time: [null] but API returned [" + retrievedFridayStartTime +"]");

        String retrievedFridayCloseTime = workingHours.getTime().getFriday().getCloseTime();
        softAssert.assertNull(retrievedFridayCloseTime,
                "Expected Friday close time: [null] but API returned [" + retrievedFridayCloseTime +"]");

        String retrievedSaturdayStartTime = workingHours.getTime().getSaturday().getStartTime();
        softAssert.assertNull(retrievedSaturdayStartTime,
                "Expected Saturday start time: [null] but API returned [" + retrievedSaturdayStartTime +"]");

        String retrievedSaturdayCloseTime = workingHours.getTime().getSaturday().getCloseTime();
        softAssert.assertNull(retrievedSaturdayCloseTime,
                "Expected Saturday close time: [null] but API returned [" + retrievedSaturdayCloseTime +"]");

        String retrievedSundayStartTime = workingHours.getTime().getSunday().getStartTime();
        softAssert.assertNull(retrievedSundayStartTime,
                "Expected Sunday start time: [null] but API returned [" + retrievedSundayStartTime +"]");

        String retrievedSundayCloseTime = workingHours.getTime().getSunday().getCloseTime();
        softAssert.assertNull(retrievedSundayCloseTime,
                "Expected Sunday close time: [null] but API returned [" + retrievedSundayCloseTime +"]");

        softAssert.assertAll();
    }

    @Test(dataProvider="LocationIDDataProvider")
    public void verify_country_value_are_retrieved(String expectedCountry, Pair<String, String> locDetails) throws Exception {
        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}",locDetails.getKey()).replace("{state_id}",locDetails.getValue()));

        Response response = RequestMethod.GET(requestSpecification);
        WorkingHours workingHours = parseResponseToObject(response, WorkingHours.class);

        String retrievedCountry = workingHours.getCountry();
        Assert.assertEquals(retrievedCountry ,expectedCountry,"Expected country: ["+expectedCountry+"] " +
                "but API returned: ["+retrievedCountry+"] for loc id: "+locDetails.getKey());

    }

    @Test
    public void verify_apptRequired_is_false_for_LocationID_801EGRTX() throws Exception {
        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","801EGRTX").replace("{state_id}","CA"));

        Response response = RequestMethod.GET(requestSpecification);
        WorkingHours workingHours = parseResponseToObject(response, WorkingHours.class);

        boolean retrievedApptRequired = workingHours.getApptRequired();
        boolean expectedApptRequire = false;
        Assert.assertEquals(retrievedApptRequired ,expectedApptRequire,"Expected apptRequired: ["+expectedApptRequire+"] " +
                "but API returned: ["+retrievedApptRequired+"]");
    }

    @Test
    public void verify_apptRequired_is_true_for_LocationID_1690TRCA() throws Exception {
        String path= TestConfig.get("LOCATION_WORKING_HOURS_API_PATH");
        requestSpecification.basePath(path.replace("{id}","1690TRCA").replace("{state_id}","CA"));

        Response response = RequestMethod.GET(requestSpecification);
        WorkingHours workingHours = parseResponseToObject(response, WorkingHours.class);

        boolean retrievedApptRequired = workingHours.getApptRequired();
        boolean expectedApptRequire = true;
        Assert.assertEquals(retrievedApptRequired ,expectedApptRequire,"Expected apptRequired: ["+expectedApptRequire+"] " +
                "but API returned: ["+retrievedApptRequired+"]");
    }

    @DataProvider(name="LocationIDDataProvider")
    public Object[][] getExpectedCommentDetails(){
        return new Object[][]
                {
                        {"USA",new Pair<>("8600PLMO","MO")},
                        {"CAN",new Pair<>("CITYCAAB","AB")},
                        {"MEX", new Pair<>("FUNDGODG","DG")},
                };
    }
}
