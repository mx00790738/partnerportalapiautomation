package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.*;
import pojoClasses.responsePojo.MasterData.Location.LocationSearchListEntry;
import testConfiguration.TestConfig;

import java.util.List;

public class Location_Search_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_SEARCH_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_search_API_health_check() throws Exception {
        String reqPayload = "{}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_location_list_returned_IsNot_MoreThan_100() throws Exception {
        String reqPayload = "{}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        int retrievedListSize = retrievedLocationList.size();
        if (retrievedListSize == 0) {
            throw new Exception("Unexpected response!! Response object is empty for location list \n Response body: \n"
                    + response.asPrettyString());
        }

        Assert.assertTrue(retrievedListSize <= 100,
                "Location list size expected: <=100 but API returned size: " + retrievedListSize);
    }

    @Test
    public void verify_location_list_entries_are_from_TMS() throws Exception {
        String reqPayload = "{}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.isEmpty()) {
            throw new Exception("Unexpected response!! Response object is empty for location list \n Response body: \n"
                    + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();

        for (LocationSearchListEntry loc : retrievedLocationList) {
            String companyID = (loc.getCompanyId() == null) ? "NULL" : loc.getCompanyId();
            softAssert.assertEquals(companyID, "TMS", "LocationID: " + loc.getId() + " expected companyID: TMS"
                    + " but API returned companyID: " + loc.getCompanyId());
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_location_1690TRCA_details_returned() throws Exception {
        Boolean expectedActiveStatus = true;
        String expectedAddressLine1 = "16900 W Schulte Rd";
        Boolean expectedAppointmentRequired = false;
        String expectedCity = "TRACY";
        int expectedCityID = 22050;
        String expectedLocationID = "1690TRCA";
        String expectedLocationCode = "1690TRCA";
        String expectedLocationType = "Un-defined";
        String expectedName = "SAFEWAY DC TRACY";
        String expectedPostalCode = "95376";
        String expectedState = "CA";

        String reqPayload = "{}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.isEmpty()) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = null;
        for (LocationSearchListEntry l : retrievedLocationList) {
            try {
                if (l.getId().equals(expectedLocationID)) {
                    retrievedLocation = l;
                }
            } catch (Exception e) {
            }
        }
        if (retrievedLocation == null) {
            throw new Exception("Location ID: " + expectedLocationID + " not found in response. \n Response: \n"
                    + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();

        String retrievedLocationID = (retrievedLocation.getId() == null) ? "NULL" : retrievedLocation.getId().trim();
        softAssert.assertEquals(retrievedLocationID, expectedLocationID,
                "Expected Customer ID: " + expectedLocationID + " but API returned " + retrievedLocationID);

        String retrievedLocationCode = (retrievedLocation.getLocationCode() == null) ? "NULL" : retrievedLocation.getLocationCode().trim();
        softAssert.assertEquals(retrievedLocationCode, expectedLocationCode,
                "Expected Customer ID: " + expectedLocationCode + " but API returned " + retrievedLocationCode);

        String retrievedLocationType = (retrievedLocation.getLocationType() == null) ? "NULL" : retrievedLocation.getLocationType().trim();
        softAssert.assertEquals(retrievedLocationType, expectedLocationType,
                "Expected Customer ID: " + expectedLocationType + " but API returned " + retrievedLocationType);

        String retrievedName = (retrievedLocation.getName() == null) ? "NULL" : retrievedLocation.getName().trim();
        softAssert.assertEquals(retrievedName, expectedName,
                "Expected Customer ID: " + expectedName + " but API returned " + retrievedName);

        String retrievedCity = (retrievedLocation.getCity() == null) ? "NULL" : retrievedLocation.getCity().trim();
        softAssert.assertEquals(retrievedCity, expectedCity,
                "Expected Customer ID: " + expectedCity + " but API returned " + retrievedCity);

        int retrievedCityID = (retrievedLocation.getCityId() == null) ? 0 : retrievedLocation.getCityId();
        softAssert.assertEquals(retrievedCityID, expectedCityID,
                "Expected Customer ID: " + expectedCityID + " but API returned "
                        + ((retrievedCityID == 0) ? "NULL" : ""));

        String retrievedState = (retrievedLocation.getState() == null) ? "NULL" : retrievedLocation.getState().trim();
        softAssert.assertEquals(retrievedState, expectedState,
                "Expected Customer ID: " + expectedState + " but API returned " + retrievedState);

        String retrievedPostalCode = (retrievedLocation.getPostalCode() == null) ? "NULL" : retrievedLocation.getPostalCode().trim();
        softAssert.assertEquals(retrievedPostalCode, expectedPostalCode,
                "Expected Customer ID: " + expectedPostalCode + " but API returned " + retrievedPostalCode);

        String retrievedAddressLine1 = (retrievedLocation.getAddressLine1() == null) ? "NULL" : retrievedLocation.getAddressLine1().trim();
        softAssert.assertEquals(retrievedAddressLine1, expectedAddressLine1,
                "Expected Customer ID: " + expectedAddressLine1 + " but API returned " + retrievedAddressLine1);

        Boolean retrievedApptRequired = (retrievedLocation.getApptRequired() == null) ? Boolean.valueOf("NULL") : retrievedLocation.getApptRequired();
        softAssert.assertEquals(retrievedApptRequired, expectedAppointmentRequired,
                "Expected Customer ID: " + expectedAppointmentRequired + " but API returned " + retrievedApptRequired);

        Boolean retrievedActive = (retrievedLocation.getActive() == null) ? Boolean.valueOf("NULL") : retrievedLocation.getActive();
        softAssert.assertEquals(retrievedActive, expectedActiveStatus,
                "Expected Customer ID: " + expectedActiveStatus + " but API returned " + retrievedActive);
    }

    @Test
    public void verify_customer_1514OCFL_is_InActive() throws Exception {
        String expectedLocationID = "1514OCFL";

        String reqPayload = "{\"locationCode\":\"" + expectedLocationID + "\",\"isActive\":\"N\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = null;
        for (LocationSearchListEntry l : retrievedLocationList) {
            try {
                if (l.getId().equals(expectedLocationID)) {
                    retrievedLocation = l;
                }
            } catch (Exception e) {
            }
        }
        if (retrievedLocation == null) {
            throw new Exception("Location ID: " + expectedLocationID + " not found in response. \n Response: \n"
                    + response.asPrettyString());
        }

        boolean retrievedActive;
        try {
            retrievedActive = retrievedLocation.getActive();
        } catch (Exception e) {
            throw new Exception("Active field is NULL for loc id: " + expectedLocationID + ". \n Response body: \n"
                    + response.asPrettyString());
        }

        Assert.assertFalse(retrievedActive,
                "Expected Customer-" + expectedLocationID + " Active state: false " + " but API returned " + retrievedActive);
    }

    @Test
    public void verify_invalid_locationID_returns_null() throws Exception {
        String reqPayload = "{\"locationCode\":\"7889\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_customerID_8600PLMO() throws Exception {
        String expectedLocID = "8600PLMO";
        String reqPayload = "{\"locationCode\":\"" + expectedLocID + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = retrievedLocationList.get(0);

        String retrievedLocID = retrievedLocation.getId();
        Assert.assertEquals(retrievedLocID, expectedLocID, "Expected location ID: " + expectedLocID + " but API returned location id: " +
                retrievedLocID);
    }

    @Test
    public void verify_user_search_by_partialLocationID_860_returns_LocationID_that_contains_860() throws Exception {
        String expectedPartialLocID = "860";
        String reqPayload = "{\"locationCode\":\"" + expectedPartialLocID + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            softAssert.assertTrue(c.getId().contains(expectedPartialLocID),
                    "Location Id: " + c.getId() + "retrieved does not contain pattern " + expectedPartialLocID);
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_locationName_returns_null() throws Exception {
        String reqPayload = "{\"locationName\":\"xyz\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_customerName_TOLMAR() throws Exception {
        String expectedLocName = "TOLMAR";
        String reqPayload = "{\"locationName\":\"" + expectedLocName + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = retrievedLocationList.get(0);

        String retrievedLocName = retrievedLocation.getName();
        Assert.assertEquals(retrievedLocName, expectedLocName, "Expected location name: " + expectedLocName + " but API returned location name: " +
                retrievedLocName);
    }

    @Test
    public void verify_user_search_by_partialLocationName_PERE_returns_LocationID_that_contains_PERE() throws Exception {
        String expectedPartialLocName = "PERE";
        String reqPayload = "{\"locationName\":\"" + expectedPartialLocName + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            softAssert.assertTrue(c.getName().toLowerCase().contains(expectedPartialLocName.toLowerCase()),
                    "Location Name: " + c.getName() + "retrieved does not contain pattern " + expectedPartialLocName);
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_location_address_601_Market_St() throws Exception {
        String expectedLocAddress = "601 Market St";
        String reqPayload = "{\"address\":\"" + expectedLocAddress + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = retrievedLocationList.get(0);

        String retrievedLocAddress = retrievedLocation.getAddressLine1();
        Assert.assertEquals(retrievedLocAddress, expectedLocAddress, "Expected location address: " + expectedLocAddress + " but API returned location address: " +
                retrievedLocAddress);
    }

    @Test
    public void verify_user_search_by_partialLocationAddress_5406_returns_LocationAddress_that_contains_5406() throws Exception {
        String expectedPartialLocAddress = "5406";
        String reqPayload = "{\"address\":\"" + expectedPartialLocAddress + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            softAssert.assertTrue(c.getAddressLine1().toLowerCase().contains(expectedPartialLocAddress.toLowerCase()),
                    "Location address: " + c.getName() + "retrieved does not contain pattern " + expectedPartialLocAddress);
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_locationAddress_returns_null() throws Exception {
        String reqPayload = "{\"address\":\"xyz\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_location_city_GRAPEVINE() throws Exception {
        String expectedLocCity = "GRAPEVINE";
        String reqPayload = "{\"city\":\"" + expectedLocCity + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocCity = c.getCity();
            Assert.assertEquals(retrievedLocCity, expectedLocCity, "Expected location city: " + expectedLocCity + " but API returned location city: " +
                    retrievedLocCity + " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_partialLocationCityName_GRAP_returns_Locations_that_contains_GRAP_in_cityName() throws Exception {
        String expectedPartialLocCity = "GRAP";
        String reqPayload = "{\"city\":\"" + expectedPartialLocCity + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocCityName = c.getCity();
            softAssert.assertTrue(retrievedLocCityName.toLowerCase().contains(expectedPartialLocCity.toLowerCase()),
                    "Location city name: " + retrievedLocCityName + "retrieved does not contain pattern " + expectedPartialLocCity +
                            " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_locationCityName_returns_null() throws Exception {
        String reqPayload = "{\"city\":\"xyz\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_location_postal_code_77803() throws Exception {
        String expectedLocPostalCode = "77803";
        String reqPayload = "{\"postalCode\":\"" + expectedLocPostalCode + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocPostalCode = c.getPostalCode();
            Assert.assertEquals(retrievedLocPostalCode, expectedLocPostalCode, "Expected location city: " + expectedLocPostalCode + " but API returned location city: " +
                    retrievedLocPostalCode + " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_partialLocationPostalCode_778_returns_Locations_that_contains_778_in_postCode() throws Exception {
        String expectedLocPostalCode = "778";
        String reqPayload = "{\"postalCode\":\"" + expectedLocPostalCode + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocPostalCode = c.getPostalCode();
            softAssert.assertTrue(retrievedLocPostalCode.toLowerCase().contains(expectedLocPostalCode.toLowerCase()),
                    "Location city name: " + retrievedLocPostalCode + "retrieved does not contain pattern " +
                            expectedLocPostalCode + " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_locationPostalCode_returns_null() throws Exception {
        String reqPayload = "{\"postalCode\":\"xyz\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_location_state_code_TX() throws Exception {
        String expectedLocStateCode = "TX";
        String reqPayload = "{\"state\":\"" + expectedLocStateCode + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocStateCode = c.getState();
            Assert.assertEquals(retrievedLocStateCode, expectedLocStateCode, "Expected location state: " + expectedLocStateCode + " but API returned location state: " +
                    retrievedLocStateCode + " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_partial_StateCode_A_returns_Locations_that_contains_A_in_StateCode() throws Exception {
        String expectedLocStateCode = "A";
        String reqPayload = "{\"state\":\"" + expectedLocStateCode + "\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or NULL \n\n Response payload: \n\n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            String retrievedLocStateCode = c.getState();
            softAssert.assertTrue(retrievedLocStateCode.toLowerCase().contains(expectedLocStateCode.toLowerCase()),
                    "Location state code: " + retrievedLocStateCode + "retrieved does not contain pattern " +
                            expectedLocStateCode + " for loc id: " + c.getId());
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_invalid_locationStateCode_returns_null() throws Exception {
        String reqPayload = "{\"state\":\"xyz\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_ALL_search_criteria() throws Exception {
        String reqPayload = "{\"locationName\":\"0022 CM BN TECH ESCORT\",\"locationCode\":\"0022ABMD\"," +
                "\"address\":\"5183 BLACKHWAK ROAD\",\"city\":\"ABER PROV GRD\"," +
                "\"postalCode\":\"21010\",\"state\":\"MD\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        LocationSearchListEntry retrievedLocation = retrievedLocationList.get(0);

        String expectedCustomerID = "0022ABMD";
        Assert.assertEquals(retrievedLocation.getId(), expectedCustomerID);
    }

    @Test
    public void verify_user_search_multiple_search_criteria_that_fetches_no_records() throws Exception {
        String reqPayload = "{\"locationName\":\"xyz\",\"locationCode\":" +
                "\"1000BRTX\",\"address\":\"1000 Independence Ave\",\"city\":\"BRYAN\"," +
                "\"postalCode\":\"77803\",\"state\":\"TX\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse<List<LocationSearchListEntry>> responsePayload = response.as(BusinessResponse.class);

        Assert.assertNull(responsePayload.getResponse(), "Expected NULL response but API returned - \n"
                + response.asPrettyString());
    }

    @Test
    public void verify_user_search_by_IsActive_Yes_returns_LocationID_that_are_active() throws Exception {
        String reqPayload = "{\"locationCode\":\"\",\"isActive\":\"Y\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            softAssert.assertTrue(c.getActive(),
                    "InActive Location Id: " + c.getId() + " returned by API");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_user_search_by_IsActive_No_returns_LocationID_that_are_InActive() throws Exception {
        String reqPayload = "{\"locationCode\":\"\",\"isActive\":\"N\"}";
        requestSpecification.queryParam("stopType", "").body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<LocationSearchListEntry> retrievedLocationList =
                parseResponseToObjectList(response, LocationSearchListEntry.class);

        if (retrievedLocationList.size() == 0) {
            throw new Exception(
                    "Unexpected!! response object is empty or null \n Response payload: \n" + response.asPrettyString());
        }

        SoftAssert softAssert = new SoftAssert();
        for (LocationSearchListEntry c : retrievedLocationList) {
            softAssert.assertFalse(c.getActive(),
                    "Active Location Id: " + c.getId() + " returned by API");
        }
        softAssert.assertAll();
    }
}

