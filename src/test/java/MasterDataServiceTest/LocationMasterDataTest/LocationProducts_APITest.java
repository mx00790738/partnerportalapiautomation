package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.LocationProductListEntry;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.HashMap;
import java.util.List;

public class LocationProducts_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_PRODUCTS_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_product_API_health_check() throws Exception {
        String reqPayload = "{}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="ProductListForLTLDetailsDataProvider")
    public void verify_LTL_details_of_the_retrieved_product(String productID, HashMap<String, String> expectedProductDetails) throws Exception {
        String reqPayload = "{}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        LocationProductListEntry retrievedRecord = null;
        for(LocationProductListEntry e:retrievedContactList) {
            if (e.getId().trim().equals(productID)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with product ID: ["+productID+"] not found");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_cubicUnits= (retrievedRecord.getCubicUnits() == null) ? "NULL" : retrievedRecord.getCubicUnits();
        softAssert.assertEquals(retrieved_cubicUnits,expectedProductDetails.get("cubicUnits"),
                "Expected cubicUnits: [" + expectedProductDetails.get("cubicUnits") + "] " +
                        "but API returned [" + retrieved_cubicUnits + "]");

        String retrieved_cubicUnitsIsPerPiece= (retrievedRecord.getCubicUnitsIsPerPiece() == null) ? "NULL" : retrievedRecord.getCubicUnitsIsPerPiece();
        softAssert.assertEquals(retrieved_cubicUnitsIsPerPiece, expectedProductDetails.get("cubicUnitsIsPerPiece"),
                "Expected cubicUnitsIsPerPiece: [" + expectedProductDetails.get("cubicUnitsIsPerPiece") + "] " +
                        "but API returned [" + retrieved_cubicUnitsIsPerPiece + "]");

        String retrieved_description= (retrievedRecord.getDescription()== null) ? "NULL" : retrievedRecord.getDescription();
        softAssert.assertEquals(retrieved_description, expectedProductDetails.get("description"),
                "Expected description: [" + expectedProductDetails.get("description") + "] " +
                        "but API returned [" + retrieved_description + "]");

        String retrieved_handlingUnits= (retrievedRecord.getHandlingUnits()== null) ? "NULL" : retrievedRecord.getHandlingUnits();
        softAssert.assertEquals(retrieved_handlingUnits, expectedProductDetails.get("handlingUnits"),
                "Expected handlingUnits: [" + expectedProductDetails.get("handlingUnits") + "] " +
                        "but API returned [" + retrieved_handlingUnits + "]");

        String retrieved_handlingUnitsIsPerPiece= (retrievedRecord.getHandlingUnitsIsPerPiece()== null) ? "NULL" : retrievedRecord.getHandlingUnitsIsPerPiece();
        softAssert.assertEquals(retrieved_handlingUnitsIsPerPiece, expectedProductDetails.get("handlingUnitsIsPerPiece"),
                "Expected handlingUnitsIsPerPiece: [" + expectedProductDetails.get("handlingUnitsIsPerPiece") + "] " +
                        "but API returned [" + retrieved_handlingUnitsIsPerPiece + "]");

        String retrieved_hazmat= (retrievedRecord.getHazmat()== null) ? "NULL" : retrievedRecord.getHazmat();
        softAssert.assertEquals(retrieved_hazmat, expectedProductDetails.get("hazmat"),
                "Expected hazmat: [" + expectedProductDetails.get("hazmat") + "] " +
                        "but API returned [" + retrieved_hazmat + "]");

        String retrieved_height= (retrievedRecord.getHeight()== null) ? "NULL" : retrievedRecord.getHeight();
        softAssert.assertEquals(retrieved_height, expectedProductDetails.get("height"),
                "Expected height: [" + expectedProductDetails.get("height") + "] " +
                        "but API returned [" + retrieved_height + "]");

        String retrieved_id= (retrievedRecord.getId()== null) ? "NULL" : retrievedRecord.getId();
        softAssert.assertEquals(retrieved_id, expectedProductDetails.get("id"),
                "Expected id: [" + expectedProductDetails.get("id") + "] " +
                        "but API returned [" + retrieved_id + "]");

        String retrieved_length= (retrievedRecord.getLength()== null) ? "NULL" : retrievedRecord.getLength();
        softAssert.assertEquals(retrieved_length, expectedProductDetails.get("length"),
                "Expected length: [" + expectedProductDetails.get("length") + "] " +
                        "but API returned [" + retrieved_length + "]");

        String retrieved_nmfcClassCode= (retrievedRecord.getNmfcClassCode()== null) ? "NULL" : retrievedRecord.getNmfcClassCode();
        softAssert.assertEquals(retrieved_nmfcClassCode, expectedProductDetails.get("nmfcClassCode"),
                "Expected nmfcClassCode: [" + expectedProductDetails.get("nmfcClassCode") + "] " +
                        "but API returned [" + retrieved_nmfcClassCode + "]");

        String retrieved_nmfcCode= (retrievedRecord.getNmfcCode()== null) ? "NULL" : retrievedRecord.getNmfcCode();
        softAssert.assertEquals(retrieved_nmfcCode, expectedProductDetails.get("nmfcCode"),
                "Expected nmfcCode: [" + expectedProductDetails.get("nmfcCode") + "] " +
                        "but API returned [" + retrieved_nmfcCode + "]");

        String retrieved_packagingTypeCode= (retrievedRecord.getPackagingTypeCode()== null) ? "NULL" : retrievedRecord.getPackagingTypeCode();
        softAssert.assertEquals(retrieved_packagingTypeCode, expectedProductDetails.get("packagingTypeCode"),
                "Expected packagingTypeCode: [" + expectedProductDetails.get("packagingTypeCode") + "] " +
                        "but API returned [" + retrieved_packagingTypeCode + "]");

        String retrieved_reqSpots= (retrievedRecord.getReqSpots()== null) ? "NULL" : retrievedRecord.getReqSpots();
        softAssert.assertEquals(retrieved_reqSpots, expectedProductDetails.get("reqSpots"),
                "Expected reqSpots: [" + expectedProductDetails.get("reqSpots") + "] " +
                        "but API returned [" + retrieved_reqSpots + "]");

        String retrieved_spotsIsPerPiece= (retrievedRecord.getSpotsIsPerPiece()== null) ? "NULL" : retrievedRecord.getSpotsIsPerPiece();
        softAssert.assertEquals(retrieved_spotsIsPerPiece, expectedProductDetails.get("spotsIsPerPiece"),
                "Expected spotsIsPerPiece: [" + expectedProductDetails.get("spotsIsPerPiece") + "] " +
                        "but API returned [" + retrieved_spotsIsPerPiece + "]");

        String retrieved_weight= (retrievedRecord.getWeight()== null) ? "NULL" : retrievedRecord.getWeight();
        softAssert.assertEquals(retrieved_weight, expectedProductDetails.get("weight"),
                "Expected weight: [" + expectedProductDetails.get("weight") + "] " +
                        "but API returned [" + retrieved_weight + "]");

        String retrieved_weightIsPerPiece= (retrievedRecord.getWeightIsPerPiece()== null) ? "NULL" : retrievedRecord.getWeightIsPerPiece();
        softAssert.assertEquals(retrieved_weightIsPerPiece, expectedProductDetails.get("weightIsPerPiece"),
                "Expected weightIsPerPiece: [" + expectedProductDetails.get("weightIsPerPiece") + "] " +
                        "but API returned [" + retrieved_weightIsPerPiece + "]");

        String retrieved_weightUomTypeCode= (retrievedRecord.getWeightUomTypeCode()== null) ? "NULL" : retrievedRecord.getWeightUomTypeCode();
        softAssert.assertEquals(retrieved_weightUomTypeCode, expectedProductDetails.get("weightUomTypeCode"),
                "Expected weightUomTypeCode: [" + expectedProductDetails.get("weightUomTypeCode") + "] " +
                        "but API returned [" + retrieved_weightUomTypeCode + "]");

        String retrieved_width= (retrievedRecord.getWidth()== null) ? "NULL" : retrievedRecord.getWidth();
        softAssert.assertEquals(retrieved_width, expectedProductDetails.get("width"),
                "Expected width: [" + expectedProductDetails.get("width") + "] " +
                        "but API returned [" + retrieved_width + "]");

        softAssert.assertAll();
    }

    @Test(dataProvider="ProductListForHAZMATDetailsDataProvider")
    public void verify_HAZMAT_details_of_the_retrieved_product(String productID, HashMap<String, String> expectedProductDetails) throws Exception {
        String reqPayload = "{}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        LocationProductListEntry retrievedRecord = null;
        for(LocationProductListEntry e:retrievedContactList) {
            if (e.getId().trim().equals(productID)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with product ID: ["+productID+"] not found");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrieved_hazmat= (retrievedRecord.getHazmat()== null) ? "NULL" : retrievedRecord.getHazmat();
        softAssert.assertEquals(retrieved_hazmat, expectedProductDetails.get("hazmat"),
                "Expected hazmat: [" + expectedProductDetails.get("hazmat") + "] " +
                        "but API returned [" + retrieved_hazmat + "]");

        String retrieved_hazmatClassCode= (retrievedRecord.getHazmatClassCode()== null) ? "NULL" : retrievedRecord.getHazmatClassCode();
        softAssert.assertEquals(retrieved_hazmatClassCode, expectedProductDetails.get("hazmatClassCode"),
                "Expected hazmatClassCode: [" + expectedProductDetails.get("hazmatClassCode") + "] " +
                        "but API returned [" + retrieved_hazmatClassCode + "]");

        String retrieved_hazmatContactName= (retrievedRecord.getHazmatContactName()== null) ? "NULL" : retrievedRecord.getHazmatContactName();
        softAssert.assertEquals(retrieved_hazmatContactName, expectedProductDetails.get("hazmatContactName"),
                "Expected hazmatContactName: [" + expectedProductDetails.get("hazmatContactName") + "] " +
                        "but API returned [" + retrieved_hazmatContactName + "]");

        String retrieved_hazmatContactNumber= (retrievedRecord.getHazmatContactNumber()== null) ? "NULL" : retrievedRecord.getHazmatContactNumber();
        softAssert.assertEquals(retrieved_hazmatContactNumber, expectedProductDetails.get("hazmatContactNumber"),
                "Expected hazmatContactNumber: [" + expectedProductDetails.get("hazmatContactNumber") + "] " +
                        "but API returned [" + retrieved_hazmatContactNumber + "]");

        String retrieved_hazmatPackageGroup= (retrievedRecord.getHazmatPackageGroup()== null) ? "NULL" : retrievedRecord.getHazmatPackageGroup();
        softAssert.assertEquals(retrieved_hazmatPackageGroup, expectedProductDetails.get("hazmatPackageGroup"),
                "Expected hazmatPackageGroup: [" + expectedProductDetails.get("hazmatPackageGroup") + "] " +
                        "but API returned [" + retrieved_hazmatPackageGroup + "]");

        String retrieved_hazmatShipname= (retrievedRecord.getHazmatShipname()== null) ? "NULL" : retrievedRecord.getHazmatShipname();
        softAssert.assertEquals(retrieved_hazmatShipname, expectedProductDetails.get("hazmatShipname"),
                "Expected hazmatShipname: [" + expectedProductDetails.get("hazmatShipname") + "] " +
                        "but API returned [" + retrieved_hazmatShipname + "]");

        String retrieved_hazmatUNNbr= (retrievedRecord.getHazmatUNNbr()== null) ? "NULL" : retrievedRecord.getHazmatUNNbr();
        softAssert.assertEquals(retrieved_hazmatUNNbr, expectedProductDetails.get("hazmatUNNbr"),
                "Expected hazmatUNNbr: [" + expectedProductDetails.get("hazmatUNNbr") + "] " +
                        "but API returned [" + retrieved_hazmatUNNbr + "]");

        softAssert.assertAll();
    }

    @Test
    public void verify_empty_list_is_retrieved_for_invalid_search_criteria() throws Exception {
        String reqPayload = "{\"itemId\":\"xyz\",\"itemDescription\":\"xyz\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        int retrievedListSize = retrievedContactList.size();
        Assert.assertEquals(retrievedListSize, 0,
                "Empty product list expected but API returned product list size ["+retrievedListSize+"]");
    }

    @Test
    public void verify_single_record_is_retrieved_when_user_search_by_itemID_CHARCOAL() throws Exception {
        String reqPayload = "{\"itemId\":\"CHARCOAL\",\"itemDescription\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        int retrievedListSize = retrievedContactList.size();
        Assert.assertEquals(retrievedListSize, 1,
                "Product list size expected [1] but API returned list size ["+retrievedListSize+"]");
    }

    @Test
    public void verify_single_record_is_retrieved_when_user_search_by_itemDescription_POLYMERS() throws Exception {
        String reqPayload = "{\"itemId\":\"\",\"itemDescription\":\"POLYMERS\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        int retrievedListSize = retrievedContactList.size();
        Assert.assertEquals(retrievedListSize, 1,
                "Product list size expected [1] but API returned list size ["+retrievedListSize+"]");
    }

    @Test(groups = { "Sanity" })
    public void verify_retrieved_itemID_is_ATV_when_user_search_by_itemID_ATV() throws Exception {
        String expectedItemID = "ATV";
        String reqPayload = "{\"itemId\":\""+expectedItemID+"\",\"itemDescription\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST_DEBUG(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        LocationProductListEntry retrievedRecord = null;
        for(LocationProductListEntry e:retrievedContactList) {
            if (e.getId().trim().equals(expectedItemID)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with product ID: ["+expectedItemID+"] not found");
        }

        String retrievedItemID = retrievedRecord.getId().trim();
        Assert.assertEquals(retrievedItemID, expectedItemID,
                "Product itemId expected ["+expectedItemID+"] but API returned itemId ["+retrievedItemID+"]");
    }

    @Test(groups = { "Sanity" })
    public void verify_retrieved_itemDescription_is_TIRES_when_user_search_by_itemDescription_TIRES() throws Exception {
        String expectedItemDesc = "TIRES";
        String reqPayload = "{\"itemId\":\"\",\"itemDescription\":\""+expectedItemDesc+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        LocationProductListEntry retrievedRecord = null;
        for(LocationProductListEntry e:retrievedContactList) {
            if (e.getId().trim().equals(expectedItemDesc)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with product ID: ["+expectedItemDesc+"] not found");
        }

        String retrievedItemDesc = retrievedRecord.getId().trim();
        Assert.assertEquals(retrievedItemDesc, expectedItemDesc,
                "Product itemDescription expected ["+expectedItemDesc+"] but API returned itemDescription ["+retrievedItemDesc+"]");
    }

    @Test
    public void verify_retrieved_itemId_contains_CAR_when_user_search_by_itemId_CAR() throws Exception {
        String expectedItemID = "CAR";
        String reqPayload = "{\"itemId\":\""+expectedItemID+"\",\"itemDescription\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        if(retrievedContactList.isEmpty()){
            throw new Exception("Unexpected!! Empty product list is returned");
        }
        SoftAssert softAssert = new SoftAssert();
        for(int i = 0; i < retrievedContactList.size(); i++ ) {
            String retrievedID = retrievedContactList.get(i).getId();
            softAssert.assertTrue(retrievedID.contains(expectedItemID), "Product record at index ["+i+"] does not contain [" +
                    expectedItemID +"] in the itemID ["+retrievedID+"] retrieved");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_retrieved_itemId_contains_car_when_user_search_by_itemId_car() throws Exception {
        String expectedItemID = "car";
        String reqPayload = "{\"itemId\":\""+expectedItemID+"\",\"itemDescription\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        if(retrievedContactList.isEmpty()){
            throw new Exception("Unexpected!! Empty product list is returned");
        }
        SoftAssert softAssert = new SoftAssert();
        for(int i = 0; i < retrievedContactList.size(); i++ ) {
            String retrievedID = retrievedContactList.get(i).getId().toLowerCase();
            softAssert.assertTrue(retrievedID.contains(expectedItemID), "Product record at index ["+i+"] does not contain [" +
                    expectedItemID +"] in the itemID ["+retrievedID+"] retrieved");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_retrieved_itemDescription_contains_CHEM_when_user_search_by_itemDescription_CHEM() throws Exception {
        String expectedItemDesc = "CHEM";
        String reqPayload = "{\"itemId\":\"\",\"itemDescription\":\""+expectedItemDesc+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        if(retrievedContactList.isEmpty()){
            throw new Exception("Unexpected!! Empty product list is returned");
        }
        SoftAssert softAssert = new SoftAssert();
        for(int i = 0; i < retrievedContactList.size(); i++ ) {
            String retrievedDesc = retrievedContactList.get(i).getDescription();
            softAssert.assertTrue(retrievedDesc.contains(expectedItemDesc), "Product record at index ["+i+"] does not contain [" +
                    expectedItemDesc +"] in the itemID ["+retrievedDesc+"] retrieved");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_retrieved_itemDescription_contains_chem_when_user_search_by_itemDescription_chem() throws Exception {
        String expectedItemDesc = "chem";
        String reqPayload = "{\"itemId\":\"\",\"itemDescription\":\""+expectedItemDesc+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<LocationProductListEntry> retrievedContactList =
                parseResponseToObjectList(response, LocationProductListEntry.class);

        if(retrievedContactList.isEmpty()){
            throw new Exception("Unexpected!! Empty product list is returned");
        }
        SoftAssert softAssert = new SoftAssert();
        for(int i = 0; i < retrievedContactList.size(); i++ ) {
            String retrievedDesc = retrievedContactList.get(i).getDescription().toLowerCase();
            softAssert.assertTrue(retrievedDesc.contains(expectedItemDesc), "Product record at index ["+i+"] does not contain [" +
                    expectedItemDesc +"] in the itemID ["+retrievedDesc+"] retrieved");
        }
        softAssert.assertAll();
    }

    @DataProvider(name="ProductListForLTLDetailsDataProvider")
    public Object[][] getProductListForLTLDetailsValidation(){
        HashMap<String, String> KAYAK = new HashMap<String, String>();
        KAYAK.put("cubicUnits","20.0");
        KAYAK.put("cubicUnitsIsPerPiece","N");
        KAYAK.put("description","KAYAKS");
        KAYAK.put("handlingUnits","2");
        KAYAK.put("handlingUnitsIsPerPiece","N");
        KAYAK.put("hazmat","N");
        KAYAK.put("height","5.0");
        KAYAK.put("id","KAYAK    ");
        KAYAK.put("length","50.0");
        KAYAK.put("nmfcClassCode","250     ");
        KAYAK.put("nmfcCode","IUHJG 456");
        KAYAK.put("packagingTypeCode","PIEC        ");
        KAYAK.put("reqSpots","1.00");
        KAYAK.put("spotsIsPerPiece","N");
        KAYAK.put("weight","200.0");
        KAYAK.put("weightIsPerPiece","N");
        KAYAK.put("weightUomTypeCode","LBS ");
        KAYAK.put("width","10.0");

        HashMap<String, String> MACHSAWS = new HashMap<String, String>();
        MACHSAWS.put("cubicUnits","200.0");
        MACHSAWS.put("cubicUnitsIsPerPiece","N");
        MACHSAWS.put("description","MACHINERY SAWS/ROUTERS");
        MACHSAWS.put("handlingUnits","9");
        MACHSAWS.put("handlingUnitsIsPerPiece","N");
        MACHSAWS.put("hazmat","N");
        MACHSAWS.put("height","5.0");
        MACHSAWS.put("id","MACHSAWS ");
        MACHSAWS.put("length","10.0");
        MACHSAWS.put("nmfcClassCode","200     ");
        MACHSAWS.put("nmfcCode","YUHJK");
        MACHSAWS.put("packagingTypeCode","BOX         ");
        MACHSAWS.put("reqSpots","3.00");
        MACHSAWS.put("spotsIsPerPiece","N");
        MACHSAWS.put("weight","100.0");
        MACHSAWS.put("weightIsPerPiece","N");
        MACHSAWS.put("weightUomTypeCode","LBS ");
        MACHSAWS.put("width","8.0");

        HashMap<String, String> TRUCKPART = new HashMap<String, String>();
        TRUCKPART.put("cubicUnits","20.0");
        TRUCKPART.put("cubicUnitsIsPerPiece","Y");
        TRUCKPART.put("description","TRUCK PARTS");
        TRUCKPART.put("handlingUnits","2");
        TRUCKPART.put("handlingUnitsIsPerPiece","Y");
        TRUCKPART.put("hazmat","N");
        TRUCKPART.put("height","30.0");
        TRUCKPART.put("id","TRUCKPART");
        TRUCKPART.put("length","10.0");
        TRUCKPART.put("nmfcClassCode","85      ");
        TRUCKPART.put("nmfcCode","6577ty");
        TRUCKPART.put("packagingTypeCode","CRAT        ");
        TRUCKPART.put("reqSpots","1.00");
        TRUCKPART.put("spotsIsPerPiece","Y");
        TRUCKPART.put("weight","500.0");
        TRUCKPART.put("weightIsPerPiece","Y");
        TRUCKPART.put("weightUomTypeCode","LBS ");
        TRUCKPART.put("width","20.0");

        return new Object[][]
                {
                        {"KAYAK",KAYAK},
                        {"MACHSAWS",MACHSAWS},
                        {"TRUCKPART",TRUCKPART}
                };
    }

    @DataProvider(name="ProductListForHAZMATDetailsDataProvider")
    public Object[][] getProductListForHAZMATDetailsValidation(){
        HashMap<String, String> HAZMAT = new HashMap<String, String>();
        HAZMAT.put("hazmat","Y");
        HAZMAT.put("hazmatClassCode","3    ");
        HAZMAT.put("hazmatContactName","DAVID");
        HAZMAT.put("hazmatContactNumber","809-754-335         ");
        HAZMAT.put("hazmatPackageGroup","II  ");
        HAZMAT.put("hazmatShipname","1,1-Dichloroethane");
        HAZMAT.put("hazmatUNNbr","UN2362");

        HashMap<String, String> HAZTOTE = new HashMap<String, String>();
        HAZTOTE.put("hazmat","Y");
        HAZTOTE.put("hazmatClassCode","3    ");
        HAZTOTE.put("hazmatContactName","MAIN");
        HAZTOTE.put("hazmatContactNumber","679-234-5673        ");
        HAZTOTE.put("hazmatPackageGroup","III ");
        HAZTOTE.put("hazmatShipname","1,2-Epoxy-3-ethoxypropane");
        HAZTOTE.put("hazmatUNNbr","UN2752");

        return new Object[][]
                {
                        {"HAZMAT",HAZMAT},
                        {"HAZTOTE",HAZTOTE}
                };
    }
}
