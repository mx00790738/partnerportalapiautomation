package MasterDataServiceTest.LocationMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class Location_Timezones_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_TIMEZONES_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    //TODO ADD NEGATIVE CASES LIKE INVALID CITY, STATE, ZIP

    @Test(groups = { "HealthCheck"})
    public void LocationTimezones_API_health_check() throws Exception {
        String reqPayload = "{\"city\":\"FORT STEWART\",\"state\":\"GA\",\"zip\":\"31314\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_EDT_timezone_is_returned_for_city_FORT_STEWART() throws Exception {
        String expectedTimezone = "EDT";
        String city = "FORT STEWART";
        String state = "GA";
        String zip = "31314";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_CDT_timezone_is_returned_for_ABBEVILLE() throws Exception {
        String expectedTimezone = "CDT";
        String city = "ABBEVILLE";
        String state = "AL";
        String zip = "36310";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_CST_timezone_is_returned_for_STEINBACH() throws Exception {
        String expectedTimezone = "CST";
        String city = "STEINBACH";
        String state = "MB";
        String zip = "R5G 1S3";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_MST_timezone_is_returned_for_ALPINE() throws Exception {
        String expectedTimezone = "MST";
        String city = "ALPINE";
        String state = "AZ";
        String zip = "85920";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_ADT_timezone_is_returned_for_AGUADILLA() throws Exception {
        String expectedTimezone = "ADT";
        String city = "AGUADILLA";
        String state = "PR";
        String zip = "00605";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_HDT_timezone_is_returned_for_WAILEA() throws Exception {
        String expectedTimezone = "HDT";
        String city = "WAILEA";
        String state = "HI";
        String zip = "96753";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_HST_timezone_is_returned_for_ANAHOLA() throws Exception {
        String expectedTimezone = "HST";
        String city = "ANAHOLA";
        String state = "HI";
        String zip = "96703";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_YDT_timezone_is_returned_for_CORDOVA() throws Exception {
        String expectedTimezone = "YDT";
        String city = "CORDOVA";
        String state = "AK";
        String zip = "99574";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_EST_timezone_is_returned_for_BRAMPTON() throws Exception {
        String expectedTimezone = "EST";
        String city = "BRAMPTON";
        String state = "ON";
        String zip = "L6Y 1G2";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_MDT_timezone_is_returned_for_CREEDE() throws Exception {
        String expectedTimezone = "MDT";
        String city = "CREEDE";
        String state = "CO";
        String zip = "81130";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_AST_timezone_is_returned_for_GLASSVILLE() throws Exception {
        String expectedTimezone = "AST";
        String city = "GLASSVILLE";
        String state = "NB";
        String zip = "E7L 1T5";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }

    @Test
    public void verify_PDT_timezone_is_returned_for_ELVERTA() throws Exception {
        String expectedTimezone = "PDT";
        String city = "ELVERTA";
        String state = "CA";
        String zip = "95626";

        String reqPayload = "{\"city\":\""+city+"\",\"state\":\""+state+"\",\"zip\":\""+zip+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedTimezone = response.jsonPath().getString("response");

        Assert.assertEquals(retrievedTimezone, expectedTimezone,
                "Expected timezone ["+expectedTimezone+"] but API returned timezone ["+retrievedTimezone+"]");
    }
}
