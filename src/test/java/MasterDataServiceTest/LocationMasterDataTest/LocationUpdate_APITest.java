package MasterDataServiceTest.LocationMasterDataTest;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class LocationUpdate_APITest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("LOCATION_UPDATE_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void location_update_API_health_check() throws Exception {
        //This is temp solution. TODO - Create payload from java obj
        String reqPayload = "{\"name\":\"TestName\",\"locationId\":\"TESTCHIL\",\"googleId\":null,\"pickupInstruction\":null," +
                "\"dropInstruction\":null,\"state\":\"IL\",\"country\":\"USA\",\"city\":\"CHICAGO\",\"postalCode\":\"60701\"," +
                "\"addressLine1\":\"TestName\",\"addressLine2\":null,\"apptRequired\":false," +
                "\"locQualifierMap\":{\"TrailerPool\":false,\"TrailerWash\":false,\"Customer\":false,\"Steamship\":false,\"CustomsBroker\":false,\"Terminal\":false,\"OutsideTerminal\":false,\"Consignee\":false,\"Shipper\":false,\"Prospect\":false,\"Truckstop\":false,\"DropYard\":false,\"DistCenter\":false,\"Geocoded\":false}," +
                "\"dayOpenCloseArr\":[{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null},{\"startTime\":null,\"closeTime\":null}]," +
                "\"dayOpenCloseMap\":{\"Monday\":{\"startTime\":null,\"closeTime\":null},\"Tuesday\":{\"startTime\":null,\"closeTime\":null},\"Wednesday\":{\"startTime\":null,\"closeTime\":null},\"Thursday\":{\"startTime\":null,\"closeTime\":null},\"Friday\":{\"startTime\":null,\"closeTime\":null},\"Saturday\":{\"startTime\":null,\"closeTime\":null},\"Sunday\":{\"startTime\":null,\"closeTime\":null}}," +
                "\"directions\":null,\"comments\":null,\"driverLoadId\":\"N\",\"driverUnloadId\":\"N\"," +
                "\"loadingTime\":null,\"unloadingTime\":null,\"contact\":[],\"contactOpType\":\"update\"," +
                "\"id\":\"TESTCHIL\",\"active\":true}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }
}
