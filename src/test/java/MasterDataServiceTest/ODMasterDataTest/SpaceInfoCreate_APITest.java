package MasterDataServiceTest.ODMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import static io.restassured.RestAssured.given;

public class SpaceInfoCreate_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("OD_SPACING_INFO_CREATE_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void space_info_create_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\n" +
                "\"companyId\": \"T100\",\n" +
                "\"tractorId\": \"104027\",\n" +
                "\"trailerId\": \"306881\",\n" +
                "\"tractorAxleSpacing1\": 2.5,\n" +
                "\"tractorAxleSpacing2\": 2.5,\n" +
                "\"tractorAxleSpacing3\": 2.5,\n" +
                "\"tractorAxleSpacing4\": 2.5,\n" +
                "\"trailerAxleSpacing1\": 2.5,\n" +
                "\"trailerAxleSpacing2\": 2.5,\n" +
                "\"trailerAxleSpacing3\": 2.5,\n" +
                "\"trailerAxleSpacing4\": 2.5,\n" +
                "\"trailerAxleSpacing5\": 2.5,\n" +
                "\"lastModified\": \"2021-10-18T21:44:07.000+0000\",\n" +
                "\"lastOrderId\": \"0325522\"\n" +
                "}";

        requestSpecification.body(reqPayload);

        Response response = given(requestSpecification).when().post();

        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 406,
                "Expected code: 406 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "FAILURE",
                "Expected status: FAILURE in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "FAILURE",
                    "Expected responseDesc: FAILURE in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_FAILURE_response_is_received_when_using_duplicate_companyID_and_Tractor() throws Exception {
        String expectedResponseMsg = "Same Combination of TrailerId and TractorId Exist";
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\n" +
                "\"companyId\": \"T100\",\n" +
                "\"tractorId\": \"104027\",\n" +
                "\"trailerId\": \"306881\",\n" +
                "\"tractorAxleSpacing1\": 2.5,\n" +
                "\"tractorAxleSpacing2\": 2.5,\n" +
                "\"tractorAxleSpacing3\": 2.5,\n" +
                "\"tractorAxleSpacing4\": 2.5,\n" +
                "\"trailerAxleSpacing1\": 2.5,\n" +
                "\"trailerAxleSpacing2\": 2.5,\n" +
                "\"trailerAxleSpacing3\": 2.5,\n" +
                "\"trailerAxleSpacing4\": 2.5,\n" +
                "\"trailerAxleSpacing5\": 2.5,\n" +
                "\"lastModified\": \"2021-10-18T21:44:07.000+0000\",\n" +
                "\"lastOrderId\": \"0325522\"\n" +
                "}";

        requestSpecification.body(reqPayload);

        Response response = given(requestSpecification).when().post();

        String retrievedResponseMsg = response.jsonPath().get("response");
        Assert.assertEquals(retrievedResponseMsg, expectedResponseMsg,
                "Expected response msg ["+expectedResponseMsg+"] but API returned ["+retrievedResponseMsg+"]");
    }
}
