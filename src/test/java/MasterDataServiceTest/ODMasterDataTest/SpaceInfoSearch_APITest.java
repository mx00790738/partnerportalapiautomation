package MasterDataServiceTest.ODMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.OD.SpaceInfo;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

import static io.restassured.RestAssured.given;

public class SpaceInfoSearch_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("OD_SPACING_INFO_SEARCH_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void space_info_search_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.queryParam("tractorId","104027");
        requestSpecification.queryParam("trailerId","306881");

        Response response = RequestMethod.GET(requestSpecification);

        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    //@Test(groups = {"Sanity"})
    @Test
    public void verify_space_info_for_tractorId_104027_and_trailerId_306881() throws Exception {
        String expected_companyId = "T100";
        String expected_tractorId = "104027";
        String expected_trailerId = "306881";
        String expected_tractorAxleSpacing1 = "2.6";
        String expected_tractorAxleSpacing2 = "2.5";
        String expected_tractorAxleSpacing3 = "2.5";
        String expected_tractorAxleSpacing4 = "2.5";
        String expected_trailerAxleSpacing1 = "2.5";
        String expected_trailerAxleSpacing2 = "2.5";
        String expected_trailerAxleSpacing3 = "2.5";
        String expected_trailerAxleSpacing4 = "2.5";
        String expected_trailerAxleSpacing5 = "2.5";

        RequestSpecification requestSpecification = this.requestSpecification;
        requestSpecification.queryParam("tractorId", "104027");
        requestSpecification.queryParam("trailerId", "306881");

        Response response = RequestMethod.GET(requestSpecification);

        List<SpaceInfo> spaceInfoList = parseResponseToObjectList(response, SpaceInfo.class);
        SpaceInfo retrievedSpaceInfo = spaceInfoList.get(0);

        SoftAssert softAssert = new SoftAssert();

        String retrievedTractorID = (retrievedSpaceInfo.getTractorId() == null) ? "NULL" : retrievedSpaceInfo.getTractorId();
        softAssert.assertEquals(retrievedTractorID,expected_tractorId,
                "Expected tractor ID ["+expected_tractorId+"] but API returned ["+retrievedTractorID+"]");

        String retrievedTrailerID = (retrievedSpaceInfo.getTrailerId() == null) ? "NULL" : retrievedSpaceInfo.getTrailerId();
        softAssert.assertEquals(retrievedTrailerID,expected_trailerId,
                "Expected trailer ID ["+expected_trailerId+"] but API returned ["+retrievedTrailerID+"]");

        String retrievedCompanyID = (retrievedSpaceInfo.getCompanyId() == null) ? "NULL" : retrievedSpaceInfo.getCompanyId();
        softAssert.assertEquals(retrievedCompanyID,expected_companyId,
                "Expected company ID ["+expected_companyId+"] but API returned ["+retrievedCompanyID+"]");

        String retrievedTractorAxleSpacing1 = (retrievedSpaceInfo.getTractorAxleSpacing1() == null) ? "NULL" :
                retrievedSpaceInfo.getTractorAxleSpacing1().toString();
        softAssert.assertEquals(retrievedTractorAxleSpacing1,expected_tractorAxleSpacing1,
                "Expected tractorAxleSpacing1 ["+expected_tractorAxleSpacing1+"] but API returned ["+retrievedTractorAxleSpacing1+"]");

        String retrievedTractorAxleSpacing2 = (retrievedSpaceInfo.getTractorAxleSpacing2() == null) ? "NULL" :
                retrievedSpaceInfo.getTractorAxleSpacing2().toString();
        softAssert.assertEquals(retrievedTractorAxleSpacing2,expected_tractorAxleSpacing2,
                "Expected tractorAxleSpacing2 ["+expected_tractorAxleSpacing2+"] but API returned ["+retrievedTractorAxleSpacing2+"]");

        String retrievedTractorAxleSpacing3 = (retrievedSpaceInfo.getTractorAxleSpacing3() == null) ? "NULL" :
                retrievedSpaceInfo.getTractorAxleSpacing3().toString();
        softAssert.assertEquals(retrievedTractorAxleSpacing3,expected_tractorAxleSpacing3,
                "Expected tractorAxleSpacing3 ["+expected_tractorAxleSpacing3+"] but API returned ["+retrievedTractorAxleSpacing3+"]");

        String retrievedTractorAxleSpacing4 = (retrievedSpaceInfo.getTractorAxleSpacing4() == null) ? "NULL" :
                retrievedSpaceInfo.getTractorAxleSpacing4().toString();
        softAssert.assertEquals(retrievedTractorAxleSpacing4,expected_tractorAxleSpacing4,
                "Expected tractorAxleSpacing4 ["+expected_tractorAxleSpacing4+"] but API returned ["+retrievedTractorAxleSpacing4+"]");

        String retrievedTrailerAxleSpacing1 = (retrievedSpaceInfo.getTrailerAxleSpacing1() == null) ? "NULL" :
                retrievedSpaceInfo.getTrailerAxleSpacing1().toString();
        softAssert.assertEquals(retrievedTrailerAxleSpacing1,expected_trailerAxleSpacing1,
                "Expected trailerAxleSpacing1 ["+expected_trailerAxleSpacing1+"] but API returned ["+retrievedTrailerAxleSpacing1+"]");

        String retrievedTrailerAxleSpacing2 = (retrievedSpaceInfo.getTrailerAxleSpacing2() == null) ? "NULL" :
                retrievedSpaceInfo.getTrailerAxleSpacing2().toString();
        softAssert.assertEquals(retrievedTrailerAxleSpacing2,expected_trailerAxleSpacing2,
                "Expected trailerAxleSpacing2 ["+expected_trailerAxleSpacing2+"] but API returned ["+retrievedTrailerAxleSpacing2+"]");

        String retrievedTrailerAxleSpacing3 = (retrievedSpaceInfo.getTrailerAxleSpacing3() == null) ? "NULL" :
                retrievedSpaceInfo.getTrailerAxleSpacing3().toString();
        softAssert.assertEquals(retrievedTrailerAxleSpacing3,expected_trailerAxleSpacing3,
                "Expected trailerAxleSpacing3 ["+expected_trailerAxleSpacing3+"] but API returned ["+retrievedTrailerAxleSpacing3+"]");

        String retrievedTrailerAxleSpacing4 = (retrievedSpaceInfo.getTrailerAxleSpacing4() == null) ? "NULL" :
                retrievedSpaceInfo.getTrailerAxleSpacing4().toString();
        softAssert.assertEquals(retrievedTrailerAxleSpacing4,expected_trailerAxleSpacing4,
                "Expected trailerAxleSpacing4 ["+expected_trailerAxleSpacing4+"] but API returned ["+retrievedTrailerAxleSpacing4+"]");

        String retrievedTrailerAxleSpacing5 = (retrievedSpaceInfo.getTrailerAxleSpacing5() == null) ? "NULL" :
                retrievedSpaceInfo.getTrailerAxleSpacing5().toString();
        softAssert.assertEquals(retrievedTrailerAxleSpacing5,expected_trailerAxleSpacing5,
                "Expected trailerAxleSpacing5 ["+expected_trailerAxleSpacing5+"] but API returned ["+retrievedTrailerAxleSpacing5+"]");

        softAssert.assertAll();
    }
}
