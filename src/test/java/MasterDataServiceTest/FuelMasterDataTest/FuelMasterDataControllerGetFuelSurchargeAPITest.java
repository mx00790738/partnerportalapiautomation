/**
 * 
 */
package MasterDataServiceTest.FuelMasterDataTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import BaseTest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.CustomerFuelSurcharge;

/**
 * @author Manoj
 *
 */
public class FuelMasterDataControllerGetFuelSurchargeAPITest extends BaseTest {

	private RequestSpecification requestSpecification;

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL"))
				.header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
						TestConfig.get("PASSWORD")))
				.headers(ApiHeaders.getRequestHeaders());
	}

	@Test(groups = {"HealthCheck"})
	public void get_customer_fuelsurcharge_endpoint_healthcheck() throws Exception {
		RequestSpecification requestSpecification = this.requestSpecification;
		String path = TestConfig.get("FUEL_MASTER_DATA_GET_FUEL_SURCHARGHE_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1002694"));

		Response response = RequestMethod.GET(requestSpecification);
		BusinessResponse responsePayload = response.as(BusinessResponse.class);
		//CustomerGetFuelSurchargeAPIBussinessResponse responsePayload = response.as(CustomerGetFuelSurchargeAPIBussinessResponse.class);

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(response.getStatusCode(), 200,
				"Expected status code: 200 instead API returned status code: " + response.getStatusCode());

		softAssert.assertEquals(responsePayload.getCode(), 200,
				"Expected code: 200 in response payload instead API returned status code: "
						+ responsePayload.getCode());

		String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
		softAssert.assertEquals(status, "SUCCESS",
				"Expected status: SUCCESS in response payload instead API returned status: "
						+ responsePayload.getStatus());

		ResponseStatus responseStatus = responsePayload.getResponseStatus();
		if (responseStatus != null) {
			String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseCode();
			softAssert.assertEquals(responseCode, "000",
					"Expected responseCode: 000 in response payload instead API returned responseCode: "
							+ responsePayload.getResponseStatus().getResponseCode());
			String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
					responsePayload.getResponseStatus().getResponseDesc();
			softAssert.assertEquals(responseDesc, "SUCCESS",
					"Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
							+ responsePayload.getResponseStatus().getResponseDesc());
		} else {
			softAssert.assertNotNull(null, "Response status obj is returned NULL");
		}
		softAssert.assertAll();

	}

	@Test
	public void Verify_get_fuelsurcharge_api_retrieve_all_type_of_fuelsurcharges() throws Exception {
		RequestSpecification requestSpecification = this.requestSpecification;
		String path = TestConfig.get("FUEL_MASTER_DATA_GET_FUEL_SURCHARGHE_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1000119"));

		Response response = RequestMethod.GET(requestSpecification);

		List<CustomerFuelSurcharge> customerFuelSurchargeList;
		try {
			customerFuelSurchargeList = response.jsonPath().getList("response", CustomerFuelSurcharge.class);
		} catch (Exception e) {
			throw new Exception(
					"Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
		}
		if (customerFuelSurchargeList.size() == 0) {
			throw new Exception(
					"Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
		}

		ArrayList<String> expectedFuelSurchargeCodes = new ArrayList<String>();
		expectedFuelSurchargeCodes.add("BTF");
		expectedFuelSurchargeCodes.add("DFS");
		expectedFuelSurchargeCodes.add("FFS");
		expectedFuelSurchargeCodes.add("FP01");
		expectedFuelSurchargeCodes.add("FP02");
		expectedFuelSurchargeCodes.add("FP03");
		expectedFuelSurchargeCodes.add("FP04");
		expectedFuelSurchargeCodes.add("FP1B");
		expectedFuelSurchargeCodes.add("FP1C");
		expectedFuelSurchargeCodes.add("FPCA");
		expectedFuelSurchargeCodes.add("FXFS");
		expectedFuelSurchargeCodes.add("MMF");
		expectedFuelSurchargeCodes.add("PFS");
		expectedFuelSurchargeCodes.add("RFS");
		expectedFuelSurchargeCodes.add("WEF");
		expectedFuelSurchargeCodes.add("YRCF");
		Collections.sort(expectedFuelSurchargeCodes);

		ArrayList<String> retrivedFuelSurchargeCodes = new ArrayList<String>();
		for (CustomerFuelSurcharge s : customerFuelSurchargeList) {
			retrivedFuelSurchargeCodes.add(s.getFuelSurchargeId());
		}
		Collections.sort(retrivedFuelSurchargeCodes);

		SoftAssert softAssert = new SoftAssert();
		for (String s : expectedFuelSurchargeCodes) {
			softAssert.assertTrue(retrivedFuelSurchargeCodes.contains(s),
					"FuelSurchargeCodes type: " + s + " is not retrived by the API");
		}
		softAssert.assertAll();
	}

	@Test(dataProvider = "fuelSurcharge", groups = {"Sanity"})
	public void Verify_fuelsurcharge_data_retrieved_for_customer_1002694(String fuelSurchargeType,
			HashMap<String, Object> expectedData) throws Exception {
		RequestSpecification requestSpecification = this.requestSpecification;
		String path = TestConfig.get("FUEL_MASTER_DATA_GET_FUEL_SURCHARGHE_API_PATH");
		requestSpecification = requestSpecification.basePath(path.replace("{id}", "1002694"));

		Response response = RequestMethod.GET(requestSpecification);

		List<CustomerFuelSurcharge> customerFuelSurchargeList;
		try {
			customerFuelSurchargeList = response.jsonPath().getList("response", CustomerFuelSurcharge.class);
		} catch (Exception e) {
			throw new Exception(
					"Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
		}
		if (customerFuelSurchargeList.size() == 0) {
			throw new Exception(
					"Unexpected!! response object is empty \n Response payload: \n" + response.asPrettyString());
		}

		CustomerFuelSurcharge retrivedFuelSurcharge = null;
		for (CustomerFuelSurcharge fuelSurcharge : customerFuelSurchargeList) {
			if (fuelSurcharge.getFuelSurchargeId().equals(fuelSurchargeType)) {
				retrivedFuelSurcharge = fuelSurcharge;
			}
		}

		if (retrivedFuelSurcharge != null) {
			SoftAssert softAssert = new SoftAssert();

			String amount = retrivedFuelSurcharge.getAmount();
			softAssert.assertEquals(amount, expectedData.get("amount"),
					"Expected amount: " + expectedData.get("amount") + " but API returned amount: " + amount);

			String apply_doe_rate = retrivedFuelSurcharge.getApplyDoeRate();
			softAssert.assertEquals(apply_doe_rate, expectedData.get("apply_doe_rate"), "Expected apply_doe_rate: "
					+ expectedData.get("apply_doe_rate") + " but API returned apply_doe_rate: " + apply_doe_rate);

			String base_price = retrivedFuelSurcharge.getBasePrice();
			softAssert.assertEquals(base_price, expectedData.get("base_price"), "Expected base_price: "
					+ expectedData.get("base_price") + " but API returned base_price: " + base_price);

			String company_id = retrivedFuelSurcharge.getCompanyId();
			softAssert.assertEquals(company_id, expectedData.get("company_id"), "Expected company_id: "
					+ expectedData.get("company_id") + " but API returned company_id: " + company_id);

			String credit = retrivedFuelSurcharge.getCredit();
			softAssert.assertEquals(credit, expectedData.get("credit"),
					"Expected credit: " + expectedData.get("credit") + " but API returned credit: " + credit);

			String customer_id = retrivedFuelSurcharge.getCustomerId();
			softAssert.assertEquals(customer_id, expectedData.get("customer_id"), "Expected customer_id: "
					+ expectedData.get("customer_id") + " but API returned customer_id: " + customer_id);

			String date_opt = retrivedFuelSurcharge.getDateOpt();
			softAssert.assertEquals(date_opt, expectedData.get("date_opt"),
					"Expected date_opt: " + expectedData.get("date_opt") + " but API returned date_opt: " + date_opt);

			String fuel_percent = retrivedFuelSurcharge.getFuelPercent();
			softAssert.assertEquals(fuel_percent, expectedData.get("fuel_percent"), "Expected fuel_percent: "
					+ expectedData.get("fuel_percent") + " but API returned fuel_percent: " + fuel_percent);

			String fuel_surcharge_id = retrivedFuelSurcharge.getFuelSurchargeId();
			softAssert.assertEquals(fuel_surcharge_id, expectedData.get("fuel_surcharge_id"),
					"Expected fuel_surcharge_id: " + expectedData.get("fuel_surcharge_id")
							+ " but API returned fuel_surcharge_id: " + fuel_surcharge_id);

			String fuel_variance = retrivedFuelSurcharge.getFuelVariance();
			softAssert.assertEquals(fuel_variance, expectedData.get("fuel_variance"), "Expected fuel_variance: "
					+ expectedData.get("fuel_variance") + " but API returned fuel_variance: " + fuel_variance);

			String method = retrivedFuelSurcharge.getMethod();
			softAssert.assertEquals(method, expectedData.get("method"),
					"Expected method: " + expectedData.get("method") + " but API returned method: " + method);

			String order_type_id = retrivedFuelSurcharge.getOrderTypeId();
			softAssert.assertEquals(order_type_id, expectedData.get("order_type_id"), "Expected order_type_id: "
					+ expectedData.get("order_type_id") + " but API returned order_type_id: " + order_type_id);

			String region = (String) retrivedFuelSurcharge.getRegion();
			softAssert.assertEquals(region, expectedData.get("region"),
					"Expected region: " + expectedData.get("region") + " but API returned region: " + region);

			String surcharge_type = retrivedFuelSurcharge.getSurchargeType();
			softAssert.assertEquals(surcharge_type, expectedData.get("surcharge_type"), "Expected surcharge_type: "
					+ expectedData.get("surcharge_type") + " but API returned surcharge_type: " + surcharge_type);

			String truckload_ltl = retrivedFuelSurcharge.getTruckloadLtl();
			softAssert.assertEquals(truckload_ltl, expectedData.get("truckload_ltl"), "Expected truckload_ltl: "
					+ expectedData.get("truckload_ltl") + " but API returned truckload_ltl: " + truckload_ltl);

			String __type = retrivedFuelSurcharge.getType();
			softAssert.assertEquals(__type, expectedData.get("__type"),
					"Expected __type: " + expectedData.get("__type") + " but API returned __type: " + __type);

			// custom_fsc_hdr_id_row
			try {
				String company_id_custom_fsc_hdr = retrivedFuelSurcharge.getCustomFscHdrIdRow().getCompanyId();
				softAssert.assertEquals(company_id_custom_fsc_hdr, expectedData.get("company_id_custom_fsc_hdr"),
						"Expected custom_fsc_hdr_id_row company_id: " + expectedData.get("company_id_custom_fsc_hdr")
								+ " but API returned custom_fsc_hdr_id_row company_id: " + company_id_custom_fsc_hdr);

				String desc_custom_fsc_hdr = retrivedFuelSurcharge.getCustomFscHdrIdRow().getDescr();
				softAssert.assertEquals(desc_custom_fsc_hdr, expectedData.get("desc_custom_fsc_hdr"),
						"Expected custom_fsc_hdr_id_row descr: " + expectedData.get("desc_custom_fsc_hdr")
								+ " but API returned custom_fsc_hdr_id_row descr: " + desc_custom_fsc_hdr);

				String id_custom_fsc_hdr = retrivedFuelSurcharge.getCustomFscHdrIdRow().getId();
				softAssert.assertEquals(id_custom_fsc_hdr, expectedData.get("id_custom_fsc_hdr"),
						"Expected custom_fsc_hdr_id_row id: " + expectedData.get("id_custom_fsc_hdr")
								+ " but API returned custom_fsc_hdr_id_row id: " + id_custom_fsc_hdr);

				String __name_custom_fsc_hdr = retrivedFuelSurcharge.getCustomFscHdrIdRow().getName();
				softAssert.assertEquals(__name_custom_fsc_hdr, expectedData.get("__name_custom_fsc_hdr"),
						"Expected custom_fsc_hdr_id_row __name: " + expectedData.get("__name_custom_fsc_hdr")
								+ " but API returned custom_fsc_hdr_id_row __name: " + __name_custom_fsc_hdr);

				String __type_custom_fsc_hdr = retrivedFuelSurcharge.getCustomFscHdrIdRow().getType();
				softAssert.assertEquals(__type_custom_fsc_hdr, expectedData.get("__type_custom_fsc_hdr"),
						"Expected custom_fsc_hdr_id_row __type: " + expectedData.get("__type_custom_fsc_hdr")
								+ " but API returned custom_fsc_hdr_id_row __type: " + __type_custom_fsc_hdr);
			} catch (Exception e) {

			}
			// fuel_surcharge_id_row
			String charge_rate_method = retrivedFuelSurcharge.getFuelSurchargeIdRow().getChargeRateMethod();
			softAssert.assertEquals(charge_rate_method, expectedData.get("charge_rate_method"),
					"Expected fuel_surcharge_id_row charge_rate_method: " + expectedData.get("charge_rate_method")
							+ " but API returned fuel_surcharge_id_row charge_rate_method: " + charge_rate_method);

			String company_id_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getCompanyId();
			softAssert.assertEquals(company_id_fsc_hdr, expectedData.get("company_id_fsc_hdr"),
					"Expected fuel_surcharge_id_row company_id: " + expectedData.get("company_id_fsc_hdr")
							+ " but API returned fuel_surcharge_id_row company_id: " + company_id_fsc_hdr);

			String desc_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getDescr();
			softAssert.assertEquals(desc_fsc_hdr, expectedData.get("desc_fsc_hdr"),
					"Expected fuel_surcharge_id_row descr: " + expectedData.get("desc_fsc_hdr")
							+ " but API returned fuel_surcharge_id_row descr: " + desc_fsc_hdr);

			String glid_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getGlid();
			softAssert.assertEquals(glid_fsc_hdr, expectedData.get("glid_fsc_hdr"),
					"Expected fuel_surcharge_id_row glid: " + expectedData.get("glid_fsc_hdr")
							+ " but API returned fuel_surcharge_id_row glid: " + glid_fsc_hdr);

			String id_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getId();
			softAssert.assertEquals(id_fsc_hdr, expectedData.get("id_fsc_hdr"), "Expected fuel_surcharge_id_row id: "
					+ expectedData.get("id_fsc_hdr") + " but API returned fuel_surcharge_id_row id: " + id_fsc_hdr);

			Boolean is_fuel_surcharge = retrivedFuelSurcharge.getFuelSurchargeIdRow().getIsFuelSurcharge();
			softAssert.assertEquals(is_fuel_surcharge, expectedData.get("is_fuel_surcharge"),
					"Expected fuel_surcharge_id_row is_fuel_surcharge: " + expectedData.get("is_fuel_surcharge")
							+ " but API returned fuel_surcharge_id_row is_fuel_surcharge: " + is_fuel_surcharge);

			Boolean is_taxable = retrivedFuelSurcharge.getFuelSurchargeIdRow().getIsTaxable();
			softAssert.assertEquals(is_taxable, expectedData.get("is_taxable"),
					"Expected fuel_surcharge_id_row is_taxable: " + expectedData.get("is_taxable")
							+ " but API returned fuel_surcharge_id_row is_taxable: " + is_taxable);

			Boolean pay_company_hourly_driver = retrivedFuelSurcharge.getFuelSurchargeIdRow()
					.getPayCompanyHourlyDriver();
			softAssert.assertEquals(pay_company_hourly_driver, expectedData.get("pay_company_hourly_driver"),
					"Expected pay_company_hourly_driver: " + expectedData.get("pay_company_hourly_driver")
							+ " but API returned pay_company_hourly_driver: " + pay_company_hourly_driver);

			Boolean prev_flag = retrivedFuelSurcharge.getFuelSurchargeIdRow().getPrevFlag();
			softAssert.assertEquals(prev_flag, expectedData.get("prev_flag"),
					"Expected fuel_surcharge_id_row prev_flag: " + expectedData.get("prev_flag")
							+ " but API returned fuel_surcharge_id_row prev_flag: " + prev_flag);

			String who_to_pay = retrivedFuelSurcharge.getFuelSurchargeIdRow().getWhoToPay();
			softAssert.assertEquals(who_to_pay, expectedData.get("who_to_pay"),
					"Expected fuel_surcharge_id_row who_to_pay: " + expectedData.get("who_to_pay")
							+ " but API returned fuel_surcharge_id_row who_to_pay: " + who_to_pay);

			String __name_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getName();
			softAssert.assertEquals(__name_fsc_hdr, expectedData.get("__name_fsc_hdr"),
					"Expected fuel_surcharge_id_row __name: " + expectedData.get("__name_fsc_hdr")
							+ " but API returned fuel_surcharge_id_row __name: " + __name_fsc_hdr);

			String __type_fsc_hdr = retrivedFuelSurcharge.getFuelSurchargeIdRow().getType();
			softAssert.assertEquals(__type_fsc_hdr, expectedData.get("__type_fsc_hdr"),
					"Expected fuel_surcharge_id_row __type: " + expectedData.get("__type_fsc_hdr")
							+ " but API returned fuel_surcharge_id_row __type: " + __type_fsc_hdr);

			softAssert.assertAll();
		} else {
			throw new Exception(
					"Test data has been modified. Check ? fuelsurcharge is added for customer 1000119 in masterfile");
		}
	}

	@DataProvider(name = "fuelSurcharge")
	public Object[][] fuelSurchargeDataprovider() {
		/*
		HashMap<String, Object> BTF = new HashMap<String, Object>();
		BTF.put("amount", "200000");
		BTF.put("apply_doe_rate", "Y");
		BTF.put("base_price", "3000");
		BTF.put("company_id", "TMS");
		BTF.put("credit", "true");
		BTF.put("customer_id", "1000119");
		BTF.put("date_opt", "S");
		BTF.put("fuel_percent", "2");
		BTF.put("fuel_surcharge_id", "BTF");
		BTF.put("fuel_variance", "20");
		BTF.put("method", "T");
		BTF.put("order_type_id", "UHV");
		BTF.put("region", null);
		BTF.put("surcharge_type", "C");
		BTF.put("truckload_ltl", "B");
		BTF.put("__type", "customer_fuel");
		BTF.put("company_id_custom_fsc_hdr", "TMS");
		BTF.put("desc_custom_fsc_hdr", "CHEWY FUEL");
		BTF.put("id_custom_fsc_hdr", "CHEWY");
		BTF.put("__name_custom_fsc_hdr", "custom_fsc_hdr_id_row");
		BTF.put("__type_custom_fsc_hdr", "custom_fsc_hdr");
		BTF.put("charge_rate_method", "F");
		BTF.put("company_id_fsc_hdr", "TMS");
		BTF.put("desc_fsc_hdr", "BREAKTHROUGH FUEL SURCHARGE");
		BTF.put("glid_fsc_hdr", "001411000000000000");
		BTF.put("id_fsc_hdr", "BTF");
		BTF.put("is_fuel_surcharge", true);
		BTF.put("is_taxable", true);
		BTF.put("pay_company_hourly_driver", false);
		BTF.put("prev_flag", false);
		BTF.put("who_to_pay", "Q");
		BTF.put("__name_fsc_hdr", "fuel_surcharge_id_row");
		BTF.put("__type_fsc_hdr", "charge_code");
*/
		HashMap<String, Object> DFS = new HashMap<String, Object>();
		DFS.put("amount", "0.01");
		DFS.put("apply_doe_rate", "Y");
		DFS.put("base_price", "1.3");
		DFS.put("company_id", "T100");
		DFS.put("credit", "false");
		DFS.put("customer_id", "1002694");
		DFS.put("date_opt", "S");
		DFS.put("fuel_percent", null);
		DFS.put("fuel_surcharge_id", "DFS");
		DFS.put("fuel_variance", "0.06");
		DFS.put("method", "D");
		DFS.put("order_type_id", null);
		DFS.put("region", "0");
		DFS.put("surcharge_type", "C");
		DFS.put("truckload_ltl", "B");
		DFS.put("__type", "customer_fuel");
		DFS.put("company_id_custom_fsc_hdr", null);
		DFS.put("desc_custom_fsc_hdr", null);
		DFS.put("id_custom_fsc_hdr", null);
		DFS.put("__name_custom_fsc_hdr", null);
		DFS.put("__type_custom_fsc_hdr", null);
		DFS.put("charge_rate_method", "D");
		DFS.put("company_id_fsc_hdr", "T100");
		DFS.put("desc_fsc_hdr", "FUEL SURCHARGE - DISTANCE");
		DFS.put("glid_fsc_hdr", "100411000000000000");
		DFS.put("id_fsc_hdr", "DFS");
		DFS.put("is_fuel_surcharge", true);
		DFS.put("is_taxable", true);
		DFS.put("pay_company_hourly_driver", false);
		DFS.put("prev_flag", false);
		DFS.put("who_to_pay", "Q");
		DFS.put("__name_fsc_hdr", "fuel_surcharge_id_row");
		DFS.put("__type_fsc_hdr", "charge_code");
/*
		HashMap<String, Object> FFS = new HashMap<String, Object>();
		FFS.put("amount", "200");
		FFS.put("apply_doe_rate", "Y");
		FFS.put("base_price", "1000");
		FFS.put("company_id", "TMS");
		FFS.put("credit", "true");
		FFS.put("customer_id", "1000119");
		FFS.put("date_opt", "S");
		FFS.put("fuel_percent", null);
		FFS.put("fuel_surcharge_id", "FFS");
		FFS.put("fuel_variance", "20");
		FFS.put("method", "D");
		FFS.put("order_type_id", "2ND");
		FFS.put("region", null);
		FFS.put("surcharge_type", "C");
		FFS.put("truckload_ltl", "B");
		FFS.put("__type", "customer_fuel");
		FFS.put("company_id_custom_fsc_hdr", null);
		FFS.put("desc_custom_fsc_hdr", null);
		FFS.put("id_custom_fsc_hdr", null);
		FFS.put("__name_custom_fsc_hdr", null);
		FFS.put("__type_custom_fsc_hdr", null);
		FFS.put("charge_rate_method", "F");
		FFS.put("company_id_fsc_hdr", "TMS");
		FFS.put("desc_fsc_hdr", "FUEL SURCHARGE - FLAT");
		FFS.put("glid_fsc_hdr", "001411000000000000");
		FFS.put("id_fsc_hdr", "FFS");
		FFS.put("is_fuel_surcharge", true);
		FFS.put("is_taxable", true);
		FFS.put("pay_company_hourly_driver", false);
		FFS.put("prev_flag", false);
		FFS.put("who_to_pay", "Q");
		FFS.put("__name_fsc_hdr", "fuel_surcharge_id_row");
		FFS.put("__type_fsc_hdr", "charge_code");

		HashMap<String, Object> FP01 = new HashMap<String, Object>();
		FP01.put("amount", "2000");
		FP01.put("apply_doe_rate", "N");
		FP01.put("base_price", null);
		FP01.put("company_id", "TMS");
		FP01.put("credit", "false");
		FP01.put("customer_id", "1000119");
		FP01.put("date_opt", "S");
		FP01.put("fuel_percent", null);
		FP01.put("fuel_surcharge_id", "FP01");
		FP01.put("fuel_variance", null);
		FP01.put("method", "F");
		FP01.put("order_type_id", "2ND-SOLO");
		FP01.put("region", null);
		FP01.put("surcharge_type", "C");
		FP01.put("truckload_ltl", "B");
		FP01.put("__type", "customer_fuel");
		FP01.put("company_id_custom_fsc_hdr", null);
		FP01.put("desc_custom_fsc_hdr", null);
		FP01.put("id_custom_fsc_hdr", null);
		FP01.put("__name_custom_fsc_hdr", null);
		FP01.put("__type_custom_fsc_hdr", null);
		FP01.put("charge_rate_method", "F");
		FP01.put("company_id_fsc_hdr", "TMS");
		FP01.put("desc_fsc_hdr", "PADD  NEW ENGLAND");
		FP01.put("glid_fsc_hdr", "001411000000000000");
		FP01.put("id_fsc_hdr", "FP01");
		FP01.put("is_fuel_surcharge", true);
		FP01.put("is_taxable", true);
		FP01.put("pay_company_hourly_driver", false);
		FP01.put("prev_flag", false);
		FP01.put("who_to_pay", "Q");
		FP01.put("__name_fsc_hdr", "fuel_surcharge_id_row");
		FP01.put("__type_fsc_hdr", "charge_code");

		return new Object[][] { { "BTF", BTF }, { "DFS", DFS }, 
			{ "FFS", FFS }, { "FP01", FP01 } };
*/
		return new Object[][] { { "DFS", DFS }};
	}
}
