package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class MasterFilesEquipmentIssuedStatus_API_Test extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("MASTER_FILES_EQUIPMENT_ISSUED_STATUS_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void master_files_equipment_issued_status_API_health_check() throws Exception {
        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_equipment_issued_status_returned_by_api() throws Exception {
        String expectedEquipIssuedStatus1 = "Issued";
        String expectedEquipIssuedStatus2 = "Missing";
        String expectedEquipIssuedStatus3 = "Returned";

        Response response = RequestMethod.GET(requestSpecification);
        String retrievedEquipIssuedStatus1 = response.jsonPath().get("response.I");
        String retrievedEquipIssuedStatus2 = response.jsonPath().get("response.M");
        String retrievedEquipIssuedStatus3 = response.jsonPath().get("response.R");

        SoftAssert softAssert = new SoftAssert();

        String status1 = (retrievedEquipIssuedStatus1 == null) ? "NULL" : retrievedEquipIssuedStatus1;
        softAssert.assertEquals(status1,expectedEquipIssuedStatus1,
                "Expected equipment issued status ["+expectedEquipIssuedStatus1+"] not found ");
        String status2 = (retrievedEquipIssuedStatus2 == null) ? "NULL" : retrievedEquipIssuedStatus2;
        softAssert.assertEquals(status2,expectedEquipIssuedStatus2,
                "Expected equipment issued status ["+expectedEquipIssuedStatus2+"] not found ");
        String status3 = (retrievedEquipIssuedStatus3 == null) ? "NULL" : retrievedEquipIssuedStatus3;
        softAssert.assertEquals(status3,expectedEquipIssuedStatus3,
                "Expected equipment issued status ["+expectedEquipIssuedStatus3+"] not found ");

        softAssert.assertAll();
    }
}
