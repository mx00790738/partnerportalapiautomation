package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Common.TractorDriverTrailerDetails;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class TractorDriverTrailerSearch_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("TRACTOR_DRIVER_TRAILER_SEARCH_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void tractor_driver_trailer_search_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"tractorId\":\"104027\",\"driverId\":\"RAGONZAL\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_TDT_search_for_104027_and_RAGONZAL_returns_TDT_details() throws Exception {
        String expectedTractorID = "104027  ";
        String expectedDriverID = "RAGONZAL";

        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"tractorId\":\"104027\",\"driverId\":\"RAGONZAL\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<TractorDriverTrailerDetails> tdtList = parseResponseToObjectList(response, TractorDriverTrailerDetails.class);
        TractorDriverTrailerDetails retrievedTDTList = tdtList.get(0);

        SoftAssert softAssert = new SoftAssert();

        String retrievedTractorID = (retrievedTDTList.getTractorId() == null) ? "NULL" : retrievedTDTList.getTractorId();
        softAssert.assertEquals(retrievedTractorID,expectedTractorID,
                "Expected tractor ID ["+expectedTractorID+"] but API returned ["+retrievedTractorID+"]");
        String retrievedDriverID = (retrievedTDTList.getDriver1Id() == null) ? "NULL" : retrievedTDTList.getDriver1Id();
        softAssert.assertEquals(retrievedDriverID,expectedDriverID,
                "Expected tractor ID ["+expectedDriverID+"] but API returned ["+retrievedDriverID+"]");

        softAssert.assertAll();
    }
}
