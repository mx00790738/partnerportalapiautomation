package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Common.EquipmentType;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.LinkedList;
import java.util.List;

public class GetEquipmentTypes_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void equipment_types_API_health_check() throws Exception {
        String path= TestConfig.get("EQUIPMENT_TYPES_API_PATH");
        requestSpecification.basePath(path.replace("{appliesTo}","L"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipment_list_returned_for_appliesTo_L() throws Exception {
        LinkedList<String> expectedEquipmentTypeList = new LinkedList<String>();
        expectedEquipmentTypeList.add("0CA");
        expectedEquipmentTypeList.add("0CS");
        expectedEquipmentTypeList.add("8RA");
        expectedEquipmentTypeList.add("8VS");
        expectedEquipmentTypeList.add("3DF");
        expectedEquipmentTypeList.add("3RAM");
        expectedEquipmentTypeList.add("3PA");
        expectedEquipmentTypeList.add("3PS");
        expectedEquipmentTypeList.add("3RA");
        expectedEquipmentTypeList.add("3VA");
        expectedEquipmentTypeList.add("3VS");
        expectedEquipmentTypeList.add("3FS");
        expectedEquipmentTypeList.add("AC");
        expectedEquipmentTypeList.add("CN");
        expectedEquipmentTypeList.add("C");
        expectedEquipmentTypeList.add("CR");
        expectedEquipmentTypeList.add("CI");
        expectedEquipmentTypeList.add("CV");
        expectedEquipmentTypeList.add("DD");
        expectedEquipmentTypeList.add("LA");
        expectedEquipmentTypeList.add("DD3");
        expectedEquipmentTypeList.add("DT");
        expectedEquipmentTypeList.add("FR");
        expectedEquipmentTypeList.add("F");
        expectedEquipmentTypeList.add("FA");
        expectedEquipmentTypeList.add("BT");
        expectedEquipmentTypeList.add("FN");
        expectedEquipmentTypeList.add("FZ");
        expectedEquipmentTypeList.add("FH");
        expectedEquipmentTypeList.add("MX");
        expectedEquipmentTypeList.add("FD");
        expectedEquipmentTypeList.add("FO");
        expectedEquipmentTypeList.add("FM");
        expectedEquipmentTypeList.add("FT");
        expectedEquipmentTypeList.add("FC");
        expectedEquipmentTypeList.add("FS");
        expectedEquipmentTypeList.add("HB");
        expectedEquipmentTypeList.add("IR");
        expectedEquipmentTypeList.add("LB");
        expectedEquipmentTypeList.add("LR");
        expectedEquipmentTypeList.add("LO");
        expectedEquipmentTypeList.add("MV");
        expectedEquipmentTypeList.add("NU");
        expectedEquipmentTypeList.add("PO");
        expectedEquipmentTypeList.add("R");
        expectedEquipmentTypeList.add("RA");
        expectedEquipmentTypeList.add("R2");
        expectedEquipmentTypeList.add("RZ");
        expectedEquipmentTypeList.add("RN");
        expectedEquipmentTypeList.add("RL");
        expectedEquipmentTypeList.add("RV");
        expectedEquipmentTypeList.add("RP");
        expectedEquipmentTypeList.add("RM");
        expectedEquipmentTypeList.add("RG");
        expectedEquipmentTypeList.add("SD");
        expectedEquipmentTypeList.add("SR");
        expectedEquipmentTypeList.add("SN");
        expectedEquipmentTypeList.add("SB");
        expectedEquipmentTypeList.add("ST");
        expectedEquipmentTypeList.add("TA");
        expectedEquipmentTypeList.add("TN");
        expectedEquipmentTypeList.add("TS");
        expectedEquipmentTypeList.add("TT");
        expectedEquipmentTypeList.add("V");
        expectedEquipmentTypeList.add("VA");
        expectedEquipmentTypeList.add("VW");
        expectedEquipmentTypeList.add("VS");
        expectedEquipmentTypeList.add("VC");
        expectedEquipmentTypeList.add("V2");
        expectedEquipmentTypeList.add("VZ");
        expectedEquipmentTypeList.add("VH");
        expectedEquipmentTypeList.add("VI");
        expectedEquipmentTypeList.add("VN");
        expectedEquipmentTypeList.add("VG");
        expectedEquipmentTypeList.add("VL");
        expectedEquipmentTypeList.add("OT");
        expectedEquipmentTypeList.add("VF");
        expectedEquipmentTypeList.add("VT");
        expectedEquipmentTypeList.add("VR");
        expectedEquipmentTypeList.add("VP");
        expectedEquipmentTypeList.add("VB");
        expectedEquipmentTypeList.add("VV");
        expectedEquipmentTypeList.add("VM");

        String path= TestConfig.get("EQUIPMENT_TYPES_API_PATH");
        requestSpecification.basePath(path.replace("{appliesTo}","L"));

        Response response = RequestMethod.GET(requestSpecification);
        LinkedList<String> retrievedEquipmentType = new LinkedList<String>();

        List<EquipmentType> equipmentTypeList = parseResponseToObjectList(response,EquipmentType.class);
        for(EquipmentType c : equipmentTypeList){
            retrievedEquipmentType.add(c.getEquipmentTypePkId().getEquipmentTypeId().trim());
        }

        SoftAssert softAssert = new SoftAssert();
        for(String s : expectedEquipmentTypeList){
            softAssert.assertTrue(retrievedEquipmentType.contains(s),
                    "Trailer equipment type: ["+s+"] not found in the retrieved trailer equipment type list ");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipment_list_returned_for_appliesTo_D() throws Exception {
        LinkedList<String> expectedEquipmentTypeList = new LinkedList<String>();
        expectedEquipmentTypeList.add("DBL");
        expectedEquipmentTypeList.add("DTX1");
        expectedEquipmentTypeList.add("HAZ");
        expectedEquipmentTypeList.add("REFD");
        expectedEquipmentTypeList.add("RVES");
        expectedEquipmentTypeList.add("TANK");
        expectedEquipmentTypeList.add("TWIC");

        String path= TestConfig.get("EQUIPMENT_TYPES_API_PATH");
        requestSpecification.basePath(path.replace("{appliesTo}","D"));

        Response response = RequestMethod.GET(requestSpecification);
        LinkedList<String> retrievedEquipmentType = new LinkedList<String>();

        List<EquipmentType> equipmentTypeList = parseResponseToObjectList(response,EquipmentType.class);
        for(EquipmentType c : equipmentTypeList){
            retrievedEquipmentType.add(c.getEquipmentTypePkId().getEquipmentTypeId().trim());
        }

        SoftAssert softAssert = new SoftAssert();
        for(String s : expectedEquipmentTypeList){
            softAssert.assertTrue(retrievedEquipmentType.contains(s),
                    "Driver additional equipment type: ["+s+"] not found in the " +
                            "retrieved driver additional equipment type list");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipment_list_returned_for_appliesTo_Y() throws Exception {
        LinkedList<String> expectedEquipmentTypeList = new LinkedList<String>();
        expectedEquipmentTypeList.add("ELDM");
        expectedEquipmentTypeList.add("ELDO");
        expectedEquipmentTypeList.add("MAV");

        String path= TestConfig.get("EQUIPMENT_TYPES_API_PATH");
        requestSpecification.basePath(path.replace("{appliesTo}","Y"));

        Response response = RequestMethod.GET(requestSpecification);
        LinkedList<String> retrievedEquipmentType = new LinkedList<String>();

        List<EquipmentType> equipmentTypeList = parseResponseToObjectList(response,EquipmentType.class);
        for(EquipmentType c : equipmentTypeList){
            retrievedEquipmentType.add(c.getEquipmentTypePkId().getEquipmentTypeId().trim());
        }

        SoftAssert softAssert = new SoftAssert();
        for(String s : expectedEquipmentTypeList){
            softAssert.assertTrue(retrievedEquipmentType.contains(s),
                    "Tractor additional equipment type: ["+s+"] not found in the " +
                            "retrieved tractor additional equipment type list");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipment_list_returned_for_appliesTo_X() throws Exception {
        LinkedList<String> expectedEquipmentTypeList = new LinkedList<String>();
        expectedEquipmentTypeList.add("CAH");
        expectedEquipmentTypeList.add("MTT");
        expectedEquipmentTypeList.add("TST1");

        String path= TestConfig.get("EQUIPMENT_TYPES_API_PATH");
        requestSpecification.basePath(path.replace("{appliesTo}","X"));

        Response response = RequestMethod.GET(requestSpecification);
        LinkedList<String> retrievedEquipmentType = new LinkedList<String>();

        List<EquipmentType> equipmentTypeList = parseResponseToObjectList(response,EquipmentType.class);
        for(EquipmentType c : equipmentTypeList){
            retrievedEquipmentType.add(c.getEquipmentTypePkId().getEquipmentTypeId().trim());
        }

        SoftAssert softAssert = new SoftAssert();
        for(String s : expectedEquipmentTypeList){
            softAssert.assertTrue(retrievedEquipmentType.contains(s),
                    "Trailer additional equipment type: ["+s+"] not found in the " +
                            "retrieved trailer additional equipment type list");
        }
        softAssert.assertAll();
    }
}
