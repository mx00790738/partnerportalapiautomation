package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Common.Service;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.LinkedList;
import java.util.List;

public class ServiceList_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void service_list_API_health_check() throws Exception {
        String path= TestConfig.get("SERVICE_LIST_API_PATH");
        requestSpecification.basePath(path.replace("{orderType}","T"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_service_list_returned_for_order_mode_P() throws Exception {
        LinkedList<String> expectedServiceList = new LinkedList<String>();
        expectedServiceList.add("PL");
        expectedServiceList.add("DL");
        expectedServiceList.add("RD");
        expectedServiceList.add("SL");
        expectedServiceList.add("ES");
        expectedServiceList.add("ID");
        expectedServiceList.add("AF");
        expectedServiceList.add("AD");
        expectedServiceList.add("AP");
        expectedServiceList.add("BS");
        expectedServiceList.add("CD");
        expectedServiceList.add("CD");
        expectedServiceList.add("CP");
        expectedServiceList.add("CH");
        expectedServiceList.add("CU");
        expectedServiceList.add("CO");
        expectedServiceList.add("CN");
        expectedServiceList.add("CN");
        expectedServiceList.add("CT");
        expectedServiceList.add("FD");
        expectedServiceList.add("FP");
        expectedServiceList.add("FR");
        expectedServiceList.add("FM");
        expectedServiceList.add("PF");
        expectedServiceList.add("GD");
        expectedServiceList.add("GP");
        expectedServiceList.add("HZ");
        expectedServiceList.add("HD");
        expectedServiceList.add("HP");
        expectedServiceList.add("HO");
        expectedServiceList.add("HS");
        expectedServiceList.add("HT");
        expectedServiceList.add("HL");
        expectedServiceList.add("LN");
        expectedServiceList.add("MD");
        expectedServiceList.add("MP");
        expectedServiceList.add("RP");
        expectedServiceList.add("SR");
        expectedServiceList.add("SA");
        expectedServiceList.add("SC");
        expectedServiceList.add("SH");
        expectedServiceList.add("SS");
        expectedServiceList.add("SG");
        expectedServiceList.add("SE");
        expectedServiceList.add("SU");
        expectedServiceList.add("SY");
        expectedServiceList.add("TR");
        expectedServiceList.add("TP");
        expectedServiceList.add("MG");
        expectedServiceList.add("IN");
        expectedServiceList.add("LI");
        expectedServiceList.add("LM");
        expectedServiceList.add("NO");
        expectedServiceList.add("CU");
        expectedServiceList.add("CC");
        expectedServiceList.add("EX");
        expectedServiceList.add("GU");
        expectedServiceList.add("G1");
        expectedServiceList.add("G2");
        expectedServiceList.add(null);
        expectedServiceList.add("G3");
        expectedServiceList.add("G3");
        expectedServiceList.add("G8");
        expectedServiceList.add("G9");
        expectedServiceList.add("GF");
        expectedServiceList.add("GM");
        expectedServiceList.add("GS");
        expectedServiceList.add("GA");
        expectedServiceList.add("GW");
        expectedServiceList.add("HC");
        expectedServiceList.add("CN");

        String path= TestConfig.get("SERVICE_LIST_API_PATH");
        requestSpecification.basePath(path.replace("{orderType}","P"));

        Response response = RequestMethod.GET(requestSpecification);
        List<Service> serviceList = parseResponseToObjectList(response, Service.class);

        SoftAssert softAssert = new SoftAssert();
        for(Service s : serviceList){
            softAssert.assertTrue(expectedServiceList.contains(s.getId()),
                    "Service: ["+s.getId()+"] not found in the expected service list for order mode:P");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_service_list_returned_for_order_mode_T() throws Exception {
        LinkedList<String> expectedServiceList = new LinkedList<String>();
        expectedServiceList.add("PL");
        expectedServiceList.add("DL");
        expectedServiceList.add("RD");
        expectedServiceList.add("SL");
        expectedServiceList.add("ES");
        expectedServiceList.add("ID");
        expectedServiceList.add("AF");
        expectedServiceList.add("AD");
        expectedServiceList.add("AP");
        expectedServiceList.add("BS");
        expectedServiceList.add("CD");
        expectedServiceList.add("CD");
        expectedServiceList.add("CP");
        expectedServiceList.add("CH");
        expectedServiceList.add("CU");
        expectedServiceList.add("CO");
        expectedServiceList.add("CN");
        expectedServiceList.add("CN");
        expectedServiceList.add("CT");
        expectedServiceList.add("FD");
        expectedServiceList.add("FP");
        expectedServiceList.add("FR");
        expectedServiceList.add("FM");
        expectedServiceList.add("PF");
        expectedServiceList.add("GD");
        expectedServiceList.add("GP");
        expectedServiceList.add("HZ");
        expectedServiceList.add("HD");
        expectedServiceList.add("HP");
        expectedServiceList.add("HO");
        expectedServiceList.add("HS");
        expectedServiceList.add("HT");
        expectedServiceList.add("HL");
        expectedServiceList.add("LN");
        expectedServiceList.add("MD");
        expectedServiceList.add("MP");
        expectedServiceList.add("RP");
        expectedServiceList.add("SR");
        expectedServiceList.add("SA");
        expectedServiceList.add("SC");
        expectedServiceList.add("SH");
        expectedServiceList.add("SS");
        expectedServiceList.add("SG");
        expectedServiceList.add("SE");
        expectedServiceList.add("SU");
        expectedServiceList.add("SY");
        expectedServiceList.add("TR");
        expectedServiceList.add("TP");
        expectedServiceList.add("MG");
        expectedServiceList.add("IN");
        expectedServiceList.add("LI");
        expectedServiceList.add("LM");
        expectedServiceList.add("NO");
        expectedServiceList.add("CU");
        expectedServiceList.add("CC");
        expectedServiceList.add("EX");
        expectedServiceList.add("GU");
        expectedServiceList.add("G1");
        expectedServiceList.add("G2");
        expectedServiceList.add(null);
        expectedServiceList.add("G3");
        expectedServiceList.add("G3");
        expectedServiceList.add("G8");
        expectedServiceList.add("G9");
        expectedServiceList.add("GF");
        expectedServiceList.add("GM");
        expectedServiceList.add("GS");
        expectedServiceList.add("GA");
        expectedServiceList.add("GW");
        expectedServiceList.add("HC");
        expectedServiceList.add("CN");

        String path= TestConfig.get("SERVICE_LIST_API_PATH");
        requestSpecification.basePath(path.replace("{orderType}","T"));

        Response response = RequestMethod.GET(requestSpecification);
        List<Service> serviceList = parseResponseToObjectList(response, Service.class);

        SoftAssert softAssert = new SoftAssert();
        for(Service s : serviceList){
            softAssert.assertTrue(expectedServiceList.contains(s.getId()),
                    "Service: ["+s.getId()+"] not found in the expected service list for order mode:P");
        }
        softAssert.assertAll();
    }
}
