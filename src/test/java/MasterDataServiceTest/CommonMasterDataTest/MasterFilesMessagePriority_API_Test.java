package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class MasterFilesMessagePriority_API_Test extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("MASTER_FILES_MESSAGE_PRIORITY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void master_files_message_priority_API_health_check() throws Exception {
        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_message_priority_returned_by_api() throws Exception {
        String expectedMsgPriority0 = "0";
        String expectedMsgPriority1 = "1";
        String expectedMsgPriority2 = "2";
        String expectedMsgPriority3 = "3";
        String expectedMsgPriority4 = "4";
        String expectedMsgPriority5 = "5";
        String expectedMsgPriority6 = "6";
        String expectedMsgPriority7 = "7";
        String expectedMsgPriority8 = "8";
        String expectedMsgPriority9 = "9";

        Response response = RequestMethod.GET(requestSpecification);
        String retrievedMsgPriority0 = response.jsonPath().get("response.0");
        String retrievedMsgPriority1 = response.jsonPath().get("response.1");
        String retrievedMsgPriority2 = response.jsonPath().get("response.2");
        String retrievedMsgPriority3 = response.jsonPath().get("response.3");
        String retrievedMsgPriority4 = response.jsonPath().get("response.4");
        String retrievedMsgPriority5 = response.jsonPath().get("response.5");
        String retrievedMsgPriority6 = response.jsonPath().get("response.6");
        String retrievedMsgPriority7 = response.jsonPath().get("response.7");
        String retrievedMsgPriority8 = response.jsonPath().get("response.8");
        String retrievedMsgPriority9 = response.jsonPath().get("response.9");

        SoftAssert softAssert = new SoftAssert();

        String priority0 = (retrievedMsgPriority0 == null) ? "NULL" : retrievedMsgPriority0;
        softAssert.assertEquals(priority0,expectedMsgPriority0,
                "Expected message priority ["+expectedMsgPriority0+"] not found ");
        String priority1 = (retrievedMsgPriority1 == null) ? "NULL" : retrievedMsgPriority1;
        softAssert.assertEquals(priority1,expectedMsgPriority1,
                "Expected message priority ["+expectedMsgPriority1+"] not found ");
        String priority2 = (retrievedMsgPriority2 == null) ? "NULL" : retrievedMsgPriority2;
        softAssert.assertEquals(priority2,expectedMsgPriority2,
                "Expected message priority ["+expectedMsgPriority2+"] not found ");
        String priority3 = (retrievedMsgPriority3 == null) ? "NULL" : retrievedMsgPriority3;
        softAssert.assertEquals(priority3,expectedMsgPriority3,
                "Expected message priority ["+expectedMsgPriority3+"] not found ");
        String priority4 = (retrievedMsgPriority4 == null) ? "NULL" : retrievedMsgPriority4;
        softAssert.assertEquals(priority4,expectedMsgPriority4,
                "Expected message priority ["+expectedMsgPriority4+"] not found ");
        String priority5 = (retrievedMsgPriority5 == null) ? "NULL" : retrievedMsgPriority5;
        softAssert.assertEquals(priority5,expectedMsgPriority5,
                "Expected message priority ["+expectedMsgPriority5+"] not found ");
        String priority6 = (retrievedMsgPriority6 == null) ? "NULL" : retrievedMsgPriority6;
        softAssert.assertEquals(priority6,expectedMsgPriority6,
                "Expected message priority ["+expectedMsgPriority5+"] not found ");
        String priority7 = (retrievedMsgPriority7 == null) ? "NULL" : retrievedMsgPriority7;
        softAssert.assertEquals(priority7,expectedMsgPriority7,
                "Expected message priority ["+expectedMsgPriority7+"] not found ");
        String priority8 = (retrievedMsgPriority8 == null) ? "NULL" : retrievedMsgPriority8;
        softAssert.assertEquals(priority8,expectedMsgPriority8,
                "Expected message priority ["+expectedMsgPriority8+"] not found ");
        String priority9 = (retrievedMsgPriority9 == null) ? "NULL" : retrievedMsgPriority9;
        softAssert.assertEquals(priority9,expectedMsgPriority9,
                "Expected message priority ["+expectedMsgPriority9+"] not found ");

        softAssert.assertAll();
    }
}
