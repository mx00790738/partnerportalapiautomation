package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class MasterFilesMessageStatus_API_Test extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("MASTER_FILES_MESSAGE_STATUS_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void master_files_message_status_API_health_check() throws Exception {
        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_message_status_returned_by_api() throws Exception {
        String expectedMsgStatus1 = "Assigned";
        String expectedMsgStatus2 = "Closed";
        String expectedMsgStatus3 = "Open";

        Response response = RequestMethod.GET(requestSpecification);
        String retrievedMsgStatus1 = response.jsonPath().get("response.A");
        String retrievedMsgStatus2 = response.jsonPath().get("response.C");
        String retrievedMsgStatus3 = response.jsonPath().get("response.O");

        SoftAssert softAssert = new SoftAssert();

        String status1 = (retrievedMsgStatus1 == null) ? "NULL" : retrievedMsgStatus1;
        softAssert.assertEquals(status1,expectedMsgStatus1,
                "Expected message status ["+expectedMsgStatus1+"] not found ");
        String status2 = (retrievedMsgStatus2 == null) ? "NULL" : retrievedMsgStatus2;
        softAssert.assertEquals(status2,expectedMsgStatus2,
                "Expected message status ["+expectedMsgStatus2+"] not found ");
        String status3 = (retrievedMsgStatus3 == null) ? "NULL" : retrievedMsgStatus3;
        softAssert.assertEquals(status3,expectedMsgStatus3,
                "Expected message status ["+expectedMsgStatus3+"] not found ");

        softAssert.assertAll();
    }
}
