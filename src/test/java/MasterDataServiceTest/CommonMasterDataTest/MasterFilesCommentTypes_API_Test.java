package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Common.CommentType;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.LinkedList;
import java.util.List;

public class MasterFilesCommentTypes_API_Test extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("MASTER_FILES_COMMENT_TYPE_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void master_files_message_status_API_health_check() throws Exception {
        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_comment_type_returned_by_api() throws Exception {
        LinkedList<String> expectedCommentType = new LinkedList<String>();
        expectedCommentType.add("ACTIVE");
        expectedCommentType.add("AGCO");
        expectedCommentType.add("AGIC");
        expectedCommentType.add("CBI");
        expectedCommentType.add("CCO");
        expectedCommentType.add("CONTACT");
        expectedCommentType.add("CREDIT");
        expectedCommentType.add("CSC");
        expectedCommentType.add("DAU");
        expectedCommentType.add("DCA");
        expectedCommentType.add("DCO");
        expectedCommentType.add("DCR");
        expectedCommentType.add("DGI");
        expectedCommentType.add("DID");
        expectedCommentType.add("DISP");
        expectedCommentType.add("DLO");
        expectedCommentType.add("DPA");
        expectedCommentType.add("DRC");
        expectedCommentType.add("DRE");
        expectedCommentType.add("DRF");
        expectedCommentType.add("DRI");
        expectedCommentType.add("DSA");
        expectedCommentType.add("DSV");
        expectedCommentType.add("DSW");
        expectedCommentType.add("DTE");
        expectedCommentType.add("DTR");
        expectedCommentType.add("DVP");
        expectedCommentType.add("DWC");
        expectedCommentType.add("DWT");
        expectedCommentType.add("ELOG");
        expectedCommentType.add("EQUIPDOC");
        expectedCommentType.add("FACTOR");
        expectedCommentType.add("FINAL_CO");
        expectedCommentType.add("FINAL_ST");
        expectedCommentType.add("FLOLOGIX");
        expectedCommentType.add("GEN");
        expectedCommentType.add("IMPORTED");
        expectedCommentType.add("INACTIVE");
        expectedCommentType.add("INT");
        expectedCommentType.add("IRE");
        expectedCommentType.add("KUDO1");
        expectedCommentType.add("KUDO2");
        expectedCommentType.add("KUDOS");
        expectedCommentType.add("LEIS");
        expectedCommentType.add("LSC");
        expectedCommentType.add("LSCA");
        expectedCommentType.add("PAYMENT");
        expectedCommentType.add("PDE");
        expectedCommentType.add("REPLACE");
        expectedCommentType.add("ROU");
        expectedCommentType.add("SDR");
        expectedCommentType.add("SETTLGAC");
        expectedCommentType.add("SHOP-UPD");
        expectedCommentType.add("TERM");
        expectedCommentType.add("TIS");
        expectedCommentType.add("TNOT");
        expectedCommentType.add("TRE");
        expectedCommentType.add("TVI");
        expectedCommentType.add("WRITEOFF");

        Response response = RequestMethod.GET(requestSpecification);
        LinkedList<String> retrievedCommentType = new LinkedList<String>();

        List<CommentType> commentTypeList = parseResponseToObjectList(response,CommentType.class);
        for(CommentType c : commentTypeList){
            retrievedCommentType.add(c.getCompanyTypePkId().getCommentTypeId().trim());;
        }

        SoftAssert softAssert = new SoftAssert();
        for(String s : expectedCommentType){
            softAssert.assertTrue(retrievedCommentType.contains(s),
                    "Comment type: ["+s+"] not found in the retrieved comment type list");
        }
        softAssert.assertAll();
    }
}
