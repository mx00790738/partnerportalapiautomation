package MasterDataServiceTest.CommonMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class MasterFilesMessagePMLevel_API_Test extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("MASTER_FILES_MESSAGE_PM_LEVEL_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void master_files_message_pmLevel_API_health_check() throws Exception {
        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_message_pmLevel_returned_by_api() throws Exception {
        String expectedMsgPMLevelA = "A";
        String expectedMsgPMLevelB = "B";
        String expectedMsgPMLevelC = "C";
        String expectedMsgPMLevelD = "D";
        String expectedMsgPMLevelE = "E";
        String expectedMsgPMLevelF = "F";
        String expectedMsgPMLevelG = "G";
        String expectedMsgPMLevelH = "H";
        String expectedMsgPMLevelI = "I";
        String expectedMsgPMLevelJ = "J";
        String expectedMsgPMLevelK = "K";
        String expectedMsgPMLevelL = "L";

        Response response = RequestMethod.GET(requestSpecification);

        String retrievedMsgPMLevelA = response.jsonPath().get("response.A");
        String retrievedMsgPMLevelB = response.jsonPath().get("response.B");
        String retrievedMsgPMLevelC = response.jsonPath().get("response.C");
        String retrievedMsgPMLevelD = response.jsonPath().get("response.D");
        String retrievedMsgPMLevelE = response.jsonPath().get("response.E");
        String retrievedMsgPMLevelF = response.jsonPath().get("response.F");
        String retrievedMsgPMLevelG = response.jsonPath().get("response.G");
        String retrievedMsgPMLevelH = response.jsonPath().get("response.H");
        String retrievedMsgPMLevelI = response.jsonPath().get("response.I");
        String retrievedMsgPMLevelJ = response.jsonPath().get("response.J");
        String retrievedMsgPMLevelK = response.jsonPath().get("response.K");
        String retrievedMsgPMLevelL = response.jsonPath().get("response.L");

        SoftAssert softAssert = new SoftAssert();

        String priorityA = (retrievedMsgPMLevelA == null) ? "NULL" : retrievedMsgPMLevelA;
        softAssert.assertEquals(priorityA,expectedMsgPMLevelA,
                "Expected message priority ["+expectedMsgPMLevelA+"] not found ");
        String priorityB = (retrievedMsgPMLevelB == null) ? "NULL" : retrievedMsgPMLevelB;
        softAssert.assertEquals(priorityB,expectedMsgPMLevelB,
                "Expected message priority ["+expectedMsgPMLevelB+"] not found ");
        String priorityC = (retrievedMsgPMLevelC == null) ? "NULL" : retrievedMsgPMLevelC;
        softAssert.assertEquals(priorityC,expectedMsgPMLevelC,
                "Expected message priority ["+expectedMsgPMLevelC+"] not found ");
        String priorityD = (retrievedMsgPMLevelD == null) ? "NULL" : retrievedMsgPMLevelD;
        softAssert.assertEquals(priorityD,expectedMsgPMLevelD,
                "Expected message priority ["+expectedMsgPMLevelD+"] not found ");
        String priorityE = (retrievedMsgPMLevelE == null) ? "NULL" : retrievedMsgPMLevelE;
        softAssert.assertEquals(priorityE,expectedMsgPMLevelE,
                "Expected message priority ["+expectedMsgPMLevelE+"] not found ");
        String priorityF = (retrievedMsgPMLevelF == null) ? "NULL" : retrievedMsgPMLevelF;
        softAssert.assertEquals(priorityF,expectedMsgPMLevelF,
                "Expected message priority ["+expectedMsgPMLevelF+"] not found ");
        String priorityG = (retrievedMsgPMLevelG == null) ? "NULL" : retrievedMsgPMLevelG;
        softAssert.assertEquals(priorityG,expectedMsgPMLevelG,
                "Expected message priority ["+expectedMsgPMLevelG+"] not found ");
        String priorityH = (retrievedMsgPMLevelH == null) ? "NULL" : retrievedMsgPMLevelH;
        softAssert.assertEquals(priorityH,expectedMsgPMLevelH,
                "Expected message priority ["+expectedMsgPMLevelH+"] not found ");
        String priorityI = (retrievedMsgPMLevelI == null) ? "NULL" : retrievedMsgPMLevelI;
        softAssert.assertEquals(priorityI,expectedMsgPMLevelI,
                "Expected message priority ["+expectedMsgPMLevelI+"] not found ");
        String priorityJ = (retrievedMsgPMLevelJ == null) ? "NULL" : retrievedMsgPMLevelJ;
        softAssert.assertEquals(priorityJ,expectedMsgPMLevelJ,
                "Expected message priority ["+expectedMsgPMLevelJ+"] not found ");
        String priorityK = (retrievedMsgPMLevelK == null) ? "NULL" : retrievedMsgPMLevelK;
        softAssert.assertEquals(priorityK,expectedMsgPMLevelK,
                "Expected message priority ["+expectedMsgPMLevelK+"] not found ");
        String priorityL = (retrievedMsgPMLevelL == null) ? "NULL" : retrievedMsgPMLevelL;
        softAssert.assertEquals(priorityL,expectedMsgPMLevelL,
                "Expected message priority ["+expectedMsgPMLevelL+"] not found ");

        softAssert.assertAll();
    }
}
