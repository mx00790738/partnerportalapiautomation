package MasterDataServiceTest.DriverMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Driver.DriverWithEquipmentListEntry;
import testConfiguration.TestConfig;

import java.util.List;

public class DriverNonBrokeredWithEquipmentList_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("DRIVER_NON_BROKERED_WITH_EQUIPMENT_LIST_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void driver_nonbrokered_with_equipmentlist_API_health_check() throws Exception {
        //TODO - Proper payload class
        String reqPayload = "{\"equipmentTypes\":[null,null],\"id\":\"DOBRAXTO\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());
/*
        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else {
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
 */
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_driver_DOBRAXTO_is_retrieved_in_driver_with_equipment_list() throws Exception {
        String expectedDriverName = "DAVION           O BRAXTON                     ";
        String reqPayload = "{\"equipmentTypes\":[null,null],\"id\":\"DOBRAXTO\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        List<DriverWithEquipmentListEntry> driverList = parseResponseToObjectList(response, DriverWithEquipmentListEntry.class);
        DriverWithEquipmentListEntry retrievedDriver =
                retrieveObjectFromObjectList(driverList, "getDriverName", expectedDriverName);

        String retrievedDriverName = retrievedDriver.getDriverName();
        Assert.assertEquals(retrievedDriverName,expectedDriverName,
                "Expected Driver Name: ["+expectedDriverName+"] but API returned: ["+retrievedDriverName+"]");
    }
}
