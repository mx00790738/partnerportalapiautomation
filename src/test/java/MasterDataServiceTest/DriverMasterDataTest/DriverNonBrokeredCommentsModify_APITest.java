package MasterDataServiceTest.DriverMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class DriverNonBrokeredCommentsModify_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("DRIVER_NON_BROKERED_COMMENTS_MODIFY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void driver_nonbrokered_comments_modify_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"ENZIRIM \",\"comments\":[{\"entered_user_id\":\"1001mgil\",\"entered_date\":\"12-29-2021\",\"comment_type_id\":\"ACTIVE  \"," +
                "\"id\":\"zz1fo2r2q0513o0LME-APP          \",\"comments\":\"Test comment\"}]}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_comments_are_modified_for_driver_ENZIRIM() throws Exception {
        String expectedStrInResponse = "Driver comments modified successfully";
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"ENZIRIM \",\"comments\":[{\"entered_user_id\":\"1001mgil\",\"entered_date\":\"12-29-2021\",\"comment_type_id\":\"ACTIVE  \"," +
                "\"id\":\"zz1fo2r2q0513o0LME-APP          \",\"comments\":\"Test comment\"}]}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedResponseMsg = response.jsonPath().get("response");

        Assert.assertTrue(retrievedResponseMsg.contains(expectedStrInResponse), "["+expectedStrInResponse+"] " +
                "str not found in retrieved response msg ["+retrievedResponseMsg+"]");
    }
}
