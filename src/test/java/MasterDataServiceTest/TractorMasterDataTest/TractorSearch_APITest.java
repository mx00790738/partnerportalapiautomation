package MasterDataServiceTest.TractorMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Tractor.Tractor;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class TractorSearch_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("TRACTOR_SEARCH_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void tractor_search_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"\",\"driver\":\"\",\"dispatcher\":\"\",\"owner\":\"\"," +
                "\"fleet\":\"\",\"status\":\"\",\"requiredEquipment\":[]}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"HealthCheck"})
    public void verify_tractor_104818_is_returned_when_searchBy_tractorID_104818() throws Exception {
        String expectedTractorID = "104818  ";
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"\",\"driver\":\"\",\"dispatcher\":\"\",\"owner\":\"\"," +
                "\"fleet\":\"\",\"status\":\"\",\"requiredEquipment\":[]}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<Tractor> tractorList = parseResponseToObjectList(response, Tractor.class);
        Tractor retrievedTractor = retrieveObjectFromObjectList(tractorList, "getTractorId", "104818  ");
        String retrievedTractorID = retrievedTractor.getTractorId();

        Assert.assertEquals(retrievedTractorID, expectedTractorID,
                "Expected tractor ID: [" + expectedTractorID + "] but API returned [" + retrievedTractorID + "]");
    }
}
