package MasterDataServiceTest.TractorMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class TractorNonBrokeredCommentsModify_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("TRACTOR_NON_BROKERED_COMMENTS_MODIFY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void tractor_nonbrokered_comments_modify_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"103245  \",\"companyId\":\"T100\",\"comments\":[{\"id\":\"zz1fo34ggdm13o0LME-APP          \",\"entered_user_id\":\"1001mgil\",\"enteredDate\":\"12-29-2021 12:31\"," +
                "\"comment_type_id\":\"AGCO    \",\"comments\":\"Test comment\"}]}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"Sanity"})
    public void verify_comments_are_modified_for_tractor_103245() throws Exception {
        String expectedStrInResponse = "Tractor comments modified successfully";
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"103245  \",\"companyId\":\"T100\",\"comments\":[{\"id\":\"zz1fo34ggdm13o0LME-APP          \",\"entered_user_id\":\"1001mgil\",\"enteredDate\":\"12-29-2021 12:31\"," +
                "\"comment_type_id\":\"AGCO    \",\"comments\":\"Test comment\"}]}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedResponseMsg = response.jsonPath().get("response");

        Assert.assertTrue(retrievedResponseMsg.contains(expectedStrInResponse), "[" + expectedStrInResponse + "] " +
                "str not found in retrieved response msg [" + retrievedResponseMsg + "]");
    }
}
