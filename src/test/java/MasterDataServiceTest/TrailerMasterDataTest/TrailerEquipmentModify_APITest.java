package MasterDataServiceTest.TrailerMasterDataTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class TrailerEquipmentModify_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(TestConfig.get("TRAILER_EQUIPMENTS_MODIFY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void trailer_equipments_modify_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"14503   \",\"companyId\":\"T100\"," +
                "\"equipments\":[{\"equipment_type_id\":\"CAH \",\"status\":\"I\",\"quantity\":\"2\",\"issuedDate\":\"manoj.than\",\"issued_user_id\":\"manoj.than\"," +
                "\"received_user_id\":null,\"returnedDate\":null,\"equipment_value\":null," +
                "\"comments\":null,\"memo\":null,\"location_id\":null,\"id\":\"zz1fo65t0mp04asRDSH-03          \"}]}";

        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipments_are_modified_for_trailer_14503() throws Exception {
        String expectedStrInResponse = "Trailer equipments modified successfully";
        RequestSpecification requestSpecification = this.requestSpecification;

        String reqPayload = "{\"id\":\"14503   \",\"companyId\":\"T100\",\"equipments\":[{\"equipment_type_id\":\"CAH \"," +
                "\"status\":\"I\",\"quantity\":\"2\",\"issuedDate\":\"manoj.than\"," +
                "\"issued_user_id\":\"manoj.than\",\"received_user_id\":null,\"returnedDate\":null," +
                "\"equipment_value\":null,\"comments\":null,\"memo\":null,\"location_id\":null,\"id\":\"zz1fo65t0mp04asRDSH-03          \"}]}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String retrievedResponseMsg = response.jsonPath().get("response");

        Assert.assertTrue(retrievedResponseMsg.contains(expectedStrInResponse), "["+expectedStrInResponse+"] " +
                "str not found in retrieved response msg ["+retrievedResponseMsg+"]");
    }
}
