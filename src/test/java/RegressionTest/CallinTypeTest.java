package RegressionTest;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class CallinTypeTest {
	static RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.COMMENT_TYPES)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}
	
	@Test
	public void getCommentTypesTest() {
		Response response = given().
			spec(requestSpecification).
		when().
			get();
		
		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	
	@Test
	public void validateTypesOfCommentsTest() throws JsonMappingException, JsonProcessingException {
		Response response = given().
			spec(requestSpecification).
		when().
			get();	
		
		Map<String,String> typeMap = response.getBody().jsonPath().getMap("response");
		
		ArrayList<String> type = new ArrayList<String>();
		ArrayList<String> desc = new ArrayList<String>();
		ArrayList<String> expectedType = new ArrayList<String>(Stream.of("B","C","O")
				.collect(Collectors.toList()));
		ArrayList<String> expectedDesc = new ArrayList<String>(Stream.of("Broker Initiated","Carrier Initiated","Comments")
				.collect(Collectors.toList()));

		for(Entry<String,String> e:typeMap.entrySet()) {
			type.add(e.getKey());
			desc.add(e.getValue());
		}
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(typeMap.size(), 3);
		softAssert.assertEquals(type, expectedType);
		softAssert.assertEquals(desc, expectedDesc);
		softAssert.assertAll();	
		
	}
}
