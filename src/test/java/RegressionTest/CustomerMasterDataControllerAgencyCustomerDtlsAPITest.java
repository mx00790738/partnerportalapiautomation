package RegressionTest;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.AgencyCustDtlsResponsePOJO;
import pojoClasses.responsePojo.AgencyCustomer;
import pojoClasses.responsePojo.BusinessResponseWithList;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class CustomerMasterDataControllerAgencyCustomerDtlsAPITest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST_MICROSERVICE"))
                .header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD")))
                .headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test
    public void masterdata_agency_customer_dtls_healthcheck() throws Exception {
        String path = TestConfig.get("CUSTOMER_MASTER_DATA_AGENCY_CUSTOMER_DTLS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{agencyId}", "1001"));

        Response response = RequestMethod.GET(requestSpecification);
        BusinessResponseWithList<AgencyCustomer> responsePayload = response.as(BusinessResponseWithList.class);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responsePayload.getResponseStatus().getResponseCode(), "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_agency_1001_customer_1000220_details() throws Exception {
        String path = TestConfig.get("CUSTOMER_MASTER_DATA_AGENCY_CUSTOMER_DTLS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{agencyId}", "1001"));

        Response response = RequestMethod.GET(requestSpecification);
        AgencyCustDtlsResponsePOJO responsePayload = response.as(AgencyCustDtlsResponsePOJO.class);

        AgencyCustomer expectedCustomerDetails = null;
        for(AgencyCustomer dts: responsePayload.getResponse()){
            if(dts.getId().equals("1000220 ")){
                expectedCustomerDetails = dts;
            }
        }

        if(expectedCustomerDetails == null){
            throw new Exception("Customer 1000220 is not found in the returned agency customer list of 1001");
        }

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(expectedCustomerDetails.getId(), "1000220 ",
                "Expected customer id: 1000220 instead API returned customer id: "
                        + expectedCustomerDetails.getId());

        softAssert.assertEquals(expectedCustomerDetails.getIsActive(), "Y",
                "Expected Is Active status: Y instead API returned IsActive status: "
                        + expectedCustomerDetails.getIsActive());

        softAssert.assertEquals(expectedCustomerDetails.getName(), "AERO TRANSPORTATION - MISSOURI",
                "Expected customer name: \"AERO TRANSPORTATION - MISSOURI\" instead API returned customer name: "
                        + expectedCustomerDetails.getName());

        softAssert.assertEquals(expectedCustomerDetails.getLocationAddress(), "330 E GEOSPACE DRIVE",
                "Expected customer address1: \"330 E GEOSPACE DRIVE\" instead API returned customer address1: "
                        + expectedCustomerDetails.getLocationAddress());

        softAssert.assertEquals(expectedCustomerDetails.getCity(), "INDEPENDENCE                                      ",
                "Expected customer city: \"INDEPENDENCE\" instead API returned customer city: "
                        + expectedCustomerDetails.getCity());

        softAssert.assertEquals(expectedCustomerDetails.getState(), "MO",
                "Expected customer state: \"MO\" instead API returned customer state: "
                        + expectedCustomerDetails.getState());

        softAssert.assertEquals(expectedCustomerDetails.getZipCode(), "64056     ",
                "Expected customer zip code: \"64056\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertEquals(expectedCustomerDetails.getCreditLimit(), 5000.0,
                "Expected customer zip code: \"5000.0\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertAll();
    }

    @Test
    public void verify_agency_1001_customer_1000820_details() throws Exception {
        String path = TestConfig.get("CUSTOMER_MASTER_DATA_AGENCY_CUSTOMER_DTLS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{agencyId}", "1001"));

        Response response = RequestMethod.GET(requestSpecification);
        AgencyCustDtlsResponsePOJO responsePayload = response.as(AgencyCustDtlsResponsePOJO.class);

        AgencyCustomer expectedCustomerDetails = null;
        for(AgencyCustomer dts: responsePayload.getResponse()){
            if(dts.getId().equals("1000820 ")){
                expectedCustomerDetails = dts;
            }
        }

        if(expectedCustomerDetails == null){
            throw new Exception("Customer 1000820 is not found in the returned agency customer list of 1001");
        }

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(expectedCustomerDetails.getId(), "1000820 ",
                "Expected customer id: 1000820 instead API returned customer id: "
                        + expectedCustomerDetails.getId());

        softAssert.assertEquals(expectedCustomerDetails.getIsActive(), "Y",
                "Expected Is Active status: Y instead API returned IsActive status: "
                        + expectedCustomerDetails.getIsActive());

        softAssert.assertEquals(expectedCustomerDetails.getName(), "BACH SIMPSON - ONTARIO",
                "Expected customer name: \"BACH SIMPSON - ONTARIO\" instead API returned customer name: "
                        + expectedCustomerDetails.getName());

        softAssert.assertEquals(expectedCustomerDetails.getLocationAddress(), "109 MEG DR",
                "Expected customer address1: \"109 MEG DR\" instead API returned customer address1: "
                        + expectedCustomerDetails.getLocationAddress());

        softAssert.assertEquals(expectedCustomerDetails.getCity(), "LONDON                                            ",
                "Expected customer city: \"INDEPENDENCE\" instead API returned customer city: "
                        + expectedCustomerDetails.getCity());

        softAssert.assertEquals(expectedCustomerDetails.getState(), "ON",
                "Expected customer state: \"ON\" instead API returned customer state: "
                        + expectedCustomerDetails.getState());

        softAssert.assertEquals(expectedCustomerDetails.getZipCode(), "N6A 4L6   ",
                "Expected customer zip code: \"N6A 4L6\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertEquals(expectedCustomerDetails.getCreditLimit(), 5000.0,
                "Expected customer zip code: \"5000.0\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertAll();
    }

    @Test
    public void verify_agency_1001_customer_1000819_details() throws Exception {
        String path = TestConfig.get("CUSTOMER_MASTER_DATA_AGENCY_CUSTOMER_DTLS_API_PATH");
        requestSpecification = requestSpecification.basePath(path.replace("{agencyId}", "1001"));

        Response response = RequestMethod.GET(requestSpecification);
        AgencyCustDtlsResponsePOJO responsePayload = response.as(AgencyCustDtlsResponsePOJO.class);

        AgencyCustomer expectedCustomerDetails = null;
        for(AgencyCustomer dts: responsePayload.getResponse()){
            if(dts.getId().equals("1000819 ")){
                expectedCustomerDetails = dts;
            }
        }

        if(expectedCustomerDetails == null){
            throw new Exception("Customer 1000819 is not found in the returned agency customer list of 1001");
        }

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(expectedCustomerDetails.getId(), "1000819 ",
                "Expected customer id: 1000819 instead API returned customer id: "
                        + expectedCustomerDetails.getId());

        softAssert.assertEquals(expectedCustomerDetails.getIsActive(), "Y",
                "Expected Is Active status: Y instead API returned IsActive status: "
                        + expectedCustomerDetails.getIsActive());

        softAssert.assertEquals(expectedCustomerDetails.getName(), "BACH SIMPSON - MICHIGAN",
                "Expected customer name: \"BACH SIMPSON - MICHIGAN\" instead API returned customer name: "
                        + expectedCustomerDetails.getName());

        softAssert.assertEquals(expectedCustomerDetails.getLocationAddress(), "3665 DOVE AVE",
                "Expected customer address1: \"3665 DOVE AVE\" instead API returned customer address1: "
                        + expectedCustomerDetails.getLocationAddress());

        softAssert.assertEquals(expectedCustomerDetails.getCity(), "PORT HURON                                        ",
                "Expected customer city: \"PORT HURON\" instead API returned customer city: "
                        + expectedCustomerDetails.getCity());

        softAssert.assertEquals(expectedCustomerDetails.getState(), "MI",
                "Expected customer state: \"MI\" instead API returned customer state: "
                        + expectedCustomerDetails.getState());

        softAssert.assertEquals(expectedCustomerDetails.getZipCode(), "48060     ",
                "Expected customer zip code: \"48060\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertEquals(expectedCustomerDetails.getCreditLimit(), 5000.0,
                "Expected customer zip code: \"5000.0\" instead API returned customer zip code: "
                        + expectedCustomerDetails.getZipCode());

        softAssert.assertAll();
    }

}
