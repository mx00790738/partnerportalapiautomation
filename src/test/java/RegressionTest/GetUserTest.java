package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class GetUserTest {
	static RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.GET_USER)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getUserRequestHeaders());
	}

	@Test
	public void getUserTest() {
		String getUserBody = "{\"loginId\":\"1001mgil\"}";
		given().
			spec(requestSpecification).
			body(getUserBody).
		when().
			post().
		then().log().all();
	}
}
