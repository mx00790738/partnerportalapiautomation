package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class ResponsibilityCodeCheckTest {

	static RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.RESPONSIBILITY_CODE_CHECK)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getResponsibilityCodeCheckRequestHeaders());
	}
	
	@Test
	public void responsibilityCodeCheckTest() {
		String requestBody = "{\"agencyId\":\"1001\"}";
		Response response = given().
			spec(requestSpecification).
			body(requestBody).
		when().
			post();
			
		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	
	@Test
	public void validateAgencyResponsibilityCodeTest() {
		String requestBody = "{\"agencyId\":\"1001\"}";
		Response response = given().
			spec(requestSpecification).
			body(requestBody).
		when().
			post();
			
		String agencyResponsibilityCode = response.body().jsonPath().get("response.agencyResponsibilityCode");
		Assert.assertEquals(agencyResponsibilityCode, "1001    ");	
	}
	
}
