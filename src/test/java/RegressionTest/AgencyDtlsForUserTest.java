package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestPayloadProvider.CreateOrderMicroservice.NewOrder.NewOrderRequestBody;
import testConfiguration.TestConfig;

public class AgencyDtlsForUserTest {
	static RequestSpecification requestSpecification;
	static NewOrderRequestBody newOrderRequestBody;

	static {
		try {
			newOrderRequestBody = new NewOrderRequestBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.TEMP_AGENCY_DTLS_1001MGIL)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}

	@Test
	public void agencyDtlsForUserTest() {
		Response response = given().spec(requestSpecification).when().get();
		System.out.println(response.getBody().asString());
		System.out.println("##########");
		System.out.println(response.body().jsonPath().getString("response.comapnyId[0]"));
		System.out.println("##########");
		System.out.println(response.body().jsonPath().getString("response.agencyPayeeId[0]"));
	}
}
