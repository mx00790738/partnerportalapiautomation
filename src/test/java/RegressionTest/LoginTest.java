package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class LoginTest {
	static RequestSpecification requestSpecification;

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.LOGIN_NEW)
				.headers(ApiHeaders.getLoginRequestHeaders());
	}

	@Test(dataProvider="LoginCredentials")
	public void ValidLoginCredentialTest(String userName,String password) {
		String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
		given().
			spec(requestSpecification).
			body(loginBody).
		when().
			post().
		then().statusLine("HTTP/1.1 200 ");		
	}
	
	@Test
	public void SuccessLoginResponseMessageTest() {
		String userName = "1001mgil";
		String password = "Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=";
		String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
		Response response = given().
			spec(requestSpecification).
			body(loginBody).
		when().
			post();
		String responseMsg = response.body().jsonPath().get("response");
		String responseStatus = response.body().jsonPath().get("status");
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseMsg, "User "+userName+" logged in successfully");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	
	@Test
	public void ValidateApiAuthorizationTokenInResponse() {
		String userName = "1001mgil";
		String password = "Y9npP06osUzX8FsHJyoTutnVQoDrpXlENxsPdHKRtqg=";
		String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
		Response response = given().
			spec(requestSpecification).
			body(loginBody).
		when().
			post();
		String authHeader = response.getHeader("APIAuthorization");	
		Assert.assertNotEquals(authHeader, "");
	}
	
	@Test
	public void IncorrectUsernameTest() {
		String userName = "abcd";
		String password = "Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=";
		String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
		
		Response response = given().
			spec(requestSpecification).
			body(loginBody).
		when().
			post();
		
		String statusLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		String responseMsg = response.body().jsonPath().get("response");
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(statusLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertEquals(responseMsg, "username not found; Please get in touch with admin or helpdesk@crst.com");
		softAssert.assertAll();
	}
	
	@Test(priority=1)
	public void IncorrectPasswordTest() {
		String userName = "1001mgil";
		String password = "Ag5tdta/QoW3K/Qt4IJ3rfUKc2WhYaN4aGE7pUZ0Xfo=";
		String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
		
		Response response = given().
			spec(requestSpecification).
			body(loginBody).
		when().
			post();
		
		String statusLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		String responseMsg = response.body().jsonPath().get("response");
		boolean flag = responseMsg.contains("Incorrect Password entered.");
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(statusLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertTrue(flag);
		softAssert.assertAll();
	}
	
	@DataProvider(name="LoginCredentials")
    public Object[][] getDataFromDataprovider(){
    return new Object[][] 
    	{
            { "1001mgil", "Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=" },
            { "1001mg", "c4gnuN7gzD+Ho+EXvNuXHlgSAlr08PRZ7QOa5bGKzdk=" },
            { "msharma", "7tD206NCfXz1vWsok6f0OFrnvaN/JOR2jFO7Dq0yKf4=" }
        };
    }
}
