package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import testConfiguration.PATH;
import enumClass.OPTION;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestPayloadProvider.CreateOrderMicroservice.NewOrder.NewOrderRequestBody;
import pojoClasses.requestPojo.NeedRevisit.Order;
import specifications.AccessorialSpecification;
import specifications.CommoditySpecification;
import specifications.CustomerRatingSpecification;
import specifications.EquipmentDetailsSpecification;
import specifications.OrderGeneralDetailsSpecifications;
import specifications.SpecialReferenceSpecification;
import specifications.StopSpecification;
import testConfiguration.TestConfig;

public class OrderCreateTest {
	private RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.NEW_ORDER)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}

	@Test
	public void orderCreateTest() throws Exception {
		NewOrderRequestBody newOrderRequestBody = new NewOrderRequestBody();
		OrderGeneralDetailsSpecifications orderGeneralDetailsSpec = new 
				OrderGeneralDetailsSpecifications();
		String customerID = "1000003";
		CommoditySpecification commoditySpec = new CommoditySpecification (OPTION.DEFAULT);
		AccessorialSpecification accessorialSpec = new AccessorialSpecification(OPTION.DEFAULT);
		EquipmentDetailsSpecification equipDetailsSpec = new EquipmentDetailsSpecification();
		SpecialReferenceSpecification specialRefSpec = new SpecialReferenceSpecification();
		StopSpecification stopSpecification = new StopSpecification(OPTION.DEFAULT);
		CustomerRatingSpecification customerRatingSpecification = new CustomerRatingSpecification(OPTION.DEFAULT);
		
		String billingMethod = "P";
		Order order = newOrderRequestBody.getNewOrderBody(orderGeneralDetailsSpec, customerID, 
				commoditySpec, accessorialSpec, stopSpecification, 
				equipDetailsSpec, specialRefSpec, customerRatingSpecification, billingMethod);
		
		requestSpecification.body(order);
		//System.out.println(requestSpecification.log().all());
		Response response = given().spec(requestSpecification).log().all().when().post();

		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		System.out.println(response.asPrettyString());
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
/*	
	@Test
	public void orderCreateSinglePickupSingleDrop() {
		Order orderSpecifications = new Order();
		List<AdditionalEquipment> additionalEquipments 
								= new ArrayList<AdditionalEquipment>();		
		orderSpecifications = newOrderRequestBody.getMultiStopOrderBody();
		orderSpecifications.setAdditionalEquipments(additionalEquipments);
		requestSpecification.body(orderSpecifications);
		//System.out.println(requestSpecification);
		Response response = given().spec(requestSpecification).when().post();

		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		System.out.println(response.asPrettyString());
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	*/
}
