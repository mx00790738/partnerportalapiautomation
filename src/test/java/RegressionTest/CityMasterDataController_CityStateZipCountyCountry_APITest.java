package RegressionTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.MasterData.Location.CityStateZipCountyCountryListEntry;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class CityMasterDataController_CityStateZipCountyCountry_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST_t100")).
                basePath(TestConfig.get("CITY_MASTER_DATA_CITY_STATE_ZIP_COUNTY_COUNTRY_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test
    public void masterdata_city_timezones_api_endpoint_healthcheck() throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"DALLAS\",\"state\":\"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="CitySearchPatternDataProvider")
    public void verify_city_data_in_the_retrieved_records_contains_the_searched_pattern_when_user_searchBy_city(String expectedPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\""+expectedPattern+"\",\"state\":\"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i<retrievedContactList.size();i++){
            CityStateZipCountyCountryListEntry e = retrievedContactList.get(i);
            String retrievedCity = e.getCity();
            boolean comparison = retrievedCity.toLowerCase().contains(expectedPattern.toLowerCase());
            softAssert.assertTrue(comparison, "Record at index ["+i+"] does not " +
                    "contains ["+expectedPattern+"] pattern in its city data: ["+retrievedCity+"]");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="InvalidCitySearchPatternDataProvider")
    public void verify_empty_list_is_retrieved_when_user_serachBy_invalid_city_name(String searchPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\""+searchPattern+"\",\"state\":\"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        boolean listIsEmpty = retrievedContactList.isEmpty();
        Assert.assertTrue(listIsEmpty, "Retrieved list is not empty for searched txt pattern: ["+searchPattern+"]");
    }

    @Test(dataProvider="StateSearchPatternDataProvider")
    public void verify_state_data_in_the_retrieved_records_contains_the_searched_pattern_when_user_searchBy_state(String searchPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\""+searchPattern+"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i<retrievedContactList.size();i++){
            CityStateZipCountyCountryListEntry e = retrievedContactList.get(i);
            String retrievedState = e.getState();
            boolean comparison = retrievedState.toLowerCase().contains(searchPattern.toLowerCase());
            softAssert.assertTrue(comparison, "Record at index ["+i+"] does not " +
                    "contains ["+searchPattern+"] pattern in its state data: ["+retrievedState+"]");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="InvalidStateSearchPatternDataProvider")
    public void verify_empty_list_is_retrieved_when_user_serachBy_invalid_state_code(String searchPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\""+searchPattern+"\",\"state\":\"\",\"postalCode\":\"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        boolean listIsEmpty = retrievedContactList.isEmpty();
        Assert.assertTrue(listIsEmpty, "Retrieved list is not empty for searched txt pattern: ["+searchPattern+"]");
    }

    @Test(dataProvider="ZipSearchPatternDataProvider")
    public void verify_zip_data_in_the_retrieved_records_contains_the_searched_pattern_when_user_searchBy_zip(String searchPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\"\",\"postalCode\":\""+searchPattern+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i<retrievedContactList.size();i++){
            CityStateZipCountyCountryListEntry e = retrievedContactList.get(i);
            String retrievedZip = e.getPostalCode();
            boolean comparison = retrievedZip.toLowerCase().contains(searchPattern.toLowerCase());
            softAssert.assertTrue(comparison, "Record at index ["+i+"] does not " +
                    "contains ["+searchPattern+"] pattern in its zip data: ["+retrievedZip+"]");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="InvalidZipSearchPatternDataProvider")
    public void verify_empty_list_is_retrieved_when_user_searchBy_invalid_zip_code(String searchPattern) throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\"\",\"postalCode\":\""+searchPattern+"\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        boolean listIsEmpty = retrievedContactList.isEmpty();
        Assert.assertTrue(listIsEmpty, "Retrieved list is not empty for searched txt pattern: ["+searchPattern+"]");
    }

    @Test
    public void verify_user_searchBy_state_IL_and_Zip_60707_yields_record_with_searched_state_and_zip() throws Exception {
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\"IL\",\"postalCode\":\"60707\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        SoftAssert softAssert = new SoftAssert();
        for(int i=0; i<retrievedContactList.size();i++){
            CityStateZipCountyCountryListEntry e = retrievedContactList.get(i);
            String retrievedState = e.getState();
            String retrievedZip = e.getPostalCode();
            boolean stateComparison = retrievedState.toLowerCase().contains("IL".toLowerCase());
            boolean zipComparison = retrievedZip.toLowerCase().contains("60707".toLowerCase());
            boolean result = (stateComparison && zipComparison);
            String errorMsg;
            if(stateComparison){
                errorMsg = "Record at index ["+i+"] does not " +
                        "contains [IL] pattern in its state data: ["+retrievedState+"]";
            } else{
                errorMsg = "Record at index ["+i+"] does not " +
                        "contains [60707] pattern in its zip data: ["+retrievedZip+"]";
            }
            softAssert.assertTrue(result, errorMsg);
        }
        softAssert.assertAll();
    }

    @Test
    public void verify_city_state_zip_county_country_details_retrieved() throws Exception {
        String expectedCityID = "312924";
        String expectedCity = "CHICAGO";
        String expectedZip = "60707";
        String expectedState = "IL";
        String expectedStateName = "ILLINOIS";
        String expectedCountry = "USA";
        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\"IL\",\"postalCode\":\"60707\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        CityStateZipCountyCountryListEntry retrievedRecord =
                retrieveObjectFromObjectList(retrievedContactList,"getCityId", expectedCityID);

        SoftAssert softAssert = new SoftAssert();

        String retrievedCity= (retrievedRecord.getCity() == null) ? "NULL" : retrievedRecord.getCity();
        softAssert.assertEquals(retrievedCity,expectedCity,
                "Expected city: ["+expectedCity+"] but API returned ["+retrievedCity+"]");

        String retrievedZip= (retrievedRecord.getPostalCode()== null) ? "NULL" : retrievedRecord.getPostalCode();
        softAssert.assertEquals(retrievedZip,expectedZip,
                "Expected zip: ["+expectedZip+"] but API returned ["+retrievedZip+"]");

        String retrievedState= (retrievedRecord.getState() == null) ? "NULL" : retrievedRecord.getState();
        softAssert.assertEquals(retrievedState,expectedState,
                "Expected state: ["+expectedState+"] but API returned ["+retrievedState+"]");

        String retrievedStateName = (retrievedRecord.getStateName() == null) ? "NULL" : retrievedRecord.getStateName();
        softAssert.assertEquals(retrievedStateName,expectedStateName,
                "Expected state name: ["+expectedStateName+"] but API returned ["+retrievedStateName+"]");

        String retrievedCountry= (retrievedRecord.getCountry() == null) ? "NULL" : retrievedRecord.getCountry();
        softAssert.assertEquals(retrievedCountry,expectedCountry,
                "Expected country: ["+expectedCountry+"] but API returned ["+retrievedCountry+"]");

        softAssert.assertAll();
    }

    @Test
    public void verify_latitude_longitude_details_retrieved() throws Exception {
        String expectedCityID = "312924";
        String expectedLatitude = "41.87419891357422";
        String expectedLongitude = "87.65139770507812";

        String reqPayload = "{\"returnTypeCode\":2,\"city\":\"\",\"state\":\"IL\",\"postalCode\":\"60707\"}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<CityStateZipCountyCountryListEntry> retrievedContactList =
                parseResponseToObjectList(response,CityStateZipCountyCountryListEntry.class);

        CityStateZipCountyCountryListEntry retrievedRecord = null;
        for(CityStateZipCountyCountryListEntry e:retrievedContactList) {
            if (e.getCityId().equals(expectedCityID)) {
                retrievedRecord = e;
            }
        }
        if(retrievedRecord == null){
            throw new Exception("Unexpected!! Record with City ID: ["+expectedCityID+"] not found");
        }

        SoftAssert softAssert = new SoftAssert();

        String retrievedLatitude= (retrievedRecord.getLatitude() == null) ? "NULL" : retrievedRecord.getLatitude().toString();
        softAssert.assertEquals(retrievedLatitude,expectedLatitude,
                "Expected city: ["+expectedLatitude+"] but API returned ["+retrievedLatitude+"]");

        String retrievedLongitude= (retrievedRecord.getLongitude()== null) ? "NULL" : retrievedRecord.getLongitude().toString();
        softAssert.assertEquals(retrievedLongitude,expectedLongitude,
                "Expected zip: ["+expectedLongitude+"] but API returned ["+retrievedLongitude+"]");

        softAssert.assertAll();
    }

    @DataProvider(name="CitySearchPatternDataProvider")
    public Object[][] getCitySearchPattern(){
        return new Object[][]
                {
                        {"DALLAS"},
                        {"DAL"},
                        {"dallas"},
                        {"dal"},
                        {"DaLLas"},
                };
    }

    @DataProvider(name="InvalidCitySearchPatternDataProvider")
    public Object[][] getInvalidCitySearchPattern(){
        return new Object[][]
                {
                        {"123"},
                        {"*&^^%"},
                        {"xyz"},
                };
    }

    @DataProvider(name="StateSearchPatternDataProvider")
    public Object[][] getStateSearchPattern(){
        return new Object[][]
                {
                        {"TX"},
                        {"A"},
                        {"BC"},
                        {"CP"},
                };
    }

    @DataProvider(name="InvalidStateSearchPatternDataProvider")
    public Object[][] getInvalidStateSearchPattern(){
        return new Object[][]
                {
                        {"00"},
                        {"^%$"},
                        {"xyz"},
                };
    }

    @DataProvider(name="ZipSearchPatternDataProvider")
    public Object[][] getZipSearchPattern(){
        return new Object[][]
                {
                        {"60707"},
                        {"607"},
                        {"V0K 2E0"},
                        {"V0K"},
                };
    }

    @DataProvider(name="InvalidZipSearchPatternDataProvider")
    public Object[][] getInvalidZipSearchPattern(){
        return new Object[][]
                {
                        {"0000"},
                        {"^1%2$"},
                        {"x98y2z"},
                };
    }
}
