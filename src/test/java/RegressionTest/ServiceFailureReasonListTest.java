package RegressionTest;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.responsePojo.ServiceFailureReason;
import testConfiguration.TestConfig;

public class ServiceFailureReasonListTest {
	static RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.SERVICE_FAILURE_REASON_LIST)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getResponsibilityCodeCheckRequestHeaders());
	}
	
	@Test
	public void ServiceFailureReasonListAPITest() {
		Response response = 
		given().
			spec(requestSpecification).
		when().
			get();
			
		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	
	@Test
	public void ValidateServiceFailureReasonList() {
		Response response = 
		given().
			spec(requestSpecification).
		when().
			get();
		
		List<String> actualServiceFailureReasonList = new ArrayList<String>();
		List<String> expectedServiceFailureReasonList = new ArrayList<String>();
		expectedServiceFailureReasonList.add("Missed Delivery");
		expectedServiceFailureReasonList.add("Incorrect Address");
		expectedServiceFailureReasonList.add("Indirect Delivery");
		expectedServiceFailureReasonList.add("Unable to Locate");
		expectedServiceFailureReasonList.add("Address Corrected - Delivery Attempted");
		expectedServiceFailureReasonList.add("Mis-sort");
		expectedServiceFailureReasonList.add("Customer Requested Future Delivery");
		expectedServiceFailureReasonList.add("Restricted Articles Unacceptable");
		expectedServiceFailureReasonList.add("Accident");
		expectedServiceFailureReasonList.add("Consignee Related");
		expectedServiceFailureReasonList.add("Driver Related");
		expectedServiceFailureReasonList.add("Mechanical Breakdown");
		expectedServiceFailureReasonList.add("Other Carrier Related");
		expectedServiceFailureReasonList.add("Damaged, Rewrapped in Hub");
		expectedServiceFailureReasonList.add("Previous Stop");
		expectedServiceFailureReasonList.add("Shipper Related");
		expectedServiceFailureReasonList.add("Holiday - Closed");
		expectedServiceFailureReasonList.add("Weather or Natural Disaster Related");
		expectedServiceFailureReasonList.add("Awaiting Export");
		expectedServiceFailureReasonList.add("Recipient Unavailable - Delivery Delayed");
		expectedServiceFailureReasonList.add("Improper International Paperwork");
		expectedServiceFailureReasonList.add("Hold Due to Customs Documentation Problems");
		expectedServiceFailureReasonList.add("Unable to Contact Recipient for Broker Information");
		expectedServiceFailureReasonList.add("Civil Event Related Delay");
		expectedServiceFailureReasonList.add("Exceeds Service Limitations");
		expectedServiceFailureReasonList.add("Past Cut-off Time");
		expectedServiceFailureReasonList.add("Insufficient Pick-up Time");
		expectedServiceFailureReasonList.add("Missed Pick-up");
		expectedServiceFailureReasonList.add("Alternate Carrier Delivered");
		expectedServiceFailureReasonList.add("Consignee Closed");
		expectedServiceFailureReasonList.add("Trap for Customer");
		expectedServiceFailureReasonList.add("Held for Payment");
		expectedServiceFailureReasonList.add("Held for Consignee");
		expectedServiceFailureReasonList.add("Improper Unloading Facility or Equipment");
		expectedServiceFailureReasonList.add("Receiving Time Restricted");
		expectedServiceFailureReasonList.add("Held per Shipper");
		expectedServiceFailureReasonList.add("Missing Documents");
		expectedServiceFailureReasonList.add("Border Clearance");
		expectedServiceFailureReasonList.add("Road Conditions");
		expectedServiceFailureReasonList.add("Carrier Keying Error");
		expectedServiceFailureReasonList.add("Other");
		expectedServiceFailureReasonList.add("Insufficient Time to Complete Delivery");
		expectedServiceFailureReasonList.add("Cartage Agent");
		expectedServiceFailureReasonList.add("Customer Wanted Earlier Delivery");
		expectedServiceFailureReasonList.add("Prearranged Appointment");
		expectedServiceFailureReasonList.add("Held for Protective Service");
		expectedServiceFailureReasonList.add("Flatcar Shortage");
		expectedServiceFailureReasonList.add("Failed to Release Billing");
		expectedServiceFailureReasonList.add("Railroad Failed to Meet Schedule");
		expectedServiceFailureReasonList.add("Load Shifted");
		expectedServiceFailureReasonList.add("Shipment Overweight");
		expectedServiceFailureReasonList.add("Train Derailment");
		expectedServiceFailureReasonList.add("Refused by Customer");
		expectedServiceFailureReasonList.add("Returned to Shipper");
		expectedServiceFailureReasonList.add("Waiting for Customer Pick-up");
		expectedServiceFailureReasonList.add("Credit Hold");
		expectedServiceFailureReasonList.add("Suspended at Customer Request");
		expectedServiceFailureReasonList.add("Customer Vacation");
		expectedServiceFailureReasonList.add("Customer Strike");
		expectedServiceFailureReasonList.add("Waiting Shipping Instructions");
		expectedServiceFailureReasonList.add("Waiting for Customer Specified Carrier");
		expectedServiceFailureReasonList.add("Collect on Delivery Required");
		expectedServiceFailureReasonList.add("Cash Not Available From Consignee");
		expectedServiceFailureReasonList.add("Customs (Import or Export)");
		expectedServiceFailureReasonList.add("No Requested Arrival Date Provided by Shipper");
		expectedServiceFailureReasonList.add("No Requested Arrival Time Provided by Shipper");
		expectedServiceFailureReasonList.add("Carrier Dispatch Error");
		expectedServiceFailureReasonList.add("Driver Not Available");
		expectedServiceFailureReasonList.add("Non-Express Clearance Delay");
		expectedServiceFailureReasonList.add("International Non-carrier Delay");
		expectedServiceFailureReasonList.add("Held Pending Appointment");
		expectedServiceFailureReasonList.add("Normal Appointment");
		expectedServiceFailureReasonList.add("Normal Status");
		expectedServiceFailureReasonList.add("Processing Delay");
		expectedServiceFailureReasonList.add("Waiting Inspection");
		expectedServiceFailureReasonList.add("Production Falldown");
		expectedServiceFailureReasonList.add("Held for Full Carrier Load");
		expectedServiceFailureReasonList.add("Reconsigned");
		expectedServiceFailureReasonList.add("Delivery Shortage");
		expectedServiceFailureReasonList.add("Tractor With Sleeper Car Not Available");
		expectedServiceFailureReasonList.add("Tractor, Conventional, Not Available");
		expectedServiceFailureReasonList.add("Trailer not Available");
		expectedServiceFailureReasonList.add("Trailer Not Usable Due to Prior Product");
		expectedServiceFailureReasonList.add("Trailer Class Not Available");
		expectedServiceFailureReasonList.add("Trailer Volume Not Available");
		expectedServiceFailureReasonList.add("Insufficient Delivery Time");
		
		List<ServiceFailureReason> serviceFailureReasonList = 
				new ArrayList<ServiceFailureReason>();
		serviceFailureReasonList = response.getBody().jsonPath().getList("response", ServiceFailureReason.class);
		
		for(ServiceFailureReason s:serviceFailureReasonList) {
			actualServiceFailureReasonList.add(s.getDescription());
		}	
		Assert.assertEquals(actualServiceFailureReasonList, expectedServiceFailureReasonList);
	}
}
