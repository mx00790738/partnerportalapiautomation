/**
 * 
 */
package RegressionTest;

import static io.restassured.RestAssured.given;

import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.databind.JsonNode;
import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestPayloadProvider.opportunityPayloadProvider.NewOpportunityRequestBody;
import requestPayloadProvider.opportunityPayloadProvider.SubjectOrderListRequestPayload;
import testConfiguration.TestConfig;

/**
 * @author MX0C92049
 *
 */
public class OpportunityOrderTest {

	private RequestSpecification requestSpecification;
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.NEW_ORDER)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
	}
	
	public void opportunityCreateTest(ITestContext context) throws Exception {
		NewOpportunityRequestBody newOpportunityRequestBody = new NewOpportunityRequestBody();
		JsonNode reqPayLoad = newOpportunityRequestBody.getOpportunityRequestBody();
		requestSpecification.body(reqPayLoad);
		Response response = given().spec(requestSpecification).when().post();
		
		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");
		String orderID = response.body().jsonPath().get("response");
		context.setAttribute("orderID", orderID);
		//context.setAttribute("orderData", reqPayLoad);
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}
	
	//@Test(dependsOnMethods = { "opportunityCreateTest" })
	@Test
	public void validateOpportunityOrderDataAfterCreate(ITestContext context) throws Exception {
		SubjectOrderListRequestPayload subjectOrderListRequestPayload = new
				SubjectOrderListRequestPayload();
		requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.SUBJECT_ORDER_LIST)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getCommentTypesRequestHeaders());
		requestSpecification.body(subjectOrderListRequestPayload.getSubjectOrderListOpenReqPayload());
		
		Response response = 
				given().
				spec(requestSpecification).
				log().all().
				when().
				post();
		System.out.println(response.getStatusCode());
		response.prettyPrint();
	}
}
