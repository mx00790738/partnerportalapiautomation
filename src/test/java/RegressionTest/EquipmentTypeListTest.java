package RegressionTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import apiHelper.EquipmentTypeListAPI;
import io.restassured.response.Response;
import pojoClasses.responsePojo.EquipmentType;

public class EquipmentTypeListTest {
	EquipmentTypeListAPI equipmentTypeListAPI = new EquipmentTypeListAPI();

	@Test
	public void getEquipmentTypeListTest() throws Exception {
		Response response = equipmentTypeListAPI.getEquipmentTypeList();

		//System.out.println(response.prettyPrint());
		String responseLine = response.getStatusLine();
		String responseStatus = response.body().jsonPath().get("status");

		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(responseLine, "HTTP/1.1 200 ");
		softAssert.assertEquals(responseStatus, "SUCCESS");
		softAssert.assertAll();
	}

	@Test
	public void ValidateEquipmentTypeListTest() throws Exception {
		Response response = equipmentTypeListAPI.getEquipmentTypeList();
        List<EquipmentType> equipmentTypeList = new ArrayList<EquipmentType>();
        equipmentTypeList = response.jsonPath().getList("response", EquipmentType.class);
        
        Map<String,String> expectedResult = new HashMap<String,String>();
        expectedResult.put("AC", "Auto Carrier (DAT)--AC");
        expectedResult.put("CN", "Conestoga (DAT)--CN");
        expectedResult.put("C", "Container (DAT)--C");
        expectedResult.put("CR", "Container Refrigerated (DAT)--CR");
        expectedResult.put("CI", "Container, Insulated (DAT)--CI");
        expectedResult.put("CV", "Conveyor (DAT)--CV");
        expectedResult.put("DD", "Double Drop (DAT)--DD");
        expectedResult.put("LA", "Drop Deck, Landoll (DAT)--LA");
        expectedResult.put("DT", "Dump Trailer (DAT)--DT");
        expectedResult.put("FR", "Flat/Van/Reefer (DAT)--FR");
        expectedResult.put("F", "Flatbed (DAT)--F");
        expectedResult.put("FA", "Flatbed Airride (DAT)--FA");
        expectedResult.put("BT", "Flatbed B-Train (DAT)--BT");
        expectedResult.put("FN", "Flatbed Conestoga (DAT)--FN");
        expectedResult.put("FZ", "Flatbed Hazmat (DAT)--FZ");
        expectedResult.put("FH", "Flatbed Hotshot (DAT)--FH");
        expectedResult.put("MX", "Flatbed Maxi (DAT)--MX");
        expectedResult.put("FO", "Flatbed Over-dimension--FO");
        expectedResult.put("FD", "Flatbed or Step Deck (DAT)--FD");
        expectedResult.put("FM", "Flatbed w/ Team (DAT)--FM");
        expectedResult.put("FT", "Flatbed w/Tarps (DAT)--FT");
        expectedResult.put("FC", "Flatbed with chains (DAT)--FC");
        expectedResult.put("3VA", "Generic Van--3VA");
        expectedResult.put("HB", "Hopper Bottom (DAT)--HB");
        expectedResult.put("IR", "Insulated Van or Reefer (DAT)--IR");
        expectedResult.put("LB", "Lowboy (DAT)--LB");
        expectedResult.put("LO", "Lowboy Overdimension (DAT)--LO");
        expectedResult.put("LR", "Lowboy or RGN (DAT)--LR");
        expectedResult.put("MV", "Moving Van (DAT)--MV");
        expectedResult.put("NU", "Pneumatic (DAT)--NU");
        expectedResult.put("PO", "Power Only (DAT)--PO");
        expectedResult.put("R", "Reefer (DAT)--R");
        expectedResult.put("RA", "Reefer Airride (DAT)--RA");
        expectedResult.put("R2", "Reefer Doubles (DAT)--R2");
        expectedResult.put("RZ", "Reefer Hazmat (DAT)--RZ");
        expectedResult.put("RN", "Reefer Intermodal (DAT)--RN");
        expectedResult.put("RL", "Reefer Logistics (DAT)--RL");
        expectedResult.put("RP", "Reefer Pallet Exchange (DAT)--RP");
        expectedResult.put("RV", "Reefer or Vented Van--RV");
        expectedResult.put("RM", "Reefer w/ Team (DAT)--RM");
        expectedResult.put("RG", "Removable GooseNeck (DAT)--RG");
        expectedResult.put("SD", "Step Deck (DAT)--SD");
        expectedResult.put("SR", "Step Deck or RGN--SR");
        expectedResult.put("SN", "Stepdeck Conestoga (DAT)--SN");
        expectedResult.put("SB", "Straight Box Truck--SB");
        expectedResult.put("ST", "Strectch Trailer (DAT)--ST");
        expectedResult.put("TA", "Tanker Aluminum (DAT)--TA");
        expectedResult.put("TN", "Tanker Intermodal (DAT)--TN");
        expectedResult.put("TS", "Tanker Steel (DAT)--TS");
        expectedResult.put("TT", "Truck and Trailer (DAT)--TT");
        expectedResult.put("V", "Van (DAT)--V");
        expectedResult.put("VA", "Van Airride (DAT)--VA");
        expectedResult.put("VW", "Van Blanket Wrap--VW");
        expectedResult.put("VS", "Van Conestoga (DAT)--VS");
        expectedResult.put("VC", "Van Curtain (DAT)--VC");
        expectedResult.put("V2", "Van Double (DAT)--V2");
        expectedResult.put("VZ", "Van Hazmat (DAT)--VZ");
        expectedResult.put("VH", "Van Hotshot (DAT)--VH");
        expectedResult.put("VI", "Van Insulated (DAT)--VI");
        expectedResult.put("VN", "Van Intermodal (DAT)--VN");
        expectedResult.put("VG", "Van Lift-Gate (DAT)--VG");
        expectedResult.put("VL", "Van Logistics (DAT)--VL");
        expectedResult.put("OT", "Van Opentop (DAT)--OT");
        expectedResult.put("VP", "Van Pallet Exchange (DAT)--VP");
        expectedResult.put("VB", "Van Roller Bed (DAT)--VB");
        expectedResult.put("VV", "Van Vented (DAT)--VV");
        expectedResult.put("VF", "Van or Flatbed (DAT)--VF");
        expectedResult.put("VT", "Van or Flats w/Tarps (DAT)--VT");
        expectedResult.put("VR", "Van or Reefer (DAT)--VR");
        expectedResult.put("VM", "Van w/ Team (DAT)--VM");
        
        Map<String,String> actualResult = new HashMap<String,String>();
        
        for(EquipmentType e: equipmentTypeList) {
        	actualResult.put(e.getEquipmentId(), e.getEquipmentName());
        }
       
        Assert.assertEquals(actualResult, expectedResult);
	}
}
