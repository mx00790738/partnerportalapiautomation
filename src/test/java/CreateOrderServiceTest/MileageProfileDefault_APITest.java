package CreateOrderServiceTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.CreateOrder.MileageProfileDefault.MileageProfileDefault;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class MileageProfileDefault_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("CREATE_ORDER_SERVICE_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void mileage_profile_default_API_health_check() throws Exception {
        String path= TestConfig.get("MILEAGE_PROFILE_DEFAULT_API_PATH");
        requestSpecification.basePath(path);

        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if(responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responsePayload.getResponseStatus().getResponseCode());
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responsePayload.getResponseStatus().getResponseDesc());
        } else{
            softAssert.assertNotNull(null, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_default_mileage_profile_retrieved() throws Exception {
        String expected_companyId = "T100";
        String expected_id = "1";
        String expected_loadDistProfile = "LOADEDMV";
        String expected_billDistProfile = "BILLING ";
        String expected_emptyDistProfile = "EMPTYMV ";
        String expected_etaDistProfile = "ETAOOR  ";
        String expected_fuelDistProfile = "FUEL    ";
        String expected_payDistProfile = "PAY     ";
        String expected_pracDistProfile = "PRACTICL";
        String expected_shortDistProfile = "SHORT   ";
        String path = TestConfig.get("MILEAGE_PROFILE_DEFAULT_API_PATH");
        requestSpecification.basePath(path);

        Response response = RequestMethod.GET(requestSpecification);
        List<MileageProfileDefault> retrievedProfileList = parseResponseToObjectList(response, MileageProfileDefault.class);
        MileageProfileDefault retrievedProfile = retrieveObjectFromObjectList(retrievedProfileList, "getId", "1");

        SoftAssert softAssert = new SoftAssert();

        String retrieved_companyId = (retrievedProfile.getCompanyId()==null)?"NULL":retrievedProfile.getCompanyId();
        softAssert.assertEquals(retrieved_companyId, expected_companyId,
                "Expected Company ID: " + expected_companyId + " but API returned " + retrieved_companyId);


        String retrieved_id = (retrievedProfile.getId() ==null)?"NULL":retrievedProfile.getId();
        softAssert.assertEquals(retrieved_id, expected_id,
                "Expected id: " + expected_id + " but API returned " + retrieved_id);

        String retrieved_loadDistProfile = (retrievedProfile.getLoadDistProfile() ==null)?"NULL":retrievedProfile.getLoadDistProfile();
        softAssert.assertEquals(retrieved_loadDistProfile, expected_loadDistProfile,
                "Expected loadDistProfile: " + expected_loadDistProfile + " but API returned " + retrieved_loadDistProfile);

        String retrieved_billDistProfile = (retrievedProfile.getBillDistProfile() ==null)?"NULL": retrievedProfile.getBillDistProfile();
        softAssert.assertEquals(retrieved_billDistProfile, expected_billDistProfile,
                "Expected billDistProfile: " + expected_billDistProfile + " but API returned " + retrieved_billDistProfile);

        String retrieved_emptyDistProfile = (retrievedProfile.getEmptyDistProfile() ==null)?"NULL": retrievedProfile.getEmptyDistProfile();
        softAssert.assertEquals(retrieved_emptyDistProfile, expected_emptyDistProfile,
                "Expected emptyDistProfile: " + expected_emptyDistProfile + " but API returned " + retrieved_emptyDistProfile);

        String retrieved_etaDistProfile = (retrievedProfile.getEtaDistProfile()==null)?"NULL": retrievedProfile.getEtaDistProfile();
        softAssert.assertEquals(retrieved_etaDistProfile, expected_etaDistProfile,
                "Expected etaDistProfile: " + expected_etaDistProfile + " but API returned " + retrieved_etaDistProfile);

        String retrieved_fuelDistProfile = (retrievedProfile.getFuelDistProfile() ==null)?"NULL": retrievedProfile.getFuelDistProfile();
        softAssert.assertEquals(retrieved_fuelDistProfile, expected_fuelDistProfile,
                "Expected fuelDistProfile: " + expected_fuelDistProfile + " but API returned " + retrieved_fuelDistProfile);

        String retrieved_payDistProfile = ( retrievedProfile.getPayDistProfile()==null)?"NULL": retrievedProfile.getPayDistProfile();
        softAssert.assertEquals(retrieved_payDistProfile, expected_payDistProfile,
                "Expected payDistProfile: " + expected_payDistProfile + " but API returned " + retrieved_payDistProfile);

        String retrieved_pracDistProfile = (retrievedProfile.getPracDistProfile() ==null)?"NULL": retrievedProfile.getPracDistProfile();
        softAssert.assertEquals(retrieved_pracDistProfile, expected_pracDistProfile,
                "Expected pracDistProfile: " + expected_pracDistProfile + " but API returned " + retrieved_pracDistProfile);

        String retrieved_shortDistProfile = (retrievedProfile.getShortDistProfile() ==null)?"NULL": retrievedProfile.getShortDistProfile();
        softAssert.assertEquals(retrieved_shortDistProfile, expected_shortDistProfile,
                "Expected shortDistProfile: " + expected_shortDistProfile + " but API returned " + retrieved_shortDistProfile);
    }
}
