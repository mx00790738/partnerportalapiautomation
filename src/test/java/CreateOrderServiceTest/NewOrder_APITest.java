package CreateOrderServiceTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import requestPayloadProvider.CreateOrderMicroservice.NewOrder.NewOrderRequestPayloadProvider;
import pojoClasses.requestPojo.NeedRevisit.Order;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class NewOrder_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("CREATE_ORDER_SERVICE_KONG_URL")).
                basePath(TestConfig.get("NEW_ORDER_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
    }

    @Test(groups = {"HealthCheck"})
    public void new_order_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        NewOrderRequestPayloadProvider requestPayloadProvider = new NewOrderRequestPayloadProvider();
        Order reqPayload = requestPayloadProvider.getRandomNewOrderRequestPayloadFTL();
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity" })
    public void verify_Flatbed_FTL_Order_is_created() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;

        NewOrderRequestPayloadProvider requestPayloadProvider = new NewOrderRequestPayloadProvider();
        Order reqPayload = requestPayloadProvider.getRandomNewOrderRequestPayloadFTL();
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        Integer code = response.jsonPath().get("code");
        String status = response.jsonPath().get("status");
        String responseDesc = response.jsonPath().get("response");

        Assert.assertNotNull(responseDesc, "Order ID is returned null. " +
                "\n Response code: ["+code+"] " +
                "\n Status: ["+status+"] " +
                "\n response: ["+responseDesc+"]");
    }
}
