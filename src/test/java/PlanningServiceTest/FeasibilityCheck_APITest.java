package PlanningServiceTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

public class FeasibilityCheck_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("PLANNING_SERVICE_KONG_URL")).
                basePath(TestConfig.get("FEASIBILITY_CHECK_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void planning_order_list_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"orderId\":\"0023761 \",\"driverId\":\"ACBECKWI\",\"tractorId\":\"103336\"," +
                "\"coDriver\":\"\",\"trailerId\":\"\"," +
                "\"movement\":{\"tractorId\":\"103336\",\"companyId\":\"T100\"," +
                "\"scheduledPickup\":\"2021-12-17T21:23:00.000Z\",\"scheduledDrop\":\"2021-12-22T21:23:00.000Z\"," +
                "\"movementId\":\"2549576                         \",\"destinationLatitude\":35.2833," +
                "\"destinationLongitude\":81.4558,\"originLatitude\":44.9022,\"originLongitude\":93.0506,\"coDriver\":\"\"}}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "HealthCheck"})
    public void verify_drive_time_feasibility_status_is_returned() throws Exception {
        boolean expectedFeasibilityStatus = true;
        String expectedMessage = "Drive Time for the Order is sufficient";
        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"orderId\":\"0023761 \",\"driverId\":\"ACBECKWI\",\"tractorId\":\"103336\"," +
                "\"coDriver\":\"\",\"trailerId\":\"\"," +
                "\"movement\":{\"tractorId\":\"103336\",\"companyId\":\"T100\"," +
                "\"scheduledPickup\":\"2021-12-17T21:23:00.000Z\",\"scheduledDrop\":\"2021-12-22T21:23:00.000Z\"," +
                "\"movementId\":\"2549576                         \",\"destinationLatitude\":35.2833," +
                "\"destinationLongitude\":81.4558,\"originLatitude\":44.9022,\"originLongitude\":93.0506,\"coDriver\":\"\"}}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);

        boolean retrievedFeasibilityStatus = response.jsonPath().get("response.driveTimeFeasibility.feasibilityStatus");
        String retrievedMessage = response.jsonPath().get("response.driveTimeFeasibility.message");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(retrievedFeasibilityStatus,expectedFeasibilityStatus,
                "Expected Drive time feasibility status: ["+expectedFeasibilityStatus+"] but API returned ["+retrievedFeasibilityStatus+"]");
        softAssert.assertEquals(retrievedMessage,expectedMessage,
                "Expected Drive time feasibility message: ["+expectedMessage+"] but API returned ["+retrievedMessage+"]");
        softAssert.assertAll();
    }
}
