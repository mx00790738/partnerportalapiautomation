package PlanningServiceTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import requestPayloadProvider.PlanningMicroservice.PlanningOrderList.PlanningOrderListRequestPayloadProvider;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.Planning.PlanningOrderList.PlanningOrder;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class PlanningOrderList_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("PLANNING_SERVICE_KONG_URL")).
                basePath(TestConfig.get("PLANNING_ORDER_LIST_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void planning_order_list_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        PlanningOrderListRequestPayloadProvider reqPayload = new PlanningOrderListRequestPayloadProvider();

        requestSpecification.body(reqPayload.getPlanningOrderListRequestPayload());

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_orders_in_planning_list_has_planningPending_Y() throws Exception {
        String expectedPlanningPendingValue = "Y";
        RequestSpecification requestSpecification = this.requestSpecification;
        PlanningOrderListRequestPayloadProvider reqPayload = new PlanningOrderListRequestPayloadProvider();

        requestSpecification.body(reqPayload.getPlanningOrderListRequestPayload());

        Response response = RequestMethod.POST(requestSpecification);

        //TODO- Write a common method in base test
        List<PlanningOrder> listOfOrders = response.jsonPath().getList("response.response",PlanningOrder.class);

        SoftAssert softAssert = new SoftAssert();
        for(PlanningOrder order: listOfOrders){
            String retrievedPlanningPendingValue = order.getPlanningPending();
            softAssert.assertEquals(retrievedPlanningPendingValue, expectedPlanningPendingValue, "Order: "+
                    order.getOrderNumber() + "planningPending is not returned as Y");
        }
        softAssert.assertAll();
    }
}
