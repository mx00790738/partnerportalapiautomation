package PlanningServiceTest;

import BaseTest.BaseTest;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.BusinessResponse;
import pojoClasses.responsePojo.Planning.FeasibilityCheck.AdditionalEquipment;
import pojoClasses.responsePojo.ResponseStatus;
import testConfiguration.TestConfig;

import java.util.List;

public class EquipmentCheck_APITest extends BaseTest {
    private RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("PLANNING_SERVICE_KONG_URL")).
                basePath(TestConfig.get("EQUIPMENT_CHECK_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
    }

    @Test(groups = { "HealthCheck"})
    public void equipment_check_API_health_check() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"orderId\":\"0023571 \",\"driverId\":\"DTAYLOR \"," +
                "\"tractorId\":\"102026  \",\"trailerId\":\"11388   \",\"movement\":{}}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        BusinessResponse responsePayload = response.as(BusinessResponse.class);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.getStatusCode(), 200,
                "Expected status code: 200 instead API returned status code: " + response.getStatusCode());

        softAssert.assertEquals(responsePayload.getCode(), 200,
                "Expected code: 200 in response payload instead API returned status code: "
                        + responsePayload.getCode());

        String status = (responsePayload.getStatus() == null) ? "NULL" : responsePayload.getStatus();
        softAssert.assertEquals(status, "SUCCESS",
                "Expected status: SUCCESS in response payload instead API returned status: "
                        + responsePayload.getStatus());

        ResponseStatus responseStatus = responsePayload.getResponseStatus();
        if (responseStatus != null) {
            String responseCode = (responsePayload.getResponseStatus().getResponseCode() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseCode();
            softAssert.assertEquals(responseCode, "000",
                    "Expected responseCode: 000 in response payload instead API returned responseCode: "
                            + responseCode);
            String responseDesc = (responsePayload.getResponseStatus().getResponseDesc() == null) ? "NULL" :
                    responsePayload.getResponseStatus().getResponseDesc();
            softAssert.assertEquals(responseDesc, "SUCCESS",
                    "Expected responseDesc: SUCCESS in response payload instead API returned responseDesc: "
                            + responseDesc);
        } else {
            softAssert.assertNotNull(responseStatus, "Response status obj is returned NULL");
        }
        softAssert.assertAll();
    }

    @Test(groups = { "Sanity"})
    public void verify_equipment_check_for_order_0023571_returns_driver_equip_REFD() throws Exception {
        RequestSpecification requestSpecification = this.requestSpecification;
        String reqPayload = "{\"orderId\":\"0023571 \",\"driverId\":\"DTAYLOR \"," +
                "\"tractorId\":\"102026  \",\"trailerId\":\"11388   \",\"movement\":{}}";
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        List<AdditionalEquipment> retrievedAddEquipList = parseResponseToObjectList(response,AdditionalEquipment.class);
        AdditionalEquipment retrievedAddEquip =
                retrieveObjectFromObjectList(retrievedAddEquipList,"getEquipTypeId","REFD");
        String retrievedAddEquipID = retrievedAddEquip.getEquipTypeId();
        String expectedAddEquipID = "REFD";
        Assert.assertEquals(retrievedAddEquipID, expectedAddEquipID,
                "Expected equipTypeId: ["+expectedAddEquipID+"] but API returned ["+retrievedAddEquipID+"]");
    }
}
