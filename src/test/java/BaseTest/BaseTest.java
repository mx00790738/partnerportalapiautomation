package BaseTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import pojoClasses.responsePojo.BusinessResponse;
import testConfiguration.TestConfig;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public abstract class BaseTest {
    private final Logger logger = LoggerFactory.getLogger("");

    @BeforeSuite(alwaysRun = true)
    public void setUpSuite() throws Exception {
        logger.info("\n\n [TEST CONFIGURATION] "
                +"\n\n TEST ENVIRONMENT: "+TestConfig.getEnv()
                +"\n HOST: "+TestConfig.get("HOST")
                +"\n USER NAME: "+TestConfig.get("USER_NAME")
                +"\n PASSWORD: "+TestConfig.get("PASSWORD")
                +"\n\n");
    }

    @BeforeClass(alwaysRun = true)
    public void logClassName() {
        logger.info("================ "+this.getClass().getSimpleName()+" ================");
    }

    @BeforeMethod(alwaysRun = true)
    public void logTestName(Method method)
    {
        logger.info("");
        logger.info("Test Name: " + method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void logTestResult(ITestResult result) {
        switch (result.getStatus()) {
            case ITestResult.SUCCESS:
                logger.info("Test Result: PASSED");
                logger.info("");
                break;

            case ITestResult.FAILURE:
                logger.info("Test Result: FAILED");
                logger.debug(result.getThrowable().getMessage());
                logger.info("");
                break;

            case ITestResult.SKIP:
                logger.info("Test Result: SKIPPED");
                logger.debug(result.getSkipCausedBy().toString());
                logger.info("");
                break;

            default:
                throw new RuntimeException("Invalid status");
        }
    }

    public <T> List<T> parseResponseToObjectList(Response response, Class<T> classOnWhichArrayIsDefined) throws Exception {
        BusinessResponse<List<T>> responsePayload = response.as(BusinessResponse.class);
        if(responsePayload.getResponse()==null){
            throw new Exception("Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        ObjectMapper mapper = new ObjectMapper();
        String json=null;
        try {
            json = mapper.writeValueAsString(responsePayload.getResponse());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + classOnWhichArrayIsDefined.getName() + ";");
        T[] objects = mapper.readValue(json, arrayClass);
        return Arrays.asList(objects);
    }

    public <T> T parseResponseToObject(Response response, Class<T> classOnWhichObjIsDefined) throws Exception {
        BusinessResponse<T> responsePayload = response.as(BusinessResponse.class);
        if(responsePayload.getResponse()==null){
            throw new Exception("Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        ObjectMapper mapper = new ObjectMapper();
        String json=null;
        try {
            json = mapper.writeValueAsString(responsePayload.getResponse());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        T object = mapper.readValue(json, classOnWhichObjIsDefined);
        return object;
    }

    public <T> T retrieveObjectFromObjectList(List<T> objList, String methodName, String arg1)
            throws Exception {
        T obj = null;
        for(T o:objList) {
            Method method = o.getClass().getMethod(methodName);
            if (method.invoke(o, null).equals(arg1)) {
                obj = o;
            }
        }
        if(obj == null){
            throw new Exception("Unexpected!! Record with "+methodName.substring(3)+": ["+ arg1 +"] not found");
        }
        return obj;
    }
}
