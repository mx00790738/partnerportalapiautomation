package testConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utilities.PropertyFileReader;

import java.util.HashMap;

public class TestConfig{
    private static final Logger logger = LoggerFactory.getLogger("TestConfig");
    private static String targetEnv = System.getProperty("env");
    private static String configFileName;
    private static HashMap<String, String> config;

    private static HashMap<String,String> loadConfig(){
        if(config!=null) {
            return config;
        }
        try {
            if (targetEnv == null) {
                configFileName = "testQA.properties";
                targetEnv = "QA";
                logger.info("No targeted env specified. Defaulting to QA");
            }else {
               switch(targetEnv){
                   case "dev":
                       configFileName = "testDev.properties";
                       targetEnv = "DEV";
                       break;
                   case "qa":
                       configFileName = "testQA.properties";
                       targetEnv = "QA";
                       break;
                   case "beta":
                       configFileName = "testBeta.properties";
                       targetEnv = "BETA";
                       break;
                   case "local":
                       configFileName = "testLocal.properties";
                       targetEnv = "LOCAL";
                       break;
                   default:
                       throw new Exception("Incorrect parameter value supplied for targetEnv. " +
                               "Allowed values are dev, qa, beta and local");
               }
            }
            config = PropertyFileReader.getTestConfigFromFile(configFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return config;
    }

    public static String get(String key) throws Exception {
        String value = loadConfig().get(key);
        if (value==null){
            throw new Exception("Value for key: "+key+" is not defined in config file");
        }
        return value;
    }

    public static String getEnv() {
        loadConfig();
        return targetEnv;
    }
}
