package testConfiguration;

/**
 * @author MX0C92049
 *
 */
public class PATH {
	PATH(){}
	public static final String LOGIN_NEW = "/crst/auth/login_new/";
	public static final String GET_USER = "/crst/auth/get-user/";
	public static final String COMMENT_TYPES = "/crst/agent/commentTypes";
	public static final String RESPONSIBILITY_CODE_CHECK = "/crst/agent/responsiblityCodeCheck";
	public static final String SERVICE_FAILURE_REASON_LIST = "/crst/agent/service-failure-reason-list";
	public static final String NEW_ORDER = "/agent/neworder";
	public static final String EQUIPMENT_TYPE_LIST = "/crst/agent/equipment-type-list";
	public static final String TEMP_AGENCY_DTLS_1001MGIL = "/crst/agent/agencyDtlsForUser/1001    /1001mgil";
	public static final String OPERATION_USER = "/crst/agent/operationagentuser/";
	public static final String CUSTOMER_MASTER = "/agent/masterdata/customers/";
	public static final String LOCATION_MASTER = "/agent/location-list-master/";
	public static final String PRODUCT_LIST_SEARCH_BY_ID = "/agent/productListSearchById";
	public static final String SUBJECT_ORDER_LIST = "/agent/portal/orders/subjectOrder-list";
	
	//Microservice endpoints
	public static final String MASTER_DATA_CUSTOMER_SERACH = "/crst/agent/masterdata/customer/search";
	public static final String MASTER_DATA_CUSTOMER_DETAILS = "/crst/agent/masterdata/customer/{id}/details";//User need to replace {id}
	public static final String MASTER_DATA_CUSTOMER_CONTACTS = "/crst/agent/masterdata/customer/{id}/contacts";//User need to replace {id}
	public static final String MASTER_DATA_CUSTOMER_COMMENTS = "/crst/agent/masterdata/customer/{id}/comments";//User need to replace {id}
	public static final String MASTER_DATA_CUSTOMER_DIRECT = "/crst/agent/masterdata/customer/direct";
	
	public static final String COMMODITY_MASTER_DATA_COMMODITY = "/crst/agent/masterdata/commodity/customer/{id}";//User need to replace {id}
	
	public static final String FUEL_MASTER_DATA_GET_FUEL_SURCHARHE = "/crst/agent/masterdata/fuel/customer/{id}";//User need to replace {id}

	public static final String LOCATION_MASTER_DATA_SERACH_ENDPOINT = "/crst/agent/masterdata/location/search";
}
