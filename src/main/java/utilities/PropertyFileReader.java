package utilities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class PropertyFileReader {
    private static HashMap<String,String> configMap;
    private static FileReader readPropertiesFile(String propertyFile) throws Exception {
        FileReader reader = null;
        try {
            reader = new FileReader(propertyFile);//set it as a constant in framework const file
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new Exception("Property file not found");
            //ADD LOG4J2 LOG LINE HERE
        }
        return reader;
    }

    public static HashMap<String,String> getTestConfigFromFile(String propertyFile) throws Exception {
        FileReader reader = readPropertiesFile(propertyFile);
        Properties prop = new Properties();
        try {
            prop.load(reader);
        } catch (IOException e) {
            throw new Exception("IO Exception when loading test configuration from config file");
        }
        configMap = new HashMap<String,String>();
        Set<Map.Entry<Object, Object>> entries = prop.entrySet();
        Iterator<Map.Entry<Object, Object>> itr = entries.iterator();
        while(itr.hasNext()){
            Map.Entry<Object, Object> entry = itr.next();
            configMap.put(entry.getKey().toString(),entry.getValue().toString());
        }
        //System.out.println(configMap);
        return configMap;
    }
}
