package utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {
	/*
	 * @param int count to increment the date
	 * @return String Incremented date in yyyy-MM-dd'T'HH:mm:ss.SSS'Z' 
	 */
	public static String getDateTimeAfterFormat1(int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");      
		LocalDateTime date = LocalDateTime.now().plusDays(count);
		return date.format(formatter);
	}
	
	/*
	 * @param int count to increment the date
	 * @return String Incremented date in MM/dd/yyyy' 'HH:mm 
	 */
	public static String getDateTimeAfterFormat2(int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy' 'HH:mm");      
		LocalDateTime date = LocalDateTime.now().plusDays(count);
		return date.format(formatter);
	}
	
	/*
	 * @param int count to increment the date
	 * @return String Incremented date in MM-dd-yyyy' 'HH:mm
	 */
	public static String getDateTimeAfterFormat3(int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy' 'HH:mm");      
		LocalDateTime date = LocalDateTime.now().plusDays(count);
		return date.format(formatter);
	}

	/*
	 * @param int count to increment the date
	 * @return String Incremented date in MM/dd/yyyy
	 */
	public static String getDateTimeAfterFormat4(int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime date = LocalDateTime.now().plusDays(count);
		return date.format(formatter);
	}

	/*
	 * @param int count to increment the date
	 * @return String Incremented date
	 */
	public static String incrementGivenDateBy(String dateStr, int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		LocalDateTime date = LocalDateTime.parse(dateStr, formatter).plusDays(count);
		return date.format(formatter);
	}

	/*
	 * @param int unit to increment the time. 1 unit equal to 1hr
	 * @return String Incremented time
	 */
	public static String incrementGivenTimeBy(String dateStr, int count) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		LocalDateTime date = LocalDateTime.parse(dateStr, formatter).plusHours(count);
		return date.format(formatter);
	}
}
