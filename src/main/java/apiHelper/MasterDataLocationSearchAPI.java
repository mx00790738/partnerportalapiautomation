package apiHelper;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.MasterData.Location.LocationSearchListEntry;
import testConfiguration.TestConfig;

import java.util.List;

public class MasterDataLocationSearchAPI {
    private RequestSpecification requestSpecification;

    public List<LocationSearchListEntry> getLocationList() throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST_MICROSERVICE")).
                basePath(TestConfig.get("LOCATION_MASTER_DATA_SEARCH_API_PATH") + "?stopType=").
                header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
                        "Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=")).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
        Response response = RequestMethod.GET(requestSpecification);
        List<LocationSearchListEntry> locList = response.jsonPath().getList("response", LocationSearchListEntry.class);

        return locList;
    }

    public LocationSearchListEntry getLocation(String locationID) throws Exception {
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST_MICROSERVICE")).
                basePath(TestConfig.get("LOCATION_MASTER_DATA_SEARCH_API_PATH") + "?stopType=").
                header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
                        "Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=")).
                headers(ApiHeaders.getCommentTypesRequestHeaders());
        Response response = RequestMethod.GET_DEBUG(requestSpecification);
        List<LocationSearchListEntry> locList = response.jsonPath().getList("response", LocationSearchListEntry.class);
        LocationSearchListEntry retrievedLoc = null;
        for (LocationSearchListEntry loc : locList) {
            if (loc.getId().equals(locationID)) {
                retrievedLoc= loc;
            }
        }
        return retrievedLoc;
    }
}
