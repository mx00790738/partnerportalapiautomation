package apiHelper;

import static io.restassured.RestAssured.given;

import io.restassured.parsing.Parser;
import requestHelper.RequestMethod;
import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class GetUserAPI {
	private Response response;
	public GetUserAPI() throws Exception {
		RestAssured.defaultParser = Parser.JSON;
		RequestSpecification requestSpecification = RestAssured.given().
				baseUri(TestConfig.get("HOST")).
				basePath(TestConfig.get("GET_USER")).
				header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=")).
				headers(ApiHeaders.getUserRequestHeaders());
		String getUserBody = "{\"loginId\":\"1001mgil\"}";
		requestSpecification.body(getUserBody);
		this.response = RequestMethod.POST(requestSpecification);
	}

	public Response getGetUserAPIResponse() {
		return this.response;
	}
	
	public String getUserType() {
		Response response = getGetUserAPIResponse();
		String agencyResponsibilityCode = response.body().jsonPath().get("response.userType");
		return agencyResponsibilityCode;
	}
	
	public Object getDirectCustomerInd() {
		Response response = getGetUserAPIResponse();
		String directCustomerInd = response.body().jsonPath().get("response.directCustomerInd");
		return directCustomerInd;
	}
	
	public String getAgencyId() {
		Response response = getGetUserAPIResponse();
		String agencyId = response.body().jsonPath().get("response.agencyId");
		return agencyId;
	}
	
	public String getMcleodUserId() {
		Response response = getGetUserAPIResponse();
		String mcleodUserId = response.body().jsonPath().get("response.mcleodUserId");
		return mcleodUserId;
	}
}
