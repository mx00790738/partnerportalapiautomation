package apiHelper;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.requestPojo.NeedRevisit.Order;
import requestHelper.RequestMethod;
import requestPayloadProvider.CreateOrderMicroservice.NewOrder.NewOrderRequestPayloadProvider;
import testConfiguration.TestConfig;

public class NewOrderAPI {
    private RequestSpecification requestSpecification;
    public String createFlatbedFTLOrder() throws Exception {
        RequestSpecification requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("CREATE_ORDER_SERVICE_KONG_URL")).
                basePath(TestConfig.get("NEW_ORDER_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());

        NewOrderRequestPayloadProvider requestPayloadProvider = new NewOrderRequestPayloadProvider();
        Order reqPayload = requestPayloadProvider.getNewOrderRequestPayloadFTLForFLAT();
        requestSpecification.body(reqPayload);

        Response response = RequestMethod.POST(requestSpecification);
        String orderID=response.jsonPath().get("response");
        return orderID;
    }
}
