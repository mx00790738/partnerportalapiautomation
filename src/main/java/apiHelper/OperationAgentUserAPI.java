package apiHelper;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class OperationAgentUserAPI {
	private Response response;
	public OperationAgentUserAPI() throws Exception {
		GetUserAPI getUserAPI = new GetUserAPI();
		RequestSpecification requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST"))
				.basePath(PATH.OPERATION_USER+getUserAPI.getAgencyId())
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getUserRequestHeaders());//change header

		this.response = given().
				spec(requestSpecification).
			when().
				get();
	}
	public Response getOperationagentUserAPIResponse() {	
		return this.response;
	}
	
	public ArrayList<Object> getOperationUserList() {
		Response response = getOperationagentUserAPIResponse();
		ArrayList<Object> opsUserList = new ArrayList<Object>();
		opsUserList	= (ArrayList<Object>) response.jsonPath().getList("response.userId");//recheck the working
		return opsUserList;
	}
}
