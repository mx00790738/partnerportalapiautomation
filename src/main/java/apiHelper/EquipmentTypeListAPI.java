package apiHelper;

import static io.restassured.RestAssured.given;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class EquipmentTypeListAPI {
	
	public Response getEquipmentTypeList() throws Exception {
	RequestSpecification requestSpecification = RestAssured.given()
			.baseUri(TestConfig.get("HOST_DEV"))
			.basePath(PATH.EQUIPMENT_TYPE_LIST)
			.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
			.headers(ApiHeaders.getEquipmentTypeRequestHeaders());
	
	Response response = given().
			spec(requestSpecification).
		when().
			get();
		return response;
	}
}
