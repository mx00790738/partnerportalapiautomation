package apiHelper;

import static io.restassured.RestAssured.given;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.responsePojo.MasterData.Location.LocationDetails;
import testConfiguration.TestConfig;

public class MasterDataLocationAPI {
	static RequestSpecification requestSpecification;

	public LocationDetails getLocation(String locationID) throws Exception {
		requestSpecification = RestAssured.given().
				baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
				basePath(PATH.LOCATION_MASTER+locationID).
				header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE=")).
				headers(ApiHeaders.getCommentTypesRequestHeaders());
		Response response = given().
				spec(requestSpecification).
				when().
				get();
		LocationDetails locDetails = response.jsonPath().getObject("response", LocationDetails.class);

		return locDetails;
	}
}
