package apiHelper;

import static io.restassured.RestAssured.given;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class ResponsibilityCodeCheckApi {
	private Response response;
	
	public ResponsibilityCodeCheckApi() throws Exception {
		RequestSpecification requestSpecification = RestAssured.given().baseUri(TestConfig.get("HOST"))
				.basePath(PATH.RESPONSIBILITY_CODE_CHECK)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil",
						"Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getResponsibilityCodeCheckRequestHeaders());

		String requestBody = "{\"agencyId\":\"1001\"}";
		this.response = given().
			spec(requestSpecification).
			body(requestBody).
		when().
			post();
	}
	
	public Response getResponsibilityCodeCheckResponse() {	
		return this.response;
	}
	
	public String getResponsibilityCode() {
		Response response = getResponsibilityCodeCheckResponse();
		String agencyResponsibilityCode = response.body().jsonPath().get("response.agencyResponsibilityCode");
		return agencyResponsibilityCode;
	}
}
