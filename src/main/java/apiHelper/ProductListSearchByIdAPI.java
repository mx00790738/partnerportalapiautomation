/**
 * 
 */
package apiHelper;

import static io.restassured.RestAssured.given;

import java.util.List;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.responsePojo.ProductListByIDEntry;
import testConfiguration.TestConfig;

/**
 * @author MX0C92049
 *
 */
public class ProductListSearchByIdAPI {
	private Response response;
	public ProductListSearchByIdAPI() throws Exception {
		RequestSpecification requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST_DEV"))
				.basePath(PATH.PRODUCT_LIST_SEARCH_BY_ID)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getUserRequestHeaders())//change header
				.body("{}");

		this.response = given().
				spec(requestSpecification).
			when().
				post();
	}
	
	public List<ProductListByIDEntry> getProductList(){
		List<ProductListByIDEntry> productList = this.response.jsonPath().getList("response", ProductListByIDEntry.class);
		return productList;
	}
	
	public ProductListByIDEntry getProductDetails(String product){
		List<ProductListByIDEntry> productList =  getProductList();
		for(ProductListByIDEntry p: productList) {
			if(p.getId().trim().equals(product)) {
				return p;
			}
		}
		return null;
	}
}
