package apiHelper;

import static io.restassured.RestAssured.given;

import java.util.List;

import requestHelper.RequestMethod;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.requestPojo.NeedRevisit.CustomerRequestPayload;
import pojoClasses.responsePojo.MasterData.Customer.Customer;
import testConfiguration.TestConfig;

public class MasterDataCustomerAPI {
	
	public Customer getCustomer(String customerID) throws Exception {
		RequestSpecification requestSpecification;
		//GetUserAPI getUserAPI = new GetUserAPI();
		CustomerRequestPayload customerRequestPayload = new CustomerRequestPayload();
		//customerRequestPayload.setAgencyId(getUserAPI.getAgencyId());
		customerRequestPayload.setAgencyId("1001    ");
		customerRequestPayload.setCustomerName("");
		//customerRequestPayload.setUserType(getUserAPI.getUserType());
		customerRequestPayload.setUserType("AGFT");
		customerRequestPayload.setCustomerId(customerID);
		customerRequestPayload.setCity("");
		customerRequestPayload.setState("");
		customerRequestPayload.setZip("");
		customerRequestPayload.setAddress("");

		requestSpecification = RestAssured.given().
				baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
				basePath(TestConfig.get("CUSTOMER_SEARCH_API_PATH")).
				header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
						TestConfig.get("PASSWORD"))).
				headers(ApiHeaders.getRequestHeaders());
		requestSpecification.body(customerRequestPayload);
		Response response = RequestMethod.POST(requestSpecification);
				//given().spec(requestSpecification).body(customerRequestPayload).when().post();
		List<Customer> customerList = response.jsonPath().getList("response", Customer.class);
		
		return customerList.get(0);
	}
}
