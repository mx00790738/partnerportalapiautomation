package apiHelper;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import pojoClasses.responsePojo.MasterData.Location.LocationDetails;
import testConfiguration.TestConfig;

public class MasterDataLocationDetailsAPI {
    private RequestSpecification requestSpecification;

    public LocationDetails getLocationDetails(String locationID) throws Exception {
        String path= TestConfig.get("LOCATION_DETAILS_API_PATH");
        requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("MASTER_FILES_KONG_URL")).
                basePath(path.replace("{id}",locationID)).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());
        Response response = RequestMethod.GET(requestSpecification);
        LocationDetails locDetails = response.jsonPath().getObject("response", LocationDetails.class);
        return locDetails;
    }
}
