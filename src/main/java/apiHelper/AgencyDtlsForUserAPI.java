package apiHelper;

import static io.restassured.RestAssured.given;

import testConfiguration.PATH;
import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testConfiguration.TestConfig;

public class AgencyDtlsForUserAPI {
	private Response response;
	public AgencyDtlsForUserAPI() throws Exception {
		RequestSpecification requestSpecification = RestAssured.given()
				.baseUri(TestConfig.get("HOST"))
				.basePath(PATH.TEMP_AGENCY_DTLS_1001MGIL)
				.header(AuthorizationHeader.getAuthorizationHeader("1001mgil","Rc24H25oh+HDijR4QWZBXtu3Njp3jYTg1bfB7OwqjIE="))
				.headers(ApiHeaders.getUserRequestHeaders());//change header

		this.response = given().
				spec(requestSpecification).
			when().
				get();
	}
	public Response getAgencyDtlsForUserAPIResponse() {		
		return this.response;
	}
	
	public String getCompanyId() {
		Response response = getAgencyDtlsForUserAPIResponse();
		String comapnyId = response.body().jsonPath().getString("response.comapnyId[0]");
		return comapnyId;
	}
	
	public String getAgencyPayeeId() {
		Response response = getAgencyDtlsForUserAPIResponse();
		String agencyPayeeId = response.body().jsonPath().getString("response.agencyPayeeId[0]");
		return agencyPayeeId;
	}
}
