package apiHelper;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.requestPojo.DispatchMicroservice.Assignment.AssignmentDetails;
import requestHelper.RequestMethod;
import requestPayloadProvider.DispatchMicroservice.AssignmentRequestPayloadProvider;
import testConfiguration.TestConfig;

public class DTTAssignmentAPI {
    public void assignDriverTractorTrailerToOrder(String orderID,String driver1,String tractor1, String trailer1) throws Exception {
        RequestSpecification requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST")+TestConfig.get("DISPATCH_SERVICE_KONG_URL")).
                basePath(TestConfig.get("ASSIGNMENT_API_PATH")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());

        AssignmentRequestPayloadProvider assignmentRequestPayloadProvider = new AssignmentRequestPayloadProvider();
        AssignmentDetails reqPayload = assignmentRequestPayloadProvider.getAssignmentRequestPayloadForOrder(orderID,driver1,tractor1,trailer1);
        requestSpecification.body(reqPayload);
        Response response = RequestMethod.POST(requestSpecification);
    }
}
