package apiHelper;

import headerHelper.ApiHeaders;
import headerHelper.AuthorizationHeader;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestHelper.RequestMethod;
import testConfiguration.TestConfig;

public class GetOrderAPI {
    public Response getOrderDetails(String orderID) throws Exception {
        RequestSpecification requestSpecification = RestAssured.given().
                baseUri(TestConfig.get("HOST") + TestConfig.get("AGENT_PORTAL_SERVICE_KONG_URL")).
                header(AuthorizationHeader.getAuthorizationHeader(TestConfig.get("USER_NAME"),
                        TestConfig.get("PASSWORD"))).
                headers(ApiHeaders.getRequestHeaders());

        String path = TestConfig.get("GET_ORDER_API_PATH");
        requestSpecification.basePath(path.replace("{order_id}", orderID));

        Response response = RequestMethod.GET(requestSpecification);
        return response;
    }
}
