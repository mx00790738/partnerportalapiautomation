/**
 * 
 */
package apiManager;

import apiHelper.*;

/**
 * @author MX0C92049
 *
 */
public class APIManager {
	private static AgencyDtlsForUserAPI agencyDtlsForUserAPIInstance;
	private static GetUserAPI getUserAPIInstance_;
	private static OperationAgentUserAPI operationAgentUserAPIInstance;
	private static ResponsibilityCodeCheckApi responsibilityCodeCheckApi;
	private static GetOrderAPI getOrderAPIInstance;
	private static NewOrderAPI newOrderAPIInstance;
    private static GetPickupLogAPI getPickupLogAPIInstance;
    private static DTTAssignmentAPI getDTTAssignmentAPIInstance;

	public static AgencyDtlsForUserAPI getAgencyDtlsForUserAPIInstance() throws Exception {
		if (agencyDtlsForUserAPIInstance == null) {
			synchronized (AgencyDtlsForUserAPI.class) {
				if (agencyDtlsForUserAPIInstance == null) {
					agencyDtlsForUserAPIInstance = new AgencyDtlsForUserAPI();
				}
			}
		}
		return agencyDtlsForUserAPIInstance;
	}

	public static GetUserAPI getUserAPIInstance() throws Exception {
		if (getUserAPIInstance_ == null) {
			synchronized (GetUserAPI.class) {
				if (getUserAPIInstance_ == null) {
					getUserAPIInstance_ = new GetUserAPI();
				}
			}
		}
		return getUserAPIInstance_;
	}

	public static OperationAgentUserAPI getOperationAgentUserAPIInstance() throws Exception {
		if (operationAgentUserAPIInstance == null) {
			synchronized (OperationAgentUserAPI.class) {
				if (operationAgentUserAPIInstance == null) {
					operationAgentUserAPIInstance = new OperationAgentUserAPI();
				}
			}
		}
		return operationAgentUserAPIInstance;
	}

	public static ResponsibilityCodeCheckApi getResponsibilityCodeCheckApiInstance() throws Exception {
		if (responsibilityCodeCheckApi == null) {
			synchronized (ResponsibilityCodeCheckApi.class) {
				if (responsibilityCodeCheckApi == null) {
					responsibilityCodeCheckApi = new ResponsibilityCodeCheckApi();
				}
			}
		}
		return responsibilityCodeCheckApi;
	}

	public static GetOrderAPI getGetOrderAPIApiInstance() throws Exception {
		if (getOrderAPIInstance == null) {
			synchronized (GetOrderAPI.class) {
				if (getOrderAPIInstance == null) {
					getOrderAPIInstance = new GetOrderAPI();
				}
			}
		}
		return getOrderAPIInstance;
	}

	public static NewOrderAPI getNewOrderAPIInstance() throws Exception {
		if (newOrderAPIInstance == null) {
			synchronized (NewOrderAPI.class) {
				if (newOrderAPIInstance == null) {
					newOrderAPIInstance = new NewOrderAPI();
				}
			}
		}
		return newOrderAPIInstance;
	}

    public static GetPickupLogAPI getPickupLogAPIInstance() throws Exception {
        if (getPickupLogAPIInstance == null) {
            synchronized (GetPickupLogAPI.class) {
                if (getPickupLogAPIInstance == null) {
                    getPickupLogAPIInstance = new GetPickupLogAPI();
                }
            }
        }
        return getPickupLogAPIInstance;
    }

    public static DTTAssignmentAPI getDTTAssignmentAPIInstance() throws Exception {
        if (getDTTAssignmentAPIInstance == null) {
            synchronized (DTTAssignmentAPI.class) {
                if (getDTTAssignmentAPIInstance == null) {
                    getDTTAssignmentAPIInstance = new DTTAssignmentAPI();
                }
            }
        }
        return getDTTAssignmentAPIInstance;
    }
}
