/**
 * 
 */
package pojoClasses.responsePojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class CustomerGetFuelSurchargeAPIBussinessResponse {
	private String status;
    private List<CustomerFuelSurcharge> customerFuelSurcharge = new ArrayList<CustomerFuelSurcharge>();;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<CustomerFuelSurcharge> getResponse() {
        return customerFuelSurcharge;
    }
    public void setResponse(List<CustomerFuelSurcharge> customerFuelSurcharge) {
        this.customerFuelSurcharge = customerFuelSurcharge;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
