/**
 * 
 */
package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author MX0C92049
 *
 */
public class ExceptionDetails {
	private String errCode;
    private String errorDetails;
    private String errorType;
    private String errorDescription;
    public String getErrCode() {
        return errCode;
    }
    @JsonSetter("errCode")
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
    public String getErrorDetails() {
        return errorDetails;
    }
    @JsonSetter("errorDetails")
    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }
    public String getErrorType() {
        return errorType;
    }
    @JsonSetter("errorType")
    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
    public String getErrorDescription() {
        return errorDescription;
    }
    @JsonSetter("errorDescription")
    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public String toString(){
        String exceptionDetailsInString = "Error code: "+errCode+"\nError Type: "+errorType+
                "\nError Description: "+errorDescription +"\nError Details: "+errorDetails;
        return exceptionDetailsInString;
    }
}
