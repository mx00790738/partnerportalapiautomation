package pojoClasses.responsePojo.Planning.PlanningOrderList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderStopEntity {
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("address")
    public Object getAddress() {
        return this.address; }
    public void setAddress(Object address) {
        this.address = address; }
    Object address;
    @JsonSetter("orderId")
    public String getOrderId() {
        return this.orderId; }
    public void setOrderId(String orderId) {
        this.orderId = orderId; }
    String orderId;
    @JsonSetter("cityName")
    public String getCityName() {
        return this.cityName; }
    public void setCityName(String cityName) {
        this.cityName = cityName; }
    String cityName;
    @JsonSetter("state")
    public String getState() {
        return this.state; }
    public void setState(String state) {
        this.state = state; }
    String state;
    @JsonSetter("zipCode")
    public String getZipCode() {
        return this.zipCode; }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode; }
    String zipCode;
    @JsonSetter("orderSequence")
    public Object getOrderSequence() {
        return this.orderSequence; }
    public void setOrderSequence(Object orderSequence) {
        this.orderSequence = orderSequence; }
    Object orderSequence;
    @JsonSetter("actualArrival")
    public Object getActualArrival() {
        return this.actualArrival; }
    public void setActualArrival(Object actualArrival) {
        this.actualArrival = actualArrival; }
    Object actualArrival;
    @JsonSetter("actualDeparture")
    public Object getActualDeparture() {
        return this.actualDeparture; }
    public void setActualDeparture(Object actualDeparture) {
        this.actualDeparture = actualDeparture; }
    Object actualDeparture;
    @JsonSetter("eta")
    public Object getEta() {
        return this.eta; }
    public void setEta(Object eta) {
        this.eta = eta; }
    Object eta;
    @JsonSetter("stopType")
    public String getStopType() {
        return this.stopType; }
    public void setStopType(String stopType) {
        this.stopType = stopType; }
    String stopType;
    @JsonSetter("serviceFailComment")
    public Object getServiceFailComment() {
        return this.serviceFailComment; }
    public void setServiceFailComment(Object serviceFailComment) {
        this.serviceFailComment = serviceFailComment; }
    Object serviceFailComment;
    @JsonSetter("serviceFailMinutesLate")
    public Object getServiceFailMinutesLate() {
        return this.serviceFailMinutesLate; }
    public void setServiceFailMinutesLate(Object serviceFailMinutesLate) {
        this.serviceFailMinutesLate = serviceFailMinutesLate; }
    Object serviceFailMinutesLate;
    @JsonSetter("scheduledArrival")
    public String getScheduledArrival() {
        return this.scheduledArrival; }
    public void setScheduledArrival(String scheduledArrival) {
        this.scheduledArrival = scheduledArrival; }
    String scheduledArrival;
    @JsonSetter("scheduledDeparture")
    public String getScheduledDeparture() {
        return this.scheduledDeparture; }
    public void setScheduledDeparture(String scheduledDeparture) {
        this.scheduledDeparture = scheduledDeparture; }
    String scheduledDeparture;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
}
