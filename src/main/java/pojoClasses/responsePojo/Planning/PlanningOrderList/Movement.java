package pojoClasses.responsePojo.Planning.PlanningOrderList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movement {
    @JsonSetter("brokeage")
    public String getBrokeage() {
        return this.brokeage; }
    public void setBrokeage(String brokeage) {
        this.brokeage = brokeage; }
    String brokeage;
    @JsonSetter("planningPending")
    public Object getPlanningPending() {
        return this.planningPending; }
    public void setPlanningPending(Object planningPending) {
        this.planningPending = planningPending; }
    Object planningPending;
    @JsonSetter("dispatchPending")
    public Object getDispatchPending() {
        return this.dispatchPending; }
    public void setDispatchPending(Object dispatchPending) {
        this.dispatchPending = dispatchPending; }
    Object dispatchPending;
    @JsonSetter("deliveryPending")
    public Object getDeliveryPending() {
        return this.deliveryPending; }
    public void setDeliveryPending(Object deliveryPending) {
        this.deliveryPending = deliveryPending; }
    Object deliveryPending;
    @JsonSetter("companyId")
    public Object getCompanyId() {
        return this.companyId; }
    public void setCompanyId(Object companyId) {
        this.companyId = companyId; }
    Object companyId;
    @JsonSetter("origin")
    public Object getOrigin() {
        return this.origin; }
    public void setOrigin(Object origin) {
        this.origin = origin; }
    Object origin;
    @JsonSetter("destination")
    public Object getDestination() {
        return this.destination; }
    public void setDestination(Object destination) {
        this.destination = destination; }
    Object destination;
    @JsonSetter("margin")
    public double getMargin() {
        return this.margin; }
    public void setMargin(double margin) {
        this.margin = margin; }
    double margin;
    @JsonSetter("pickupDate")
    public String getPickupDate() {
        return this.pickupDate; }
    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate; }
    String pickupDate;
    @JsonSetter("deliveryDate")
    public String getDeliveryDate() {
        return this.deliveryDate; }
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate; }
    String deliveryDate;
    @JsonSetter("pickupScheduledEarly")
    public Object getPickupScheduledEarly() {
        return this.pickupScheduledEarly; }
    public void setPickupScheduledEarly(Object pickupScheduledEarly) {
        this.pickupScheduledEarly = pickupScheduledEarly; }
    Object pickupScheduledEarly;
    @JsonSetter("pickScheduledLate")
    public Object getPickScheduledLate() {
        return this.pickScheduledLate; }
    public void setPickScheduledLate(Object pickScheduledLate) {
        this.pickScheduledLate = pickScheduledLate; }
    Object pickScheduledLate;
    @JsonSetter("dropScheduledEarly")
    public Object getDropScheduledEarly() {
        return this.dropScheduledEarly; }
    public void setDropScheduledEarly(Object dropScheduledEarly) {
        this.dropScheduledEarly = dropScheduledEarly; }
    Object dropScheduledEarly;
    @JsonSetter("dropScheduledLate")
    public Object getDropScheduledLate() {
        return this.dropScheduledLate; }
    public void setDropScheduledLate(Object dropScheduledLate) {
        this.dropScheduledLate = dropScheduledLate; }
    Object dropScheduledLate;
    @JsonSetter("originAddress")
    public Object getOriginAddress() {
        return this.originAddress; }
    public void setOriginAddress(Object originAddress) {
        this.originAddress = originAddress; }
    Object originAddress;
    @JsonSetter("originCity")
    public String getOriginCity() {
        return this.originCity; }
    public void setOriginCity(String originCity) {
        this.originCity = originCity; }
    String originCity;
    @JsonSetter("originState")
    public String getOriginState() {
        return this.originState; }
    public void setOriginState(String originState) {
        this.originState = originState; }
    String originState;
    @JsonSetter("originZip")
    public String getOriginZip() {
        return this.originZip; }
    public void setOriginZip(String originZip) {
        this.originZip = originZip; }
    String originZip;
    @JsonSetter("destinationAddress")
    public Object getDestinationAddress() {
        return this.destinationAddress; }
    public void setDestinationAddress(Object destinationAddress) {
        this.destinationAddress = destinationAddress; }
    Object destinationAddress;
    @JsonSetter("destinationCity")
    public String getDestinationCity() {
        return this.destinationCity; }
    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity; }
    String destinationCity;
    @JsonSetter("destinationState")
    public String getDestinationState() {
        return this.destinationState; }
    public void setDestinationState(String destinationState) {
        this.destinationState = destinationState; }
    String destinationState;
    @JsonSetter("destinationZip")
    public String getDestinationZip() {
        return this.destinationZip; }
    public void setDestinationZip(String destinationZip) {
        this.destinationZip = destinationZip; }
    String destinationZip;
    @JsonSetter("shipperName")
    public Object getShipperName() {
        return this.shipperName; }
    public void setShipperName(Object shipperName) {
        this.shipperName = shipperName; }
    Object shipperName;
    @JsonSetter("consigneeName")
    public Object getConsigneeName() {
        return this.consigneeName; }
    public void setConsigneeName(Object consigneeName) {
        this.consigneeName = consigneeName; }
    Object consigneeName;
    @JsonSetter("distance")
    public Object getDistance() {
        return this.distance; }
    public void setDistance(Object distance) {
        this.distance = distance; }
    Object distance;
    @JsonSetter("originDistance")
    public Object getOriginDistance() {
        return this.originDistance; }
    public void setOriginDistance(Object originDistance) {
        this.originDistance = originDistance; }
    Object originDistance;
    @JsonSetter("destinationDistance")
    public Object getDestinationDistance() {
        return this.destinationDistance; }
    public void setDestinationDistance(Object destinationDistance) {
        this.destinationDistance = destinationDistance; }
    Object destinationDistance;
    @JsonSetter("shipperId")
    public Object getShipperId() {
        return this.shipperId; }
    public void setShipperId(Object shipperId) {
        this.shipperId = shipperId; }
    Object shipperId;
    @JsonSetter("consigneeId")
    public Object getConsigneeId() {
        return this.consigneeId; }
    public void setConsigneeId(Object consigneeId) {
        this.consigneeId = consigneeId; }
    Object consigneeId;
    @JsonSetter("brokerageStatus")
    public Object getBrokerageStatus() {
        return this.brokerageStatus; }
    public void setBrokerageStatus(Object brokerageStatus) {
        this.brokerageStatus = brokerageStatus; }
    Object brokerageStatus;
    @JsonSetter("originLatitude")
    public double getOriginLatitude() {
        return this.originLatitude; }
    public void setOriginLatitude(double originLatitude) {
        this.originLatitude = originLatitude; }
    double originLatitude;
    @JsonSetter("originLongitude")
    public double getOriginLongitude() {
        return this.originLongitude; }
    public void setOriginLongitude(double originLongitude) {
        this.originLongitude = originLongitude; }
    double originLongitude;
    @JsonSetter("destinationLatitude")
    public double getDestinationLatitude() {
        return this.destinationLatitude; }
    public void setDestinationLatitude(double destinationLatitude) {
        this.destinationLatitude = destinationLatitude; }
    double destinationLatitude;
    @JsonSetter("destinationLongitude")
    public double getDestinationLongitude() {
        return this.destinationLongitude; }
    public void setDestinationLongitude(double destinationLongitude) {
        this.destinationLongitude = destinationLongitude; }
    double destinationLongitude;
    @JsonSetter("marginPercentages")
    public Object getMarginPercentages() {
        return this.marginPercentages; }
    public void setMarginPercentages(Object marginPercentages) {
        this.marginPercentages = marginPercentages; }
    Object marginPercentages;
    @JsonSetter("stopCount")
    public Object getStopCount() {
        return this.stopCount; }
    public void setStopCount(Object stopCount) {
        this.stopCount = stopCount; }
    Object stopCount;
    @JsonSetter("originHasHotComment")
    public Object getOriginHasHotComment() {
        return this.originHasHotComment; }
    public void setOriginHasHotComment(Object originHasHotComment) {
        this.originHasHotComment = originHasHotComment; }
    Object originHasHotComment;
    @JsonSetter("originComments")
    public Object getOriginComments() {
        return this.originComments; }
    public void setOriginComments(Object originComments) {
        this.originComments = originComments; }
    Object originComments;
    @JsonSetter("destinationHasHotComment")
    public Object getDestinationHasHotComment() {
        return this.destinationHasHotComment; }
    public void setDestinationHasHotComment(Object destinationHasHotComment) {
        this.destinationHasHotComment = destinationHasHotComment; }
    Object destinationHasHotComment;
    @JsonSetter("destinationComments")
    public Object getDestinationComments() {
        return this.destinationComments; }
    public void setDestinationComments(Object destinationComments) {
        this.destinationComments = destinationComments; }
    Object destinationComments;
    @JsonSetter("movementStatus")
    public String getMovementStatus() {
        return this.movementStatus; }
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus; }
    String movementStatus;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("carrierName")
    public Object getCarrierName() {
        return this.carrierName; }
    public void setCarrierName(Object carrierName) {
        this.carrierName = carrierName; }
    Object carrierName;
    @JsonSetter("carrierContact")
    public Object getCarrierContact() {
        return this.carrierContact; }
    public void setCarrierContact(Object carrierContact) {
        this.carrierContact = carrierContact; }
    Object carrierContact;
    @JsonSetter("carrierEmail")
    public Object getCarrierEmail() {
        return this.carrierEmail; }
    public void setCarrierEmail(Object carrierEmail) {
        this.carrierEmail = carrierEmail; }
    Object carrierEmail;
    @JsonSetter("carrierPhone")
    public Object getCarrierPhone() {
        return this.carrierPhone; }
    public void setCarrierPhone(Object carrierPhone) {
        this.carrierPhone = carrierPhone; }
    Object carrierPhone;
    @JsonSetter("carrierMobile")
    public Object getCarrierMobile() {
        return this.carrierMobile; }
    public void setCarrierMobile(Object carrierMobile) {
        this.carrierMobile = carrierMobile; }
    Object carrierMobile;
    @JsonSetter("carrierManager")
    public Object getCarrierManager() {
        return this.carrierManager; }
    public void setCarrierManager(Object carrierManager) {
        this.carrierManager = carrierManager; }
    Object carrierManager;
    @JsonSetter("safetyRating")
    public Object getSafetyRating() {
        return this.safetyRating; }
    public void setSafetyRating(Object safetyRating) {
        this.safetyRating = safetyRating; }
    Object safetyRating;
    @JsonSetter("moveDistance")
    public Object getMoveDistance() {
        return this.moveDistance; }
    public void setMoveDistance(Object moveDistance) {
        this.moveDistance = moveDistance; }
    Object moveDistance;
    @JsonSetter("carrierPay")
    public Object getCarrierPay() {
        return this.carrierPay; }
    public void setCarrierPay(Object carrierPay) {
        this.carrierPay = carrierPay; }
    Object carrierPay;
    @JsonSetter("carrierOtherPaySum")
    public Object getCarrierOtherPaySum() {
        return this.carrierOtherPaySum; }
    public void setCarrierOtherPaySum(Object carrierOtherPaySum) {
        this.carrierOtherPaySum = carrierOtherPaySum; }
    Object carrierOtherPaySum;
    @JsonSetter("totalPay")
    public Object getTotalPay() {
        return this.totalPay; }
    public void setTotalPay(Object totalPay) {
        this.totalPay = totalPay; }
    Object totalPay;
    @JsonSetter("movementDispatcherId")
    public Object getMovementDispatcherId() {
        return this.movementDispatcherId; }
    public void setMovementDispatcherId(Object movementDispatcherId) {
        this.movementDispatcherId = movementDispatcherId; }
    Object movementDispatcherId;
    @JsonSetter("movementCapacityRep")
    public Object getMovementCapacityRep() {
        return this.movementCapacityRep; }
    public void setMovementCapacityRep(Object movementCapacityRep) {
        this.movementCapacityRep = movementCapacityRep; }
    Object movementCapacityRep;
    @JsonSetter("carrierId")
    public Object getCarrierId() {
        return this.carrierId; }
    public void setCarrierId(Object carrierId) {
        this.carrierId = carrierId; }
    Object carrierId;
    @JsonSetter("carrierCity")
    public Object getCarrierCity() {
        return this.carrierCity; }
    public void setCarrierCity(Object carrierCity) {
        this.carrierCity = carrierCity; }
    Object carrierCity;
    @JsonSetter("carrierState")
    public Object getCarrierState() {
        return this.carrierState; }
    public void setCarrierState(Object carrierState) {
        this.carrierState = carrierState; }
    Object carrierState;
    @JsonSetter("carrierZipCode")
    public Object getCarrierZipCode() {
        return this.carrierZipCode; }
    public void setCarrierZipCode(Object carrierZipCode) {
        this.carrierZipCode = carrierZipCode; }
    Object carrierZipCode;
    @JsonSetter("carrierFax")
    public Object getCarrierFax() {
        return this.carrierFax; }
    public void setCarrierFax(Object carrierFax) {
        this.carrierFax = carrierFax; }
    Object carrierFax;
    @JsonSetter("movementStatusDescription")
    public Object getMovementStatusDescription() {
        return this.movementStatusDescription; }
    public void setMovementStatusDescription(Object movementStatusDescription) {
        this.movementStatusDescription = movementStatusDescription; }
    Object movementStatusDescription;
    @JsonSetter("orderMovementPayDistance")
    public Object getOrderMovementPayDistance() {
        return this.orderMovementPayDistance; }
    public void setOrderMovementPayDistance(Object orderMovementPayDistance) {
        this.orderMovementPayDistance = orderMovementPayDistance; }
    Object orderMovementPayDistance;
    @JsonSetter("isPreassigned")
    public Object getIsPreassigned() {
        return this.isPreassigned; }
    public void setIsPreassigned(Object isPreassigned) {
        this.isPreassigned = isPreassigned; }
    Object isPreassigned;
    @JsonSetter("isPreassignedByDriver")
    public Object getIsPreassignedByDriver() {
        return this.isPreassignedByDriver; }
    public void setIsPreassignedByDriver(Object isPreassignedByDriver) {
        this.isPreassignedByDriver = isPreassignedByDriver; }
    Object isPreassignedByDriver;
    @JsonSetter("preassignedDriver")
    public Object getPreassignedDriver() {
        return this.preassignedDriver; }
    public void setPreassignedDriver(Object preassignedDriver) {
        this.preassignedDriver = preassignedDriver; }
    Object preassignedDriver;
    @JsonSetter("preassignedTractor")
    public Object getPreassignedTractor() {
        return this.preassignedTractor; }
    public void setPreassignedTractor(Object preassignedTractor) {
        this.preassignedTractor = preassignedTractor; }
    Object preassignedTractor;
    @JsonSetter("preassignedTrailer")
    public Object getPreassignedTrailer() {
        return this.preassignedTrailer; }
    public void setPreassignedTrailer(Object preassignedTrailer) {
        this.preassignedTrailer = preassignedTrailer; }
    Object preassignedTrailer;
    @JsonSetter("preassignedTrailer2")
    public Object getPreassignedTrailer2() {
        return this.preassignedTrailer2; }
    public void setPreassignedTrailer2(Object preassignedTrailer2) {
        this.preassignedTrailer2 = preassignedTrailer2; }
    Object preassignedTrailer2;
    @JsonSetter("tractorFleet")
    public Object getTractorFleet() {
        return this.tractorFleet; }
    public void setTractorFleet(Object tractorFleet) {
        this.tractorFleet = tractorFleet; }
    Object tractorFleet;
    @JsonSetter("movementEquipmentGroupId")
    public Object getMovementEquipmentGroupId() {
        return this.movementEquipmentGroupId; }
    public void setMovementEquipmentGroupId(Object movementEquipmentGroupId) {
        this.movementEquipmentGroupId = movementEquipmentGroupId; }
    Object movementEquipmentGroupId;
    @JsonSetter("movementPreassignSequence")
    public Object getMovementPreassignSequence() {
        return this.movementPreassignSequence; }
    public void setMovementPreassignSequence(Object movementPreassignSequence) {
        this.movementPreassignSequence = movementPreassignSequence; }
    Object movementPreassignSequence;
    @JsonSetter("driverAssigned")
    public Object getDriverAssigned() {
        return this.driverAssigned; }
    public void setDriverAssigned(Object driverAssigned) {
        this.driverAssigned = driverAssigned; }
    Object driverAssigned;
    @JsonSetter("orderMovementCapacityRep")
    public Object getOrderMovementCapacityRep() {
        return this.orderMovementCapacityRep; }
    public void setOrderMovementCapacityRep(Object orderMovementCapacityRep) {
        this.orderMovementCapacityRep = orderMovementCapacityRep; }
    Object orderMovementCapacityRep;
    @JsonSetter("orderMovementCarrierContact")
    public Object getOrderMovementCarrierContact() {
        return this.orderMovementCarrierContact; }
    public void setOrderMovementCarrierContact(Object orderMovementCarrierContact) {
        this.orderMovementCarrierContact = orderMovementCarrierContact; }
    Object orderMovementCarrierContact;
    @JsonSetter("orderMovementCarrierEmail")
    public Object getOrderMovementCarrierEmail() {
        return this.orderMovementCarrierEmail; }
    public void setOrderMovementCarrierEmail(Object orderMovementCarrierEmail) {
        this.orderMovementCarrierEmail = orderMovementCarrierEmail; }
    Object orderMovementCarrierEmail;
    @JsonSetter("orderMovementCarrierPhone")
    public Object getOrderMovementCarrierPhone() {
        return this.orderMovementCarrierPhone; }
    public void setOrderMovementCarrierPhone(Object orderMovementCarrierPhone) {
        this.orderMovementCarrierPhone = orderMovementCarrierPhone; }
    Object orderMovementCarrierPhone;
    @JsonSetter("movementProNumber")
    public Object getMovementProNumber() {
        return this.movementProNumber; }
    public void setMovementProNumber(Object movementProNumber) {
        this.movementProNumber = movementProNumber; }
    Object movementProNumber;
    @JsonSetter("customerNumber")
    public String getCustomerNumber() {
        return this.customerNumber; }
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber; }
    String customerNumber;
    @JsonSetter("customerName")
    public String getCustomerName() {
        return this.customerName; }
    public void setCustomerName(String customerName) {
        this.customerName = customerName; }
    String customerName;
    @JsonSetter("orderNumber")
    public String getOrderNumber() {
        return this.orderNumber; }
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber; }
    String orderNumber;
    @JsonSetter("coDriver")
    public Object getCoDriver() {
        return this.coDriver; }
    public void setCoDriver(Object coDriver) {
        this.coDriver = coDriver; }
    Object coDriver;
    @JsonSetter("drivers")
    public List<Object> getDrivers() {
        return this.drivers; }
    public void setDrivers(List<Object> drivers) {
        this.drivers = drivers; }
    List<Object> drivers;
    @JsonSetter("allDriversJSONString")
    public Object getAllDriversJSONString() {
        return this.allDriversJSONString; }
    public void setAllDriversJSONString(Object allDriversJSONString) {
        this.allDriversJSONString = allDriversJSONString; }
    Object allDriversJSONString;
}
