package pojoClasses.responsePojo.Planning.PlanningOrderList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanningOrder {
    @JsonSetter("planId")
    public String getPlanId() {
        return this.planId; }
    public void setPlanId(String planId) {
        this.planId = planId; }
    String planId;
    @JsonSetter("orderNumber")
    public String getOrderNumber() {
        return this.orderNumber; }
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber; }
    String orderNumber;
    @JsonSetter("customerId")
    public String getCustomerId() {
        return this.customerId; }
    public void setCustomerId(String customerId) {
        this.customerId = customerId; }
    String customerId;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("orderType")
    public String getOrderType() {
        return this.orderType; }
    public void setOrderType(String orderType) {
        this.orderType = orderType; }
    String orderType;
    @JsonSetter("originCity")
    public String getOriginCity() {
        return this.originCity; }
    public void setOriginCity(String originCity) {
        this.originCity = originCity; }
    String originCity;
    @JsonSetter("originState")
    public String getOriginState() {
        return this.originState; }
    public void setOriginState(String originState) {
        this.originState = originState; }
    String originState;
    @JsonSetter("originZip")
    public String getOriginZip() {
        return this.originZip; }
    public void setOriginZip(String originZip) {
        this.originZip = originZip; }
    String originZip;
    @JsonSetter("orderOriginLocationId")
    public String getOrderOriginLocationId() {
        return this.orderOriginLocationId; }
    public void setOrderOriginLocationId(String orderOriginLocationId) {
        this.orderOriginLocationId = orderOriginLocationId; }
    String orderOriginLocationId;
    @JsonSetter("orderOriginLocationName")
    public String getOrderOriginLocationName() {
        return this.orderOriginLocationName; }
    public void setOrderOriginLocationName(String orderOriginLocationName) {
        this.orderOriginLocationName = orderOriginLocationName; }
    String orderOriginLocationName;
    @JsonSetter("originZone")
    public String getOriginZone() {
        return this.originZone; }
    public void setOriginZone(String originZone) {
        this.originZone = originZone; }
    String originZone;
    @JsonSetter("destinationCity")
    public String getDestinationCity() {
        return this.destinationCity; }
    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity; }
    String destinationCity;
    @JsonSetter("destinationState")
    public String getDestinationState() {
        return this.destinationState; }
    public void setDestinationState(String destinationState) {
        this.destinationState = destinationState; }
    String destinationState;
    @JsonSetter("destinationZip")
    public String getDestinationZip() {
        return this.destinationZip; }
    public void setDestinationZip(String destinationZip) {
        this.destinationZip = destinationZip; }
    String destinationZip;
    @JsonSetter("orderDestLocationId")
    public String getOrderDestLocationId() {
        return this.orderDestLocationId; }
    public void setOrderDestLocationId(String orderDestLocationId) {
        this.orderDestLocationId = orderDestLocationId; }
    String orderDestLocationId;
    @JsonSetter("orderDestLocationName")
    public String getOrderDestLocationName() {
        return this.orderDestLocationName; }
    public void setOrderDestLocationName(String orderDestLocationName) {
        this.orderDestLocationName = orderDestLocationName; }
    String orderDestLocationName;
    @JsonSetter("destinationZone")
    public String getDestinationZone() {
        return this.destinationZone; }
    public void setDestinationZone(String destinationZone) {
        this.destinationZone = destinationZone; }
    String destinationZone;
    @JsonSetter("stopCount")
    public String getStopCount() {
        return this.stopCount; }
    public void setStopCount(String stopCount) {
        this.stopCount = stopCount; }
    String stopCount;
    @JsonSetter("pickupDate")
    public String getPickupDate() {
        return this.pickupDate; }
    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate; }
    String pickupDate;
    @JsonSetter("deliveryDate")
    public String getDeliveryDate() {
        return this.deliveryDate; }
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate; }
    String deliveryDate;
    @JsonSetter("isBrokered")
    public String getIsBrokered() {
        return this.isBrokered; }
    public void setIsBrokered(String isBrokered) {
        this.isBrokered = isBrokered; }
    String isBrokered;
    @JsonSetter("preassignedDriver")
    public Object getPreassignedDriver() {
        return this.preassignedDriver; }
    public void setPreassignedDriver(Object preassignedDriver) {
        this.preassignedDriver = preassignedDriver; }
    Object preassignedDriver;
    @JsonSetter("coDriver")
    public Object getCoDriver() {
        return this.coDriver; }
    public void setCoDriver(Object coDriver) {
        this.coDriver = coDriver; }
    Object coDriver;
    @JsonSetter("preassignedTractor")
    public Object getPreassignedTractor() {
        return this.preassignedTractor; }
    public void setPreassignedTractor(Object preassignedTractor) {
        this.preassignedTractor = preassignedTractor; }
    Object preassignedTractor;
    @JsonSetter("preassignedTrailer")
    public Object getPreassignedTrailer() {
        return this.preassignedTrailer; }
    public void setPreassignedTrailer(Object preassignedTrailer) {
        this.preassignedTrailer = preassignedTrailer; }
    Object preassignedTrailer;
    @JsonSetter("preassignedTrailer2")
    public Object getPreassignedTrailer2() {
        return this.preassignedTrailer2; }
    public void setPreassignedTrailer2(Object preassignedTrailer2) {
        this.preassignedTrailer2 = preassignedTrailer2; }
    Object preassignedTrailer2;
    @JsonSetter("agentPayeeId")
    public String getAgentPayeeId() {
        return this.agentPayeeId; }
    public void setAgentPayeeId(String agentPayeeId) {
        this.agentPayeeId = agentPayeeId; }
    String agentPayeeId;
    @JsonSetter("orderRFCode")
    public String getOrderRFCode() {
        return this.orderRFCode; }
    public void setOrderRFCode(String orderRFCode) {
        this.orderRFCode = orderRFCode; }
    String orderRFCode;
    @JsonSetter("movementRFCode")
    public String getMovementRFCode() {
        return this.movementRFCode; }
    public void setMovementRFCode(String movementRFCode) {
        this.movementRFCode = movementRFCode; }
    String movementRFCode;
    @JsonSetter("brokerageStatus")
    public Object getBrokerageStatus() {
        return this.brokerageStatus; }
    public void setBrokerageStatus(Object brokerageStatus) {
        this.brokerageStatus = brokerageStatus; }
    Object brokerageStatus;
    @JsonSetter("orderStatusCode")
    public String getOrderStatusCode() {
        return this.orderStatusCode; }
    public void setOrderStatusCode(String orderStatusCode) {
        this.orderStatusCode = orderStatusCode; }
    String orderStatusCode;
    @JsonSetter("orderStatus")
    public String getOrderStatus() {
        return this.orderStatus; }
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus; }
    String orderStatus;
    @JsonSetter("distance")
    public double getDistance() {
        return this.distance; }
    public void setDistance(double distance) {
        this.distance = distance; }
    double distance;
    @JsonSetter("totalOrderValue")
    public String getTotalOrderValue() {
        return this.totalOrderValue; }
    public void setTotalOrderValue(String totalOrderValue) {
        this.totalOrderValue = totalOrderValue; }
    String totalOrderValue;
    @JsonSetter("movementProNumber")
    public Object getMovementProNumber() {
        return this.movementProNumber; }
    public void setMovementProNumber(Object movementProNumber) {
        this.movementProNumber = movementProNumber; }
    Object movementProNumber;
    @JsonSetter("movementStatus")
    public String getMovementStatus() {
        return this.movementStatus; }
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus; }
    String movementStatus;
    @JsonSetter("moveDistance")
    public String getMoveDistance() {
        return this.moveDistance; }
    public void setMoveDistance(String moveDistance) {
        this.moveDistance = moveDistance; }
    String moveDistance;
    @JsonSetter("isHighValue")
    public String getIsHighValue() {
        return this.isHighValue; }
    public void setIsHighValue(String isHighValue) {
        this.isHighValue = isHighValue; }
    String isHighValue;
    @JsonSetter("carrierId")
    public Object getCarrierId() {
        return this.carrierId; }
    public void setCarrierId(Object carrierId) {
        this.carrierId = carrierId; }
    Object carrierId;
    @JsonSetter("opsUser")
    public String getOpsUser() {
        return this.opsUser; }
    public void setOpsUser(String opsUser) {
        this.opsUser = opsUser; }
    String opsUser;
    @JsonSetter("createByUserId")
    public String getCreateByUserId() {
        return this.createByUserId; }
    public void setCreateByUserId(String createByUserId) {
        this.createByUserId = createByUserId; }
    String createByUserId;
    @JsonSetter("customerName")
    public String getCustomerName() {
        return this.customerName; }
    public void setCustomerName(String customerName) {
        this.customerName = customerName; }
    String customerName;
    @JsonSetter("equipmentTypeId")
    public Object getEquipmentTypeId() {
        return this.equipmentTypeId; }
    public void setEquipmentTypeId(Object equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId; }
    Object equipmentTypeId;
    @JsonSetter("destinationLatitude")
    public double getDestinationLatitude() {
        return this.destinationLatitude; }
    public void setDestinationLatitude(double destinationLatitude) {
        this.destinationLatitude = destinationLatitude; }
    double destinationLatitude;
    @JsonSetter("destinationLongitude")
    public double getDestinationLongitude() {
        return this.destinationLongitude; }
    public void setDestinationLongitude(double destinationLongitude) {
        this.destinationLongitude = destinationLongitude; }
    double destinationLongitude;
    @JsonSetter("originLatitude")
    public double getOriginLatitude() {
        return this.originLatitude; }
    public void setOriginLatitude(double originLatitude) {
        this.originLatitude = originLatitude; }
    double originLatitude;
    @JsonSetter("originLongitude")
    public double getOriginLongitude() {
        return this.originLongitude; }
    public void setOriginLongitude(double originLongitude) {
        this.originLongitude = originLongitude; }
    double originLongitude;
    @JsonSetter("nextAction")
    public String getNextAction() {
        return this.nextAction; }
    public void setNextAction(String nextAction) {
        this.nextAction = nextAction; }
    String nextAction;
    @JsonSetter("nextActionAccess")
    public Object getNextActionAccess() {
        return this.nextActionAccess; }
    public void setNextActionAccess(Object nextActionAccess) {
        this.nextActionAccess = nextActionAccess; }
    Object nextActionAccess;
    @JsonSetter("nextActionDescription")
    public String getNextActionDescription() {
        return this.nextActionDescription; }
    public void setNextActionDescription(String nextActionDescription) {
        this.nextActionDescription = nextActionDescription; }
    String nextActionDescription;
    @JsonSetter("orderTypeId")
    public String getOrderTypeId() {
        return this.orderTypeId; }
    public void setOrderTypeId(String orderTypeId) {
        this.orderTypeId = orderTypeId; }
    String orderTypeId;
    @JsonSetter("orderMode")
    public String getOrderMode() {
        return this.orderMode; }
    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode; }
    String orderMode;
    @JsonSetter("orderWeight")
    public String getOrderWeight() {
        return this.orderWeight; }
    public void setOrderWeight(String orderWeight) {
        this.orderWeight = orderWeight; }
    String orderWeight;
    @JsonSetter("orderPieces")
    public String getOrderPieces() {
        return this.orderPieces; }
    public void setOrderPieces(String orderPieces) {
        this.orderPieces = orderPieces; }
    String orderPieces;
    @JsonSetter("currentMovementId")
    public String getCurrentMovementId() {
        return this.currentMovementId; }
    public void setCurrentMovementId(String currentMovementId) {
        this.currentMovementId = currentMovementId; }
    String currentMovementId;
    @JsonSetter("allStopsInfo")
    public String getAllStopsInfo() {
        return this.allStopsInfo; }
    public void setAllStopsInfo(String allStopsInfo) {
        this.allStopsInfo = allStopsInfo; }
    String allStopsInfo;
    @JsonSetter("totalCharge")
    public String getTotalCharge() {
        return this.totalCharge; }
    public void setTotalCharge(String totalCharge) {
        this.totalCharge = totalCharge; }
    String totalCharge;
    @JsonSetter("originHasHotComment")
    public String getOriginHasHotComment() {
        return this.originHasHotComment; }
    public void setOriginHasHotComment(String originHasHotComment) {
        this.originHasHotComment = originHasHotComment; }
    String originHasHotComment;
    @JsonSetter("destinationHasHotComment")
    public String getDestinationHasHotComment() {
        return this.destinationHasHotComment; }
    public void setDestinationHasHotComment(String destinationHasHotComment) {
        this.destinationHasHotComment = destinationHasHotComment; }
    String destinationHasHotComment;
    @JsonSetter("carrierContact")
    public Object getCarrierContact() {
        return this.carrierContact; }
    public void setCarrierContact(Object carrierContact) {
        this.carrierContact = carrierContact; }
    Object carrierContact;
    @JsonSetter("carrierPhone")
    public Object getCarrierPhone() {
        return this.carrierPhone; }
    public void setCarrierPhone(Object carrierPhone) {
        this.carrierPhone = carrierPhone; }
    Object carrierPhone;
    @JsonSetter("carrierEmail")
    public Object getCarrierEmail() {
        return this.carrierEmail; }
    public void setCarrierEmail(Object carrierEmail) {
        this.carrierEmail = carrierEmail; }
    Object carrierEmail;
    @JsonSetter("assignedDriverEmail")
    public Object getAssignedDriverEmail() {
        return this.assignedDriverEmail; }
    public void setAssignedDriverEmail(Object assignedDriverEmail) {
        this.assignedDriverEmail = assignedDriverEmail; }
    Object assignedDriverEmail;
    @JsonSetter("assignedDriverCellphone")
    public Object getAssignedDriverCellphone() {
        return this.assignedDriverCellphone; }
    public void setAssignedDriverCellphone(Object assignedDriverCellphone) {
        this.assignedDriverCellphone = assignedDriverCellphone; }
    Object assignedDriverCellphone;
    @JsonSetter("equipmentType")
    public String getEquipmentType() {
        return this.equipmentType; }
    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType; }
    String equipmentType;
    @JsonSetter("lastUpdateDate")
    public String getLastUpdateDate() {
        return this.lastUpdateDate; }
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate; }
    String lastUpdateDate;
    @JsonSetter("totalPay")
    public Object getTotalPay() {
        return this.totalPay; }
    public void setTotalPay(Object totalPay) {
        this.totalPay = totalPay; }
    Object totalPay;
    @JsonSetter("billDate")
    public String getBillDate() {
        return this.billDate; }
    public void setBillDate(String billDate) {
        this.billDate = billDate; }
    String billDate;
    @JsonSetter("orderOpsRevComplete")
    public String getOrderOpsRevComplete() {
        return this.orderOpsRevComplete; }
    public void setOrderOpsRevComplete(String orderOpsRevComplete) {
        this.orderOpsRevComplete = orderOpsRevComplete; }
    String orderOpsRevComplete;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("customerRatingDone")
    public String getCustomerRatingDone() {
        return this.customerRatingDone; }
    public void setCustomerRatingDone(String customerRatingDone) {
        this.customerRatingDone = customerRatingDone; }
    String customerRatingDone;
    @JsonSetter("carrierRatingDone")
    public String getCarrierRatingDone() {
        return this.carrierRatingDone; }
    public void setCarrierRatingDone(String carrierRatingDone) {
        this.carrierRatingDone = carrierRatingDone; }
    String carrierRatingDone;
    @JsonSetter("orderMovementExecutionDivision")
    public String getOrderMovementExecutionDivision() {
        return this.orderMovementExecutionDivision; }
    public void setOrderMovementExecutionDivision(String orderMovementExecutionDivision) {
        this.orderMovementExecutionDivision = orderMovementExecutionDivision; }
    String orderMovementExecutionDivision;
    @JsonSetter("originHotComment")
    public String getOriginHotComment() {
        return this.originHotComment; }
    public void setOriginHotComment(String originHotComment) {
        this.originHotComment = originHotComment; }
    String originHotComment;
    @JsonSetter("destHotComment")
    public Object getDestHotComment() {
        return this.destHotComment; }
    public void setDestHotComment(Object destHotComment) {
        this.destHotComment = destHotComment; }
    Object destHotComment;
    @JsonSetter("driverDotInfo")
    public Object getDriverDotInfo() {
        return this.driverDotInfo; }
    public void setDriverDotInfo(Object driverDotInfo) {
        this.driverDotInfo = driverDotInfo; }
    Object driverDotInfo;
    @JsonSetter("brokeredNotPreferred")
    public String getBrokeredNotPreferred() {
        return this.brokeredNotPreferred; }
    public void setBrokeredNotPreferred(String brokeredNotPreferred) {
        this.brokeredNotPreferred = brokeredNotPreferred; }
    String brokeredNotPreferred;
    @JsonSetter("isPreassigned")
    public Object getIsPreassigned() {
        return this.isPreassigned; }
    public void setIsPreassigned(Object isPreassigned) {
        this.isPreassigned = isPreassigned; }
    Object isPreassigned;
    @JsonSetter("isPartialShipment")
    public String getIsPartialShipment() {
        return this.isPartialShipment; }
    public void setIsPartialShipment(String isPartialShipment) {
        this.isPartialShipment = isPartialShipment; }
    String isPartialShipment;
    @JsonSetter("isCaptiveOrder")
    public String getIsCaptiveOrder() {
        return this.isCaptiveOrder; }
    public void setIsCaptiveOrder(String isCaptiveOrder) {
        this.isCaptiveOrder = isCaptiveOrder; }
    String isCaptiveOrder;
    @JsonSetter("daysOut")
    public Object getDaysOut() {
        return this.daysOut; }
    public void setDaysOut(Object daysOut) {
        this.daysOut = daysOut; }
    Object daysOut;
    @JsonSetter("originZip3")
    public String getOriginZip3() {
        return this.originZip3; }
    public void setOriginZip3(String originZip3) {
        this.originZip3 = originZip3; }
    String originZip3;
    @JsonSetter("destZip3")
    public String getDestZip3() {
        return this.destZip3; }
    public void setDestZip3(String destZip3) {
        this.destZip3 = destZip3; }
    String destZip3;
    @JsonSetter("originApptRequired")
    public String getOriginApptRequired() {
        return this.originApptRequired; }
    public void setOriginApptRequired(String originApptRequired) {
        this.originApptRequired = originApptRequired; }
    String originApptRequired;
    @JsonSetter("originApptConfirmed")
    public String getOriginApptConfirmed() {
        return this.originApptConfirmed; }
    public void setOriginApptConfirmed(String originApptConfirmed) {
        this.originApptConfirmed = originApptConfirmed; }
    String originApptConfirmed;
    @JsonSetter("destApptRequired")
    public String getDestApptRequired() {
        return this.destApptRequired; }
    public void setDestApptRequired(String destApptRequired) {
        this.destApptRequired = destApptRequired; }
    String destApptRequired;
    @JsonSetter("destApptConfirmed")
    public String getDestApptConfirmed() {
        return this.destApptConfirmed; }
    public void setDestApptConfirmed(String destApptConfirmed) {
        this.destApptConfirmed = destApptConfirmed; }
    String destApptConfirmed;
    @JsonSetter("movements")
    public List<Movement> getMovements() {
        return this.movements; }
    public void setMovements(List<Movement> movements) {
        this.movements = movements; }
    List<Movement> movements;
    @JsonSetter("orderStopEntity")
    public List<OrderStopEntity> getOrderStopEntity() {
        return this.orderStopEntity; }
    public void setOrderStopEntity(List<OrderStopEntity> orderStopEntity) {
        this.orderStopEntity = orderStopEntity; }
    List<OrderStopEntity> orderStopEntity;
    @JsonSetter("planningPending")
    public String getPlanningPending() {
        return this.planningPending; }
    public void setPlanningPending(String planningPending) {
        this.planningPending = planningPending; }
    String planningPending;
    @JsonSetter("dispatchPending")
    public String getDispatchPending() {
        return this.dispatchPending; }
    public void setDispatchPending(String dispatchPending) {
        this.dispatchPending = dispatchPending; }
    String dispatchPending;
    @JsonSetter("deliveryPending")
    public String getDeliveryPending() {
        return this.deliveryPending; }
    public void setDeliveryPending(String deliveryPending) {
        this.deliveryPending = deliveryPending; }
    String deliveryPending;
    @JsonSetter("movementOriginCity")
    public String getMovementOriginCity() {
        return this.movementOriginCity; }
    public void setMovementOriginCity(String movementOriginCity) {
        this.movementOriginCity = movementOriginCity; }
    String movementOriginCity;
    @JsonSetter("movementOriginState")
    public String getMovementOriginState() {
        return this.movementOriginState; }
    public void setMovementOriginState(String movementOriginState) {
        this.movementOriginState = movementOriginState; }
    String movementOriginState;
    @JsonSetter("movementOriginZip")
    public String getMovementOriginZip() {
        return this.movementOriginZip; }
    public void setMovementOriginZip(String movementOriginZip) {
        this.movementOriginZip = movementOriginZip; }
    String movementOriginZip;
    @JsonSetter("movementDestinationCity")
    public String getMovementDestinationCity() {
        return this.movementDestinationCity; }
    public void setMovementDestinationCity(String movementDestinationCity) {
        this.movementDestinationCity = movementDestinationCity; }
    String movementDestinationCity;
    @JsonSetter("movementDestinationState")
    public String getMovementDestinationState() {
        return this.movementDestinationState; }
    public void setMovementDestinationState(String movementDestinationState) {
        this.movementDestinationState = movementDestinationState; }
    String movementDestinationState;
    @JsonSetter("movementDestinationZip")
    public String getMovementDestinationZip() {
        return this.movementDestinationZip; }
    public void setMovementDestinationZip(String movementDestinationZip) {
        this.movementDestinationZip = movementDestinationZip; }
    String movementDestinationZip;
}
