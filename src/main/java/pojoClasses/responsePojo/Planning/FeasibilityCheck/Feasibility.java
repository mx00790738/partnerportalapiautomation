package pojoClasses.responsePojo.Planning.FeasibilityCheck;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

public class Feasibility {
    @JsonSetter("orderOverlapFeasibility")
    public List<Object> getOrderOverlapFeasibility() {
        return this.orderOverlapFeasibility; }
    public void setOrderOverlapFeasibility(List<Object> orderOverlapFeasibility) {
        this.orderOverlapFeasibility = orderOverlapFeasibility; }
    List<Object> orderOverlapFeasibility;
    @JsonSetter("driverEventsFeasibility")
    public DriverEventsFeasibility getDriverEventsFeasibility() {
        return this.driverEventsFeasibility; }
    public void setDriverEventsFeasibility(DriverEventsFeasibility driverEventsFeasibility) {
        this.driverEventsFeasibility = driverEventsFeasibility; }
    DriverEventsFeasibility driverEventsFeasibility;
    @JsonSetter("driveTimeFeasibility")
    public DriveTimeFeasibility getDriveTimeFeasibility() {
        return this.driveTimeFeasibility; }
    public void setDriveTimeFeasibility(DriveTimeFeasibility driveTimeFeasibility) {
        this.driveTimeFeasibility = driveTimeFeasibility; }
    DriveTimeFeasibility driveTimeFeasibility;
    @JsonSetter("equipmentFeasibility")
    public List<Object> getEquipmentFeasibility() {
        return this.equipmentFeasibility; }
    public void setEquipmentFeasibility(List<Object> equipmentFeasibility) {
        this.equipmentFeasibility = equipmentFeasibility; }
    List<Object> equipmentFeasibility;
}
