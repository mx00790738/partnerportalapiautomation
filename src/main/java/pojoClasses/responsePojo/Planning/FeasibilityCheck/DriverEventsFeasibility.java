package pojoClasses.responsePojo.Planning.FeasibilityCheck;

import com.fasterxml.jackson.annotation.JsonSetter;

public class DriverEventsFeasibility {
    @JsonSetter("feasibilityStatus")
    public boolean getFeasibilityStatus() {
        return this.feasibilityStatus; }
    public void setFeasibilityStatus(boolean feasibilityStatus) {
        this.feasibilityStatus = feasibilityStatus; }
    boolean feasibilityStatus;
    @JsonSetter("message")
    public String getMessage() {
        return this.message; }
    public void setMessage(String message) {
        this.message = message; }
    String message;
}
