package pojoClasses.responsePojo.Planning.FeasibilityCheck;

import com.fasterxml.jackson.annotation.JsonSetter;

public class AdditionalEquipment {
    @JsonSetter("equipTypeId")
    public String getEquipTypeId() {
        return this.equipTypeId; }
    public void setEquipTypeId(String equipTypeId) {
        this.equipTypeId = equipTypeId; }
    String equipTypeId;
    @JsonSetter("quantity")
    public String getQuantity() {
        return this.quantity; }
    public void setQuantity(String quantity) {
        this.quantity = quantity; }
    String quantity;
    @JsonSetter("equipReq")
    public String getEquipReq() {
        return this.equipReq; }
    public void setEquipReq(String equipReq) {
        this.equipReq = equipReq; }
    String equipReq;
    @JsonSetter("appliesTo")
    public String getAppliesTo() {
        return this.appliesTo; }
    public void setAppliesTo(String appliesTo) {
        this.appliesTo = appliesTo; }
    String appliesTo;
    @JsonSetter("exists")
    public String getExists() {
        return this.exists; }
    public void setExists(String exists) {
        this.exists = exists; }
    String exists;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
}
