package pojoClasses.responsePojo.MasterData.Commodity;

import com.fasterxml.jackson.annotation.JsonSetter;

public class FreightClass {
    @JsonSetter("id")
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    @JsonSetter("value")
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    String value;
}
