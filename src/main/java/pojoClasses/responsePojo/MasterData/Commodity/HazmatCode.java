package pojoClasses.responsePojo.MasterData.Commodity;

import com.fasterxml.jackson.annotation.JsonSetter;

public class HazmatCode {
    @JsonSetter("code")
    public String getCode() {
        return this.code; }
    public void setCode(String code) {
        this.code = code; }
    String code;
    @JsonSetter("description")
    public String getDescription() {
        return this.description; }
    public void setDescription(String description) {
        this.description = description; }
    String description;
    @JsonSetter("hazmatClass")
    public String getHazmatClass() {
        return this.hazmatClass; }
    public void setHazmatClass(String hazmatClass) {
        this.hazmatClass = hazmatClass; }
    String hazmatClass;
    @JsonSetter("packageGroup")
    public String getPackageGroup() {
        return this.packageGroup; }
    public void setPackageGroup(String packageGroup) {
        this.packageGroup = packageGroup; }
    String packageGroup;
    @JsonSetter("hazmatErgNumber")
    public String getHazmatErgNumber() {
        return this.hazmatErgNumber; }
    public void setHazmatErgNumber(String hazmatErgNumber) {
        this.hazmatErgNumber = hazmatErgNumber; }
    String hazmatErgNumber;
    @JsonSetter("hazmatProperShipname")
    public String getHazmatProperShipname() {
        return this.hazmatProperShipname; }
    public void setHazmatProperShipname(String hazmatProperShipname) {
        this.hazmatProperShipname = hazmatProperShipname; }
    String hazmatProperShipname;
}
