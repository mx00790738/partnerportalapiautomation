/**
 * 
 */
package pojoClasses.responsePojo.MasterData.Commodity;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author Manoj
 *
 */
public class CustomerCommodity {
	private String commodityId;
    private String description;
    private String hazmat;
    public String getCommodityId() {
        return commodityId;
    }
    @JsonSetter("commodityId")
    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }
    public String getDescription() {
        return description;
    }
    @JsonSetter("description")
    public void setDescription(String description) {
        this.description = description;
    }
    public String getHazmat() {
        return hazmat;
    }
    @JsonSetter("hazmat")
    public void setHazmat(String hazmat) {
        this.hazmat = hazmat;
    }
}
