package pojoClasses.responsePojo.MasterData.Driver;

import com.fasterxml.jackson.annotation.JsonSetter;

public class DriverID {
    @JsonSetter("id")
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    private String companyId;
}
