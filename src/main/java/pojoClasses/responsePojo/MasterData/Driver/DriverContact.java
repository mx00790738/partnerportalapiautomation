package pojoClasses.responsePojo.MasterData.Driver;

import com.fasterxml.jackson.annotation.JsonSetter;

public class DriverContact {
    @JsonSetter("comapnyId")
    public String getComapnyId() {
        return this.comapnyId;
    }

    public void setComapnyId(String comapnyId) {
        this.comapnyId = comapnyId;
    }

    String comapnyId;

    @JsonSetter("parentrowId")
    public String getParentrowId() {
        return this.parentrowId;
    }

    public void setParentrowId(String parentrowId) {
        this.parentrowId = parentrowId;
    }

    String parentrowId;

    @JsonSetter("parentrowType")
    public String getParentrowType() {
        return this.parentrowType;
    }

    public void setParentrowType(String parentrowType) {
        this.parentrowType = parentrowType;
    }

    String parentrowType;

    @JsonSetter("id")
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    @JsonSetter("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    @JsonSetter("contactName")
    public Object getContactName() {
        return this.contactName;
    }

    public void setContactName(Object contactName) {
        this.contactName = contactName;
    }

    Object contactName;

    @JsonSetter("comments")
    public Object getComments() {
        return this.comments;
    }

    public void setComments(Object comments) {
        this.comments = comments;
    }

    Object comments;

    @JsonSetter("email")
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email;

    @JsonSetter("fax")
    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    String fax;

    @JsonSetter("active")
    public String getActive() {
        return this.active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    String active;

    @JsonSetter("mobilePhone")
    public String getMobilePhone() {
        return this.mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    String mobilePhone;

    @JsonSetter("termsContact")
    public String getTermsContact() {
        return this.termsContact;
    }

    public void setTermsContact(String termsContact) {
        this.termsContact = termsContact;
    }

    String termsContact;

    @JsonSetter("webAccess")
    public String getWebAccess() {
        return this.webAccess;
    }

    public void setWebAccess(String webAccess) {
        this.webAccess = webAccess;
    }

    String webAccess;

    @JsonSetter("title")
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    String title;

    @JsonSetter("phone")
    public Object getPhone() {
        return this.phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    Object phone;

    @JsonSetter("payableContact")
    public String getPayableContact() {
        return this.payableContact;
    }

    public void setPayableContact(String payableContact) {
        this.payableContact = payableContact;
    }

    String payableContact;

    @JsonSetter("rapidAlertNotify")
    public Object getRapidAlertNotify() {
        return this.rapidAlertNotify;
    }

    public void setRapidAlertNotify(Object rapidAlertNotify) {
        this.rapidAlertNotify = rapidAlertNotify;
    }

    Object rapidAlertNotify;

    @JsonSetter("detentionContact")
    public String getDetentionContact() {
        return this.detentionContact;
    }

    public void setDetentionContact(String detentionContact) {
        this.detentionContact = detentionContact;
    }

    String detentionContact;
}
