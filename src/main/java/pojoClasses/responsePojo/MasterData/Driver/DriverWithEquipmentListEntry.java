package pojoClasses.responsePojo.MasterData.Driver;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

public class DriverWithEquipmentListEntry {
    @JsonSetter("driverId")
    public DriverID getDriverId() {
        return this.driverId;
    }

    public void setDriverId(DriverID driverId) {
        this.driverId = driverId;
    }

    DriverID driverId;

    @JsonSetter("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    @JsonSetter("firstName")
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    String firstName;

    @JsonSetter("fleetManager")
    public String getFleetManager() {
        return this.fleetManager;
    }

    public void setFleetManager(String fleetManager) {
        this.fleetManager = fleetManager;
    }

    String fleetManager;

    @JsonSetter("driverName")
    public String getDriverName() {
        return this.driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    String driverName;

    @JsonSetter("statusCode")
    public Object getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(Object statusCode) {
        this.statusCode = statusCode;
    }

    Object statusCode;

    @JsonSetter("payeeId")
    public String getPayeeId() {
        return this.payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    String payeeId;

    @JsonSetter("type")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;

    @JsonSetter("address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;

    @JsonSetter("city")
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    String city;

    @JsonSetter("state")
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    String state;

    @JsonSetter("zip")
    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    String zip;

    @JsonSetter("ssn")
    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    String ssn;

    @JsonSetter("race")
    public String getRace() {
        return this.race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    String race;

    @JsonSetter("sex")
    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    String sex;

    @JsonSetter("is_active")
    public String getIs_active() {
        return this.is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    String is_active;

    @JsonSetter("expAtHire")
    public String getExpAtHire() {
        return this.expAtHire;
    }

    public void setExpAtHire(String expAtHire) {
        this.expAtHire = expAtHire;
    }

    String expAtHire;

    @JsonSetter("issuedEquipments")
    public List<Object> getIssuedEquipments() {
        return this.issuedEquipments;
    }

    public void setIssuedEquipments(List<Object> issuedEquipments) {
        this.issuedEquipments = issuedEquipments;
    }

    List<Object> issuedEquipments;

    @JsonSetter("midInitialName")
    public String getMidInitialName() {
        return this.midInitialName;
    }

    public void setMidInitialName(String midInitialName) {
        this.midInitialName = midInitialName;
    }

    String midInitialName;
}
