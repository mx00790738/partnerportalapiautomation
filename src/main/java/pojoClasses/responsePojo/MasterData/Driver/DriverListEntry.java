package pojoClasses.responsePojo.MasterData.Driver;

import com.fasterxml.jackson.annotation.JsonSetter;

public class DriverListEntry {
    @JsonSetter("driverId")
    public DriverID getDriverId() {
        return this.driverId;
    }

    public void setDriverId(DriverID driverId) {
        this.driverId = driverId;
    }

    private DriverID driverId;

    @JsonSetter("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @JsonSetter("firstName")
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String firstName;

    @JsonSetter("fleetManager")
    public String getFleetManager() {
        return this.fleetManager;
    }

    public void setFleetManager(String fleetManager) {
        this.fleetManager = fleetManager;
    }

    private String fleetManager;

    @JsonSetter("driverName")
    public String getDriverName() {
        return this.driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    private String driverName;

    @JsonSetter("statusCode")
    public Object getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(Object statusCode) {
        this.statusCode = statusCode;
    }

    private Object statusCode;

    @JsonSetter("city")
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

    @JsonSetter("payeeId")
    public String getPayeeId() {
        return this.payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    private String payeeId;

    @JsonSetter("type")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    @JsonSetter("state")
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    private String state;

    @JsonSetter("zip")
    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    private String zip;

    @JsonSetter("address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    @JsonSetter("phone")
    public Object getPhone() {
        return this.phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    private Object phone;

    @JsonSetter("cellPhone")
    public Object getCellPhone() {
        return this.cellPhone;
    }

    public void setCellPhone(Object cellPhone) {
        this.cellPhone = cellPhone;
    }

    private Object cellPhone;

    @JsonSetter("nextHomeDate")
    public String getNextHomeDate() {
        return this.nextHomeDate;
    }

    public void setNextHomeDate(String nextHomeDate) {
        this.nextHomeDate = nextHomeDate;
    }

    private String nextHomeDate;

    @JsonSetter("driveTimeAvailable")
    public double getDriveTimeAvailable() {
        return this.driveTimeAvailable;
    }

    public void setDriveTimeAvailable(double driveTimeAvailable) {
        this.driveTimeAvailable = driveTimeAvailable;
    }

    private double driveTimeAvailable;

    @JsonSetter("availableHours")
    public Object getAvailableHours() {
        return this.availableHours;
    }

    public void setAvailableHours(Object availableHours) {
        this.availableHours = availableHours;
    }

    private Object availableHours;

    @JsonSetter("asOfDate")
    public Object getAsOfDate() {
        return this.asOfDate;
    }

    public void setAsOfDate(Object asOfDate) {
        this.asOfDate = asOfDate;
    }

    private Object asOfDate;

    @JsonSetter("isActive")
    public String getIsActive() {
        return this.isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    private String isActive;

    @JsonSetter("midInitialName")
    public Object getMidInitialName() {
        return this.midInitialName;
    }

    public void setMidInitialName(Object midInitialName) {
        this.midInitialName = midInitialName;
    }

    private Object midInitialName;
}
