package pojoClasses.responsePojo.MasterData.Driver;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

public class DriverNonBrokered {
    @JsonSetter("driverId")
    public DriverID getDriverId() {
        return this.driverId;
    }

    public void setDriverId(DriverID driverId) {
        this.driverId = driverId;
    }

    private DriverID driverId;

    @JsonSetter("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @JsonSetter("firstName")
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String firstName;

    @JsonSetter("driverName")
    public String getDriverName() {
        return this.driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    private String driverName;

    @JsonSetter("statusCode")
    public Object getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(Object statusCode) {
        this.statusCode = statusCode;
    }

    private Object statusCode;

    @JsonSetter("payeeId")
    public String getPayeeId() {
        return this.payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    private String payeeId;

    @JsonSetter("type")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    @JsonSetter("address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    @JsonSetter("city")
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

    @JsonSetter("state")
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    private String state;

    @JsonSetter("zip")
    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    private String zip;

    @JsonSetter("ssn")
    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    private String ssn;

    @JsonSetter("race")
    public String getRace() {
        return this.race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    private String race;

    @JsonSetter("sex")
    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    private String sex;

    @JsonSetter("birthDate")
    public String getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    private String birthDate;

    @JsonSetter("spouseName")
    public Object getSpouseName() {
        return this.spouseName;
    }

    public void setSpouseName(Object spouseName) {
        this.spouseName = spouseName;
    }

    private Object spouseName;

    @JsonSetter("operationType")
    public String getOperationType() {
        return this.operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    private String operationType;

    @JsonSetter("hazmatDate")
    public Object getHazmatDate() {
        return this.hazmatDate;
    }

    public void setHazmatDate(Object hazmatDate) {
        this.hazmatDate = hazmatDate;
    }

    private Object hazmatDate;

    @JsonSetter("codriverCert")
    public String getCodriverCert() {
        return this.codriverCert;
    }

    public void setCodriverCert(String codriverCert) {
        this.codriverCert = codriverCert;
    }

    private String codriverCert;

    @JsonSetter("hm126ReviewDate")
    public String getHm126ReviewDate() {
        return this.hm126ReviewDate;
    }

    public void setHm126ReviewDate(String hm126ReviewDate) {
        this.hm126ReviewDate = hm126ReviewDate;
    }

    private String hm126ReviewDate;

    @JsonSetter("leadDriverCert")
    public Object getLeadDriverCert() {
        return this.leadDriverCert;
    }

    public void setLeadDriverCert(Object leadDriverCert) {
        this.leadDriverCert = leadDriverCert;
    }

    private Object leadDriverCert;

    @JsonSetter("tenStreetId")
    public Object getTenStreetId() {
        return this.tenStreetId;
    }

    public void setTenStreetId(Object tenStreetId) {
        this.tenStreetId = tenStreetId;
    }

    private Object tenStreetId;

    @JsonSetter("kronosId")
    public Object getKronosId() {
        return this.kronosId;
    }

    public void setKronosId(Object kronosId) {
        this.kronosId = kronosId;
    }

    private Object kronosId;

    @JsonSetter("licenseNumber")
    public String getLicenseNumber() {
        return this.licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    private String licenseNumber;

    @JsonSetter("licenseState")
    public String getLicenseState() {
        return this.licenseState;
    }

    public void setLicenseState(String licenseState) {
        this.licenseState = licenseState;
    }

    private String licenseState;

    @JsonSetter("licenseExpDate")
    public String getLicenseExpDate() {
        return this.licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }

    private String licenseExpDate;

    @JsonSetter("licenseType")
    public String getLicenseType() {
        return this.licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    private String licenseType;

    @JsonSetter("hireDate")
    public String getHireDate() {
        return this.hireDate;
    }

    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    private String hireDate;

    @JsonSetter("reviewDate")
    public Object getReviewDate() {
        return this.reviewDate;
    }

    public void setReviewDate(Object reviewDate) {
        this.reviewDate = reviewDate;
    }

    private Object reviewDate;

    @JsonSetter("physicalDueDate")
    public String getPhysicalDueDate() {
        return this.physicalDueDate;
    }

    public void setPhysicalDueDate(String physicalDueDate) {
        this.physicalDueDate = physicalDueDate;
    }

    private String physicalDueDate;

    @JsonSetter("mvrDate")
    public String getMvrDate() {
        return this.mvrDate;
    }

    public void setMvrDate(String mvrDate) {
        this.mvrDate = mvrDate;
    }

    private String mvrDate;

    @JsonSetter("medicalCertExempt")
    public String getMedicalCertExempt() {
        return this.medicalCertExempt;
    }

    public void setMedicalCertExempt(String medicalCertExempt) {
        this.medicalCertExempt = medicalCertExempt;
    }

    private String medicalCertExempt;

    @JsonSetter("canRehire")
    public String getCanRehire() {
        return this.canRehire;
    }

    public void setCanRehire(String canRehire) {
        this.canRehire = canRehire;
    }

    private String canRehire;

    @JsonSetter("medicalCertExpire")
    public Object getMedicalCertExpire() {
        return this.medicalCertExpire;
    }

    public void setMedicalCertExpire(Object medicalCertExpire) {
        this.medicalCertExpire = medicalCertExpire;
    }

    private Object medicalCertExpire;

    @JsonSetter("nextHomeDate")
    public Object getNextHomeDate() {
        return this.nextHomeDate;
    }

    public void setNextHomeDate(Object nextHomeDate) {
        this.nextHomeDate = nextHomeDate;
    }

    private Object nextHomeDate;

    @JsonSetter("returnFlag")
    public String getReturnFlag() {
        return this.returnFlag;
    }

    public void setReturnFlag(String returnFlag) {
        this.returnFlag = returnFlag;
    }

    private String returnFlag;

    @JsonSetter("mobileUnit")
    public String getMobileUnit() {
        return this.mobileUnit;
    }

    public void setMobileUnit(String mobileUnit) {
        this.mobileUnit = mobileUnit;
    }

    private String mobileUnit;

    @JsonSetter("engineSensor")
    public Object getEngineSensor() {
        return this.engineSensor;
    }

    public void setEngineSensor(Object engineSensor) {
        this.engineSensor = engineSensor;
    }

    private Object engineSensor;

    @JsonSetter("login")
    public Object getLogin() {
        return this.login;
    }

    public void setLogin(Object login) {
        this.login = login;
    }

    private Object login;

    @JsonSetter("site")
    public Object getSite() {
        return this.site;
    }

    public void setSite(Object site) {
        this.site = site;
    }

    private Object site;

    @JsonSetter("returnFlagDescr")
    public String getReturnFlagDescr() {
        return this.returnFlagDescr;
    }

    public void setReturnFlagDescr(String returnFlagDescr) {
        this.returnFlagDescr = returnFlagDescr;
    }

    private String returnFlagDescr;

    @JsonSetter("lastHomeDate")
    public String getLastHomeDate() {
        return this.lastHomeDate;
    }

    public void setLastHomeDate(String lastHomeDate) {
        this.lastHomeDate = lastHomeDate;
    }

    private String lastHomeDate;

    @JsonSetter("recruiter")
    public String getRecruiter() {
        return this.recruiter;
    }

    public void setRecruiter(String recruiter) {
        this.recruiter = recruiter;
    }

    private String recruiter;

    @JsonSetter("crstTermDate")
    public Object getCrstTermDate() {
        return this.crstTermDate;
    }

    public void setCrstTermDate(Object crstTermDate) {
        this.crstTermDate = crstTermDate;
    }

    private Object crstTermDate;

    @JsonSetter("expAtHire")
    public String getExpAtHire() {
        return this.expAtHire;
    }

    public void setExpAtHire(String expAtHire) {
        this.expAtHire = expAtHire;
    }

    private String expAtHire;

    @JsonSetter("dot_log_required")
    public String getDot_log_required() {
        return this.dot_log_required;
    }

    public void setDot_log_required(String dot_log_required) {
        this.dot_log_required = dot_log_required;
    }

    private String dot_log_required;

    @JsonSetter("pharma")
    public Object getPharma() {
        return this.pharma;
    }

    public void setPharma(Object pharma) {
        this.pharma = pharma;
    }

    private Object pharma;

    @JsonSetter("rxcBackup")
    public Object getRxcBackup() {
        return this.rxcBackup;
    }

    public void setRxcBackup(Object rxcBackup) {
        this.rxcBackup = rxcBackup;
    }

    private Object rxcBackup;

    @JsonSetter("canada")
    public Object getCanada() {
        return this.canada;
    }

    public void setCanada(Object canada) {
        this.canada = canada;
    }

    private Object canada;

    @JsonSetter("doublesCertified")
    public String getDoublesCertified() {
        return this.doublesCertified;
    }

    public void setDoublesCertified(String doublesCertified) {
        this.doublesCertified = doublesCertified;
    }

    private String doublesCertified;

    @JsonSetter("doublesEndorsement")
    public String getDoublesEndorsement() {
        return this.doublesEndorsement;
    }

    public void setDoublesEndorsement(String doublesEndorsement) {
        this.doublesEndorsement = doublesEndorsement;
    }

    private String doublesEndorsement;

    @JsonSetter("warningLetters")
    public List<Object> getWarningLetters() {
        return this.warningLetters;
    }

    public void setWarningLetters(List<Object> warningLetters) {
        this.warningLetters = warningLetters;
    }

    private List<Object> warningLetters;

    @JsonSetter("trafficViolations")
    public List<Object> getTrafficViolations() {
        return this.trafficViolations;
    }

    public void setTrafficViolations(List<Object> trafficViolations) {
        this.trafficViolations = trafficViolations;
    }

    private List<Object> trafficViolations;

    @JsonSetter("contacts")
    public List<Object> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<Object> contacts) {
        this.contacts = contacts;
    }

    private List<Object> contacts;

    @JsonSetter("driverManager")
    public String getDriverManager() {
        return this.driverManager;
    }

    public void setDriverManager(String driverManager) {
        this.driverManager = driverManager;
    }

    private String driverManager;

    @JsonSetter("fleetManager")
    public Object getFleetManager() {
        return this.fleetManager;
    }

    public void setFleetManager(Object fleetManager) {
        this.fleetManager = fleetManager;
    }

    private Object fleetManager;

    @JsonSetter("availableHours")
    public Object getAvailableHours() {
        return this.availableHours;
    }

    public void setAvailableHours(Object availableHours) {
        this.availableHours = availableHours;
    }

    private Object availableHours;

    @JsonSetter("pta")
    public String getPta() {
        return this.pta;
    }

    public void setPta(String pta) {
        this.pta = pta;
    }

    private String pta;

    @JsonSetter("homeTerminalDisplay")
    public Object getHomeTerminalDisplay() {
        return this.homeTerminalDisplay;
    }

    public void setHomeTerminalDisplay(Object homeTerminalDisplay) {
        this.homeTerminalDisplay = homeTerminalDisplay;
    }

    private Object homeTerminalDisplay;

    @JsonSetter("preferredDaysOut")
    public Object getPreferredDaysOut() {
        return this.preferredDaysOut;
    }

    public void setPreferredDaysOut(Object preferredDaysOut) {
        this.preferredDaysOut = preferredDaysOut;
    }

    private Object preferredDaysOut;

    @JsonSetter("driveTimeAvailable")
    public double getDriveTimeAvailable() {
        return this.driveTimeAvailable;
    }

    public void setDriveTimeAvailable(double driveTimeAvailable) {
        this.driveTimeAvailable = driveTimeAvailable;
    }

    private double driveTimeAvailable;

    @JsonSetter("onDutyTimeAvailable")
    public double getOnDutyTimeAvailable() {
        return this.onDutyTimeAvailable;
    }

    public void setOnDutyTimeAvailable(double onDutyTimeAvailable) {
        this.onDutyTimeAvailable = onDutyTimeAvailable;
    }

    private double onDutyTimeAvailable;

    @JsonSetter("asOfDate")
    public Object getAsOfDate() {
        return this.asOfDate;
    }

    public void setAsOfDate(Object asOfDate) {
        this.asOfDate = asOfDate;
    }

    private Object asOfDate;

    @JsonSetter("issuedEquipments")
    public List<Object> getIssuedEquipments() {
        return this.issuedEquipments;
    }

    public void setIssuedEquipments(List<Object> issuedEquipments) {
        this.issuedEquipments = issuedEquipments;
    }

    private List<Object> issuedEquipments;

    @JsonSetter("seniorLeadDate")
    public Object getSeniorLeadDate() {
        return this.seniorLeadDate;
    }

    public void setSeniorLeadDate(Object seniorLeadDate) {
        this.seniorLeadDate = seniorLeadDate;
    }

    private Object seniorLeadDate;

    @JsonSetter("transferDate")
    public Object getTransferDate() {
        return this.transferDate;
    }

    public void setTransferDate(Object transferDate) {
        this.transferDate = transferDate;
    }

    private Object transferDate;

    @JsonSetter("contractExpirationDate")
    public Object getContractExpirationDate() {
        return this.contractExpirationDate;
    }

    public void setContractExpirationDate(Object contractExpirationDate) {
        this.contractExpirationDate = contractExpirationDate;
    }

    private Object contractExpirationDate;

    @JsonSetter("latitude")
    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    private double latitude;

    @JsonSetter("longitude")
    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private double longitude;

    @JsonSetter("isActive")
    public String getIsActive() {
        return this.isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    private String isActive;

    @JsonSetter("hazmatCertified")
    public String getHazmatCertified() {
        return this.hazmatCertified;
    }

    public void setHazmatCertified(String hazmatCertified) {
        this.hazmatCertified = hazmatCertified;
    }

    private String hazmatCertified;

    @JsonSetter("licenseTypeDescr")
    public String getLicenseTypeDescr() {
        return this.licenseTypeDescr;
    }

    public void setLicenseTypeDescr(String licenseTypeDescr) {
        this.licenseTypeDescr = licenseTypeDescr;
    }

    private String licenseTypeDescr;

    @JsonSetter("operationTypeDescr")
    public String getOperationTypeDescr() {
        return this.operationTypeDescr;
    }

    public void setOperationTypeDescr(String operationTypeDescr) {
        this.operationTypeDescr = operationTypeDescr;
    }

    private String operationTypeDescr;

    @JsonSetter("fuelCards")
    public List<Object> getFuelCards() {
        return this.fuelCards;
    }

    public void setFuelCards(List<Object> fuelCards) {
        this.fuelCards = fuelCards;
    }

    private List<Object> fuelCards;

    @JsonSetter("messages")
    public List<Object> getMessages() {
        return this.messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    private List<Object> messages;

    @JsonSetter("events")
    public List<Object> getEvents() {
        return this.events;
    }

    public void setEvents(List<Object> events) {
        this.events = events;
    }

    private List<Object> events;

    @JsonSetter("midInitialName")
    public Object getMidInitialName() {
        return this.midInitialName;
    }

    public void setMidInitialName(Object midInitialName) {
        this.midInitialName = midInitialName;
    }

    private Object midInitialName;
}
