package pojoClasses.responsePojo.MasterData.FuelCharge;

import com.fasterxml.jackson.annotation.JsonSetter;

public class FuelCharge {
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("averagePrice")
    public String getAveragePrice() {
        return this.averagePrice; }
    public void setAveragePrice(String averagePrice) {
        this.averagePrice = averagePrice; }
    String averagePrice;
    @JsonSetter("californiaPrice")
    public Object getCaliforniaPrice() {
        return this.californiaPrice; }
    public void setCaliforniaPrice(Object californiaPrice) {
        this.californiaPrice = californiaPrice; }
    Object californiaPrice;
    @JsonSetter("controlAtlanticPrice")
    public String getControlAtlanticPrice() {
        return this.controlAtlanticPrice; }
    public void setControlAtlanticPrice(String controlAtlanticPrice) {
        this.controlAtlanticPrice = controlAtlanticPrice; }
    String controlAtlanticPrice;
    @JsonSetter("eastCoastPrice")
    public String getEastCoastPrice() {
        return this.eastCoastPrice; }
    public void setEastCoastPrice(String eastCoastPrice) {
        this.eastCoastPrice = eastCoastPrice; }
    String eastCoastPrice;
    @JsonSetter("gulfCoastPrice")
    public String getGulfCoastPrice() {
        return this.gulfCoastPrice; }
    public void setGulfCoastPrice(String gulfCoastPrice) {
        this.gulfCoastPrice = gulfCoastPrice; }
    String gulfCoastPrice;
    @JsonSetter("lowerAtlanticPrice")
    public String getLowerAtlanticPrice() {
        return this.lowerAtlanticPrice; }
    public void setLowerAtlanticPrice(String lowerAtlanticPrice) {
        this.lowerAtlanticPrice = lowerAtlanticPrice; }
    String lowerAtlanticPrice;
    @JsonSetter("midwestPrice")
    public String getMidwestPrice() {
        return this.midwestPrice; }
    public void setMidwestPrice(String midwestPrice) {
        this.midwestPrice = midwestPrice; }
    String midwestPrice;
    @JsonSetter("newEnglandPrice")
    public String getNewEnglandPrice() {
        return this.newEnglandPrice; }
    public void setNewEnglandPrice(String newEnglandPrice) {
        this.newEnglandPrice = newEnglandPrice; }
    String newEnglandPrice;
    @JsonSetter("rockyMtnPrice")
    public String getRockyMtnPrice() {
        return this.rockyMtnPrice; }
    public void setRockyMtnPrice(String rockyMtnPrice) {
        this.rockyMtnPrice = rockyMtnPrice; }
    String rockyMtnPrice;
    @JsonSetter("westCoastPrice")
    public String getWestCoastPrice() {
        return this.westCoastPrice; }
    public void setWestCoastPrice(String westCoastPrice) {
        this.westCoastPrice = westCoastPrice; }
    String westCoastPrice;
    @JsonSetter("westCoastLessPrice")
    public String getWestCoastLessPrice() {
        return this.westCoastLessPrice; }
    public void setWestCoastLessPrice(String westCoastLessPrice) {
        this.westCoastLessPrice = westCoastLessPrice; }
    String westCoastLessPrice;
    @JsonSetter("priceDate")
    public String getPriceDate() {
        return this.priceDate; }
    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate; }
    String priceDate;
}
