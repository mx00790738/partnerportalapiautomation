package pojoClasses.responsePojo.MasterData.Common;

import com.fasterxml.jackson.annotation.JsonSetter;

public class CommentType {
    @JsonSetter("companyTypePkId")
    public CompanyTypePkId getCompanyTypePkId() {
        return this.companyTypePkId; }
    public void setCompanyTypePkId(CompanyTypePkId companyTypePkId) {
        this.companyTypePkId = companyTypePkId; }
    CompanyTypePkId companyTypePkId;
    @JsonSetter("commentTypeDescription")
    public String getCommentTypeDescription() {
        return this.commentTypeDescription; }
    public void setCommentTypeDescription(String commentTypeDescription) {
        this.commentTypeDescription = commentTypeDescription; }
    String commentTypeDescription;
    @JsonSetter("commentTypeIsActive")
    public String getCommentTypeIsActive() {
        return this.commentTypeIsActive; }
    public void setCommentTypeIsActive(String commentTypeIsActive) {
        this.commentTypeIsActive = commentTypeIsActive; }
    String commentTypeIsActive;
}
