package pojoClasses.responsePojo.MasterData.Common;

import com.fasterxml.jackson.annotation.JsonSetter;

public class EquipmentType {
    @JsonSetter("equipmentTypePkId")
    public EquipmentTypePkId getEquipmentTypePkId() {
        return this.equipmentTypePkId; }
    public void setEquipmentTypePkId(EquipmentTypePkId equipmentTypePkId) {
        this.equipmentTypePkId = equipmentTypePkId; }
    EquipmentTypePkId equipmentTypePkId;
    @JsonSetter("equipmentTypeDescr")
    public String getEquipmentTypeDescr() {
        return this.equipmentTypeDescr; }
    public void setEquipmentTypeDescr(String equipmentTypeDescr) {
        this.equipmentTypeDescr = equipmentTypeDescr; }
    String equipmentTypeDescr;
    @JsonSetter("equipmentTypeAppliesTo")
    public String getEquipmentTypeAppliesTo() {
        return this.equipmentTypeAppliesTo; }
    public void setEquipmentTypeAppliesTo(String equipmentTypeAppliesTo) {
        this.equipmentTypeAppliesTo = equipmentTypeAppliesTo; }
    String equipmentTypeAppliesTo;
}
