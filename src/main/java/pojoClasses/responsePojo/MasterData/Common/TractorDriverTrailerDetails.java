package pojoClasses.responsePojo.MasterData.Common;

import com.fasterxml.jackson.annotation.JsonSetter;

public class TractorDriverTrailerDetails {
    @JsonSetter("tractorId")
    public String getTractorId() {
        return this.tractorId; }
    public void setTractorId(String tractorId) {
        this.tractorId = tractorId; }
    String tractorId;
    @JsonSetter("tractorDispatcher")
    public Object getTractorDispatcher() {
        return this.tractorDispatcher; }
    public void setTractorDispatcher(Object tractorDispatcher) {
        this.tractorDispatcher = tractorDispatcher; }
    Object tractorDispatcher;
    @JsonSetter("tractorOwner")
    public Object getTractorOwner() {
        return this.tractorOwner; }
    public void setTractorOwner(Object tractorOwner) {
        this.tractorOwner = tractorOwner; }
    Object tractorOwner;
    @JsonSetter("tractorFleetId")
    public Object getTractorFleetId() {
        return this.tractorFleetId; }
    public void setTractorFleetId(Object tractorFleetId) {
        this.tractorFleetId = tractorFleetId; }
    Object tractorFleetId;
    @JsonSetter("tractorStatus")
    public String getTractorStatus() {
        return this.tractorStatus; }
    public void setTractorStatus(String tractorStatus) {
        this.tractorStatus = tractorStatus; }
    String tractorStatus;
    @JsonSetter("currentEqpGrpId")
    public String getCurrentEqpGrpId() {
        return this.currentEqpGrpId; }
    public void setCurrentEqpGrpId(String currentEqpGrpId) {
        this.currentEqpGrpId = currentEqpGrpId; }
    String currentEqpGrpId;
    @JsonSetter("driver1Id")
    public String getDriver1Id() {
        return this.driver1Id; }
    public void setDriver1Id(String driver1Id) {
        this.driver1Id = driver1Id; }
    String driver1Id;
    @JsonSetter("driver1Name")
    public String getDriver1Name() {
        return this.driver1Name; }
    public void setDriver1Name(String driver1Name) {
        this.driver1Name = driver1Name; }
    String driver1Name;
    @JsonSetter("driver1Manager")
    public String getDriver1Manager() {
        return this.driver1Manager; }
    public void setDriver1Manager(String driver1Manager) {
        this.driver1Manager = driver1Manager; }
    String driver1Manager;
    @JsonSetter("driver1Type")
    public String getDriver1Type() {
        return this.driver1Type; }
    public void setDriver1Type(String driver1Type) {
        this.driver1Type = driver1Type; }
    String driver1Type;
    @JsonSetter("driver1payeeId")
    public String getDriver1payeeId() {
        return this.driver1payeeId; }
    public void setDriver1payeeId(String driver1payeeId) {
        this.driver1payeeId = driver1payeeId; }
    String driver1payeeId;
    @JsonSetter("driver1Location")
    public String getDriver1Location() {
        return this.driver1Location; }
    public void setDriver1Location(String driver1Location) {
        this.driver1Location = driver1Location; }
    String driver1Location;
    @JsonSetter("driver2Id")
    public String getDriver2Id() {
        return this.driver2Id; }
    public void setDriver2Id(String driver2Id) {
        this.driver2Id = driver2Id; }
    String driver2Id;
    @JsonSetter("driver2Name")
    public String getDriver2Name() {
        return this.driver2Name; }
    public void setDriver2Name(String driver2Name) {
        this.driver2Name = driver2Name; }
    String driver2Name;
    @JsonSetter("driver2Manager")
    public String getDriver2Manager() {
        return this.driver2Manager; }
    public void setDriver2Manager(String driver2Manager) {
        this.driver2Manager = driver2Manager; }
    String driver2Manager;
    @JsonSetter("driver2Type")
    public String getDriver2Type() {
        return this.driver2Type; }
    public void setDriver2Type(String driver2Type) {
        this.driver2Type = driver2Type; }
    String driver2Type;
    @JsonSetter("driver2payeeId")
    public String getDriver2payeeId() {
        return this.driver2payeeId; }
    public void setDriver2payeeId(String driver2payeeId) {
        this.driver2payeeId = driver2payeeId; }
    String driver2payeeId;
    @JsonSetter("driver2Location")
    public String getDriver2Location() {
        return this.driver2Location; }
    public void setDriver2Location(String driver2Location) {
        this.driver2Location = driver2Location; }
    String driver2Location;
    @JsonSetter("trailer1Id")
    public String getTrailer1Id() {
        return this.trailer1Id; }
    public void setTrailer1Id(String trailer1Id) {
        this.trailer1Id = trailer1Id; }
    String trailer1Id;
    @JsonSetter("trailer2Id")
    public Object getTrailer2Id() {
        return this.trailer2Id; }
    public void setTrailer2Id(Object trailer2Id) {
        this.trailer2Id = trailer2Id; }
    Object trailer2Id;
    @JsonSetter("tractorLastLocation")
    public String getTractorLastLocation() {
        return this.tractorLastLocation; }
    public void setTractorLastLocation(String tractorLastLocation) {
        this.tractorLastLocation = tractorLastLocation; }
    String tractorLastLocation;
    @JsonSetter("nextHome")
    public String getNextHome() {
        return this.nextHome; }
    public void setNextHome(String nextHome) {
        this.nextHome = nextHome; }
    String nextHome;
    @JsonSetter("nextAvailable")
    public Object getNextAvailable() {
        return this.nextAvailable; }
    public void setNextAvailable(Object nextAvailable) {
        this.nextAvailable = nextAvailable; }
    Object nextAvailable;
    @JsonSetter("driver1Cell")
    public Object getDriver1Cell() {
        return this.driver1Cell; }
    public void setDriver1Cell(Object driver1Cell) {
        this.driver1Cell = driver1Cell; }
    Object driver1Cell;
    @JsonSetter("driver1Phone")
    public Object getDriver1Phone() {
        return this.driver1Phone; }
    public void setDriver1Phone(Object driver1Phone) {
        this.driver1Phone = driver1Phone; }
    Object driver1Phone;
    @JsonSetter("driver1IsActive")
    public String getDriver1IsActive() {
        return this.driver1IsActive; }
    public void setDriver1IsActive(String driver1IsActive) {
        this.driver1IsActive = driver1IsActive; }
    String driver1IsActive;
    @JsonSetter("driver2IsActive")
    public String getDriver2IsActive() {
        return this.driver2IsActive; }
    public void setDriver2IsActive(String driver2IsActive) {
        this.driver2IsActive = driver2IsActive; }
    String driver2IsActive;
    @JsonSetter("trailer1IsActive")
    public String getTrailer1IsActive() {
        return this.trailer1IsActive; }
    public void setTrailer1IsActive(String trailer1IsActive) {
        this.trailer1IsActive = trailer1IsActive; }
    String trailer1IsActive;
    @JsonSetter("trailer2IsActive")
    public Object getTrailer2IsActive() {
        return this.trailer2IsActive; }
    public void setTrailer2IsActive(Object trailer2IsActive) {
        this.trailer2IsActive = trailer2IsActive; }
    Object trailer2IsActive;
    @JsonSetter("status")
    public Object getStatus() {
        return this.status; }
    public void setStatus(Object status) {
        this.status = status; }
    Object status;
}
