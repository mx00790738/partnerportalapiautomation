package pojoClasses.responsePojo.MasterData.Common;

import com.fasterxml.jackson.annotation.JsonSetter;

public class CompanyTypePkId {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("commentTypeId")
    public String getCommentTypeId() {
        return this.commentTypeId; }
    public void setCommentTypeId(String commentTypeId) {
        this.commentTypeId = commentTypeId; }
    String commentTypeId;
}
