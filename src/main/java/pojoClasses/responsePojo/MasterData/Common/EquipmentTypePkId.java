package pojoClasses.responsePojo.MasterData.Common;

import com.fasterxml.jackson.annotation.JsonSetter;

public class EquipmentTypePkId {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("equipmentTypeId")
    public String getEquipmentTypeId() {
        return this.equipmentTypeId; }
    public void setEquipmentTypeId(String equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId; }
    String equipmentTypeId;
}
