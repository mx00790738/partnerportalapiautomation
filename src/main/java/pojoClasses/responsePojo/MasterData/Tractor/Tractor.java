package pojoClasses.responsePojo.MasterData.Tractor;

import com.fasterxml.jackson.annotation.JsonSetter;

public class Tractor {
    @JsonSetter("tractorId")
    public String getTractorId() {
        return this.tractorId; }
    public void setTractorId(String tractorId) {
        this.tractorId = tractorId; }
    String tractorId;
    @JsonSetter("allocation")
    public String getAllocation() {
        return this.allocation; }
    public void setAllocation(String allocation) {
        this.allocation = allocation; }
    String allocation;
    @JsonSetter("allowRelay")
    public String getAllowRelay() {
        return this.allowRelay; }
    public void setAllowRelay(String allowRelay) {
        this.allowRelay = allowRelay; }
    String allowRelay;
    @JsonSetter("assign_date")
    public String getAssign_date() {
        return this.assign_date; }
    public void setAssign_date(String assign_date) {
        this.assign_date = assign_date; }
    String assign_date;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("currentEquipmentGroup")
    public String getCurrentEquipmentGroup() {
        return this.currentEquipmentGroup; }
    public void setCurrentEquipmentGroup(String currentEquipmentGroup) {
        this.currentEquipmentGroup = currentEquipmentGroup; }
    String currentEquipmentGroup;
    @JsonSetter("dispatcherOpt")
    public String getDispatcherOpt() {
        return this.dispatcherOpt; }
    public void setDispatcherOpt(String dispatcherOpt) {
        this.dispatcherOpt = dispatcherOpt; }
    String dispatcherOpt;
    @JsonSetter("dispatcher")
    public Object getDispatcher() {
        return this.dispatcher; }
    public void setDispatcher(Object dispatcher) {
        this.dispatcher = dispatcher; }
    Object dispatcher;
    @JsonSetter("dispatcherLogin")
    public String getDispatcherLogin() {
        return this.dispatcherLogin; }
    public void setDispatcherLogin(String dispatcherLogin) {
        this.dispatcherLogin = dispatcherLogin; }
    String dispatcherLogin;
    @JsonSetter("driver1Id")
    public String getDriver1Id() {
        return this.driver1Id; }
    public void setDriver1Id(String driver1Id) {
        this.driver1Id = driver1Id; }
    String driver1Id;
    @JsonSetter("driver2Id")
    public Object getDriver2Id() {
        return this.driver2Id; }
    public void setDriver2Id(Object driver2Id) {
        this.driver2Id = driver2Id; }
    Object driver2Id;
    @JsonSetter("equipOwner")
    public Object getEquipOwner() {
        return this.equipOwner; }
    public void setEquipOwner(Object equipOwner) {
        this.equipOwner = equipOwner; }
    Object equipOwner;
    @JsonSetter("excludeFuelTax")
    public String getExcludeFuelTax() {
        return this.excludeFuelTax; }
    public void setExcludeFuelTax(String excludeFuelTax) {
        this.excludeFuelTax = excludeFuelTax; }
    String excludeFuelTax;
    @JsonSetter("fleet")
    public Object getFleet() {
        return this.fleet; }
    public void setFleet(Object fleet) {
        this.fleet = fleet; }
    Object fleet;
    @JsonSetter("forceAssign")
    public String getForceAssign() {
        return this.forceAssign; }
    public void setForceAssign(String forceAssign) {
        this.forceAssign = forceAssign; }
    String forceAssign;
    @JsonSetter("inserviceDate")
    public String getInserviceDate() {
        return this.inserviceDate; }
    public void setInserviceDate(String inserviceDate) {
        this.inserviceDate = inserviceDate; }
    String inserviceDate;
    @JsonSetter("isLocal")
    public String getIsLocal() {
        return this.isLocal; }
    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal; }
    String isLocal;
    @JsonSetter("liabilityInsured")
    public String getLiabilityInsured() {
        return this.liabilityInsured; }
    public void setLiabilityInsured(String liabilityInsured) {
        this.liabilityInsured = liabilityInsured; }
    String liabilityInsured;
    @JsonSetter("make")
    public String getMake() {
        return this.make; }
    public void setMake(String make) {
        this.make = make; }
    String make;
    @JsonSetter("model")
    public Object getModel() {
        return this.model; }
    public void setModel(Object model) {
        this.model = model; }
    Object model;
    @JsonSetter("modelYear")
    public String getModelYear() {
        return this.modelYear; }
    public void setModelYear(String modelYear) {
        this.modelYear = modelYear; }
    String modelYear;
    @JsonSetter("outserviceDate")
    public Object getOutserviceDate() {
        return this.outserviceDate; }
    public void setOutserviceDate(Object outserviceDate) {
        this.outserviceDate = outserviceDate; }
    Object outserviceDate;
    @JsonSetter("owner")
    public Object getOwner() {
        return this.owner; }
    public void setOwner(Object owner) {
        this.owner = owner; }
    Object owner;
    @JsonSetter("payOwner")
    public String getPayOwner() {
        return this.payOwner; }
    public void setPayOwner(String payOwner) {
        this.payOwner = payOwner; }
    String payOwner;
    @JsonSetter("phantom")
    public String getPhantom() {
        return this.phantom; }
    public void setPhantom(String phantom) {
        this.phantom = phantom; }
    String phantom;
    @JsonSetter("planningComment")
    public Object getPlanningComment() {
        return this.planningComment; }
    public void setPlanningComment(Object planningComment) {
        this.planningComment = planningComment; }
    Object planningComment;
    @JsonSetter("pnnAvailFromCity")
    public Object getPnnAvailFromCity() {
        return this.pnnAvailFromCity; }
    public void setPnnAvailFromCity(Object pnnAvailFromCity) {
        this.pnnAvailFromCity = pnnAvailFromCity; }
    Object pnnAvailFromCity;
    @JsonSetter("pnnAvailFromState")
    public Object getPnnAvailFromState() {
        return this.pnnAvailFromState; }
    public void setPnnAvailFromState(Object pnnAvailFromState) {
        this.pnnAvailFromState = pnnAvailFromState; }
    Object pnnAvailFromState;
    @JsonSetter("pnnAvailFromZip")
    public Object getPnnAvailFromZip() {
        return this.pnnAvailFromZip; }
    public void setPnnAvailFromZip(Object pnnAvailFromZip) {
        this.pnnAvailFromZip = pnnAvailFromZip; }
    Object pnnAvailFromZip;
    @JsonSetter("pnnAvailToCity")
    public Object getPnnAvailToCity() {
        return this.pnnAvailToCity; }
    public void setPnnAvailToCity(Object pnnAvailToCity) {
        this.pnnAvailToCity = pnnAvailToCity; }
    Object pnnAvailToCity;
    @JsonSetter("pnnAvailToState")
    public Object getPnnAvailToState() {
        return this.pnnAvailToState; }
    public void setPnnAvailToState(Object pnnAvailToState) {
        this.pnnAvailToState = pnnAvailToState; }
    Object pnnAvailToState;
    @JsonSetter("pnnAvailToType")
    public String getPnnAvailToType() {
        return this.pnnAvailToType; }
    public void setPnnAvailToType(String pnnAvailToType) {
        this.pnnAvailToType = pnnAvailToType; }
    String pnnAvailToType;
    @JsonSetter("pnnComment")
    public Object getPnnComment() {
        return this.pnnComment; }
    public void setPnnComment(Object pnnComment) {
        this.pnnComment = pnnComment; }
    Object pnnComment;
    @JsonSetter("pnnComment2")
    public Object getPnnComment2() {
        return this.pnnComment2; }
    public void setPnnComment2(Object pnnComment2) {
        this.pnnComment2 = pnnComment2; }
    Object pnnComment2;
    @JsonSetter("pnnCubicFeet")
    public Object getPnnCubicFeet() {
        return this.pnnCubicFeet; }
    public void setPnnCubicFeet(Object pnnCubicFeet) {
        this.pnnCubicFeet = pnnCubicFeet; }
    Object pnnCubicFeet;
    @JsonSetter("pnnDeckLength")
    public Object getPnnDeckLength() {
        return this.pnnDeckLength; }
    public void setPnnDeckLength(Object pnnDeckLength) {
        this.pnnDeckLength = pnnDeckLength; }
    Object pnnDeckLength;
    @JsonSetter("pnnEnhancements")
    public Object getPnnEnhancements() {
        return this.pnnEnhancements; }
    public void setPnnEnhancements(Object pnnEnhancements) {
        this.pnnEnhancements = pnnEnhancements; }
    Object pnnEnhancements;
    @JsonSetter("pnnLoadWeight")
    public Object getPnnLoadWeight() {
        return this.pnnLoadWeight; }
    public void setPnnLoadWeight(Object pnnLoadWeight) {
        this.pnnLoadWeight = pnnLoadWeight; }
    Object pnnLoadWeight;
    @JsonSetter("pnnCallback")
    public Object getPnnCallback() {
        return this.pnnCallback; }
    public void setPnnCallback(Object pnnCallback) {
        this.pnnCallback = pnnCallback; }
    Object pnnCallback;
    @JsonSetter("pnnPostType")
    public String getPnnPostType() {
        return this.pnnPostType; }
    public void setPnnPostType(String pnnPostType) {
        this.pnnPostType = pnnPostType; }
    String pnnPostType;
    @JsonSetter("pwu_uid")
    public String getPwu_uid() {
        return this.pwu_uid; }
    public void setPwu_uid(String pwu_uid) {
        this.pwu_uid = pwu_uid; }
    String pwu_uid;
    @JsonSetter("serialNum")
    public String getSerialNum() {
        return this.serialNum; }
    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum; }
    String serialNum;
    @JsonSetter("serviceStatus")
    public String getServiceStatus() {
        return this.serviceStatus; }
    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus; }
    String serviceStatus;
    @JsonSetter("status")
    public Object getStatus() {
        return this.status; }
    public void setStatus(Object status) {
        this.status = status; }
    Object status;
    @JsonSetter("swap")
    public String getSwap() {
        return this.swap; }
    public void setSwap(String swap) {
        this.swap = swap; }
    String swap;
    @JsonSetter("tag")
    public Object getTag() {
        return this.tag; }
    public void setTag(Object tag) {
        this.tag = tag; }
    Object tag;
    @JsonSetter("tagState")
    public Object getTagState() {
        return this.tagState; }
    public void setTagState(Object tagState) {
        this.tagState = tagState; }
    Object tagState;
    @JsonSetter("tagExpires")
    public Object getTagExpires() {
        return this.tagExpires; }
    public void setTagExpires(Object tagExpires) {
        this.tagExpires = tagExpires; }
    Object tagExpires;
    @JsonSetter("weight")
    public Object getWeight() {
        return this.weight; }
    public void setWeight(Object weight) {
        this.weight = weight; }
    Object weight;
    @JsonSetter("weightUoM")
    public String getWeightUoM() {
        return this.weightUoM; }
    public void setWeightUoM(String weightUoM) {
        this.weightUoM = weightUoM; }
    String weightUoM;
    @JsonSetter("zmitFuelOpt")
    public String getZmitFuelOpt() {
        return this.zmitFuelOpt; }
    public void setZmitFuelOpt(String zmitFuelOpt) {
        this.zmitFuelOpt = zmitFuelOpt; }
    String zmitFuelOpt;
    @JsonSetter("excludeFromTripMgt")
    public Object getExcludeFromTripMgt() {
        return this.excludeFromTripMgt; }
    public void setExcludeFromTripMgt(Object excludeFromTripMgt) {
        this.excludeFromTripMgt = excludeFromTripMgt; }
    Object excludeFromTripMgt;
    @JsonSetter("maintenanceHubA")
    public Object getMaintenanceHubA() {
        return this.maintenanceHubA; }
    public void setMaintenanceHubA(Object maintenanceHubA) {
        this.maintenanceHubA = maintenanceHubA; }
    Object maintenanceHubA;
    @JsonSetter("maintenanceHubB")
    public Object getMaintenanceHubB() {
        return this.maintenanceHubB; }
    public void setMaintenanceHubB(Object maintenanceHubB) {
        this.maintenanceHubB = maintenanceHubB; }
    Object maintenanceHubB;
    @JsonSetter("currentHub")
    public Object getCurrentHub() {
        return this.currentHub; }
    public void setCurrentHub(Object currentHub) {
        this.currentHub = currentHub; }
    Object currentHub;
    @JsonSetter("dashHubDelta")
    public Object getDashHubDelta() {
        return this.dashHubDelta; }
    public void setDashHubDelta(Object dashHubDelta) {
        this.dashHubDelta = dashHubDelta; }
    Object dashHubDelta;
    @JsonSetter("fillHub")
    public Object getFillHub() {
        return this.fillHub; }
    public void setFillHub(Object fillHub) {
        this.fillHub = fillHub; }
    Object fillHub;
    @JsonSetter("fuelCapacity")
    public String getFuelCapacity() {
        return this.fuelCapacity; }
    public void setFuelCapacity(String fuelCapacity) {
        this.fuelCapacity = fuelCapacity; }
    String fuelCapacity;
    @JsonSetter("fuelLevel")
    public String getFuelLevel() {
        return this.fuelLevel; }
    public void setFuelLevel(String fuelLevel) {
        this.fuelLevel = fuelLevel; }
    String fuelLevel;
    @JsonSetter("hubChange")
    public Object getHubChange() {
        return this.hubChange; }
    public void setHubChange(Object hubChange) {
        this.hubChange = hubChange; }
    Object hubChange;
    @JsonSetter("hubChangeDate")
    public Object getHubChangeDate() {
        return this.hubChangeDate; }
    public void setHubChangeDate(Object hubChangeDate) {
        this.hubChangeDate = hubChangeDate; }
    Object hubChangeDate;
    @JsonSetter("hubNew")
    public Object getHubNew() {
        return this.hubNew; }
    public void setHubNew(Object hubNew) {
        this.hubNew = hubNew; }
    Object hubNew;
    @JsonSetter("hubRequired")
    public Object getHubRequired() {
        return this.hubRequired; }
    public void setHubRequired(Object hubRequired) {
        this.hubRequired = hubRequired; }
    Object hubRequired;
    @JsonSetter("oilHub")
    public Object getOilHub() {
        return this.oilHub; }
    public void setOilHub(Object oilHub) {
        this.oilHub = oilHub; }
    Object oilHub;
    @JsonSetter("inspectionDate")
    public Object getInspectionDate() {
        return this.inspectionDate; }
    public void setInspectionDate(Object inspectionDate) {
        this.inspectionDate = inspectionDate; }
    Object inspectionDate;
    @JsonSetter("liabilityStartDate")
    public Object getLiabilityStartDate() {
        return this.liabilityStartDate; }
    public void setLiabilityStartDate(Object liabilityStartDate) {
        this.liabilityStartDate = liabilityStartDate; }
    Object liabilityStartDate;
    @JsonSetter("liabilityEndDate")
    public Object getLiabilityEndDate() {
        return this.liabilityEndDate; }
    public void setLiabilityEndDate(Object liabilityEndDate) {
        this.liabilityEndDate = liabilityEndDate; }
    Object liabilityEndDate;
    @JsonSetter("insuranceAmount")
    public Object getInsuranceAmount() {
        return this.insuranceAmount; }
    public void setInsuranceAmount(Object insuranceAmount) {
        this.insuranceAmount = insuranceAmount; }
    Object insuranceAmount;
    @JsonSetter("videoSafetyId")
    public Object getVideoSafetyId() {
        return this.videoSafetyId; }
    public void setVideoSafetyId(Object videoSafetyId) {
        this.videoSafetyId = videoSafetyId; }
    Object videoSafetyId;
    @JsonSetter("purchaseDate")
    public Object getPurchaseDate() {
        return this.purchaseDate; }
    public void setPurchaseDate(Object purchaseDate) {
        this.purchaseDate = purchaseDate; }
    Object purchaseDate;
}
