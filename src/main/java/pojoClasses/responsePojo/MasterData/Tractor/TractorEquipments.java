package pojoClasses.responsePojo.MasterData.Tractor;

import com.fasterxml.jackson.annotation.JsonSetter;

public class TractorEquipments {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("comments")
    public Object getComments() {
        return this.comments; }
    public void setComments(Object comments) {
        this.comments = comments; }
    Object comments;
    @JsonSetter("equipmentType")
    public String getEquipmentType() {
        return this.equipmentType; }
    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType; }
    String equipmentType;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("issuedDate")
    public String getIssuedDate() {
        return this.issuedDate; }
    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate; }
    String issuedDate;
    @JsonSetter("issuedUser")
    public String getIssuedUser() {
        return this.issuedUser; }
    public void setIssuedUser(String issuedUser) {
        this.issuedUser = issuedUser; }
    String issuedUser;
    @JsonSetter("parentRowId")
    public String getParentRowId() {
        return this.parentRowId; }
    public void setParentRowId(String parentRowId) {
        this.parentRowId = parentRowId; }
    String parentRowId;
    @JsonSetter("parentRowType")
    public String getParentRowType() {
        return this.parentRowType; }
    public void setParentRowType(String parentRowType) {
        this.parentRowType = parentRowType; }
    String parentRowType;
    @JsonSetter("quantity")
    public String getQuantity() {
        return this.quantity; }
    public void setQuantity(String quantity) {
        this.quantity = quantity; }
    String quantity;
    @JsonSetter("status")
    public String getStatus() {
        return this.status; }
    public void setStatus(String status) {
        this.status = status; }
    String status;
    @JsonSetter("statusDescr")
    public String getStatusDescr() {
        return this.statusDescr; }
    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr; }
    String statusDescr;
    @JsonSetter("equipTypeDescr")
    public String getEquipTypeDescr() {
        return this.equipTypeDescr; }
    public void setEquipTypeDescr(String equipTypeDescr) {
        this.equipTypeDescr = equipTypeDescr; }
    String equipTypeDescr;
    @JsonSetter("returnedDate")
    public Object getReturnedDate() {
        return this.returnedDate; }
    public void setReturnedDate(Object returnedDate) {
        this.returnedDate = returnedDate; }
    Object returnedDate;
    @JsonSetter("memo")
    public Object getMemo() {
        return this.memo; }
    public void setMemo(Object memo) {
        this.memo = memo; }
    Object memo;
    @JsonSetter("location")
    public Object getLocation() {
        return this.location; }
    public void setLocation(Object location) {
        this.location = location; }
    Object location;
    @JsonSetter("receive")
    public Object getReceive() {
        return this.receive; }
    public void setReceive(Object receive) {
        this.receive = receive; }
    Object receive;
    @JsonSetter("expiredDate")
    public Object getExpiredDate() {
        return this.expiredDate; }
    public void setExpiredDate(Object expiredDate) {
        this.expiredDate = expiredDate; }
    Object expiredDate;
    @JsonSetter("equipmentValue")
    public Object getEquipmentValue() {
        return this.equipmentValue; }
    public void setEquipmentValue(Object equipmentValue) {
        this.equipmentValue = equipmentValue; }
    Object equipmentValue;
}
