package pojoClasses.responsePojo.MasterData.Tractor;

import com.fasterxml.jackson.annotation.JsonSetter;

public class TractorMessages {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("message")
    public String getMessage() {
        return this.message; }
    public void setMessage(String message) {
        this.message = message; }
    String message;
    @JsonSetter("priority")
    public String getPriority() {
        return this.priority; }
    public void setPriority(String priority) {
        this.priority = priority; }
    String priority;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("messageDate")
    public String getMessageDate() {
        return this.messageDate; }
    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate; }
    String messageDate;
    @JsonSetter("unitType")
    public String getUnitType() {
        return this.unitType; }
    public void setUnitType(String unitType) {
        this.unitType = unitType; }
    String unitType;
    @JsonSetter("unit")
    public String getUnit() {
        return this.unit; }
    public void setUnit(String unit) {
        this.unit = unit; }
    String unit;
    @JsonSetter("status")
    public String getStatus() {
        return this.status; }
    public void setStatus(String status) {
        this.status = status; }
    String status;
}
