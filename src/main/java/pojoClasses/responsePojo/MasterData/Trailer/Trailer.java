package pojoClasses.responsePojo.MasterData.Trailer;

import com.fasterxml.jackson.annotation.JsonSetter;

public class Trailer {
    @JsonSetter("id")
    public Id getId() {
        return this.id; }
    public void setId(Id id) {
        this.id = id; }
    Id id;
    @JsonSetter("axles")
    public Object getAxles() {
        return this.axles; }
    public void setAxles(Object axles) {
        this.axles = axles; }
    Object axles;
    @JsonSetter("currentEquipmentGroup")
    public Object getCurrentEquipmentGroup() {
        return this.currentEquipmentGroup; }
    public void setCurrentEquipmentGroup(Object currentEquipmentGroup) {
        this.currentEquipmentGroup = currentEquipmentGroup; }
    Object currentEquipmentGroup;
    @JsonSetter("dollarValue")
    public Object getDollarValue() {
        return this.dollarValue; }
    public void setDollarValue(Object dollarValue) {
        this.dollarValue = dollarValue; }
    Object dollarValue;
    @JsonSetter("doorHeight")
    public Object getDoorHeight() {
        return this.doorHeight; }
    public void setDoorHeight(Object doorHeight) {
        this.doorHeight = doorHeight; }
    Object doorHeight;
    @JsonSetter("doorHeightUoM")
    public Object getDoorHeightUoM() {
        return this.doorHeightUoM; }
    public void setDoorHeightUoM(Object doorHeightUoM) {
        this.doorHeightUoM = doorHeightUoM; }
    Object doorHeightUoM;
    @JsonSetter("empty")
    public String getEmpty() {
        return this.empty; }
    public void setEmpty(String empty) {
        this.empty = empty; }
    String empty;
    @JsonSetter("features")
    public Object getFeatures() {
        return this.features; }
    public void setFeatures(Object features) {
        this.features = features; }
    Object features;
    @JsonSetter("floor_space")
    public Object getFloor_space() {
        return this.floor_space; }
    public void setFloor_space(Object floor_space) {
        this.floor_space = floor_space; }
    Object floor_space;
    @JsonSetter("grossWeight")
    public Object getGrossWeight() {
        return this.grossWeight; }
    public void setGrossWeight(Object grossWeight) {
        this.grossWeight = grossWeight; }
    Object grossWeight;
    @JsonSetter("grossWeightUoM")
    public Object getGrossWeightUoM() {
        return this.grossWeightUoM; }
    public void setGrossWeightUoM(Object grossWeightUoM) {
        this.grossWeightUoM = grossWeightUoM; }
    Object grossWeightUoM;
    @JsonSetter("height")
    public Object getHeight() {
        return this.height; }
    public void setHeight(Object height) {
        this.height = height; }
    Object height;
    @JsonSetter("heightUoM")
    public Object getHeightUoM() {
        return this.heightUoM; }
    public void setHeightUoM(Object heightUoM) {
        this.heightUoM = heightUoM; }
    Object heightUoM;
    @JsonSetter("inserviceDate")
    public JsonSetter getInserviceDate() {
        return this.inserviceDate; }
    public void setInserviceDate(JsonSetter inserviceDate) {
        this.inserviceDate = inserviceDate; }
    JsonSetter inserviceDate;
    @JsonSetter("inspectionDate")
    public Object getInspectionDate() {
        return this.inspectionDate; }
    public void setInspectionDate(Object inspectionDate) {
        this.inspectionDate = inspectionDate; }
    Object inspectionDate;
    @JsonSetter("status")
    public String getStatus() {
        return this.status; }
    public void setStatus(String status) {
        this.status = status; }
    String status;
    @JsonSetter("length")
    public Object getLength() {
        return this.length; }
    public void setLength(Object length) {
        this.length = length; }
    Object length;
    @JsonSetter("lengthUoM")
    public Object getLengthUoM() {
        return this.lengthUoM; }
    public void setLengthUoM(Object lengthUoM) {
        this.lengthUoM = lengthUoM; }
    Object lengthUoM;
    @JsonSetter("licenseNumber")
    public String getLicenseNumber() {
        return this.licenseNumber; }
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber; }
    String licenseNumber;
    @JsonSetter("licenseState")
    public Object getLicenseState() {
        return this.licenseState; }
    public void setLicenseState(Object licenseState) {
        this.licenseState = licenseState; }
    Object licenseState;
    @JsonSetter("make")
    public String getMake() {
        return this.make; }
    public void setMake(String make) {
        this.make = make; }
    String make;
    @JsonSetter("maxTemp")
    public Object getMaxTemp() {
        return this.maxTemp; }
    public void setMaxTemp(Object maxTemp) {
        this.maxTemp = maxTemp; }
    Object maxTemp;
    @JsonSetter("min_temp")
    public Object getMin_temp() {
        return this.min_temp; }
    public void setMin_temp(Object min_temp) {
        this.min_temp = min_temp; }
    Object min_temp;
    @JsonSetter("modelYear")
    public String getModelYear() {
        return this.modelYear; }
    public void setModelYear(String modelYear) {
        this.modelYear = modelYear; }
    String modelYear;
    @JsonSetter("pallets")
    public Object getPallets() {
        return this.pallets; }
    public void setPallets(Object pallets) {
        this.pallets = pallets; }
    Object pallets;
    @JsonSetter("planningComment")
    public Object getPlanningComment() {
        return this.planningComment; }
    public void setPlanningComment(Object planningComment) {
        this.planningComment = planningComment; }
    Object planningComment;
    @JsonSetter("purchaseDate")
    public Object getPurchaseDate() {
        return this.purchaseDate; }
    public void setPurchaseDate(Object purchaseDate) {
        this.purchaseDate = purchaseDate; }
    Object purchaseDate;
    @JsonSetter("reeferId")
    public Object getReeferId() {
        return this.reeferId; }
    public void setReeferId(Object reeferId) {
        this.reeferId = reeferId; }
    Object reeferId;
    @JsonSetter("serialNumber")
    public String getSerialNumber() {
        return this.serialNumber; }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber; }
    String serialNumber;
    @JsonSetter("tagExpireDate")
    public Object getTagExpireDate() {
        return this.tagExpireDate; }
    public void setTagExpireDate(Object tagExpireDate) {
        this.tagExpireDate = tagExpireDate; }
    Object tagExpireDate;
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("trlUid")
    public String getTrlUid() {
        return this.trlUid; }
    public void setTrlUid(String trlUid) {
        this.trlUid = trlUid; }
    String trlUid;
    @JsonSetter("volumeUoM")
    public Object getVolumeUoM() {
        return this.volumeUoM; }
    public void setVolumeUoM(Object volumeUoM) {
        this.volumeUoM = volumeUoM; }
    Object volumeUoM;
    @JsonSetter("volume")
    public Object getVolume() {
        return this.volume; }
    public void setVolume(Object volume) {
        this.volume = volume; }
    Object volume;
    @JsonSetter("weight")
    public Object getWeight() {
        return this.weight; }
    public void setWeight(Object weight) {
        this.weight = weight; }
    Object weight;
    @JsonSetter("weightUoM")
    public String getWeightUoM() {
        return this.weightUoM; }
    public void setWeightUoM(String weightUoM) {
        this.weightUoM = weightUoM; }
    String weightUoM;
    @JsonSetter("width")
    public Object getWidth() {
        return this.width; }
    public void setWidth(Object width) {
        this.width = width; }
    Object width;
    @JsonSetter("widthUoM")
    public String getWidthUoM() {
        return this.widthUoM; }
    public void setWidthUoM(String widthUoM) {
        this.widthUoM = widthUoM; }
    String widthUoM;
    @JsonSetter("ownership")
    public String getOwnership() {
        return this.ownership; }
    public void setOwnership(String ownership) {
        this.ownership = ownership; }
    String ownership;
    @JsonSetter("owner")
    public String getOwner() {
        return this.owner; }
    public void setOwner(String owner) {
        this.owner = owner; }
    String owner;
    @JsonSetter("trailerGroup")
    public Object getTrailerGroup() {
        return this.trailerGroup; }
    public void setTrailerGroup(Object trailerGroup) {
        this.trailerGroup = trailerGroup; }
    Object trailerGroup;
    @JsonSetter("odometer")
    public Object getOdometer() {
        return this.odometer; }
    public void setOdometer(Object odometer) {
        this.odometer = odometer; }
    Object odometer;
    @JsonSetter("lastOdometerDate")
    public Object getLastOdometerDate() {
        return this.lastOdometerDate; }
    public void setLastOdometerDate(Object lastOdometerDate) {
        this.lastOdometerDate = lastOdometerDate; }
    Object lastOdometerDate;
    @JsonSetter("trailerBatteryStatus")
    public Object getTrailerBatteryStatus() {
        return this.trailerBatteryStatus; }
    public void setTrailerBatteryStatus(Object trailerBatteryStatus) {
        this.trailerBatteryStatus = trailerBatteryStatus; }
    Object trailerBatteryStatus;
    @JsonSetter("disconnectDate")
    public Object getDisconnectDate() {
        return this.disconnectDate; }
    public void setDisconnectDate(Object disconnectDate) {
        this.disconnectDate = disconnectDate; }
    Object disconnectDate;
    @JsonSetter("outserviceDate")
    public Object getOutserviceDate() {
        return this.outserviceDate; }
    public void setOutserviceDate(Object outserviceDate) {
        this.outserviceDate = outserviceDate; }
    Object outserviceDate;
}
