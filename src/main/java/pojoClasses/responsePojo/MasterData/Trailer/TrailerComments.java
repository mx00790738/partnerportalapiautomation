package pojoClasses.responsePojo.MasterData.Trailer;

import com.fasterxml.jackson.annotation.JsonSetter;

public class TrailerComments {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("comments")
    public String getComments() {
        return this.comments; }
    public void setComments(String comments) {
        this.comments = comments; }
    String comments;
    @JsonSetter("parent_row_id")
    public String getParent_row_id() {
        return this.parent_row_id; }
    public void setParent_row_id(String parent_row_id) {
        this.parent_row_id = parent_row_id; }
    String parent_row_id;
    @JsonSetter("parent_row_type")
    public String getParent_row_type() {
        return this.parent_row_type; }
    public void setParent_row_type(String parent_row_type) {
        this.parent_row_type = parent_row_type; }
    String parent_row_type;
    @JsonSetter("display_atdispatch")
    public String getDisplay_atdispatch() {
        return this.display_atdispatch; }
    public void setDisplay_atdispatch(String display_atdispatch) {
        this.display_atdispatch = display_atdispatch; }
    String display_atdispatch;
    @JsonSetter("attach_filename")
    public Object getAttach_filename() {
        return this.attach_filename; }
    public void setAttach_filename(Object attach_filename) {
        this.attach_filename = attach_filename; }
    Object attach_filename;
    @JsonSetter("comment_type_id")
    public String getComment_type_id() {
        return this.comment_type_id; }
    public void setComment_type_id(String comment_type_id) {
        this.comment_type_id = comment_type_id; }
    String comment_type_id;
    @JsonSetter("copy_to_order")
    public String getCopy_to_order() {
        return this.copy_to_order; }
    public void setCopy_to_order(String copy_to_order) {
        this.copy_to_order = copy_to_order; }
    String copy_to_order;
    @JsonSetter("entered_date")
    public String getEntered_date() {
        return this.entered_date; }
    public void setEntered_date(String entered_date) {
        this.entered_date = entered_date; }
    String entered_date;
    @JsonSetter("entered_user_id")
    public String getEntered_user_id() {
        return this.entered_user_id; }
    public void setEntered_user_id(String entered_user_id) {
        this.entered_user_id = entered_user_id; }
    String entered_user_id;
    @JsonSetter("onboard_lock")
    public String getOnboard_lock() {
        return this.onboard_lock; }
    public void setOnboard_lock(String onboard_lock) {
        this.onboard_lock = onboard_lock; }
    String onboard_lock;
    @JsonSetter("order_comment_type")
    public Object getOrder_comment_type() {
        return this.order_comment_type; }
    public void setOrder_comment_type(Object order_comment_type) {
        this.order_comment_type = order_comment_type; }
    Object order_comment_type;
    @JsonSetter("descr")
    public String getDescr() {
        return this.descr; }
    public void setDescr(String descr) {
        this.descr = descr; }
    String descr;
}
