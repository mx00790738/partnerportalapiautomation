package pojoClasses.responsePojo.MasterData.Trailer;

import com.fasterxml.jackson.annotation.JsonSetter;

public class Id {
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
}
