package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class LoadUnloadType {
    private String type;
    private String companyId;
    private String descr;
    private String id;
    private Boolean isActive;

    public String getType() {
        return type;
    }
    @JsonSetter("__type")
    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDescr() {
        return descr;
    }
    @JsonSetter("descr")
    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }
    @JsonSetter("is_active")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
