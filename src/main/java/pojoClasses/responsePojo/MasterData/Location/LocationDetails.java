package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;
import java.util.List;

public class LocationDetails {
	private Object userId;
    private Object mcleodUserId;
    private String companyId;
    private Object agentPortalUserId;
    private Object agencyPayeeId;
    private Object agencyId;
    private Boolean active;
    private String id;
    private String name;
    private String googleId;
    private String addressLine1;
    private String addressLine2;
    private Integer cityId;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private Object contactOpType;
    private Boolean apptRequired;
    private DayOpenCloseMap dayOpenCloseMap;
    private List<LocationContact> contact = new ArrayList<LocationContact>();
    private List<LocationComment> comment;
    private Object consignments;
    private Object comments;
    private LocQualifierMap locQualifierMap;
    private List<Object> deletedContacts = new ArrayList<Object>();
    private String driverLoadId;
    private String driverUnloadId;
    private String pickupInstruction;
    private String dropInstruction;
    private String bseg;
    private String loadingTime;
    private String unloadingTime;
    private String directions;

    public Object getUserId() {
        return userId;
    }
    @JsonSetter("userId")
    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public Object getMcleodUserId() {
        return mcleodUserId;
    }
    @JsonSetter("mcleodUserId")
    public void setMcleodUserId(Object mcleodUserId) {
        this.mcleodUserId = mcleodUserId;
    }

    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("companyId")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Object getAgentPortalUserId() {
        return agentPortalUserId;
    }
    @JsonSetter("agentPortalUserId")
    public void setAgentPortalUserId(Object agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId;
    }

    public Object getAgencyPayeeId() {
        return agencyPayeeId;
    }
    @JsonSetter("agencyPayeeId")
    public void setAgencyPayeeId(Object agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId;
    }

    public Object getAgencyId() {
        return agencyId;
    }
    @JsonSetter("agencyId")
    public void setAgencyId(Object agencyId) {
        this.agencyId = agencyId;
    }

    public Boolean getActive() {
        return active;
    }
    @JsonSetter("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    @JsonSetter("name")
    public void setName(String name) {
        this.name = name;
    }

    public String getGoogleId() {
        return googleId;
    }
    @JsonSetter("googleId")
    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }
    @JsonSetter("addressLine1")
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }
    @JsonSetter("addressLine2")
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public Integer getCityId() {
        return cityId;
    }
    @JsonSetter("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }
    @JsonSetter("city")
    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }
    @JsonSetter("state")
    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }
    @JsonSetter("postalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }
    @JsonSetter("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Object getContactOpType() {
        return contactOpType;
    }
    @JsonSetter("contactOpType")
    public void setContactOpType(Object contactOpType) {
        this.contactOpType = contactOpType;
    }

    public Boolean getApptRequired() {
        return apptRequired;
    }
    @JsonSetter("apptRequired")
    public void setApptRequired(Boolean apptRequired) {
        this.apptRequired = apptRequired;
    }

    public DayOpenCloseMap getDayOpenCloseMap() {
        return dayOpenCloseMap;
    }
    @JsonSetter("dayOpenCloseMap")
    public void setDayOpenCloseMap(DayOpenCloseMap dayOpenCloseMap) {
        this.dayOpenCloseMap = dayOpenCloseMap;
    }

    public List<LocationContact> getContact() {
        return contact;
    }
    @JsonSetter("contact")
    public void setContact(List<LocationContact> contact) {
        this.contact = contact;
    }

    public List<LocationComment> getComment() {
        return comment;
    }
    @JsonSetter("comment")
    public void setComment(List<LocationComment> comment) {
        this.comment = comment;
    }

    public Object getConsignments() {
        return consignments;
    }
    @JsonSetter("consignments")
    public void setConsignments(Object consignments) {
        this.consignments = consignments;
    }

    public Object getComments() {
        return comments;
    }
    @JsonSetter("comments")
    public void setComments(Object comments) {
        this.comments = comments;
    }

    public LocQualifierMap getLocQualifierMap() {
        return locQualifierMap;
    }
    @JsonSetter("locQualifierMap")
    public void setLocQualifierMap(LocQualifierMap locQualifierMap) {
        this.locQualifierMap = locQualifierMap;
    }

    public List<Object> getDeletedContacts() {
        return deletedContacts;
    }
    @JsonSetter("deletedContacts")
    public void setDeletedContacts(List<Object> deletedContacts) {
        this.deletedContacts = deletedContacts;
    }

    public String getDriverLoadId() {
        return driverLoadId;
    }
    @JsonSetter("driverLoadId")
    public void setDriverLoadId(String driverLoadId) {
        this.driverLoadId = driverLoadId;
    }

    public String getDriverUnloadId() {
        return driverUnloadId;
    }
    @JsonSetter("driverUnloadId")
    public void setDriverUnloadId(String driverUnloadId) {
        this.driverUnloadId = driverUnloadId;
    }

    public String getPickupInstruction() {
        return pickupInstruction;
    }
    @JsonSetter("pickupInstruction")
    public void setPickupInstruction(String pickupInstruction) {
        this.pickupInstruction = pickupInstruction;
    }

    public String getDropInstruction() {
        return dropInstruction;
    }
    @JsonSetter("dropInstruction")
    public void setDropInstruction(String dropInstruction) {
        this.dropInstruction = dropInstruction;
    }

    public String getBseg() {
        return bseg;
    }
    @JsonSetter("bseg")
    public void setBseg(String bseg) {
        this.bseg = bseg;
    }

    public String getLoadingTime() {
        return loadingTime;
    }
    @JsonSetter("loadingTime")
    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getUnloadingTime() {
        return unloadingTime;
    }
    @JsonSetter("unloadingTime")
    public void setUnloadingTime(String unloadingTime) {
        this.unloadingTime = unloadingTime;
    }

    public String getDirections() {
        return directions;
    }
    @JsonSetter("directions")
    public void setDirections(String directions) {
        this.directions = directions;
    }
}
