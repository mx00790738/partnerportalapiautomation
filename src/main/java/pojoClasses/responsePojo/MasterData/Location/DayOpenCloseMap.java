package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DayOpenCloseMap {
	private Monday monday;
    private Thursday thursday;
    private Friday friday;
    private Sunday sunday;
    private Wednesday wednesday;
    private Tuesday tuesday;
    private Saturday saturday;

    public Monday getMonday() {
        return monday;
    }
    @JsonSetter("Monday")
    public void setMonday(Monday monday) {
        this.monday = monday;
    }

    public Thursday getThursday() {
        return thursday;
    }
    @JsonSetter("Thursday")
    public void setThursday(Thursday thursday) {
        this.thursday = thursday;
    }

    public Friday getFriday() {
        return friday;
    }
    @JsonSetter("Friday")
    public void setFriday(Friday friday) {
        this.friday = friday;
    }

    public Sunday getSunday() {
        return sunday;
    }
    @JsonSetter("Sunday")
    public void setSunday(Sunday sunday) {
        this.sunday = sunday;
    }

    public Wednesday getWednesday() {
        return wednesday;
    }
    @JsonSetter("Wednesday")
    public void setWednesday(Wednesday wednesday) {
        this.wednesday = wednesday;
    }

    public Tuesday getTuesday() {
        return tuesday;
    }
    @JsonSetter("Tuesday")
    public void setTuesday(Tuesday tuesday) {
        this.tuesday = tuesday;
    }

    public Saturday getSaturday() {
        return saturday;
    }
    @JsonSetter("Saturday")
    public void setSaturday(Saturday saturday) {
        this.saturday = saturday;
    }
}
