package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;
import pojoClasses.responsePojo.CommentTypeDescr;

public class LocationComment {
    private String type;
    private String companyId;
    private String comments;
    private String parentRowId;
    private String parentRowType;
    private String enteredUserId;
    private String enteredDate;
    private CommentTypeDescr commentTypeDescr;
    private String id;
    private String commentTypeId;

    public String getType() {
        return type;
    }
    @JsonSetter("__type")
    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getComments() {
        return comments;
    }
    @JsonSetter("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getParentRowId() {
        return parentRowId;
    }
    @JsonSetter("parent_row_id")
    public void setParentRowId(String parentRowId) {
        this.parentRowId = parentRowId;
    }

    public String getParentRowType() {
        return parentRowType;
    }
    @JsonSetter("parent_row_type")
    public void setParentRowType(String parentRowType) {
        this.parentRowType = parentRowType;
    }

    public String getEnteredUserId() {
        return enteredUserId;
    }
    @JsonSetter("entered_user_id")
    public void setEnteredUserId(String enteredUserId) {
        this.enteredUserId = enteredUserId;
    }

    public String getEnteredDate() {
        return enteredDate;
    }
    @JsonSetter("entered_date")
    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }

    public CommentTypeDescr getCommentTypeDescr() {
        return commentTypeDescr;
    }
    @JsonSetter("commentTypeDescr")
    public void setCommentTypeDescr(CommentTypeDescr commentTypeDescr) {
        this.commentTypeDescr = commentTypeDescr;
    }

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getCommentTypeId() {
        return commentTypeId;
    }
    @JsonSetter("comment_type_id")
    public void setCommentTypeId(String commentTypeId) {
        this.commentTypeId = commentTypeId;
    }
}
