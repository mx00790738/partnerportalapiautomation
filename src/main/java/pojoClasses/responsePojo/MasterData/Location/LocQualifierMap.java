package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class LocQualifierMap {
	private Boolean trailerPool;
    private Boolean trailerWash;
    private Boolean customer;
    private Boolean steamship;
    private Boolean customsBroker;
    private Boolean terminal;
    private Boolean outsideTerminal;
    private Boolean consignee;
    private Boolean shipper;
    private Boolean prospect;
    private Boolean truckstop;
    private Boolean dropYard;
    private Boolean distCenter;
    private Boolean geocoded;

    public Boolean getTrailerPool() {
        return trailerPool;
    }
    @JsonSetter("TrailerPool")
    public void setTrailerPool(Boolean trailerPool) {
        this.trailerPool = trailerPool;
    }

    public Boolean getTrailerWash() {
        return trailerWash;
    }
    @JsonSetter("TrailerWash")
    public void setTrailerWash(Boolean trailerWash) {
        this.trailerWash = trailerWash;
    }

    public Boolean getCustomer() {
        return customer;
    }
    @JsonSetter("Customer")
    public void setCustomer(Boolean customer) {
        this.customer = customer;
    }

    public Boolean getSteamship() {
        return steamship;
    }
    @JsonSetter("Steamship")
    public void setSteamship(Boolean steamship) {
        this.steamship = steamship;
    }

    public Boolean getCustomsBroker() {
        return customsBroker;
    }
    @JsonSetter("CustomsBroker")
    public void setCustomsBroker(Boolean customsBroker) {
        this.customsBroker = customsBroker;
    }

    public Boolean getTerminal() {
        return terminal;
    }
    @JsonSetter("Terminal")
    public void setTerminal(Boolean terminal) {
        this.terminal = terminal;
    }

    public Boolean getOutsideTerminal() {
        return outsideTerminal;
    }
    @JsonSetter("OutsideTerminal")
    public void setOutsideTerminal(Boolean outsideTerminal) {
        this.outsideTerminal = outsideTerminal;
    }

    public Boolean getConsignee() {
        return consignee;
    }
    @JsonSetter("Consignee")
    public void setConsignee(Boolean consignee) {
        this.consignee = consignee;
    }

    public Boolean getShipper() {
        return shipper;
    }
    @JsonSetter("Shipper")
    public void setShipper(Boolean shipper) {
        this.shipper = shipper;
    }

    public Boolean getProspect() {
        return prospect;
    }
    @JsonSetter("Prospect")
    public void setProspect(Boolean prospect) {
        this.prospect = prospect;
    }

    public Boolean getTruckstop() {
        return truckstop;
    }
    @JsonSetter("Truckstop")
    public void setTruckstop(Boolean truckstop) {
        this.truckstop = truckstop;
    }

    public Boolean getDropYard() {
        return dropYard;
    }
    @JsonSetter("DropYard")
    public void setDropYard(Boolean dropYard) {
        this.dropYard = dropYard;
    }

    public Boolean getDistCenter() {
        return distCenter;
    }
    @JsonSetter("DistCenter")
    public void setDistCenter(Boolean distCenter) {
        this.distCenter = distCenter;
    }

    public Boolean getGeocoded() {
        return geocoded;
    }
    @JsonSetter("Geocoded")
    public void setGeocoded(Boolean geocoded) {
        this.geocoded = geocoded;
    }
}
