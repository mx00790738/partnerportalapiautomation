package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class LocationContact {
    private String type;
    private String companyId;
    private String name;
    private String mobilePhone;
    private String email;
    private String parentRowId;
    private String parentRowType;
    private Boolean isActive;
    private String id;
    private Object comments;
    private String title;
    private String contactName;
    private String phone;
    private String fax;

    public String getType() {
        return type;
    }
    @JsonSetter("__type")
    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }
    @JsonSetter("name")
    public void setName(String name) {
        this.name = name;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }
    @JsonSetter("mobile_phone")
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }
    @JsonSetter("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentRowId() {
        return parentRowId;
    }
    @JsonSetter("parent_row_id")
    public void setParentRowId(String parentRowId) {
        this.parentRowId = parentRowId;
    }

    public String getParentRowType() {
        return parentRowType;
    }
    @JsonSetter("parent_row_type")
    public void setParentRowType(String parentRowType) {
        this.parentRowType = parentRowType;
    }

    public Boolean getIsActive() {
        return isActive;
    }
    @JsonSetter("is_active")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }

    public Object getComments() {
        return comments;
    }
    @JsonSetter("comments")
    public void setComments(Object comments) {
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }
    @JsonSetter("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContactName() {
        return contactName;
    }
    @JsonSetter("contact_name")
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }
    @JsonSetter("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }
    @JsonSetter("fax")
    public void setFax(String fax) {
        this.fax = fax;
    }
}
