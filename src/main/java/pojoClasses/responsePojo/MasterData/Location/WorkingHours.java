package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

//TODO - recheck
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkingHours {
    private Boolean apptRequired;
    private Time time;
    private String country;

    public Boolean getApptRequired() {
        return apptRequired;
    }
    @JsonSetter("apptRequired")
    public void setApptRequired(Boolean apptRequired) {
        this.apptRequired = apptRequired;
    }

    public Time getTime() {
        return time;
    }
    @JsonSetter("time")
    public void setTime(Time time) {
        this.time = time;
    }

    public String getCountry() {
        return country;
    }
    @JsonSetter("country")
    public void setCountry(String country) {
        this.country = country;
    }
}
