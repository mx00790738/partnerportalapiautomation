package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class Time {
    private Monday monday;
    private Thursday thursday;
    private Friday friday;
    private Sunday sunday;
    private Wednesday wednesday;
    private Tuesday tuesday;
    private Saturday saturday;

    public Monday getMonday() {
        return monday;
    }
    @JsonSetter("MONDAY")
    public void setMonday(Monday monday) {
        this.monday = monday;
    }

    public Thursday getThursday() {
        return thursday;
    }
    @JsonSetter("THURSDAY")
    public void setThursday(Thursday thursday) {
        this.thursday = thursday;
    }

    public Friday getFriday() {
        return friday;
    }
    @JsonSetter("FRIDAY")
    public void setFriday(Friday friday) {
        this.friday = friday;
    }

    public Sunday getSunday() {
        return sunday;
    }
    @JsonSetter("SUNDAY")
    public void setSunday(Sunday sunday) {
        this.sunday = sunday;
    }

    public Wednesday getWednesday() {
        return wednesday;
    }
    @JsonSetter("WEDNESDAY")
    public void setWednesday(Wednesday wednesday) {
        this.wednesday = wednesday;
    }

    public Tuesday getTuesday() {
        return tuesday;
    }
    @JsonSetter("TUESDAY")
    public void setTuesday(Tuesday tuesday) {
        this.tuesday = tuesday;
    }

    public Saturday getSaturday() {
        return saturday;
    }
    @JsonSetter("SATURDAY")
    public void setSaturday(Saturday saturday) {
        this.saturday = saturday;
    }
}
