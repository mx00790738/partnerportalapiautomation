package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationProductListEntry {
    private String id;
    private String description;
    private String hazmat;
    private String hazmatUNNbr;
    private String hazmatPackageGroup;
    private String hazmatClassCode;
    private String hazmatShipname;
    private String hazmatContactName;
    private String hazmatContactNumber;
    private String height;
    private String width;
    private String nmfcClassCode;
    private String length;
    private String nmfcCode;
    private String reqSpots;
    private String spotsIsPerPiece;
    private String weight;
    private String weightIsPerPiece;
    private String weightUomTypeCode;
    private String handlingUnits;
    private String handlingUnitsIsPerPiece;
    private String cubicUnits;
    private String cubicUnitsIsPerPiece;
    private String packagingTypeCode;

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    @JsonSetter("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getHazmat() {
        return hazmat;
    }
    @JsonSetter("hazmat")
    public void setHazmat(String hazmat) {
        this.hazmat = hazmat;
    }

    public String getHazmatUNNbr() {
        return hazmatUNNbr;
    }
    @JsonSetter("hazmatUNNbr")
    public void setHazmatUNNbr(String hazmatUNNbr) {
        this.hazmatUNNbr = hazmatUNNbr;
    }

    public String getHazmatPackageGroup() {
        return hazmatPackageGroup;
    }
    @JsonSetter("hazmatPackageGroup")
    public void setHazmatPackageGroup(String hazmatPackageGroup) {
        this.hazmatPackageGroup = hazmatPackageGroup;
    }

    public String getHazmatClassCode() {
        return hazmatClassCode;
    }
    @JsonSetter("hazmatClassCode")
    public void setHazmatClassCode(String hazmatClassCode) {
        this.hazmatClassCode = hazmatClassCode;
    }

    public String getHazmatShipname() {
        return hazmatShipname;
    }
    @JsonSetter("hazmatShipname")
    public void setHazmatShipname(String hazmatShipname) {
        this.hazmatShipname = hazmatShipname;
    }

    public String getHazmatContactName() {
        return hazmatContactName;
    }
    @JsonSetter("hazmatContactName")
    public void setHazmatContactName(String hazmatContactName) {
        this.hazmatContactName = hazmatContactName;
    }

    public String getHazmatContactNumber() {
        return hazmatContactNumber;
    }
    @JsonSetter("hazmatContactNumber")
    public void setHazmatContactNumber(String hazmatContactNumber) {
        this.hazmatContactNumber = hazmatContactNumber;
    }

    public String getHeight() {
        return height;
    }
    @JsonSetter("height")
    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }
    @JsonSetter("")
    public void setWidth(String width) {
        this.width = width;
    }

    public String getNmfcClassCode() {
        return nmfcClassCode;
    }
    @JsonSetter("nmfcClassCode")
    public void setNmfcClassCode(String nmfcClassCode) {
        this.nmfcClassCode = nmfcClassCode;
    }

    public String getLength() {
        return length;
    }
    @JsonSetter("length")
    public void setLength(String length) {
        this.length = length;
    }

    public String getNmfcCode() {
        return nmfcCode;
    }
    @JsonSetter("nmfcCode")
    public void setNmfcCode(String nmfcCode) {
        this.nmfcCode = nmfcCode;
    }

    public String getReqSpots() {
        return reqSpots;
    }
    @JsonSetter("reqSpots")
    public void setReqSpots(String reqSpots) {
        this.reqSpots = reqSpots;
    }

    public String getSpotsIsPerPiece() {
        return spotsIsPerPiece;
    }
    @JsonSetter("spotsIsPerPiece")
    public void setSpotsIsPerPiece(String spotsIsPerPiece) {
        this.spotsIsPerPiece = spotsIsPerPiece;
    }

    public String getWeight() {
        return weight;
    }
    @JsonSetter("weight")
    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeightIsPerPiece() {
        return weightIsPerPiece;
    }
    @JsonSetter("weightIsPerPiece")
    public void setWeightIsPerPiece(String weightIsPerPiece) {
        this.weightIsPerPiece = weightIsPerPiece;
    }

    public String getWeightUomTypeCode() {
        return weightUomTypeCode;
    }
    @JsonSetter("weightUomTypeCode")
    public void setWeightUomTypeCode(String weightUomTypeCode) {
        this.weightUomTypeCode = weightUomTypeCode;
    }

    public String getHandlingUnits() {
        return handlingUnits;
    }
    @JsonSetter("handlingUnits")
    public void setHandlingUnits(String handlingUnits) {
        this.handlingUnits = handlingUnits;
    }

    public String getHandlingUnitsIsPerPiece() {
        return handlingUnitsIsPerPiece;
    }
    @JsonSetter("handlingUnitsIsPerPiece")
    public void setHandlingUnitsIsPerPiece(String handlingUnitsIsPerPiece) {
        this.handlingUnitsIsPerPiece = handlingUnitsIsPerPiece;
    }

    public String getCubicUnits() {
        return cubicUnits;
    }
    @JsonSetter("cubicUnits")
    public void setCubicUnits(String cubicUnits) {
        this.cubicUnits = cubicUnits;
    }

    public String getCubicUnitsIsPerPiece() {
        return cubicUnitsIsPerPiece;
    }
    @JsonSetter("cubicUnitsIsPerPiece")
    public void setCubicUnitsIsPerPiece(String cubicUnitsIsPerPiece) {
        this.cubicUnitsIsPerPiece = cubicUnitsIsPerPiece;
    }

    public String getPackagingTypeCode() {
        return packagingTypeCode;
    }
    @JsonSetter("packagingTypeCode")
    public void setPackagingTypeCode(String packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }
}
