package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class CityStateZipCountyCountry {
    private String cityId;
    private String city;
    private String postalCode;
    private String county;
    private String state;
    private String stateName;
    private String country;
    private Double latitude;
    private Double longitude;
    private Integer returnTypeCode;
    private String timeZone;

    public String getCityId() {
        return cityId;
    }
    @JsonSetter("cityId")
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }
    @JsonSetter("city")
    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }
    @JsonSetter("postalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCounty() {
        return county;
    }
    @JsonSetter("county")
    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }
    @JsonSetter("state")
    public void setState(String state) {
        this.state = state;
    }

    public String getStateName() {
        return stateName;
    }
    @JsonSetter("stateName")
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountry() {
        return country;
    }
    @JsonSetter("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }
    @JsonSetter("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
    @JsonSetter("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getReturnTypeCode() {
        return returnTypeCode;
    }
    @JsonSetter("returnTypeCode")
    public void setReturnTypeCode(Integer returnTypeCode) {
        this.returnTypeCode = returnTypeCode;
    }

    public String getTimeZone() {
        return timeZone;
    }
    @JsonSetter("timeZone")
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
