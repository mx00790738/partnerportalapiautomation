package pojoClasses.responsePojo.MasterData.Location;

import com.fasterxml.jackson.annotation.JsonSetter;

public class LocationInfo {
    @JsonSetter("userId")
    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    String userId;

    @JsonSetter("mcleodUserId")
    public String getMcleodUserId() {
        return this.mcleodUserId;
    }

    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId;
    }

    String mcleodUserId;

    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    String companyId;

    @JsonSetter("agentPortalUserId")
    public String getAgentPortalUserId() {
        return this.agentPortalUserId;
    }

    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId;
    }

    String agentPortalUserId;

    @JsonSetter("agencyPayeeId")
    public String getAgencyPayeeId() {
        return this.agencyPayeeId;
    }

    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId;
    }

    String agencyPayeeId;

    @JsonSetter("agencyId")
    public String getAgencyId() {
        return this.agencyId;
    }

    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    String agencyId;

    @JsonSetter("id")
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    @JsonSetter("addressLine1")
    public String getAddressLine1() {
        return this.addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    String addressLine1;

    @JsonSetter("addressLine2")
    public String getAddressLine2() {
        return this.addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    String addressLine2;

    @JsonSetter("city")
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    String city;

    @JsonSetter("cityId")
    public Integer getCityId() {
        return this.cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    Integer cityId;

    @JsonSetter("locationCode")
    public String getLocationCode() {
        return this.locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    String locationCode;

    @JsonSetter("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    @JsonSetter("state")
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    String state;

    @JsonSetter("postalCode")
    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    String postalCode;

    @JsonSetter("locationType")
    public String getLocationType() {
        return this.locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    String locationType;

    @JsonSetter("zoneId")
    public String getZoneId() {
        return this.zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    String zoneId;

    @JsonSetter("apptRequired")
    public Boolean getApptRequired() {
        return this.apptRequired;
    }

    public void setApptRequired(Boolean apptRequired) {
        this.apptRequired = apptRequired;
    }

    Boolean apptRequired;

    @JsonSetter("locationName")
    public String getLocationName() {
        return this.locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    String locationName;

    @JsonSetter("active")
    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    Boolean active;
}

