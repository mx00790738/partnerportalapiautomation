package pojoClasses.responsePojo.MasterData.Location;

public class Sunday {
	private String closeTime;
    private String startTime;
    public String getCloseTime() {
        return closeTime;
    }
    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }
    public String getStartTime() {
        return startTime;
    }
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
