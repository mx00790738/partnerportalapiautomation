package pojoClasses.responsePojo.MasterData.Location;

public class LocationSearchListEntry {
    private Object userId;
    private Object mcleodUserId;
    private String companyId;
    private Object agentPortalUserId;
    private Object agencyPayeeId;
    private Object agencyId;
    private String id;
    private String addressLine1;
    private Object addressLine2;
    private String city;
    private Integer cityId;
    private String locationCode;
    private String name;
    private String state;
    private String postalCode;
    private String locationType;
    private Object zoneId;
    private Boolean apptRequired;
    private Object locationName;
    private Boolean active;
    public Object getUserId() {
        return userId;
    }
    public void setUserId(Object userId) {
        this.userId = userId;
    }
    public Object getMcleodUserId() {
        return mcleodUserId;
    }
    public void setMcleodUserId(Object mcleodUserId) {
        this.mcleodUserId = mcleodUserId;
    }
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public Object getAgentPortalUserId() {
        return agentPortalUserId;
    }
    public void setAgentPortalUserId(Object agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId;
    }
    public Object getAgencyPayeeId() {
        return agencyPayeeId;
    }
    public void setAgencyPayeeId(Object agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId;
    }
    public Object getAgencyId() {
        return agencyId;
    }
    public void setAgencyId(Object agencyId) {
        this.agencyId = agencyId;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getAddressLine1() {
        return addressLine1;
    }
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }
    public Object getAddressLine2() {
        return addressLine2;
    }
    public void setAddressLine2(Object addressLine2) {
        this.addressLine2 = addressLine2;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public Integer getCityId() {
        return cityId;
    }
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public String getLocationCode() {
        return locationCode;
    }
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getLocationType() {
        return locationType;
    }
    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
    public Object getZoneId() {
        return zoneId;
    }
    public void setZoneId(Object zoneId) {
        this.zoneId = zoneId;
    }
    public Boolean getApptRequired() {
        return apptRequired;
    }
    public void setApptRequired(Boolean apptRequired) {
        this.apptRequired = apptRequired;
    }
    public Object getLocationName() {
        return locationName;
    }
    public void setLocationName(Object locationName) {
        this.locationName = locationName;
    }
    public Boolean getActive() {
        return active;
    }
    public void setActive(Boolean active) {
        this.active = active;
    }
}
