/**
 * 
 */
package pojoClasses.responsePojo.MasterData.Customer;

/**
 * @author MX0C92049
 *
 */
public class CustomerContact {
	private String comapnyId;
	private String parentrowId;
	private String parentrowType;
	private String id;
	private String name;
	private Object contactName;
	private Object comments;
	private String email;
	private Object fax;
	private String active;
	private Object mobilePhone;
	private String termsContact;
	private String webAccess;
	private Object title;
	private Object phone;
	private String payableContact;
	private Object rapidAlertNotify;
	private String detentionContact;

	public String getComapnyId() {
		return comapnyId;
	}

	public void setComapnyId(String comapnyId) {
		this.comapnyId = comapnyId;
	}

	public String getParentrowId() {
		return parentrowId;
	}

	public void setParentrowId(String parentrowId) {
		this.parentrowId = parentrowId;
	}

	public String getParentrowType() {
		return parentrowType;
	}

	public void setParentrowType(String parentrowType) {
		this.parentrowType = parentrowType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getContactName() {
		return contactName;
	}

	public void setContactName(Object contactName) {
		this.contactName = contactName;
	}

	public Object getComments() {
		return comments;
	}

	public void setComments(Object comments) {
		this.comments = comments;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Object getFax() {
		return fax;
	}

	public void setFax(Object fax) {
		this.fax = fax;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Object getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(Object mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getTermsContact() {
		return termsContact;
	}

	public void setTermsContact(String termsContact) {
		this.termsContact = termsContact;
	}

	public String getWebAccess() {
		return webAccess;
	}

	public void setWebAccess(String webAccess) {
		this.webAccess = webAccess;
	}

	public Object getTitle() {
		return title;
	}

	public void setTitle(Object title) {
		this.title = title;
	}

	public Object getPhone() {
		return phone;
	}

	public void setPhone(Object phone) {
		this.phone = phone;
	}

	public String getPayableContact() {
		return payableContact;
	}

	public void setPayableContact(String payableContact) {
		this.payableContact = payableContact;
	}

	public Object getRapidAlertNotify() {
		return rapidAlertNotify;
	}

	public void setRapidAlertNotify(Object rapidAlertNotify) {
		this.rapidAlertNotify = rapidAlertNotify;
	}

	public String getDetentionContact() {
		return detentionContact;
	}

	public void setDetentionContact(String detentionContact) {
		this.detentionContact = detentionContact;
	}
}
