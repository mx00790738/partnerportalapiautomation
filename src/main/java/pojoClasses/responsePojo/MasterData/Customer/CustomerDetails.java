/**
 * 
 */
package pojoClasses.responsePojo.MasterData.Customer;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author Manoj
 * POJO Calls customer details returned in Master Files
 */
public class CustomerDetails {
	private String id;
    private String isActive;
    private String name;
    private Object locationId;
    private String address1;
    private String address2;
    private String city;
    private String stateId;
    private String zipCode;
    private Object balance;
    private Object highBalance;
    private Object lastShipDate;
    private Object lastBillDate;
    private Object lastPayDate;
    private Object billedLoads;
    private Object paidLoads;
    private Object averageBill;
    private Object averagePayDays;
    private Object motorCarrierId;
    private Object dotNumber;
    private String category;
    private String prevCode;
    private String bridgeId;
    private String conversionDate;
    private String certOfInsDate;
    private String enteredDate;
    private String pastDue;
    private double creditLimit;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getIsActive() {
        return isActive;
    }
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Object getLocationId() {
        return locationId;
    }
    
    @JsonSetter("location_id")
    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }
    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    public String getAddress2() {
        return address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStateId() {
        return stateId;
    }
    
    @JsonSetter("state_id")
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
    public String getZipCode() {
        return zipCode;
    }
    
    @JsonSetter("zip_code")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public Object getBalance() {
        return balance;
    }
    
    @JsonSetter("balance")
    public void setBalance(Object balance) {
        this.balance = balance;
    }
    public Object getHighBalance() {
        return highBalance;
    }
    
    @JsonSetter("high_balance")
    public void setHighBalance(Object highBalance) {
        this.highBalance = highBalance;
    }
    public Object getLastShipDate() {
        return lastShipDate;
    }
    
    @JsonSetter("last_ship_date")
    public void setLastShipDate(Object lastShipDate) {
        this.lastShipDate = lastShipDate;
    }
    public Object getLastBillDate() {
        return lastBillDate;
    }
    
    @JsonSetter("last_bill_date")
    public void setLastBillDate(Object lastBillDate) {
        this.lastBillDate = lastBillDate;
    }
    public Object getLastPayDate() {
        return lastPayDate;
    }
    
    @JsonSetter("last_pay_date")
    public void setLastPayDate(Object lastPayDate) {
        this.lastPayDate = lastPayDate;
    }
    public Object getBilledLoads() {
        return billedLoads;
    }
    
    @JsonSetter("billed_loads")
    public void setBilledLoads(Object billedLoads) {
        this.billedLoads = billedLoads;
    }
    public Object getPaidLoads() {
        return paidLoads;
    }
    
    @JsonSetter("paid_loads")
    public void setPaidLoads(Object paidLoads) {
        this.paidLoads = paidLoads;
    }
    public Object getAverageBill() {
        return averageBill;
    }
    
    @JsonSetter("average_bill")
    public void setAverageBill(Object averageBill) {
        this.averageBill = averageBill;
    }
    public Object getAveragePayDays() {
        return averagePayDays;
    }
    
    @JsonSetter("average_pay_days")
    public void setAveragePayDays(Object averagePayDays) {
        this.averagePayDays = averagePayDays;
    }
    public Object getMotorCarrierId() {
        return motorCarrierId;
    }
    
    @JsonSetter("motor_carrier_id")
    public void setMotorCarrierId(Object motorCarrierId) {
        this.motorCarrierId = motorCarrierId;
    }
    public Object getDotNumber() {
        return dotNumber;
    }
    
    @JsonSetter("dot_number")
    public void setDotNumber(Object dotNumber) {
        this.dotNumber = dotNumber;
    }
    public String getCategory() {
        return category;
    }
    
    @JsonSetter("category")
    public void setCategory(String category) {
        this.category = category;
    }
    public String getPrevCode() {
        return prevCode;
    }
    
    @JsonSetter("prev_code")
    public void setPrevCode(String prevCode) {
        this.prevCode = prevCode;
    }
    public String getBridgeId() {
        return bridgeId;
    }
    
    @JsonSetter("bridge_id")
    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }
    public String getConversionDate() {
        return conversionDate;
    }
    
    @JsonSetter("conversion_date")
    public void setConversionDate(String conversionDate) {
        this.conversionDate = conversionDate;
    }
    public String getCertOfInsDate() {
        return certOfInsDate;
    }
    
    @JsonSetter("cert_of_ins_date")
    public void setCertOfInsDate(String certOfInsDate) {
        this.certOfInsDate = certOfInsDate;
    }
    public String getEnteredDate() {
        return enteredDate;
    }
    
    @JsonSetter("entered_date")
    public void setEnteredDate(String enteredDate) {
        this.enteredDate = enteredDate;
    }
    public String getPastDue() {
        return pastDue;
    }
    
    @JsonSetter("__pastDue")
    public void setPastDue(String pastDue) {
        this.pastDue = pastDue;
    }

    @JsonSetter("creditLimit")
    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }
    public double getCreditLimit() {
        return creditLimit;
    }
}
