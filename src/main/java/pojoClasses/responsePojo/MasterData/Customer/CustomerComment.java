/**
 * 
 */
package pojoClasses.responsePojo.MasterData.Customer;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author MX0C92049
 *
 */
public class CustomerComment {
	private String companyId;
	private String id;
	private String comments;
	private String parentRowId;
	private String parentRowType;
	private String displayAtdispatch;
	private Object attachFilename;
	private String commentTypeId;
	private String copyToOrder;
	private String enteredDate;
	private String enteredUserId;
	private String onboardLock;
	private String orderCommentType;

	public String getCompanyId() {
		return companyId;
	}

	@JsonSetter("companyId")
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getComments() {
		return comments;
	}

	@JsonSetter("comments")
	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getParentRowId() {
		return parentRowId;
	}

	@JsonSetter("parent_row_id")
	public void setParentRowId(String parentRowId) {
		this.parentRowId = parentRowId;
	}

	public String getParentRowType() {
		return parentRowType;
	}

	@JsonSetter("parent_row_type")
	public void setParentRowType(String parentRowType) {
		this.parentRowType = parentRowType;
	}

	public String getDisplayAtdispatch() {
		return displayAtdispatch;
	}

	@JsonSetter("display_atdispatch")
	public void setDisplayAtdispatch(String displayAtdispatch) {
		this.displayAtdispatch = displayAtdispatch;
	}

	public Object getAttachFilename() {
		return attachFilename;
	}

	@JsonSetter("attach_filename")
	public void setAttachFilename(Object attachFilename) {
		this.attachFilename = attachFilename;
	}

	public String getCommentTypeId() {
		return commentTypeId;
	}

	@JsonSetter("comment_type_id")
	public void setCommentTypeId(String commentTypeId) {
		this.commentTypeId = commentTypeId;
	}

	public String getCopyToOrder() {
		return copyToOrder;
	}

	@JsonSetter("copy_to_order")
	public void setCopyToOrder(String copyToOrder) {
		this.copyToOrder = copyToOrder;
	}

	public String getEnteredDate() {
		return enteredDate;
	}

	@JsonSetter("entered_date")
	public void setEnteredDate(String enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getEnteredUserId() {
		return enteredUserId;
	}

	@JsonSetter("entered_user_id")
	public void setEnteredUserId(String enteredUserId) {
		this.enteredUserId = enteredUserId;
	}

	public String getOnboardLock() {
		return onboardLock;
	}

	@JsonSetter("onboard_lock")
	public void setOnboardLock(String onboardLock) {
		this.onboardLock = onboardLock;
	}

	public String getOrderCommentType() {
		return orderCommentType;
	}

	@JsonSetter("order_comment_type")
	public void setOrderCommentType(String orderCommentType) {
		this.orderCommentType = orderCommentType;
	}
}
