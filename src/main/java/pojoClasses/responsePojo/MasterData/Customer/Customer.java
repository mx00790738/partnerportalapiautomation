package pojoClasses.responsePojo.MasterData.Customer;

//TODO - Change name as CustomerSearchListEntry
public class Customer {
	private String companyId;
    private String locationAddress;
    private Object locationName;
    private String name;
    private String zip_code;
    private String city;
    private String state;
    private String id;
    private String isActive;
    private Double creditLimit;
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getLocationAddress() {
        return locationAddress;
    }
    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }
    public Object getLocationName() {
        return locationName;
    }
    public void setLocationName(Object locationName) {
        this.locationName = locationName;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getZipCode() {
        return zip_code;
    }
    public void setzip_code(String zipCode) {
        this.zip_code = zipCode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getIsActive() {
        return isActive;
    }
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    public Double getCreditLimit() {
        return creditLimit;
    }
    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }
}
