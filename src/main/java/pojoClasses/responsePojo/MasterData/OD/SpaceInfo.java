package pojoClasses.responsePojo.MasterData.OD;

import com.fasterxml.jackson.annotation.JsonSetter;

public class SpaceInfo {
    @JsonSetter("id")
    public int getId() {
        return this.id; }
    public void setId(int id) {
        this.id = id; }
    int id;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("tractorId")
    public String getTractorId() {
        return this.tractorId; }
    public void setTractorId(String tractorId) {
        this.tractorId = tractorId; }
    String tractorId;
    @JsonSetter("trailerId")
    public String getTrailerId() {
        return this.trailerId; }
    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId; }
    String trailerId;
    @JsonSetter("tractorAxleSpacing1")
    public Double getTractorAxleSpacing1() {
        return this.tractorAxleSpacing1; }
    public void setTractorAxleSpacing1(Double tractorAxleSpacing1) {
        this.tractorAxleSpacing1 = tractorAxleSpacing1; }
    Double tractorAxleSpacing1;
    @JsonSetter("tractorAxleSpacing2")
    public Double getTractorAxleSpacing2() {
        return this.tractorAxleSpacing2; }
    public void setTractorAxleSpacing2(Double tractorAxleSpacing2) {
        this.tractorAxleSpacing2 = tractorAxleSpacing2; }
    Double tractorAxleSpacing2;
    @JsonSetter("tractorAxleSpacing3")
    public Double getTractorAxleSpacing3() {
        return this.tractorAxleSpacing3; }
    public void setTractorAxleSpacing3(Double tractorAxleSpacing3) {
        this.tractorAxleSpacing3 = tractorAxleSpacing3; }
    Double tractorAxleSpacing3;
    @JsonSetter("tractorAxleSpacing4")
    public Double getTractorAxleSpacing4() {
        return this.tractorAxleSpacing4; }
    public void setTractorAxleSpacing4(Double tractorAxleSpacing4) {
        this.tractorAxleSpacing4 = tractorAxleSpacing4; }
    Double tractorAxleSpacing4;
    @JsonSetter("trailerAxleSpacing1")
    public Double getTrailerAxleSpacing1() {
        return this.trailerAxleSpacing1; }
    public void setTrailerAxleSpacing1(Double trailerAxleSpacing1) {
        this.trailerAxleSpacing1 = trailerAxleSpacing1; }
    Double trailerAxleSpacing1;
    @JsonSetter("trailerAxleSpacing2")
    public Double getTrailerAxleSpacing2() {
        return this.trailerAxleSpacing2; }
    public void setTrailerAxleSpacing2(Double trailerAxleSpacing2) {
        this.trailerAxleSpacing2 = trailerAxleSpacing2; }
    Double trailerAxleSpacing2;
    @JsonSetter("trailerAxleSpacing3")
    public Double getTrailerAxleSpacing3() {
        return this.trailerAxleSpacing3; }
    public void setTrailerAxleSpacing3(Double trailerAxleSpacing3) {
        this.trailerAxleSpacing3 = trailerAxleSpacing3; }
    Double trailerAxleSpacing3;
    @JsonSetter("trailerAxleSpacing4")
    public Double getTrailerAxleSpacing4() {
        return this.trailerAxleSpacing4; }
    public void setTrailerAxleSpacing4(Double trailerAxleSpacing4) {
        this.trailerAxleSpacing4 = trailerAxleSpacing4; }
    Double trailerAxleSpacing4;
    @JsonSetter("trailerAxleSpacing5")
    public Double getTrailerAxleSpacing5() {
        return this.trailerAxleSpacing5; }
    public void setTrailerAxleSpacing5(Double trailerAxleSpacing5) {
        this.trailerAxleSpacing5 = trailerAxleSpacing5; }
    Double trailerAxleSpacing5;
    @JsonSetter("lastModified")
    public String getLastModified() {
        return this.lastModified; }
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified; }
    String lastModified;
    @JsonSetter("lastOrderId")
    public String getLastOrderId() {
        return this.lastOrderId; }
    public void setLastOrderId(String lastOrderId) {
        this.lastOrderId = lastOrderId; }
    String lastOrderId;
}
