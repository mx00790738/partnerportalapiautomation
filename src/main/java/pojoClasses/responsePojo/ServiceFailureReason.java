package pojoClasses.responsePojo;

public class ServiceFailureReason {
	private Object id;
	private String description;
	private String standardCode;
	private Object partnerId;

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStandardCode() {
		return standardCode;
	}

	public void setStandardCode(String standardCode) {
		this.standardCode = standardCode;
	}

	public Object getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Object partnerId) {
		this.partnerId = partnerId;
	}
}
