/**
 * 
 */
package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author MX0C92049
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessResponse<T> {
	private String status;
    private T response;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public T getResponse() {
        return response;
    }
    public void setResponse(T response) {
        this.response = response;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
