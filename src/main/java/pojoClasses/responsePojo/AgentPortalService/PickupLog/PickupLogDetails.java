package pojoClasses.responsePojo.AgentPortalService.PickupLog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import pojoClasses.commonPojo.*;

import java.util.ArrayList;

//todo GetOrder-OrderDetails and PickupLogDetails pojo are same need to move this to common pojo with a meaningful name
@JsonIgnoreProperties(ignoreUnknown = true)
public class PickupLogDetails {
    @JsonSetter("userId")
    public String getUserId() {
        return this.userId; }
    public void setUserId(String userId) {
        this.userId = userId; }
    String userId;
    @JsonSetter("mcleodUserId")
    public String getMcleodUserId() {
        return this.mcleodUserId; }
    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId; }
    String mcleodUserId;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("agentPortalUserId")
    public String getAgentPortalUserId() {
        return this.agentPortalUserId; }
    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId; }
    String agentPortalUserId;
    @JsonSetter("agencyPayeeId")
    public String getAgencyPayeeId() {
        return this.agencyPayeeId; }
    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId; }
    String agencyPayeeId;
    @JsonSetter("agencyId")
    public String getAgencyId() {
        return this.agencyId; }
    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId; }
    String agencyId;
    @JsonSetter("systemId")
    public Object getSystemId() {
        return this.systemId; }
    public void setSystemId(Object systemId) {
        this.systemId = systemId; }
    Object systemId;
    @JsonSetter("saveCompleteCarrierRating")
    public boolean getSaveCompleteCarrierRating() {
        return this.saveCompleteCarrierRating; }
    public void setSaveCompleteCarrierRating(boolean saveCompleteCarrierRating) {
        this.saveCompleteCarrierRating = saveCompleteCarrierRating; }
    boolean saveCompleteCarrierRating;
    @JsonSetter("saveCompleteCustomerRating")
    public boolean getSaveCompleteCustomerRating() {
        return this.saveCompleteCustomerRating; }
    public void setSaveCompleteCustomerRating(boolean saveCompleteCustomerRating) {
        this.saveCompleteCustomerRating = saveCompleteCustomerRating; }
    boolean saveCompleteCustomerRating;
    @JsonSetter("saveCloseSetAppoIntegerment")
    public boolean getSaveCloseSetAppoIntegerment() {
        return this.saveCloseSetAppoIntegerment; }
    public void setSaveCloseSetAppoIntegerment(boolean saveCloseSetAppoIntegerment) {
        this.saveCloseSetAppoIntegerment = saveCloseSetAppoIntegerment; }
    boolean saveCloseSetAppoIntegerment;
    @JsonSetter("saveCloseCarrierConfirmation")
    public boolean getSaveCloseCarrierConfirmation() {
        return this.saveCloseCarrierConfirmation; }
    public void setSaveCloseCarrierConfirmation(boolean saveCloseCarrierConfirmation) {
        this.saveCloseCarrierConfirmation = saveCloseCarrierConfirmation; }
    boolean saveCloseCarrierConfirmation;
    @JsonSetter("saveClosePickupLog")
    public boolean getSaveClosePickupLog() {
        return this.saveClosePickupLog; }
    public void setSaveClosePickupLog(boolean saveClosePickupLog) {
        this.saveClosePickupLog = saveClosePickupLog; }
    boolean saveClosePickupLog;
    @JsonSetter("saveCloseDeliveryLog")
    public boolean getSaveCloseDeliveryLog() {
        return this.saveCloseDeliveryLog; }
    public void setSaveCloseDeliveryLog(boolean saveCloseDeliveryLog) {
        this.saveCloseDeliveryLog = saveCloseDeliveryLog; }
    boolean saveCloseDeliveryLog;
    @JsonSetter("customerReRating")
    public boolean getCustomerReRating() {
        return this.customerReRating; }
    public void setCustomerReRating(boolean customerReRating) {
        this.customerReRating = customerReRating; }
    boolean customerReRating;
    @JsonSetter("deletedCarrierRatingCharges")
    public ArrayList<Object> getDeletedCarrierRatingCharges() {
        return this.deletedCarrierRatingCharges; }
    public void setDeletedCarrierRatingCharges(ArrayList<Object> deletedCarrierRatingCharges) {
        this.deletedCarrierRatingCharges = deletedCarrierRatingCharges; }
    ArrayList<Object> deletedCarrierRatingCharges;
    @JsonSetter("deletedCustomerRatingCharges")
    public ArrayList<Object> getDeletedCustomerRatingCharges() {
        return this.deletedCustomerRatingCharges; }
    public void setDeletedCustomerRatingCharges(ArrayList<Object> deletedCustomerRatingCharges) {
        this.deletedCustomerRatingCharges = deletedCustomerRatingCharges; }
    ArrayList<Object> deletedCustomerRatingCharges;
    @JsonSetter("deletedStops")
    public ArrayList<Object> getDeletedStops() {
        return this.deletedStops; }
    public void setDeletedStops(ArrayList<Object> deletedStops) {
        this.deletedStops = deletedStops; }
    ArrayList<Object> deletedStops;
    @JsonSetter("deletedReferences")
    public ArrayList<Object> getDeletedReferences() {
        return this.deletedReferences; }
    public void setDeletedReferences(ArrayList<Object> deletedReferences) {
        this.deletedReferences = deletedReferences; }
    ArrayList<Object> deletedReferences;
    @JsonSetter("deletedConsignments")
    public ArrayList<Object> getDeletedConsignments() {
        return this.deletedConsignments; }
    public void setDeletedConsignments(ArrayList<Object> deletedConsignments) {
        this.deletedConsignments = deletedConsignments; }
    ArrayList<Object> deletedConsignments;
    @JsonSetter("deletedStopNotes")
    public ArrayList<Object> getDeletedStopNotes() {
        return this.deletedStopNotes; }
    public void setDeletedStopNotes(ArrayList<Object> deletedStopNotes) {
        this.deletedStopNotes = deletedStopNotes; }
    ArrayList<Object> deletedStopNotes;
    @JsonSetter("deletedServices")
    public ArrayList<Object> getDeletedServices() {
        return this.deletedServices; }
    public void setDeletedServices(ArrayList<Object> deletedServices) {
        this.deletedServices = deletedServices; }
    ArrayList<Object> deletedServices;
    @JsonSetter("billNumber")
    public String getBillNumber() {
        return this.billNumber; }
    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber; }
    String billNumber;
    @JsonSetter("consigneeRefNumber")
    public String getConsigneeRefNumber() {
        return this.consigneeRefNumber; }
    public void setConsigneeRefNumber(String consigneeRefNumber) {
        this.consigneeRefNumber = consigneeRefNumber; }
    String consigneeRefNumber;
    @JsonSetter("equipmentTypeId")
    public String getEquipmentTypeId() {
        return this.equipmentTypeId; }
    public void setEquipmentTypeId(String equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId; }
    String equipmentTypeId;
    @JsonSetter("equipmentTypeDescr")
    public String getEquipmentTypeDescr() {
        return this.equipmentTypeDescr; }
    public void setEquipmentTypeDescr(String equipmentTypeDescr) {
        this.equipmentTypeDescr = equipmentTypeDescr; }
    String equipmentTypeDescr;
    @JsonSetter("customerId")
    public String getCustomerId() {
        return this.customerId; }
    public void setCustomerId(String customerId) {
        this.customerId = customerId; }
    String customerId;
    @JsonSetter("distance")
    public String getDistance() {
        return this.distance; }
    public void setDistance(String distance) {
        this.distance = distance; }
    String distance;
    @JsonSetter("commodity")
    public ArrayList<Object> getCommodity() {
        return this.commodity; }
    public void setCommodity(ArrayList<Object> commodity) {
        this.commodity = commodity; }
    ArrayList<Object> commodity;
    @JsonSetter("commodities")
    public Commodities getCommodities() {
        return this.commodities; }
    public void setCommodities(Commodities commodities) {
        this.commodities = commodities; }
    Commodities commodities;
    @JsonSetter("stops")
    public ArrayList<Stops> getStops() {
        return this.stops; }
    public void setStops(ArrayList<Stops> stops) {
        this.stops = stops; }
    ArrayList<Stops> stops;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("totalWeight")
    public Integer getTotalWeight() {
        return this.totalWeight; }
    public void setTotalWeight(Integer totalWeight) {
        this.totalWeight = totalWeight; }
    Integer totalWeight;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("movementIndex")
    public Integer getMovementIndex() {
        return this.movementIndex; }
    public void setMovementIndex(Integer movementIndex) {
        this.movementIndex = movementIndex; }
    Integer movementIndex;
    @JsonSetter("statusDescr")
    public String getStatusDescr() {
        return this.statusDescr; }
    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr; }
    String statusDescr;
    @JsonSetter("action")
    public Object getAction() {
        return this.action; }
    public void setAction(Object action) {
        this.action = action; }
    Object action;
    @JsonSetter("otherchargetotal")
    public Object getOtherchargetotal() {
        return this.otherchargetotal; }
    public void setOtherchargetotal(Object otherchargetotal) {
        this.otherchargetotal = otherchargetotal; }
    Object otherchargetotal;
    @JsonSetter("rate")
    public Object getRate() {
        return this.rate; }
    public void setRate(Object rate) {
        this.rate = rate; }
    Object rate;
    @JsonSetter("rateType")
    public Object getRateType() {
        return this.rateType; }
    public void setRateType(Object rateType) {
        this.rateType = rateType; }
    Object rateType;
    @JsonSetter("lineHaulCharges")
    public Integer getLineHaulCharges() {
        return this.lineHaulCharges; }
    public void setLineHaulCharges(Integer lineHaulCharges) {
        this.lineHaulCharges = lineHaulCharges; }
    Integer lineHaulCharges;
    @JsonSetter("rateUnit")
    public Object getRateUnit() {
        return this.rateUnit; }
    public void setRateUnit(Object rateUnit) {
        this.rateUnit = rateUnit; }
    Object rateUnit;
    @JsonSetter("rateCurrencyType")
    public Object getRateCurrencyType() {
        return this.rateCurrencyType; }
    public void setRateCurrencyType(Object rateCurrencyType) {
        this.rateCurrencyType = rateCurrencyType; }
    Object rateCurrencyType;
    @JsonSetter("totalCharge")
    public Integer getTotalCharge() {
        return this.totalCharge; }
    public void setTotalCharge(Integer totalCharge) {
        this.totalCharge = totalCharge; }
    Integer totalCharge;
    @JsonSetter("otherCharges")
    public ArrayList<Object> getOtherCharges() {
        return this.otherCharges; }
    public void setOtherCharges(ArrayList<Object> otherCharges) {
        this.otherCharges = otherCharges; }
    ArrayList<Object> otherCharges;
    @JsonSetter("rateTypeDescription")
    public Object getRateTypeDescription() {
        return this.rateTypeDescription; }
    public void setRateTypeDescription(Object rateTypeDescription) {
        this.rateTypeDescription = rateTypeDescription; }
    Object rateTypeDescription;
    @JsonSetter("unlockReason")
    public Object getUnlockReason() {
        return this.unlockReason; }
    public void setUnlockReason(Object unlockReason) {
        this.unlockReason = unlockReason; }
    Object unlockReason;
    @JsonSetter("orderValue")
    public Integer getOrderValue() {
        return this.orderValue; }
    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue; }
    Integer orderValue;
    @JsonSetter("planningComments")
    public String getPlanningComments() {
        return this.planningComments; }
    public void setPlanningComments(String planningComments) {
        this.planningComments = planningComments; }
    String planningComments;
    @JsonSetter("readytoBill")
    public String getReadytoBill() {
        return this.readytoBill; }
    public void setReadytoBill(String readytoBill) {
        this.readytoBill = readytoBill; }
    String readytoBill;
    @JsonSetter("opsrevComplete")
    public String getOpsrevComplete() {
        return this.opsrevComplete; }
    public void setOpsrevComplete(String opsrevComplete) {
        this.opsrevComplete = opsrevComplete; }
    String opsrevComplete;
    @JsonSetter("operationsUser")
    public String getOperationsUser() {
        return this.operationsUser; }
    public void setOperationsUser(String operationsUser) {
        this.operationsUser = operationsUser; }
    String operationsUser;
    @JsonSetter("orderType")
    public String getOrderType() {
        return this.orderType; }
    public void setOrderType(String orderType) {
        this.orderType = orderType; }
    String orderType;
    @JsonSetter("enteredUserId")
    public String getEnteredUserId() {
        return this.enteredUserId; }
    public void setEnteredUserId(String enteredUserId) {
        this.enteredUserId = enteredUserId; }
    String enteredUserId;
    @JsonSetter("operationsUserId")
    public String getOperationsUserId() {
        return this.operationsUserId; }
    public void setOperationsUserId(String operationsUserId) {
        this.operationsUserId = operationsUserId; }
    String operationsUserId;
    @JsonSetter("enteredStrUserDate")
    public String getEnteredStrUserDate() {
        return this.enteredStrUserDate; }
    public void setEnteredStrUserDate(String enteredStrUserDate) {
        this.enteredStrUserDate = enteredStrUserDate; }
    String enteredStrUserDate;
    @JsonSetter("operationsStrUserDate")
    public String getOperationsStrUserDate() {
        return this.operationsStrUserDate; }
    public void setOperationsStrUserDate(String operationsStrUserDate) {
        this.operationsStrUserDate = operationsStrUserDate; }
    String operationsStrUserDate;
    @JsonSetter("enteredUserDate")
    public String getEnteredUserDate() {
        return this.enteredUserDate; }
    public void setEnteredUserDate(String enteredUserDate) {
        this.enteredUserDate = enteredUserDate; }
    String enteredUserDate;
    @JsonSetter("operationsUserDate")
    public String getOperationsUserDate() {
        return this.operationsUserDate; }
    public void setOperationsUserDate(String operationsUserDate) {
        this.operationsUserDate = operationsUserDate; }
    String operationsUserDate;
    @JsonSetter("totalPieces")
    public Integer getTotalPieces() {
        return this.totalPieces; }
    public void setTotalPieces(Integer totalPieces) {
        this.totalPieces = totalPieces; }
    Integer totalPieces;
    @JsonSetter("reRatingReason")
    public Object getReRatingReason() {
        return this.reRatingReason; }
    public void setReRatingReason(Object reRatingReason) {
        this.reRatingReason = reRatingReason; }
    Object reRatingReason;
    @JsonSetter("movements")
    public ArrayList<Movements> getMovements() {
        return this.movements; }
    public void setMovements(ArrayList<Movements> movements) {
        this.movements = movements; }
    ArrayList<Movements> movements;
    @JsonSetter("specialInstructions")
    public Object getSpecialInstructions() {
        return this.specialInstructions; }
    public void setSpecialInstructions(Object specialInstructions) {
        this.specialInstructions = specialInstructions; }
    Object specialInstructions;
    @JsonSetter("customer")
    public Object getCustomer() {
        return this.customer; }
    public void setCustomer(Object customer) {
        this.customer = customer; }
    Object customer;
    @JsonSetter("customerLocationAddress")
    public String getCustomerLocationAddress() {
        return this.customerLocationAddress; }
    public void setCustomerLocationAddress(String customerLocationAddress) {
        this.customerLocationAddress = customerLocationAddress; }
    String customerLocationAddress;
    @JsonSetter("customerLocation")
    public Object getCustomerLocation() {
        return this.customerLocation; }
    public void setCustomerLocation(Object customerLocation) {
        this.customerLocation = customerLocation; }
    Object customerLocation;
    @JsonSetter("customerName")
    public String getCustomerName() {
        return this.customerName; }
    public void setCustomerName(String customerName) {
        this.customerName = customerName; }
    String customerName;
    @JsonSetter("customerCity")
    public String getCustomerCity() {
        return this.customerCity; }
    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity; }
    String customerCity;
    @JsonSetter("customerState")
    public String getCustomerState() {
        return this.customerState; }
    public void setCustomerState(String customerState) {
        this.customerState = customerState; }
    String customerState;
    @JsonSetter("customerZip")
    public String getCustomerZip() {
        return this.customerZip; }
    public void setCustomerZip(String customerZip) {
        this.customerZip = customerZip; }
    String customerZip;
    @JsonSetter("temperatureMin")
    public Integer getTemperatureMin() {
        return this.temperatureMin; }
    public void setTemperatureMin(Integer temperatureMin) {
        this.temperatureMin = temperatureMin; }
    Integer temperatureMin;
    @JsonSetter("temperatureMax")
    public Integer getTemperatureMax() {
        return this.temperatureMax; }
    public void setTemperatureMax(Integer temperatureMax) {
        this.temperatureMax = temperatureMax; }
    Integer temperatureMax;
    @JsonSetter("temperatureUOM")
    public Object getTemperatureUOM() {
        return this.temperatureUOM; }
    public void setTemperatureUOM(Object temperatureUOM) {
        this.temperatureUOM = temperatureUOM; }
    Object temperatureUOM;
    @JsonSetter("carrierOtherPay")
    public ArrayList<Object> getCarrierOtherPay() {
        return this.carrierOtherPay; }
    public void setCarrierOtherPay(ArrayList<Object> carrierOtherPay) {
        this.carrierOtherPay = carrierOtherPay; }
    ArrayList<Object> carrierOtherPay;
    @JsonSetter("logArrivalTime")
    public Object getLogArrivalTime() {
        return this.logArrivalTime; }
    public void setLogArrivalTime(Object logArrivalTime) {
        this.logArrivalTime = logArrivalTime; }
    Object logArrivalTime;
    @JsonSetter("deleteStops")
    public ArrayList<Object> getDeleteStops() {
        return this.deleteStops; }
    public void setDeleteStops(ArrayList<Object> deleteStops) {
        this.deleteStops = deleteStops; }
    ArrayList<Object> deleteStops;
    @JsonSetter("currentMovementId")
    public Object getCurrentMovementId() {
        return this.currentMovementId; }
    public void setCurrentMovementId(Object currentMovementId) {
        this.currentMovementId = currentMovementId; }
    Object currentMovementId;
    @JsonSetter("orderDetailsHistory")
    public OrderDetailsHistory getOrderDetailsHistory() {
        return this.orderDetailsHistory; }
    public void setOrderDetailsHistory(OrderDetailsHistory orderDetailsHistory) {
        this.orderDetailsHistory = orderDetailsHistory; }
    OrderDetailsHistory orderDetailsHistory;
    @JsonSetter("noOfLoads")
    public Integer getNoOfLoads() {
        return this.noOfLoads; }
    public void setNoOfLoads(Integer noOfLoads) {
        this.noOfLoads = noOfLoads; }
    Integer noOfLoads;
    @JsonSetter("on_hold")
    public String getOn_hold() {
        return this.on_hold; }
    public void setOn_hold(String on_hold) {
        this.on_hold = on_hold; }
    String on_hold;
    @JsonSetter("hold_reason")
    public Object getHold_reason() {
        return this.hold_reason; }
    public void setHold_reason(Object hold_reason) {
        this.hold_reason = hold_reason; }
    Object hold_reason;
    @JsonSetter("orderStatus")
    public String getOrderStatus() {
        return this.orderStatus; }
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus; }
    String orderStatus;
    @JsonSetter("voidReasoncode")
    public Object getVoidReasoncode() {
        return this.voidReasoncode; }
    public void setVoidReasoncode(Object voidReasoncode) {
        this.voidReasoncode = voidReasoncode; }
    Object voidReasoncode;
    @JsonSetter("voidResonDescription")
    public Object getVoidResonDescription() {
        return this.voidResonDescription; }
    public void setVoidResonDescription(Object voidResonDescription) {
        this.voidResonDescription = voidResonDescription; }
    Object voidResonDescription;
    @JsonSetter("voidComments")
    public Object getVoidComments() {
        return this.voidComments; }
    public void setVoidComments(Object voidComments) {
        this.voidComments = voidComments; }
    Object voidComments;
    @JsonSetter("autorateStatus")
    public Object getAutorateStatus() {
        return this.autorateStatus; }
    public void setAutorateStatus(Object autorateStatus) {
        this.autorateStatus = autorateStatus; }
    Object autorateStatus;
    @JsonSetter("orderTypeId")
    public String getOrderTypeId() {
        return this.orderTypeId; }
    public void setOrderTypeId(String orderTypeId) {
        this.orderTypeId = orderTypeId; }
    String orderTypeId;
    @JsonSetter("orderMode")
    public String getOrderMode() {
        return this.orderMode; }
    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode; }
    String orderMode;
    @JsonSetter("billingMethod")
    public String getBillingMethod() {
        return this.billingMethod; }
    public void setBillingMethod(String billingMethod) {
        this.billingMethod = billingMethod; }
    String billingMethod;
    @JsonSetter("allCarrierRates")
    public Object getAllCarrierRates() {
        return this.allCarrierRates; }
    public void setAllCarrierRates(Object allCarrierRates) {
        this.allCarrierRates = allCarrierRates; }
    Object allCarrierRates;
    @JsonSetter("appliedRevenueDetails")
    public Object getAppliedRevenueDetails() {
        return this.appliedRevenueDetails; }
    public void setAppliedRevenueDetails(Object appliedRevenueDetails) {
        this.appliedRevenueDetails = appliedRevenueDetails; }
    Object appliedRevenueDetails;
    @JsonSetter("carrierUndoReasonCode")
    public Object getCarrierUndoReasonCode() {
        return this.carrierUndoReasonCode; }
    public void setCarrierUndoReasonCode(Object carrierUndoReasonCode) {
        this.carrierUndoReasonCode = carrierUndoReasonCode; }
    Object carrierUndoReasonCode;
    @JsonSetter("carrierUndoReasonDescription")
    public Object getCarrierUndoReasonDescription() {
        return this.carrierUndoReasonDescription; }
    public void setCarrierUndoReasonDescription(Object carrierUndoReasonDescription) {
        this.carrierUndoReasonDescription = carrierUndoReasonDescription; }
    Object carrierUndoReasonDescription;
    @JsonSetter("fgpUidIndicator")
    public boolean getFgpUidIndicator() {
        return this.fgpUidIndicator; }
    public void setFgpUidIndicator(boolean fgpUidIndicator) {
        this.fgpUidIndicator = fgpUidIndicator; }
    boolean fgpUidIndicator;
    @JsonSetter("segAllocCode")
    public Object getSegAllocCode() {
        return this.segAllocCode; }
    public void setSegAllocCode(Object segAllocCode) {
        this.segAllocCode = segAllocCode; }
    Object segAllocCode;
    @JsonSetter("agencyResponsiblityCode")
    public Object getAgencyResponsiblityCode() {
        return this.agencyResponsiblityCode; }
    public void setAgencyResponsiblityCode(Object agencyResponsiblityCode) {
        this.agencyResponsiblityCode = agencyResponsiblityCode; }
    Object agencyResponsiblityCode;
    @JsonSetter("customerResponsiblityCode")
    public Object getCustomerResponsiblityCode() {
        return this.customerResponsiblityCode; }
    public void setCustomerResponsiblityCode(Object customerResponsiblityCode) {
        this.customerResponsiblityCode = customerResponsiblityCode; }
    Object customerResponsiblityCode;
    @JsonSetter("agentOrderReRatingStatus")
    public String getAgentOrderReRatingStatus() {
        return this.agentOrderReRatingStatus; }
    public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
        this.agentOrderReRatingStatus = agentOrderReRatingStatus; }
    String agentOrderReRatingStatus;
    @JsonSetter("userType")
    public Object getUserType() {
        return this.userType; }
    public void setUserType(Object userType) {
        this.userType = userType; }
    Object userType;
    @JsonSetter("directCustomerInd")
    public boolean getDirectCustomerInd() {
        return this.directCustomerInd; }
    public void setDirectCustomerInd(boolean directCustomerInd) {
        this.directCustomerInd = directCustomerInd; }
    boolean directCustomerInd;
    @JsonSetter("currentCustomerId")
    public Object getCurrentCustomerId() {
        return this.currentCustomerId; }
    public void setCurrentCustomerId(Object currentCustomerId) {
        this.currentCustomerId = currentCustomerId; }
    Object currentCustomerId;
    @JsonSetter("resetCustomerRate")
    public Object getResetCustomerRate() {
        return this.resetCustomerRate; }
    public void setResetCustomerRate(Object resetCustomerRate) {
        this.resetCustomerRate = resetCustomerRate; }
    Object resetCustomerRate;
    @JsonSetter("deleteServiceFailureRecords")
    public Object getDeleteServiceFailureRecords() {
        return this.deleteServiceFailureRecords; }
    public void setDeleteServiceFailureRecords(Object deleteServiceFailureRecords) {
        this.deleteServiceFailureRecords = deleteServiceFailureRecords; }
    Object deleteServiceFailureRecords;
    @JsonSetter("postedtype")
    public Object getPostedtype() {
        return this.postedtype; }
    public void setPostedtype(Object postedtype) {
        this.postedtype = postedtype; }
    Object postedtype;
    @JsonSetter("revenueCode")
    public String getRevenueCode() {
        return this.revenueCode; }
    public void setRevenueCode(String revenueCode) {
        this.revenueCode = revenueCode; }
    String revenueCode;
    @JsonSetter("billDate")
    public Object getBillDate() {
        return this.billDate; }
    public void setBillDate(Object billDate) {
        this.billDate = billDate; }
    Object billDate;
    @JsonSetter("orderModeForTemplate")
    public Object getOrderModeForTemplate() {
        return this.orderModeForTemplate; }
    public void setOrderModeForTemplate(Object orderModeForTemplate) {
        this.orderModeForTemplate = orderModeForTemplate; }
    Object orderModeForTemplate;
    @JsonSetter("subjectOrderStatus")
    public Object getSubjectOrderStatus() {
        return this.subjectOrderStatus; }
    public void setSubjectOrderStatus(Object subjectOrderStatus) {
        this.subjectOrderStatus = subjectOrderStatus; }
    Object subjectOrderStatus;
    @JsonSetter("subjectOrderId")
    public Object getSubjectOrderId() {
        return this.subjectOrderId; }
    public void setSubjectOrderId(Object subjectOrderId) {
        this.subjectOrderId = subjectOrderId; }
    Object subjectOrderId;
    @JsonSetter("subjectOrderVoidDate")
    public Object getSubjectOrderVoidDate() {
        return this.subjectOrderVoidDate; }
    public void setSubjectOrderVoidDate(Object subjectOrderVoidDate) {
        this.subjectOrderVoidDate = subjectOrderVoidDate; }
    Object subjectOrderVoidDate;
    @JsonSetter("subjectOrderVoidDateString")
    public Object getSubjectOrderVoidDateString() {
        return this.subjectOrderVoidDateString; }
    public void setSubjectOrderVoidDateString(Object subjectOrderVoidDateString) {
        this.subjectOrderVoidDateString = subjectOrderVoidDateString; }
    Object subjectOrderVoidDateString;
    @JsonSetter("highValue")
    public boolean getHighValue() {
        return this.highValue; }
    public void setHighValue(boolean highValue) {
        this.highValue = highValue; }
    boolean highValue;
    @JsonSetter("routeCosts")
    public ArrayList<Object> getRouteCosts() {
        return this.routeCosts; }
    public void setRouteCosts(ArrayList<Object> routeCosts) {
        this.routeCosts = routeCosts; }
    ArrayList<Object> routeCosts;
    @JsonSetter("routeStateLocalities")
    public ArrayList<Object> getRouteStateLocalities() {
        return this.routeStateLocalities; }
    public void setRouteStateLocalities(ArrayList<Object> routeStateLocalities) {
        this.routeStateLocalities = routeStateLocalities; }
    ArrayList<Object> routeStateLocalities;
    @JsonSetter("routePermitType")
    public ArrayList<Object> getRoutePermitType() {
        return this.routePermitType; }
    public void setRoutePermitType(ArrayList<Object> routePermitType) {
        this.routePermitType = routePermitType; }
    ArrayList<Object> routePermitType;
    @JsonSetter("routeDates")
    public Object getRouteDates() {
        return this.routeDates; }
    public void setRouteDates(Object routeDates) {
        this.routeDates = routeDates; }
    Object routeDates;
    @JsonSetter("routeDatesStr")
    public ArrayList<Object> getRouteDatesStr() {
        return this.routeDatesStr; }
    public void setRouteDatesStr(ArrayList<Object> routeDatesStr) {
        this.routeDatesStr = routeDatesStr; }
    ArrayList<Object> routeDatesStr;
    @JsonSetter("routeNotes")
    public ArrayList<Object> getRouteNotes() {
        return this.routeNotes; }
    public void setRouteNotes(ArrayList<Object> routeNotes) {
        this.routeNotes = routeNotes; }
    ArrayList<Object> routeNotes;
    @JsonSetter("routeRcvdDates")
    public Object getRouteRcvdDates() {
        return this.routeRcvdDates; }
    public void setRouteRcvdDates(Object routeRcvdDates) {
        this.routeRcvdDates = routeRcvdDates; }
    Object routeRcvdDates;
    @JsonSetter("routeRcvdDatesStr")
    public ArrayList<Object> getRouteRcvdDatesStr() {
        return this.routeRcvdDatesStr; }
    public void setRouteRcvdDatesStr(ArrayList<Object> routeRcvdDatesStr) {
        this.routeRcvdDatesStr = routeRcvdDatesStr; }
    ArrayList<Object> routeRcvdDatesStr;
    @JsonSetter("axleWeights")
    public ArrayList<Object> getAxleWeights() {
        return this.axleWeights; }
    public void setAxleWeights(ArrayList<Object> axleWeights) {
        this.axleWeights = axleWeights; }
    ArrayList<Object> axleWeights;
    @JsonSetter("tractorAxleSpacings")
    public ArrayList<Object> getTractorAxleSpacings() {
        return this.tractorAxleSpacings; }
    public void setTractorAxleSpacings(ArrayList<Object> tractorAxleSpacings) {
        this.tractorAxleSpacings = tractorAxleSpacings; }
    ArrayList<Object> tractorAxleSpacings;
    @JsonSetter("trailerAxleSpacings")
    public ArrayList<Object> getTrailerAxleSpacings() {
        return this.trailerAxleSpacings; }
    public void setTrailerAxleSpacings(ArrayList<Object> trailerAxleSpacings) {
        this.trailerAxleSpacings = trailerAxleSpacings; }
    ArrayList<Object> trailerAxleSpacings;
    @JsonSetter("overallWeight")
    public Object getOverallWeight() {
        return this.overallWeight; }
    public void setOverallWeight(Object overallWeight) {
        this.overallWeight = overallWeight; }
    Object overallWeight;
    @JsonSetter("overallWidth")
    public Object getOverallWidth() {
        return this.overallWidth; }
    public void setOverallWidth(Object overallWidth) {
        this.overallWidth = overallWidth; }
    Object overallWidth;
    @JsonSetter("overallLength")
    public Object getOverallLength() {
        return this.overallLength; }
    public void setOverallLength(Object overallLength) {
        this.overallLength = overallLength; }
    Object overallLength;
    @JsonSetter("overallHeight")
    public Object getOverallHeight() {
        return this.overallHeight; }
    public void setOverallHeight(Object overallHeight) {
        this.overallHeight = overallHeight; }
    Object overallHeight;
    @JsonSetter("overhangFront")
    public Object getOverhangFront() {
        return this.overhangFront; }
    public void setOverhangFront(Object overhangFront) {
        this.overhangFront = overhangFront; }
    Object overhangFront;
    @JsonSetter("overhangBack")
    public Object getOverhangBack() {
        return this.overhangBack; }
    public void setOverhangBack(Object overhangBack) {
        this.overhangBack = overhangBack; }
    Object overhangBack;
    @JsonSetter("kingpinLength")
    public Object getKingpinLength() {
        return this.kingpinLength; }
    public void setKingpinLength(Object kingpinLength) {
        this.kingpinLength = kingpinLength; }
    Object kingpinLength;
    @JsonSetter("agentCaptiveOrder")
    public String getAgentCaptiveOrder() {
        return this.agentCaptiveOrder; }
    public void setAgentCaptiveOrder(String agentCaptiveOrder) {
        this.agentCaptiveOrder = agentCaptiveOrder; }
    String agentCaptiveOrder;
    @JsonSetter("division")
    public Object getDivision() {
        return this.division; }
    public void setDivision(Object division) {
        this.division = division; }
    Object division;
    @JsonSetter("loadboard")
    public boolean getLoadboard() {
        return this.loadboard; }
    public void setLoadboard(boolean loadboard) {
        this.loadboard = loadboard; }
    boolean loadboard;
    @JsonSetter("pnnCallback")
    public Object getPnnCallback() {
        return this.pnnCallback; }
    public void setPnnCallback(Object pnnCallback) {
        this.pnnCallback = pnnCallback; }
    Object pnnCallback;
    @JsonSetter("pnnComment")
    public Object getPnnComment() {
        return this.pnnComment; }
    public void setPnnComment(Object pnnComment) {
        this.pnnComment = pnnComment; }
    Object pnnComment;
    @JsonSetter("pnnComment2")
    public Object getPnnComment2() {
        return this.pnnComment2; }
    public void setPnnComment2(Object pnnComment2) {
        this.pnnComment2 = pnnComment2; }
    Object pnnComment2;
    @JsonSetter("pnnPosttype")
    public Object getPnnPosttype() {
        return this.pnnPosttype; }
    public void setPnnPosttype(Object pnnPosttype) {
        this.pnnPosttype = pnnPosttype; }
    Object pnnPosttype;
    @JsonSetter("pnnRate")
    public Object getPnnRate() {
        return this.pnnRate; }
    public void setPnnRate(Object pnnRate) {
        this.pnnRate = pnnRate; }
    Object pnnRate;
    @JsonSetter("pnnRatetype")
    public Object getPnnRatetype() {
        return this.pnnRatetype; }
    public void setPnnRatetype(Object pnnRatetype) {
        this.pnnRatetype = pnnRatetype; }
    Object pnnRatetype;
    @JsonSetter("pnnTrailerLlength")
    public Object getPnnTrailerLlength() {
        return this.pnnTrailerLlength; }
    public void setPnnTrailerLlength(Object pnnTrailerLlength) {
        this.pnnTrailerLlength = pnnTrailerLlength; }
    Object pnnTrailerLlength;
    @JsonSetter("pnnTrailerWidth")
    public Object getPnnTrailerWidth() {
        return this.pnnTrailerWidth; }
    public void setPnnTrailerWidth(Object pnnTrailerWidth) {
        this.pnnTrailerWidth = pnnTrailerWidth; }
    Object pnnTrailerWidth;
    @JsonSetter("customerRatingDone")
    public String getCustomerRatingDone() {
        return this.customerRatingDone; }
    public void setCustomerRatingDone(String customerRatingDone) {
        this.customerRatingDone = customerRatingDone; }
    String customerRatingDone;
    @JsonSetter("permitStatus")
    public Object getPermitStatus() {
        return this.permitStatus; }
    public void setPermitStatus(Object permitStatus) {
        this.permitStatus = permitStatus; }
    Object permitStatus;
    @JsonSetter("additionalEquipments")
    public ArrayList<AdditionalEquipment> getAdditionalEquipments() {
        return this.additionalEquipments; }
    public void setAdditionalEquipments(ArrayList<AdditionalEquipment> additionalEquipments) {
        this.additionalEquipments = additionalEquipments; }
    ArrayList<AdditionalEquipment> additionalEquipments;
    @JsonSetter("deletedAdditionalEquips")
    public ArrayList<Object> getDeletedAdditionalEquips() {
        return this.deletedAdditionalEquips; }
    public void setDeletedAdditionalEquips(ArrayList<Object> deletedAdditionalEquips) {
        this.deletedAdditionalEquips = deletedAdditionalEquips; }
    ArrayList<Object> deletedAdditionalEquips;
    @JsonSetter("_order_updated")
    public boolean get_order_updated() {
        return this._order_updated; }
    public void set_order_updated(boolean _order_updated) {
        this._order_updated = _order_updated; }
    boolean _order_updated;
    @JsonSetter("ponumber")
    public Object getPonumber() {
        return this.ponumber; }
    public void setPonumber(Object ponumber) {
        this.ponumber = ponumber; }
    Object ponumber;
}
