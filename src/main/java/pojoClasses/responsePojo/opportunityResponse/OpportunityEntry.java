/**
 * 
 */
package pojoClasses.responsePojo.opportunityResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class OpportunityEntry {
	private List<Movement> movements = new ArrayList<Movement>();
	private String companyId;
	private String orderNumber;
	private String customerNumber;
	private String customerName;
	private String orderStatus;
	private Object origin;
	private Object destination;
	private Object billedWeight;
	private Integer moveCount;
	private Integer margin;
	private Object billDate;
	private Integer totalOrderValue;
	private String pickupDate;
	private String deliveryDate;
	private Object pickupScheduledEarly;
	private Object pickScheduledLate;
	private Object dropScheduledEarly;
	private Object dropScheduledLate;
	private Object agentId;
	private Integer totalCharge;
	private Object originAddress;
	private String originCity;
	private String originState;
	private String originZip;
	private Object destinationAddress;
	private String destinationCity;
	private String destinationState;
	private String destinationZip;
	private String equipmentType;
	private Object shipperName;
	private Object consigneeName;
	private String commodity;
	private Integer distance;
	private Object originDistance;
	private Object destinationDistance;
	private Integer freightCharges;
	private Integer otherCharges;
	private String hold;
	private Object holdReason;
	private Object planningComments;
	private Object bol;
	private Object shipperId;
	private Object consigneeId;
	private String agentPayeeId;
	private String currentMovementId;
	private Object brokerageStatus;
	private Object opsUser;
	private Object enteredBy;
	private Double originLatitude;
	private Double originLongitude;
	private Double destinationLatitude;
	private Double destinationLongitude;
	private Object nextAction;
	private Object billAmount;
	private Object cargoInsRenewDt;
	private Object liabExpireDate;
	private Object insuranceCurrent;
	private Object marginPercentages;
	private Object orderType;
	private String readyToBill;
	private Object nextActionDescription;
	private Object nextActionAccess;
	private String orderMode;
	private String orderTypeId;
	private String orderStatusCode;
	private String lastUpdateDate;
	private String orderPieces;
	private String orderWeight;
	private String orderOperationsUser;
	private Integer stopCount;
	private String orderOpsRevComplete;
	private String createByUserId;
	private Integer minTemperature;
	private Integer maxTemperature;
	private String allStopInfo;
	private Object orderStopEntity;
	private String originHasHotComment;
	private Object originComments;
	private String destinationHasHotComment;
	private Object destinationComments;
	private String movementStatus;
	private String movementId;
	private Object carrierName;
	private Object carrierContact;
	private Object carrierEmail;
	private Object carrierPhone;
	private Object carrierMobile;
	private Object carrierManager;
	private Object safetyRating;
	private Object moveDistance;
	private Integer carrierPay;
	private Integer carrierOtherPaySum;
	private Integer totalPay;
	private Object movementDispatcherId;
	private Object movementCapacityRep;
	private Object carrierId;
	private Object carrierCity;
	private Object carrierState;
	private Object carrierZipCode;
	private Object carrierFax;
	private Object movementStatusDescription;
	private Object orderMovementPayDistance;
	private String isPreassigned;
	private String isPreassignedByDriver;
	private Object preassignedDriver;
	private Object preassignedTractor;
	private Object preassignedTrailer;
	private Object tractorFleet;
	private Object movementEquipmentGroupId;
	private Integer movementPreassignSequence;
	private Object driverAssigned;
	private Object orderMovementCapacityRep;
	private Object orderMovementCarrierContact;
	private Object orderMovementCarrierEmail;
	private Object orderMovementCarrierPhone;
	private Object movementProNumber;
	private String subjectOrderStatus;
	private String subjectOrderId;
	private String subjectOrderVoidDate;
	private String orderOriginCity;
	private String orderOriginStateId;
	private String orderOriginZipCode;
	private String orderOriginpickUpDate;
	private Object orderOriginActualArrival;
	private Object orderOriginActualDeparture;
	private Object orderOriginLocationId;
	private Object orderOriginAddress;
	private Object orderOriginAddress2;
	private String orderDestCity;
	private String orderDestStateId;
	private String orderDestZipCode;
	private String orderDestpickUpDate;
	private Object orderDestActualArrival;
	private Object orderDestActualDeparture;
	private Object orderDestLocationId;
	private Object orderDestAddress;
	private Object orderDestAddress2;
	private Object planningPending;
	private Object dispatchPending;
	private Object deliveryPending;
	private String brokeage;
	private String isHighValue;
	private Object brokered;
	private Object contractSigned;

	public List<Movement> getMovements() {
		return movements;
	}

	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Object getOrigin() {
		return origin;
	}

	public void setOrigin(Object origin) {
		this.origin = origin;
	}

	public Object getDestination() {
		return destination;
	}

	public void setDestination(Object destination) {
		this.destination = destination;
	}

	public Object getBilledWeight() {
		return billedWeight;
	}

	public void setBilledWeight(Object billedWeight) {
		this.billedWeight = billedWeight;
	}

	public Integer getMoveCount() {
		return moveCount;
	}

	public void setMoveCount(Integer moveCount) {
		this.moveCount = moveCount;
	}

	public Integer getMargin() {
		return margin;
	}

	public void setMargin(Integer margin) {
		this.margin = margin;
	}

	public Object getBillDate() {
		return billDate;
	}

	public void setBillDate(Object billDate) {
		this.billDate = billDate;
	}

	public Integer getTotalOrderValue() {
		return totalOrderValue;
	}

	public void setTotalOrderValue(Integer totalOrderValue) {
		this.totalOrderValue = totalOrderValue;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Object getPickupScheduledEarly() {
		return pickupScheduledEarly;
	}

	public void setPickupScheduledEarly(Object pickupScheduledEarly) {
		this.pickupScheduledEarly = pickupScheduledEarly;
	}

	public Object getPickScheduledLate() {
		return pickScheduledLate;
	}

	public void setPickScheduledLate(Object pickScheduledLate) {
		this.pickScheduledLate = pickScheduledLate;
	}

	public Object getDropScheduledEarly() {
		return dropScheduledEarly;
	}

	public void setDropScheduledEarly(Object dropScheduledEarly) {
		this.dropScheduledEarly = dropScheduledEarly;
	}

	public Object getDropScheduledLate() {
		return dropScheduledLate;
	}

	public void setDropScheduledLate(Object dropScheduledLate) {
		this.dropScheduledLate = dropScheduledLate;
	}

	public Object getAgentId() {
		return agentId;
	}

	public void setAgentId(Object agentId) {
		this.agentId = agentId;
	}

	public Integer getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Integer totalCharge) {
		this.totalCharge = totalCharge;
	}

	public Object getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(Object originAddress) {
		this.originAddress = originAddress;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getOriginZip() {
		return originZip;
	}

	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}

	public Object getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(Object destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getDestinationZip() {
		return destinationZip;
	}

	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}

	public String getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Object getShipperName() {
		return shipperName;
	}

	public void setShipperName(Object shipperName) {
		this.shipperName = shipperName;
	}

	public Object getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(Object consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public Object getOriginDistance() {
		return originDistance;
	}

	public void setOriginDistance(Object originDistance) {
		this.originDistance = originDistance;
	}

	public Object getDestinationDistance() {
		return destinationDistance;
	}

	public void setDestinationDistance(Object destinationDistance) {
		this.destinationDistance = destinationDistance;
	}

	public Integer getFreightCharges() {
		return freightCharges;
	}

	public void setFreightCharges(Integer freightCharges) {
		this.freightCharges = freightCharges;
	}

	public Integer getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(Integer otherCharges) {
		this.otherCharges = otherCharges;
	}

	public String getHold() {
		return hold;
	}

	public void setHold(String hold) {
		this.hold = hold;
	}

	public Object getHoldReason() {
		return holdReason;
	}

	public void setHoldReason(Object holdReason) {
		this.holdReason = holdReason;
	}

	public Object getPlanningComments() {
		return planningComments;
	}

	public void setPlanningComments(Object planningComments) {
		this.planningComments = planningComments;
	}

	public Object getBol() {
		return bol;
	}

	public void setBol(Object bol) {
		this.bol = bol;
	}

	public Object getShipperId() {
		return shipperId;
	}

	public void setShipperId(Object shipperId) {
		this.shipperId = shipperId;
	}

	public Object getConsigneeId() {
		return consigneeId;
	}

	public void setConsigneeId(Object consigneeId) {
		this.consigneeId = consigneeId;
	}

	public String getAgentPayeeId() {
		return agentPayeeId;
	}

	public void setAgentPayeeId(String agentPayeeId) {
		this.agentPayeeId = agentPayeeId;
	}

	public String getCurrentMovementId() {
		return currentMovementId;
	}

	public void setCurrentMovementId(String currentMovementId) {
		this.currentMovementId = currentMovementId;
	}

	public Object getBrokerageStatus() {
		return brokerageStatus;
	}

	public void setBrokerageStatus(Object brokerageStatus) {
		this.brokerageStatus = brokerageStatus;
	}

	public Object getOpsUser() {
		return opsUser;
	}

	public void setOpsUser(Object opsUser) {
		this.opsUser = opsUser;
	}

	public Object getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Object enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Double getOriginLatitude() {
		return originLatitude;
	}

	public void setOriginLatitude(Double originLatitude) {
		this.originLatitude = originLatitude;
	}

	public Double getOriginLongitude() {
		return originLongitude;
	}

	public void setOriginLongitude(Double originLongitude) {
		this.originLongitude = originLongitude;
	}

	public Double getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public Double getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Object getNextAction() {
		return nextAction;
	}

	public void setNextAction(Object nextAction) {
		this.nextAction = nextAction;
	}

	public Object getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Object billAmount) {
		this.billAmount = billAmount;
	}

	public Object getCargoInsRenewDt() {
		return cargoInsRenewDt;
	}

	public void setCargoInsRenewDt(Object cargoInsRenewDt) {
		this.cargoInsRenewDt = cargoInsRenewDt;
	}

	public Object getLiabExpireDate() {
		return liabExpireDate;
	}

	public void setLiabExpireDate(Object liabExpireDate) {
		this.liabExpireDate = liabExpireDate;
	}

	public Object getInsuranceCurrent() {
		return insuranceCurrent;
	}

	public void setInsuranceCurrent(Object insuranceCurrent) {
		this.insuranceCurrent = insuranceCurrent;
	}

	public Object getMarginPercentages() {
		return marginPercentages;
	}

	public void setMarginPercentages(Object marginPercentages) {
		this.marginPercentages = marginPercentages;
	}

	public Object getOrderType() {
		return orderType;
	}

	public void setOrderType(Object orderType) {
		this.orderType = orderType;
	}

	public String getReadyToBill() {
		return readyToBill;
	}

	public void setReadyToBill(String readyToBill) {
		this.readyToBill = readyToBill;
	}

	public Object getNextActionDescription() {
		return nextActionDescription;
	}

	public void setNextActionDescription(Object nextActionDescription) {
		this.nextActionDescription = nextActionDescription;
	}

	public Object getNextActionAccess() {
		return nextActionAccess;
	}

	public void setNextActionAccess(Object nextActionAccess) {
		this.nextActionAccess = nextActionAccess;
	}

	public String getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(String orderMode) {
		this.orderMode = orderMode;
	}

	public String getOrderTypeId() {
		return orderTypeId;
	}

	public void setOrderTypeId(String orderTypeId) {
		this.orderTypeId = orderTypeId;
	}

	public String getOrderStatusCode() {
		return orderStatusCode;
	}

	public void setOrderStatusCode(String orderStatusCode) {
		this.orderStatusCode = orderStatusCode;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getOrderPieces() {
		return orderPieces;
	}

	public void setOrderPieces(String orderPieces) {
		this.orderPieces = orderPieces;
	}

	public String getOrderWeight() {
		return orderWeight;
	}

	public void setOrderWeight(String orderWeight) {
		this.orderWeight = orderWeight;
	}

	public String getOrderOperationsUser() {
		return orderOperationsUser;
	}

	public void setOrderOperationsUser(String orderOperationsUser) {
		this.orderOperationsUser = orderOperationsUser;
	}

	public Integer getStopCount() {
		return stopCount;
	}

	public void setStopCount(Integer stopCount) {
		this.stopCount = stopCount;
	}

	public String getOrderOpsRevComplete() {
		return orderOpsRevComplete;
	}

	public void setOrderOpsRevComplete(String orderOpsRevComplete) {
		this.orderOpsRevComplete = orderOpsRevComplete;
	}

	public String getCreateByUserId() {
		return createByUserId;
	}

	public void setCreateByUserId(String createByUserId) {
		this.createByUserId = createByUserId;
	}

	public Integer getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(Integer minTemperature) {
		this.minTemperature = minTemperature;
	}

	public Integer getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(Integer maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public String getAllStopInfo() {
		return allStopInfo;
	}

	public void setAllStopInfo(String allStopInfo) {
		this.allStopInfo = allStopInfo;
	}

	public Object getOrderStopEntity() {
		return orderStopEntity;
	}

	public void setOrderStopEntity(Object orderStopEntity) {
		this.orderStopEntity = orderStopEntity;
	}

	public String getOriginHasHotComment() {
		return originHasHotComment;
	}

	public void setOriginHasHotComment(String originHasHotComment) {
		this.originHasHotComment = originHasHotComment;
	}

	public Object getOriginComments() {
		return originComments;
	}

	public void setOriginComments(Object originComments) {
		this.originComments = originComments;
	}

	public String getDestinationHasHotComment() {
		return destinationHasHotComment;
	}

	public void setDestinationHasHotComment(String destinationHasHotComment) {
		this.destinationHasHotComment = destinationHasHotComment;
	}

	public Object getDestinationComments() {
		return destinationComments;
	}

	public void setDestinationComments(Object destinationComments) {
		this.destinationComments = destinationComments;
	}

	public String getMovementStatus() {
		return movementStatus;
	}

	public void setMovementStatus(String movementStatus) {
		this.movementStatus = movementStatus;
	}

	public String getMovementId() {
		return movementId;
	}

	public void setMovementId(String movementId) {
		this.movementId = movementId;
	}

	public Object getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(Object carrierName) {
		this.carrierName = carrierName;
	}

	public Object getCarrierContact() {
		return carrierContact;
	}

	public void setCarrierContact(Object carrierContact) {
		this.carrierContact = carrierContact;
	}

	public Object getCarrierEmail() {
		return carrierEmail;
	}

	public void setCarrierEmail(Object carrierEmail) {
		this.carrierEmail = carrierEmail;
	}

	public Object getCarrierPhone() {
		return carrierPhone;
	}

	public void setCarrierPhone(Object carrierPhone) {
		this.carrierPhone = carrierPhone;
	}

	public Object getCarrierMobile() {
		return carrierMobile;
	}

	public void setCarrierMobile(Object carrierMobile) {
		this.carrierMobile = carrierMobile;
	}

	public Object getCarrierManager() {
		return carrierManager;
	}

	public void setCarrierManager(Object carrierManager) {
		this.carrierManager = carrierManager;
	}

	public Object getSafetyRating() {
		return safetyRating;
	}

	public void setSafetyRating(Object safetyRating) {
		this.safetyRating = safetyRating;
	}

	public Object getMoveDistance() {
		return moveDistance;
	}

	public void setMoveDistance(Object moveDistance) {
		this.moveDistance = moveDistance;
	}

	public Integer getCarrierPay() {
		return carrierPay;
	}

	public void setCarrierPay(Integer carrierPay) {
		this.carrierPay = carrierPay;
	}

	public Integer getCarrierOtherPaySum() {
		return carrierOtherPaySum;
	}

	public void setCarrierOtherPaySum(Integer carrierOtherPaySum) {
		this.carrierOtherPaySum = carrierOtherPaySum;
	}

	public Integer getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(Integer totalPay) {
		this.totalPay = totalPay;
	}

	public Object getMovementDispatcherId() {
		return movementDispatcherId;
	}

	public void setMovementDispatcherId(Object movementDispatcherId) {
		this.movementDispatcherId = movementDispatcherId;
	}

	public Object getMovementCapacityRep() {
		return movementCapacityRep;
	}

	public void setMovementCapacityRep(Object movementCapacityRep) {
		this.movementCapacityRep = movementCapacityRep;
	}

	public Object getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(Object carrierId) {
		this.carrierId = carrierId;
	}

	public Object getCarrierCity() {
		return carrierCity;
	}

	public void setCarrierCity(Object carrierCity) {
		this.carrierCity = carrierCity;
	}

	public Object getCarrierState() {
		return carrierState;
	}

	public void setCarrierState(Object carrierState) {
		this.carrierState = carrierState;
	}

	public Object getCarrierZipCode() {
		return carrierZipCode;
	}

	public void setCarrierZipCode(Object carrierZipCode) {
		this.carrierZipCode = carrierZipCode;
	}

	public Object getCarrierFax() {
		return carrierFax;
	}

	public void setCarrierFax(Object carrierFax) {
		this.carrierFax = carrierFax;
	}

	public Object getMovementStatusDescription() {
		return movementStatusDescription;
	}

	public void setMovementStatusDescription(Object movementStatusDescription) {
		this.movementStatusDescription = movementStatusDescription;
	}

	public Object getOrderMovementPayDistance() {
		return orderMovementPayDistance;
	}

	public void setOrderMovementPayDistance(Object orderMovementPayDistance) {
		this.orderMovementPayDistance = orderMovementPayDistance;
	}

	public String getIsPreassigned() {
		return isPreassigned;
	}

	public void setIsPreassigned(String isPreassigned) {
		this.isPreassigned = isPreassigned;
	}

	public String getIsPreassignedByDriver() {
		return isPreassignedByDriver;
	}

	public void setIsPreassignedByDriver(String isPreassignedByDriver) {
		this.isPreassignedByDriver = isPreassignedByDriver;
	}

	public Object getPreassignedDriver() {
		return preassignedDriver;
	}

	public void setPreassignedDriver(Object preassignedDriver) {
		this.preassignedDriver = preassignedDriver;
	}

	public Object getPreassignedTractor() {
		return preassignedTractor;
	}

	public void setPreassignedTractor(Object preassignedTractor) {
		this.preassignedTractor = preassignedTractor;
	}

	public Object getPreassignedTrailer() {
		return preassignedTrailer;
	}

	public void setPreassignedTrailer(Object preassignedTrailer) {
		this.preassignedTrailer = preassignedTrailer;
	}

	public Object getTractorFleet() {
		return tractorFleet;
	}

	public void setTractorFleet(Object tractorFleet) {
		this.tractorFleet = tractorFleet;
	}

	public Object getMovementEquipmentGroupId() {
		return movementEquipmentGroupId;
	}

	public void setMovementEquipmentGroupId(Object movementEquipmentGroupId) {
		this.movementEquipmentGroupId = movementEquipmentGroupId;
	}

	public Integer getMovementPreassignSequence() {
		return movementPreassignSequence;
	}

	public void setMovementPreassignSequence(Integer movementPreassignSequence) {
		this.movementPreassignSequence = movementPreassignSequence;
	}

	public Object getDriverAssigned() {
		return driverAssigned;
	}

	public void setDriverAssigned(Object driverAssigned) {
		this.driverAssigned = driverAssigned;
	}

	public Object getOrderMovementCapacityRep() {
		return orderMovementCapacityRep;
	}

	public void setOrderMovementCapacityRep(Object orderMovementCapacityRep) {
		this.orderMovementCapacityRep = orderMovementCapacityRep;
	}

	public Object getOrderMovementCarrierContact() {
		return orderMovementCarrierContact;
	}

	public void setOrderMovementCarrierContact(Object orderMovementCarrierContact) {
		this.orderMovementCarrierContact = orderMovementCarrierContact;
	}

	public Object getOrderMovementCarrierEmail() {
		return orderMovementCarrierEmail;
	}

	public void setOrderMovementCarrierEmail(Object orderMovementCarrierEmail) {
		this.orderMovementCarrierEmail = orderMovementCarrierEmail;
	}

	public Object getOrderMovementCarrierPhone() {
		return orderMovementCarrierPhone;
	}

	public void setOrderMovementCarrierPhone(Object orderMovementCarrierPhone) {
		this.orderMovementCarrierPhone = orderMovementCarrierPhone;
	}

	public Object getMovementProNumber() {
		return movementProNumber;
	}

	public void setMovementProNumber(Object movementProNumber) {
		this.movementProNumber = movementProNumber;
	}

	public String getSubjectOrderStatus() {
		return subjectOrderStatus;
	}

	public void setSubjectOrderStatus(String subjectOrderStatus) {
		this.subjectOrderStatus = subjectOrderStatus;
	}

	public String getSubjectOrderId() {
		return subjectOrderId;
	}

	public void setSubjectOrderId(String subjectOrderId) {
		this.subjectOrderId = subjectOrderId;
	}

	public String getSubjectOrderVoidDate() {
		return subjectOrderVoidDate;
	}

	public void setSubjectOrderVoidDate(String subjectOrderVoidDate) {
		this.subjectOrderVoidDate = subjectOrderVoidDate;
	}

	public String getOrderOriginCity() {
		return orderOriginCity;
	}

	public void setOrderOriginCity(String orderOriginCity) {
		this.orderOriginCity = orderOriginCity;
	}

	public String getOrderOriginStateId() {
		return orderOriginStateId;
	}

	public void setOrderOriginStateId(String orderOriginStateId) {
		this.orderOriginStateId = orderOriginStateId;
	}

	public String getOrderOriginZipCode() {
		return orderOriginZipCode;
	}

	public void setOrderOriginZipCode(String orderOriginZipCode) {
		this.orderOriginZipCode = orderOriginZipCode;
	}

	public String getOrderOriginpickUpDate() {
		return orderOriginpickUpDate;
	}

	public void setOrderOriginpickUpDate(String orderOriginpickUpDate) {
		this.orderOriginpickUpDate = orderOriginpickUpDate;
	}

	public Object getOrderOriginActualArrival() {
		return orderOriginActualArrival;
	}

	public void setOrderOriginActualArrival(Object orderOriginActualArrival) {
		this.orderOriginActualArrival = orderOriginActualArrival;
	}

	public Object getOrderOriginActualDeparture() {
		return orderOriginActualDeparture;
	}

	public void setOrderOriginActualDeparture(Object orderOriginActualDeparture) {
		this.orderOriginActualDeparture = orderOriginActualDeparture;
	}

	public Object getOrderOriginLocationId() {
		return orderOriginLocationId;
	}

	public void setOrderOriginLocationId(Object orderOriginLocationId) {
		this.orderOriginLocationId = orderOriginLocationId;
	}

	public Object getOrderOriginAddress() {
		return orderOriginAddress;
	}

	public void setOrderOriginAddress(Object orderOriginAddress) {
		this.orderOriginAddress = orderOriginAddress;
	}

	public Object getOrderOriginAddress2() {
		return orderOriginAddress2;
	}

	public void setOrderOriginAddress2(Object orderOriginAddress2) {
		this.orderOriginAddress2 = orderOriginAddress2;
	}

	public String getOrderDestCity() {
		return orderDestCity;
	}

	public void setOrderDestCity(String orderDestCity) {
		this.orderDestCity = orderDestCity;
	}

	public String getOrderDestStateId() {
		return orderDestStateId;
	}

	public void setOrderDestStateId(String orderDestStateId) {
		this.orderDestStateId = orderDestStateId;
	}

	public String getOrderDestZipCode() {
		return orderDestZipCode;
	}

	public void setOrderDestZipCode(String orderDestZipCode) {
		this.orderDestZipCode = orderDestZipCode;
	}

	public String getOrderDestpickUpDate() {
		return orderDestpickUpDate;
	}

	public void setOrderDestpickUpDate(String orderDestpickUpDate) {
		this.orderDestpickUpDate = orderDestpickUpDate;
	}

	public Object getOrderDestActualArrival() {
		return orderDestActualArrival;
	}

	public void setOrderDestActualArrival(Object orderDestActualArrival) {
		this.orderDestActualArrival = orderDestActualArrival;
	}

	public Object getOrderDestActualDeparture() {
		return orderDestActualDeparture;
	}

	public void setOrderDestActualDeparture(Object orderDestActualDeparture) {
		this.orderDestActualDeparture = orderDestActualDeparture;
	}

	public Object getOrderDestLocationId() {
		return orderDestLocationId;
	}

	public void setOrderDestLocationId(Object orderDestLocationId) {
		this.orderDestLocationId = orderDestLocationId;
	}

	public Object getOrderDestAddress() {
		return orderDestAddress;
	}

	public void setOrderDestAddress(Object orderDestAddress) {
		this.orderDestAddress = orderDestAddress;
	}

	public Object getOrderDestAddress2() {
		return orderDestAddress2;
	}

	public void setOrderDestAddress2(Object orderDestAddress2) {
		this.orderDestAddress2 = orderDestAddress2;
	}

	public Object getPlanningPending() {
		return planningPending;
	}

	public void setPlanningPending(Object planningPending) {
		this.planningPending = planningPending;
	}

	public Object getDispatchPending() {
		return dispatchPending;
	}

	public void setDispatchPending(Object dispatchPending) {
		this.dispatchPending = dispatchPending;
	}

	public Object getDeliveryPending() {
		return deliveryPending;
	}

	public void setDeliveryPending(Object deliveryPending) {
		this.deliveryPending = deliveryPending;
	}

	public String getBrokeage() {
		return brokeage;
	}

	public void setBrokeage(String brokeage) {
		this.brokeage = brokeage;
	}

	public String getIsHighValue() {
		return isHighValue;
	}

	public void setIsHighValue(String isHighValue) {
		this.isHighValue = isHighValue;
	}

	public Object getBrokered() {
		return brokered;
	}

	public void setBrokered(Object brokered) {
		this.brokered = brokered;
	}

	public Object getContractSigned() {
		return contractSigned;
	}

	public void setContractSigned(Object contractSigned) {
		this.contractSigned = contractSigned;
	}
}
