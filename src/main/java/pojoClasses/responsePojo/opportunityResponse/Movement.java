/**
 * 
 */
package pojoClasses.responsePojo.opportunityResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class Movement {

	private String brokeage;
	private Object planningPending;
	private Object dispatchPending;
	private Object deliveryPending;
	private String companyId;
	private Object origin;
	private Object destination;
	private Integer margin;
	private String pickupDate;
	private String deliveryDate;
	private Object pickupScheduledEarly;
	private Object pickScheduledLate;
	private Object dropScheduledEarly;
	private Object dropScheduledLate;
	private Object originAddress;
	private String originCity;
	private String originState;
	private String originZip;
	private Object destinationAddress;
	private String destinationCity;
	private String destinationState;
	private String destinationZip;
	private Object shipperName;
	private Object consigneeName;
	private Object distance;
	private Object originDistance;
	private Object destinationDistance;
	private Object shipperId;
	private Object consigneeId;
	private Object brokerageStatus;
	private Double originLatitude;
	private Double originLongitude;
	private Double destinationLatitude;
	private Double destinationLongitude;
	private Object marginPercentages;
	private Integer stopCount;
	private String originHasHotComment;
	private Object originComments;
	private String destinationHasHotComment;
	private Object destinationComments;
	private String movementStatus;
	private String movementId;
	private Object carrierName;
	private Object carrierContact;
	private Object carrierEmail;
	private Object carrierPhone;
	private Object carrierMobile;
	private Object carrierManager;
	private Object safetyRating;
	private Object moveDistance;
	private Integer carrierPay;
	private Integer carrierOtherPaySum;
	private Integer totalPay;
	private Object movementDispatcherId;
	private Object movementCapacityRep;
	private Object carrierId;
	private Object carrierCity;
	private Object carrierState;
	private Object carrierZipCode;
	private Object carrierFax;
	private Object movementStatusDescription;
	private Object orderMovementPayDistance;
	private Object isPreassigned;
	private Object isPreassignedByDriver;
	private Object preassignedDriver;
	private Object preassignedTractor;
	private Object preassignedTrailer;
	private Object tractorFleet;
	private Object movementEquipmentGroupId;
	private Integer movementPreassignSequence;
	private Object driverAssigned;
	private Object orderMovementCapacityRep;
	private Object orderMovementCarrierContact;
	private Object orderMovementCarrierEmail;
	private Object orderMovementCarrierPhone;
	private Object movementProNumber;
	private String customerNumber;
	private String customerName;
	private String orderNumber;
	private Object coDriver;
	private List<Object> drivers = new ArrayList<Object>();
	private Object allDriversJSONString;

	public String getBrokeage() {
		return brokeage;
	}

	public void setBrokeage(String brokeage) {
		this.brokeage = brokeage;
	}

	public Object getPlanningPending() {
		return planningPending;
	}

	public void setPlanningPending(Object planningPending) {
		this.planningPending = planningPending;
	}

	public Object getDispatchPending() {
		return dispatchPending;
	}

	public void setDispatchPending(Object dispatchPending) {
		this.dispatchPending = dispatchPending;
	}

	public Object getDeliveryPending() {
		return deliveryPending;
	}

	public void setDeliveryPending(Object deliveryPending) {
		this.deliveryPending = deliveryPending;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Object getOrigin() {
		return origin;
	}

	public void setOrigin(Object origin) {
		this.origin = origin;
	}

	public Object getDestination() {
		return destination;
	}

	public void setDestination(Object destination) {
		this.destination = destination;
	}

	public Integer getMargin() {
		return margin;
	}

	public void setMargin(Integer margin) {
		this.margin = margin;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Object getPickupScheduledEarly() {
		return pickupScheduledEarly;
	}

	public void setPickupScheduledEarly(Object pickupScheduledEarly) {
		this.pickupScheduledEarly = pickupScheduledEarly;
	}

	public Object getPickScheduledLate() {
		return pickScheduledLate;
	}

	public void setPickScheduledLate(Object pickScheduledLate) {
		this.pickScheduledLate = pickScheduledLate;
	}

	public Object getDropScheduledEarly() {
		return dropScheduledEarly;
	}

	public void setDropScheduledEarly(Object dropScheduledEarly) {
		this.dropScheduledEarly = dropScheduledEarly;
	}

	public Object getDropScheduledLate() {
		return dropScheduledLate;
	}

	public void setDropScheduledLate(Object dropScheduledLate) {
		this.dropScheduledLate = dropScheduledLate;
	}

	public Object getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(Object originAddress) {
		this.originAddress = originAddress;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getOriginZip() {
		return originZip;
	}

	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}

	public Object getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(Object destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getDestinationZip() {
		return destinationZip;
	}

	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}

	public Object getShipperName() {
		return shipperName;
	}

	public void setShipperName(Object shipperName) {
		this.shipperName = shipperName;
	}

	public Object getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(Object consigneeName) {
		this.consigneeName = consigneeName;
	}

	public Object getDistance() {
		return distance;
	}

	public void setDistance(Object distance) {
		this.distance = distance;
	}

	public Object getOriginDistance() {
		return originDistance;
	}

	public void setOriginDistance(Object originDistance) {
		this.originDistance = originDistance;
	}

	public Object getDestinationDistance() {
		return destinationDistance;
	}

	public void setDestinationDistance(Object destinationDistance) {
		this.destinationDistance = destinationDistance;
	}

	public Object getShipperId() {
		return shipperId;
	}

	public void setShipperId(Object shipperId) {
		this.shipperId = shipperId;
	}

	public Object getConsigneeId() {
		return consigneeId;
	}

	public void setConsigneeId(Object consigneeId) {
		this.consigneeId = consigneeId;
	}

	public Object getBrokerageStatus() {
		return brokerageStatus;
	}

	public void setBrokerageStatus(Object brokerageStatus) {
		this.brokerageStatus = brokerageStatus;
	}

	public Double getOriginLatitude() {
		return originLatitude;
	}

	public void setOriginLatitude(Double originLatitude) {
		this.originLatitude = originLatitude;
	}

	public Double getOriginLongitude() {
		return originLongitude;
	}

	public void setOriginLongitude(Double originLongitude) {
		this.originLongitude = originLongitude;
	}

	public Double getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public Double getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Object getMarginPercentages() {
		return marginPercentages;
	}

	public void setMarginPercentages(Object marginPercentages) {
		this.marginPercentages = marginPercentages;
	}

	public Integer getStopCount() {
		return stopCount;
	}

	public void setStopCount(Integer stopCount) {
		this.stopCount = stopCount;
	}

	public String getOriginHasHotComment() {
		return originHasHotComment;
	}

	public void setOriginHasHotComment(String originHasHotComment) {
		this.originHasHotComment = originHasHotComment;
	}

	public Object getOriginComments() {
		return originComments;
	}

	public void setOriginComments(Object originComments) {
		this.originComments = originComments;
	}

	public String getDestinationHasHotComment() {
		return destinationHasHotComment;
	}

	public void setDestinationHasHotComment(String destinationHasHotComment) {
		this.destinationHasHotComment = destinationHasHotComment;
	}

	public Object getDestinationComments() {
		return destinationComments;
	}

	public void setDestinationComments(Object destinationComments) {
		this.destinationComments = destinationComments;
	}

	public String getMovementStatus() {
		return movementStatus;
	}

	public void setMovementStatus(String movementStatus) {
		this.movementStatus = movementStatus;
	}

	public String getMovementId() {
		return movementId;
	}

	public void setMovementId(String movementId) {
		this.movementId = movementId;
	}

	public Object getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(Object carrierName) {
		this.carrierName = carrierName;
	}

	public Object getCarrierContact() {
		return carrierContact;
	}

	public void setCarrierContact(Object carrierContact) {
		this.carrierContact = carrierContact;
	}

	public Object getCarrierEmail() {
		return carrierEmail;
	}

	public void setCarrierEmail(Object carrierEmail) {
		this.carrierEmail = carrierEmail;
	}

	public Object getCarrierPhone() {
		return carrierPhone;
	}

	public void setCarrierPhone(Object carrierPhone) {
		this.carrierPhone = carrierPhone;
	}

	public Object getCarrierMobile() {
		return carrierMobile;
	}

	public void setCarrierMobile(Object carrierMobile) {
		this.carrierMobile = carrierMobile;
	}

	public Object getCarrierManager() {
		return carrierManager;
	}

	public void setCarrierManager(Object carrierManager) {
		this.carrierManager = carrierManager;
	}

	public Object getSafetyRating() {
		return safetyRating;
	}

	public void setSafetyRating(Object safetyRating) {
		this.safetyRating = safetyRating;
	}

	public Object getMoveDistance() {
		return moveDistance;
	}

	public void setMoveDistance(Object moveDistance) {
		this.moveDistance = moveDistance;
	}

	public Integer getCarrierPay() {
		return carrierPay;
	}

	public void setCarrierPay(Integer carrierPay) {
		this.carrierPay = carrierPay;
	}

	public Integer getCarrierOtherPaySum() {
		return carrierOtherPaySum;
	}

	public void setCarrierOtherPaySum(Integer carrierOtherPaySum) {
		this.carrierOtherPaySum = carrierOtherPaySum;
	}

	public Integer getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(Integer totalPay) {
		this.totalPay = totalPay;
	}

	public Object getMovementDispatcherId() {
		return movementDispatcherId;
	}

	public void setMovementDispatcherId(Object movementDispatcherId) {
		this.movementDispatcherId = movementDispatcherId;
	}

	public Object getMovementCapacityRep() {
		return movementCapacityRep;
	}

	public void setMovementCapacityRep(Object movementCapacityRep) {
		this.movementCapacityRep = movementCapacityRep;
	}

	public Object getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(Object carrierId) {
		this.carrierId = carrierId;
	}

	public Object getCarrierCity() {
		return carrierCity;
	}

	public void setCarrierCity(Object carrierCity) {
		this.carrierCity = carrierCity;
	}

	public Object getCarrierState() {
		return carrierState;
	}

	public void setCarrierState(Object carrierState) {
		this.carrierState = carrierState;
	}

	public Object getCarrierZipCode() {
		return carrierZipCode;
	}

	public void setCarrierZipCode(Object carrierZipCode) {
		this.carrierZipCode = carrierZipCode;
	}

	public Object getCarrierFax() {
		return carrierFax;
	}

	public void setCarrierFax(Object carrierFax) {
		this.carrierFax = carrierFax;
	}

	public Object getMovementStatusDescription() {
		return movementStatusDescription;
	}

	public void setMovementStatusDescription(Object movementStatusDescription) {
		this.movementStatusDescription = movementStatusDescription;
	}

	public Object getOrderMovementPayDistance() {
		return orderMovementPayDistance;
	}

	public void setOrderMovementPayDistance(Object orderMovementPayDistance) {
		this.orderMovementPayDistance = orderMovementPayDistance;
	}

	public Object getIsPreassigned() {
		return isPreassigned;
	}

	public void setIsPreassigned(Object isPreassigned) {
		this.isPreassigned = isPreassigned;
	}

	public Object getIsPreassignedByDriver() {
		return isPreassignedByDriver;
	}

	public void setIsPreassignedByDriver(Object isPreassignedByDriver) {
		this.isPreassignedByDriver = isPreassignedByDriver;
	}

	public Object getPreassignedDriver() {
		return preassignedDriver;
	}

	public void setPreassignedDriver(Object preassignedDriver) {
		this.preassignedDriver = preassignedDriver;
	}

	public Object getPreassignedTractor() {
		return preassignedTractor;
	}

	public void setPreassignedTractor(Object preassignedTractor) {
		this.preassignedTractor = preassignedTractor;
	}

	public Object getPreassignedTrailer() {
		return preassignedTrailer;
	}

	public void setPreassignedTrailer(Object preassignedTrailer) {
		this.preassignedTrailer = preassignedTrailer;
	}

	public Object getTractorFleet() {
		return tractorFleet;
	}

	public void setTractorFleet(Object tractorFleet) {
		this.tractorFleet = tractorFleet;
	}

	public Object getMovementEquipmentGroupId() {
		return movementEquipmentGroupId;
	}

	public void setMovementEquipmentGroupId(Object movementEquipmentGroupId) {
		this.movementEquipmentGroupId = movementEquipmentGroupId;
	}

	public Integer getMovementPreassignSequence() {
		return movementPreassignSequence;
	}

	public void setMovementPreassignSequence(Integer movementPreassignSequence) {
		this.movementPreassignSequence = movementPreassignSequence;
	}

	public Object getDriverAssigned() {
		return driverAssigned;
	}

	public void setDriverAssigned(Object driverAssigned) {
		this.driverAssigned = driverAssigned;
	}

	public Object getOrderMovementCapacityRep() {
		return orderMovementCapacityRep;
	}

	public void setOrderMovementCapacityRep(Object orderMovementCapacityRep) {
		this.orderMovementCapacityRep = orderMovementCapacityRep;
	}

	public Object getOrderMovementCarrierContact() {
		return orderMovementCarrierContact;
	}

	public void setOrderMovementCarrierContact(Object orderMovementCarrierContact) {
		this.orderMovementCarrierContact = orderMovementCarrierContact;
	}

	public Object getOrderMovementCarrierEmail() {
		return orderMovementCarrierEmail;
	}

	public void setOrderMovementCarrierEmail(Object orderMovementCarrierEmail) {
		this.orderMovementCarrierEmail = orderMovementCarrierEmail;
	}

	public Object getOrderMovementCarrierPhone() {
		return orderMovementCarrierPhone;
	}

	public void setOrderMovementCarrierPhone(Object orderMovementCarrierPhone) {
		this.orderMovementCarrierPhone = orderMovementCarrierPhone;
	}

	public Object getMovementProNumber() {
		return movementProNumber;
	}

	public void setMovementProNumber(Object movementProNumber) {
		this.movementProNumber = movementProNumber;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Object getCoDriver() {
		return coDriver;
	}

	public void setCoDriver(Object coDriver) {
		this.coDriver = coDriver;
	}

	public List<Object> getDrivers() {
		return drivers;
	}

	public void setDrivers(List<Object> drivers) {
		this.drivers = drivers;
	}

	public Object getAllDriversJSONString() {
		return allDriversJSONString;
	}

	public void setAllDriversJSONString(Object allDriversJSONString) {
		this.allDriversJSONString = allDriversJSONString;
	}

}
