/**
 * 
 */
package pojoClasses.responsePojo;

import pojoClasses.responsePojo.MasterData.Customer.CustomerComment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class CustomerCommentsResponsePOJO {
	private String status;
    private List<CustomerComment> customerComment = new ArrayList<CustomerComment>();;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<CustomerComment> getResponse() {
        return customerComment;
    }
    public void setResponse(List<CustomerComment> customerComment) {
        this.customerComment = customerComment;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
