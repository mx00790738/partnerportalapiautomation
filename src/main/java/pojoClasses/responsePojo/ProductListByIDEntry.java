/**
 * 
 */
package pojoClasses.responsePojo;

/**
 * @author MX0C92049
 *
 */
public class ProductListByIDEntry {
	private String companyId;
    private Object commodityId;
    private String description;
    private String id;
    private Object locationId;
    private Object parentRowId;
    private Object parentRowType;
    private Object cubicUnits;
    private String cubicUnitsIsPerPiece;
    private Object handlingUnits;
    private String handlingUnitsIsPerPiece;
    private Object packagingTypeCode;
    private Object productSKU;
    private Object nmfcClassCode;
    private Object spots;
    private String spotsIsPerPiece;
    private Object weight;
    private String weightIsPerPiece;
    private Object weightUOM;
    private String hazmat;
    private Object hazmatUNNbr;
    private Object hazmatPackageGroup;
    private Object hazmatClassCode;
    private Object hazmatShipname;
    private Object hazmatContactName;
    private Object hazmatContactNumber;
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public Object getCommodityId() {
        return commodityId;
    }
    public void setCommodityId(Object commodityId) {
        this.commodityId = commodityId;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Object getLocationId() {
        return locationId;
    }
    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }
    public Object getParentRowId() {
        return parentRowId;
    }
    public void setParentRowId(Object parentRowId) {
        this.parentRowId = parentRowId;
    }
    public Object getParentRowType() {
        return parentRowType;
    }
    public void setParentRowType(Object parentRowType) {
        this.parentRowType = parentRowType;
    }
    public Object getCubicUnits() {
        return cubicUnits;
    }
    public void setCubicUnits(Object cubicUnits) {
        this.cubicUnits = cubicUnits;
    }
    public String getCubicUnitsIsPerPiece() {
        return cubicUnitsIsPerPiece;
    }
    public void setCubicUnitsIsPerPiece(String cubicUnitsIsPerPiece) {
        this.cubicUnitsIsPerPiece = cubicUnitsIsPerPiece;
    }
    public Object getHandlingUnits() {
        return handlingUnits;
    }
    public void setHandlingUnits(Object handlingUnits) {
        this.handlingUnits = handlingUnits;
    }
    public String getHandlingUnitsIsPerPiece() {
        return handlingUnitsIsPerPiece;
    }
    public void setHandlingUnitsIsPerPiece(String handlingUnitsIsPerPiece) {
        this.handlingUnitsIsPerPiece = handlingUnitsIsPerPiece;
    }
    public Object getPackagingTypeCode() {
        return packagingTypeCode;
    }
    public void setPackagingTypeCode(Object packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }
    public Object getProductSKU() {
        return productSKU;
    }
    public void setProductSKU(Object productSKU) {
        this.productSKU = productSKU;
    }
    public Object getNmfcClassCode() {
        return nmfcClassCode;
    }
    public void setNmfcClassCode(Object nmfcClassCode) {
        this.nmfcClassCode = nmfcClassCode;
    }
    public Object getSpots() {
        return spots;
    }
    public void setSpots(Object spots) {
        this.spots = spots;
    }
    public String getSpotsIsPerPiece() {
        return spotsIsPerPiece;
    }
    public void setSpotsIsPerPiece(String spotsIsPerPiece) {
        this.spotsIsPerPiece = spotsIsPerPiece;
    }
    public Object getWeight() {
        return weight;
    }
    public void setWeight(Object weight) {
        this.weight = weight;
    }
    public String getWeightIsPerPiece() {
        return weightIsPerPiece;
    }
    public void setWeightIsPerPiece(String weightIsPerPiece) {
        this.weightIsPerPiece = weightIsPerPiece;
    }
    public Object getWeightUOM() {
        return weightUOM;
    }
    public void setWeightUOM(Object weightUOM) {
        this.weightUOM = weightUOM;
    }
    public String getHazmat() {
        return hazmat;
    }
    public void setHazmat(String hazmat) {
        this.hazmat = hazmat;
    }
    public Object getHazmatUNNbr() {
        return hazmatUNNbr;
    }
    public void setHazmatUNNbr(Object hazmatUNNbr) {
        this.hazmatUNNbr = hazmatUNNbr;
    }
    public Object getHazmatPackageGroup() {
        return hazmatPackageGroup;
    }
    public void setHazmatPackageGroup(Object hazmatPackageGroup) {
        this.hazmatPackageGroup = hazmatPackageGroup;
    }
    public Object getHazmatClassCode() {
        return hazmatClassCode;
    }
    public void setHazmatClassCode(Object hazmatClassCode) {
        this.hazmatClassCode = hazmatClassCode;
    }
    public Object getHazmatShipname() {
        return hazmatShipname;
    }
    public void setHazmatShipname(Object hazmatShipname) {
        this.hazmatShipname = hazmatShipname;
    }
    public Object getHazmatContactName() {
        return hazmatContactName;
    }
    public void setHazmatContactName(Object hazmatContactName) {
        this.hazmatContactName = hazmatContactName;
    }
    public Object getHazmatContactNumber() {
        return hazmatContactNumber;
    }
    public void setHazmatContactNumber(Object hazmatContactNumber) {
        this.hazmatContactNumber = hazmatContactNumber;
    }
}
