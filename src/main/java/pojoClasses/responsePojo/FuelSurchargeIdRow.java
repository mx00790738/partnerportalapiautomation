/**
 * 
 */
package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author MX0C92049
 *
 */
public class FuelSurchargeIdRow {
	private String name;
    private Boolean payCompanyHourlyDriver;
    private Boolean prevFlag;
    private Boolean isFuelSurcharge;
    private String chargeRateMethod;
    private String glid;
    private String descr;
    private String type;
    private String id;
    private String whoToPay;
    private Boolean isTaxable;
    private String companyId;
    public String getName() {
        return name;
    }
    @JsonSetter("__name")
    public void setName(String name) {
        this.name = name;
    }
    public Boolean getPayCompanyHourlyDriver() {
        return payCompanyHourlyDriver;
    }
    @JsonSetter("pay_company_hourly_driver")
    public void setPayCompanyHourlyDriver(Boolean payCompanyHourlyDriver) {
        this.payCompanyHourlyDriver = payCompanyHourlyDriver;
    }
    public Boolean getPrevFlag() {
        return prevFlag;
    }
    @JsonSetter("prev_flag")
    public void setPrevFlag(Boolean prevFlag) {
        this.prevFlag = prevFlag;
    }
    public Boolean getIsFuelSurcharge() {
        return isFuelSurcharge;
    }
    @JsonSetter("is_fuel_surcharge")
    public void setIsFuelSurcharge(Boolean isFuelSurcharge) {
        this.isFuelSurcharge = isFuelSurcharge;
    }
    public String getChargeRateMethod() {
        return chargeRateMethod;
    }
    @JsonSetter("charge_rate_method")
    public void setChargeRateMethod(String chargeRateMethod) {
        this.chargeRateMethod = chargeRateMethod;
    }
    public String getGlid() {
        return glid;
    }
    @JsonSetter("glid")
    public void setGlid(String glid) {
        this.glid = glid;
    }
    public String getDescr() {
        return descr;
    }
    @JsonSetter("descr")
    public void setDescr(String descr) {
        this.descr = descr;
    }
    public String getType() {
        return type;
    }
    @JsonSetter("__type")
    public void setType(String type) {
        this.type = type;
    }
    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }
    public String getWhoToPay() {
        return whoToPay;
    }
    @JsonSetter("who_to_pay")
    public void setWhoToPay(String whoToPay) {
        this.whoToPay = whoToPay;
    }
    public Boolean getIsTaxable() {
        return isTaxable;
    }
    @JsonSetter("is_taxable")
    public void setIsTaxable(Boolean isTaxable) {
        this.isTaxable = isTaxable;
    }
    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
