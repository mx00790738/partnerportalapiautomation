package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonSetter;

public class AgencyCustomer {
    private String companyId;
    private String locationAddress;
    private String locationName;
    private String name;
    private String zipCode;
    private String city;
    private String state;
    private String id;
    private Double creditLimit;
    private String isActive;
    public String getCompanyId() {
        return companyId;
    }

    @JsonSetter("companyId")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getLocationAddress() {
        return locationAddress;
    }

    @JsonSetter("locationAddress")
    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }
    public String getLocationName() {
        return locationName;
    }

    @JsonSetter("locationName")
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public String getName() {
        return name;
    }

    @JsonSetter("name")
    public void setName(String name) {
        this.name = name;
    }
    public String getZipCode() {
        return zipCode;
    }

    @JsonSetter("zip_code")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public String getCity() {
        return city;
    }

    @JsonSetter("city")
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }

    @JsonSetter("state")
    public void setState(String state) {
        this.state = state;
    }
    public String getId() {
        return id;
    }

    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }
    public Double getCreditLimit() {
        return creditLimit;
    }

    @JsonSetter("creditLimit")
    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }
    public String getIsActive() {
        return isActive;
    }

    @JsonSetter("isActive")
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
