package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonSetter;

public class CommentTypeDescr {
    private String name;
    private String descr;
    private Boolean isActive;
    private String companyId;
    private String type;
    private String id;

    public String getName() {
        return name;
    }
    @JsonSetter("__name")
    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }
    @JsonSetter("descr")
    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Boolean getIsActive() {
        return isActive;
    }
    @JsonSetter("is_active")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getCompanyId() {
        return companyId;
    }
    @JsonSetter("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getType() {
        return type;
    }
    @JsonSetter("__type")
    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }
    @JsonSetter("id")
    public void setId(String id) {
        this.id = id;
    }
}
