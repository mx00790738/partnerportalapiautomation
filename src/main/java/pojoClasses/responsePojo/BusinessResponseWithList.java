/**
 * 
 */
package pojoClasses.responsePojo;

import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class BusinessResponseWithList<T> {
	private String status;
    private List<T> response;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<T> getResponse() {
        return response;
    }
    public void setResponse(List<T> response) {
        this.response = response;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
