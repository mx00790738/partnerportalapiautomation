/**
 * 
 */
package pojoClasses.responsePojo;

/**
 * @author MX0C92049
 *
 */
public class ResponseStatus {
	private String responseCode;
    private String responseDesc;
    private ExceptionDetails exceptionDetails;
    public String getResponseCode() {
        return responseCode;
    }
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
    public String getResponseDesc() {
        return responseDesc;
    }
    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }
}
