package pojoClasses.responsePojo.CreateOrder.MileageProfileDefault;

import com.fasterxml.jackson.annotation.JsonSetter;

public class MileageProfileDefault {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("loadDistProfile")
    public String getLoadDistProfile() {
        return this.loadDistProfile; }
    public void setLoadDistProfile(String loadDistProfile) {
        this.loadDistProfile = loadDistProfile; }
    String loadDistProfile;
    @JsonSetter("billDistProfile")
    public String getBillDistProfile() {
        return this.billDistProfile; }
    public void setBillDistProfile(String billDistProfile) {
        this.billDistProfile = billDistProfile; }
    String billDistProfile;
    @JsonSetter("emptyDistProfile")
    public String getEmptyDistProfile() {
        return this.emptyDistProfile; }
    public void setEmptyDistProfile(String emptyDistProfile) {
        this.emptyDistProfile = emptyDistProfile; }
    String emptyDistProfile;
    @JsonSetter("etaDistProfile")
    public String getEtaDistProfile() {
        return this.etaDistProfile; }
    public void setEtaDistProfile(String etaDistProfile) {
        this.etaDistProfile = etaDistProfile; }
    String etaDistProfile;
    @JsonSetter("fuelDistProfile")
    public String getFuelDistProfile() {
        return this.fuelDistProfile; }
    public void setFuelDistProfile(String fuelDistProfile) {
        this.fuelDistProfile = fuelDistProfile; }
    String fuelDistProfile;
    @JsonSetter("payDistProfile")
    public String getPayDistProfile() {
        return this.payDistProfile; }
    public void setPayDistProfile(String payDistProfile) {
        this.payDistProfile = payDistProfile; }
    String payDistProfile;
    @JsonSetter("pracDistProfile")
    public String getPracDistProfile() {
        return this.pracDistProfile; }
    public void setPracDistProfile(String pracDistProfile) {
        this.pracDistProfile = pracDistProfile; }
    String pracDistProfile;
    @JsonSetter("shortDistProfile")
    public String getShortDistProfile() {
        return this.shortDistProfile; }
    public void setShortDistProfile(String shortDistProfile) {
        this.shortDistProfile = shortDistProfile; }
    String shortDistProfile;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
}
