package pojoClasses.responsePojo;

import java.util.List;

public class AgencyCustDtlsResponsePOJO {
    private String status;
    private List<AgencyCustomer> customerDetails;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<AgencyCustomer> getResponse() {
        return customerDetails;
    }
    public void setResponse(List<AgencyCustomer> customerDetails) {
        this.customerDetails = customerDetails;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
