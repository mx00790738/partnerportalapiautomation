/**
 * 
 */
package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author MX0C92049
 *
 */
public class CustomFscHdrIdRow {
	private String name;
	private String id;
	private String companyId;
	private String descr;
	private String type;

	public String getName() {
		return name;
	}

	@JsonSetter("__name")
	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	@JsonSetter("company_id")
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getDescr() {
		return descr;
	}

	@JsonSetter("descr")
	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getType() {
		return type;
	}

	@JsonSetter("__type")
	public void setType(String type) {
		this.type = type;
	}
}
