/**
 * 
 */
package pojoClasses.responsePojo;

import pojoClasses.responsePojo.MasterData.Customer.CustomerContact;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX0C92049
 *
 */
public class CustomerCreditListResponsePOJO {
	private String status;
    private List<CustomerContact> customerContact = new ArrayList<CustomerContact>();;
    private int code;
    private ResponseStatus responseStatus;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<CustomerContact> getResponse() {
        return customerContact;
    }
    public void setResponse(List<CustomerContact> customerContact) {
        this.customerContact = customerContact;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
