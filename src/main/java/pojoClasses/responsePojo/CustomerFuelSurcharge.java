/**
 * 
 */
package pojoClasses.responsePojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author MX0C92049
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerFuelSurcharge {
	private String amount;
	private String dateOpt;
	private String fuelVariance;
	private String basePrice;
	private String truckloadLtl;
	private String companyId;
	private String method;
	private String fuelSurchargeId;
	private String surchargeType;
	private String type;
	private String id;
	private String credit;
	private Object region;
	private String applyDoeRate;
	private String fuelPercent;
	private String customerId;
	private String orderTypeId;
	private FuelSurchargeIdRow fuelSurchargeIdRow;
	private CustomFscHdrIdRow customFscHdrIdRow;

	public String getAmount() {
		return amount;
	}

	@JsonSetter("amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDateOpt() {
		return dateOpt;
	}

	@JsonSetter("date_opt")
	public void setDateOpt(String dateOpt) {
		this.dateOpt = dateOpt;
	}

	public String getFuelVariance() {
		return fuelVariance;
	}

	@JsonSetter("fuel_variance")
	public void setFuelVariance(String fuelVariance) {
		this.fuelVariance = fuelVariance;
	}

	public String getBasePrice() {
		return basePrice;
	}

	@JsonSetter("base_price")
	public void setBasePrice(String basePrice) {
		this.basePrice = basePrice;
	}

	public String getTruckloadLtl() {
		return truckloadLtl;
	}

	@JsonSetter("truckload_ltl")
	public void setTruckloadLtl(String truckloadLtl) {
		this.truckloadLtl = truckloadLtl;
	}

	public String getCompanyId() {
		return companyId;
	}

	@JsonSetter("company_id")
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getMethod() {
		return method;
	}

	@JsonSetter("method")
	public void setMethod(String method) {
		this.method = method;
	}

	public String getFuelSurchargeId() {
		return fuelSurchargeId;
	}

	@JsonSetter("fuel_surcharge_id")
	public void setFuelSurchargeId(String fuelSurchargeId) {
		this.fuelSurchargeId = fuelSurchargeId;
	}

	public String getSurchargeType() {
		return surchargeType;
	}

	@JsonSetter("surcharge_type")
	public void setSurchargeType(String surchargeType) {
		this.surchargeType = surchargeType;
	}

	public String getType() {
		return type;
	}

	@JsonSetter("__type")
	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getCredit() {
		return credit;
	}

	@JsonSetter("credit")
	public void setCredit(String credit) {
		this.credit = credit;
	}

	public Object getRegion() {
		return region;
	}

	@JsonSetter("region")
	public void setRegion(Object region) {
		this.region = region;
	}

	public String getApplyDoeRate() {
		return applyDoeRate;
	}

	@JsonSetter("apply_doe_rate")
	public void setApplyDoeRate(String applyDoeRate) {
		this.applyDoeRate = applyDoeRate;
	}

	public String getFuelPercent() {
		return fuelPercent;
	}

	@JsonSetter("fuel_percent")
	public void setFuelPercent(String fuelPercent) {
		this.fuelPercent = fuelPercent;
	}

	public String getCustomerId() {
		return customerId;
	}

	@JsonSetter("customer_id")
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOrderTypeId() {
		return orderTypeId;
	}

	@JsonSetter("order_type_id")
	public void setOrderTypeId(String orderTypeId) {
		this.orderTypeId = orderTypeId;
	}

	public FuelSurchargeIdRow getFuelSurchargeIdRow() {
		return fuelSurchargeIdRow;
	}

	@JsonSetter("fuel_surcharge_id_row")
	public void setFuelSurchargeIdRow(FuelSurchargeIdRow fuelSurchargeIdRow) {
		this.fuelSurchargeIdRow = fuelSurchargeIdRow;
	}

	public CustomFscHdrIdRow getCustomFscHdrIdRow() {
		return customFscHdrIdRow;
	}

	@JsonSetter("custom_fsc_hdr_id_row")
	public void setCustomFscHdrIdRow(CustomFscHdrIdRow customFscHdrIdRow) {
		this.customFscHdrIdRow = customFscHdrIdRow;
	}
}
