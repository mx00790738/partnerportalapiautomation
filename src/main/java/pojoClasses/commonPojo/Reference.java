package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reference {
    @JsonSetter("referenceNumber")
    public String getReferenceNumber() {
        return this.referenceNumber; }
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber; }
    String referenceNumber;
    @JsonSetter("referenceType")
    public String getReferenceType() {
        return this.referenceType; }
    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType; }
    String referenceType;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("stopId")
    public String getStopId() {
        return this.stopId; }
    public void setStopId(String stopId) {
        this.stopId = stopId; }
    String stopId;
    @JsonSetter("referenceTypeDescr")
    public String getReferenceTypeDescr() {
        return this.referenceTypeDescr; }
    public void setReferenceTypeDescr(String referenceTypeDescr) {
        this.referenceTypeDescr = referenceTypeDescr; }
    String referenceTypeDescr;
    @JsonSetter("weight")
    public String getWeight() {
        return this.weight; }
    public void setWeight(String weight) {
        this.weight = weight; }
    String weight;
    @JsonSetter("pieces")
    public String getPieces() {
        return this.pieces; }
    public void setPieces(String pieces) {
        this.pieces = pieces; }
    String pieces;
    @JsonSetter("sendToDriver")
    public boolean getSendToDriver() {
        return this.sendToDriver; }
    public void setSendToDriver(boolean sendToDriver) {
        this.sendToDriver = sendToDriver; }
    boolean sendToDriver;
    @JsonSetter("version")
    public String getVersion() {
        return this.version; }
    public void setVersion(String version) {
        this.version = version; }
    String version;
    @JsonSetter("elementId")
    public String getElementId() {
        return this.elementId; }
    public void setElementId(String elementId) {
        this.elementId = elementId; }
    String elementId;
    @JsonSetter("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("partnerId")
    public String getPartnerId() {
        return this.partnerId; }
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId; }
    String partnerId;
    @JsonSetter("deleteFlagReference")
    public boolean getDeleteFlagReference() {
        return this.deleteFlagReference; }
    public void setDeleteFlagReference(boolean deleteFlagReference) {
        this.deleteFlagReference = deleteFlagReference; }
    boolean deleteFlagReference;
}
