package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commodities{
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("fgpUid")
    public int getFgpUid() {
        return this.fgpUid; }
    public void setFgpUid(int fgpUid) {
        this.fgpUid = fgpUid; }
    int fgpUid;
    @JsonSetter("orderId")
    public String getOrderId() {
        return this.orderId; }
    public void setOrderId(String orderId) {
        this.orderId = orderId; }
    String orderId;
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonSetter("commodity")
    public ArrayList<Commodity> getCommodity() {
        return this.commodity; }
    public void setCommodity(ArrayList<Commodity> commodity) {
        this.commodity = commodity; }
    ArrayList<Commodity> commodity;
    @JsonSetter("services")
    public ArrayList<Object> getServices() {
        return this.services; }
    public void setServices(ArrayList<Object> services) {
        this.services = services; }
    ArrayList<Object> services;
}
