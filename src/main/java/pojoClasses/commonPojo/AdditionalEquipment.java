package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdditionalEquipment {
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("appliesTo")
    public String getAppliesTo() {
        return this.appliesTo; }
    public void setAppliesTo(String appliesTo) {
        this.appliesTo = appliesTo; }
    String appliesTo;
    @JsonSetter("equipReq")
    public String getEquipReq() {
        return this.equipReq; }
    public void setEquipReq(String equipReq) {
        this.equipReq = equipReq; }
    String equipReq;
    @JsonSetter("equipTypeId")
    public String getEquipTypeId() {
        return this.equipTypeId; }
    public void setEquipTypeId(String equipTypeId) {
        this.equipTypeId = equipTypeId; }
    String equipTypeId;
    @JsonSetter("zmit")
    public Object getZmit() {
        return this.zmit; }
    public void setZmit(Object zmit) {
        this.zmit = zmit; }
    Object zmit;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("active")
    public String getActive() {
        return this.active; }
    public void setActive(String active) {
        this.active = active; }
    String active;
    @JsonSetter("shipperConsignee")
    public String getShipperConsignee() {
        return this.shipperConsignee; }
    public void setShipperConsignee(String shipperConsignee) {
        this.shipperConsignee = shipperConsignee; }
    String shipperConsignee;
    @JsonSetter("orderId")
    public String getOrderId() {
        return this.orderId; }
    public void setOrderId(String orderId) {
        this.orderId = orderId; }
    String orderId;
    @JsonSetter("quantity")
    public int getQuantity() {
        return this.quantity; }
    public void setQuantity(int quantity) {
        this.quantity = quantity; }
    int quantity;
}
