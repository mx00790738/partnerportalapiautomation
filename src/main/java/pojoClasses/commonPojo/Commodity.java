package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commodity {
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("description")
    public String getDescription() {
        return this.description; }
    public void setDescription(String description) {
        this.description = description; }
    String description;
    @JsonSetter("freightClass")
    public String getFreightClass() {
        return this.freightClass; }
    public void setFreightClass(String freightClass) {
        this.freightClass = freightClass; }
    String freightClass;
    @JsonSetter("quantity")
    public int getQuantity() {
        return this.quantity; }
    public void setQuantity(int quantity) {
        this.quantity = quantity; }
    int quantity;
    @JsonSetter("packaging")
    public String getPackaging() {
        return this.packaging; }
    public void setPackaging(String packaging) {
        this.packaging = packaging; }
    String packaging;
    @JsonSetter("length")
    public int getLength() {
        return this.length; }
    public void setLength(int length) {
        this.length = length; }
    int length;
    @JsonSetter("width")
    public int getWidth() {
        return this.width; }
    public void setWidth(int width) {
        this.width = width; }
    int width;
    @JsonSetter("height")
    public int getHeight() {
        return this.height; }
    public void setHeight(int height) {
        this.height = height; }
    int height;
    @JsonSetter("density")
    public Object getDensity() {
        return this.density; }
    public void setDensity(Object density) {
        this.density = density; }
    Object density;
    @JsonSetter("weight")
    public int getWeight() {
        return this.weight; }
    public void setWeight(int weight) {
        this.weight = weight; }
    int weight;
    @JsonSetter("weightUOM")
    public String getWeightUOM() {
        return this.weightUOM; }
    public void setWeightUOM(String weightUOM) {
        this.weightUOM = weightUOM; }
    String weightUOM;
    @JsonSetter("handlingUnits")
    public Object getHandlingUnits() {
        return this.handlingUnits; }
    public void setHandlingUnits(Object handlingUnits) {
        this.handlingUnits = handlingUnits; }
    Object handlingUnits;
    @JsonSetter("reqSpots")
    public int getReqSpots() {
        return this.reqSpots; }
    public void setReqSpots(int reqSpots) {
        this.reqSpots = reqSpots; }
    int reqSpots;
    @JsonSetter("value")
    public Object getValue() {
        return this.value; }
    public void setValue(Object value) {
        this.value = value; }
    Object value;
    @JsonSetter("nmfcCode")
    public Object getNmfcCode() {
        return this.nmfcCode; }
    public void setNmfcCode(Object nmfcCode) {
        this.nmfcCode = nmfcCode; }
    Object nmfcCode;
    @JsonSetter("hazmat")
    public boolean getHazmat() {
        return this.hazmat; }
    public void setHazmat(boolean hazmat) {
        this.hazmat = hazmat; }
    boolean hazmat;
    @JsonSetter("hazmatPackageGroup")
    public Object getHazmatPackageGroup() {
        return this.hazmatPackageGroup; }
    public void setHazmatPackageGroup(Object hazmatPackageGroup) {
        this.hazmatPackageGroup = hazmatPackageGroup; }
    Object hazmatPackageGroup;
    @JsonSetter("hazmatClassCode")
    public Object getHazmatClassCode() {
        return this.hazmatClassCode; }
    public void setHazmatClassCode(Object hazmatClassCode) {
        this.hazmatClassCode = hazmatClassCode; }
    Object hazmatClassCode;
    @JsonSetter("hazmatUNNbr")
    public Object getHazmatUNNbr() {
        return this.hazmatUNNbr; }
    public void setHazmatUNNbr(Object hazmatUNNbr) {
        this.hazmatUNNbr = hazmatUNNbr; }
    Object hazmatUNNbr;
    @JsonSetter("hazmatShipname")
    public Object getHazmatShipname() {
        return this.hazmatShipname; }
    public void setHazmatShipname(Object hazmatShipname) {
        this.hazmatShipname = hazmatShipname; }
    Object hazmatShipname;
    @JsonSetter("hazmatContactName")
    public Object getHazmatContactName() {
        return this.hazmatContactName; }
    public void setHazmatContactName(Object hazmatContactName) {
        this.hazmatContactName = hazmatContactName; }
    Object hazmatContactName;
    @JsonSetter("hazmatContactNumber")
    public Object getHazmatContactNumber() {
        return this.hazmatContactNumber; }
    public void setHazmatContactNumber(Object hazmatContactNumber) {
        this.hazmatContactNumber = hazmatContactNumber; }
    Object hazmatContactNumber;
    @JsonSetter("hazmatDescription")
    public Object getHazmatDescription() {
        return this.hazmatDescription; }
    public void setHazmatDescription(Object hazmatDescription) {
        this.hazmatDescription = hazmatDescription; }
    Object hazmatDescription;
    @JsonSetter("linearUnits")
    public Object getLinearUnits() {
        return this.linearUnits; }
    public void setLinearUnits(Object linearUnits) {
        this.linearUnits = linearUnits; }
    Object linearUnits;
    @JsonSetter("cubicUnits")
    public Object getCubicUnits() {
        return this.cubicUnits; }
    public void setCubicUnits(Object cubicUnits) {
        this.cubicUnits = cubicUnits; }
    Object cubicUnits;
    @JsonSetter("deleteFlagConsignment")
    public boolean getDeleteFlagConsignment() {
        return this.deleteFlagConsignment; }
    public void setDeleteFlagConsignment(boolean deleteFlagConsignment) {
        this.deleteFlagConsignment = deleteFlagConsignment; }
    boolean deleteFlagConsignment;
    @JsonSetter("fgiUid")
    public int getFgiUid() {
        return this.fgiUid; }
    public void setFgiUid(int fgiUid) {
        this.fgiUid = fgiUid; }
    int fgiUid;
}
