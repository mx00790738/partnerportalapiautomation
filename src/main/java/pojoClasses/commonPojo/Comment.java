package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment {
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonSetter("comment")
    public String getComment() {
        return this.comment; }
    public void setComment(String comment) {
        this.comment = comment; }
    String comment;
    @JsonSetter("commentType")
    public String getCommentType() {
        return this.commentType; }
    public void setCommentType(String commentType) {
        this.commentType = commentType; }
    String commentType;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("deleteFlagStopNotes")
    public boolean getDeleteFlagStopNotes() {
        return this.deleteFlagStopNotes; }
    public void setDeleteFlagStopNotes(boolean deleteFlagStopNotes) {
        this.deleteFlagStopNotes = deleteFlagStopNotes; }
    boolean deleteFlagStopNotes;
    @JsonSetter("stopId")
    public String getStopId() {
        return this.stopId; }
    public void setStopId(String stopId) {
        this.stopId = stopId; }
    String stopId;
}
