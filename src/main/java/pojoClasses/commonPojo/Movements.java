package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movements{
    @JsonSetter("movementAction")
    public Object getMovementAction() {
        return this.movementAction; }
    public void setMovementAction(Object movementAction) {
        this.movementAction = movementAction; }
    Object movementAction;
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonSetter("carrierReRating")
    public boolean getCarrierReRating() {
        return this.carrierReRating; }
    public void setCarrierReRating(boolean carrierReRating) {
        this.carrierReRating = carrierReRating; }
    boolean carrierReRating;
    @JsonSetter("driverDispatchUpdate")
    public boolean getDriverDispatchUpdate() {
        return this.driverDispatchUpdate; }
    public void setDriverDispatchUpdate(boolean driverDispatchUpdate) {
        this.driverDispatchUpdate = driverDispatchUpdate; }
    boolean driverDispatchUpdate;
    @JsonSetter("saveCompleteCustomerRating")
    public boolean getSaveCompleteCustomerRating() {
        return this.saveCompleteCustomerRating; }
    public void setSaveCompleteCustomerRating(boolean saveCompleteCustomerRating) {
        this.saveCompleteCustomerRating = saveCompleteCustomerRating; }
    boolean saveCompleteCustomerRating;
    @JsonSetter("saveCompleteDriverDispatch")
    public boolean getSaveCompleteDriverDispatch() {
        return this.saveCompleteDriverDispatch; }
    public void setSaveCompleteDriverDispatch(boolean saveCompleteDriverDispatch) {
        this.saveCompleteDriverDispatch = saveCompleteDriverDispatch; }
    boolean saveCompleteDriverDispatch;
    @JsonSetter("saveCloseSetAppointment")
    public boolean getSaveCloseSetAppointment() {
        return this.saveCloseSetAppointment; }
    public void setSaveCloseSetAppointment(boolean saveCloseSetAppointment) {
        this.saveCloseSetAppointment = saveCloseSetAppointment; }
    boolean saveCloseSetAppointment;
    @JsonSetter("saveCloseCarrierConfirmation")
    public boolean getSaveCloseCarrierConfirmation() {
        return this.saveCloseCarrierConfirmation; }
    public void setSaveCloseCarrierConfirmation(boolean saveCloseCarrierConfirmation) {
        this.saveCloseCarrierConfirmation = saveCloseCarrierConfirmation; }
    boolean saveCloseCarrierConfirmation;
    @JsonSetter("saveClosePickupLog")
    public boolean getSaveClosePickupLog() {
        return this.saveClosePickupLog; }
    public void setSaveClosePickupLog(boolean saveClosePickupLog) {
        this.saveClosePickupLog = saveClosePickupLog; }
    boolean saveClosePickupLog;
    @JsonSetter("saveCloseDeliveryLog")
    public boolean getSaveCloseDeliveryLog() {
        return this.saveCloseDeliveryLog; }
    public void setSaveCloseDeliveryLog(boolean saveCloseDeliveryLog) {
        this.saveCloseDeliveryLog = saveCloseDeliveryLog; }
    boolean saveCloseDeliveryLog;
    @JsonSetter("saveNClosePickupLogClicked")
    public boolean getSaveNClosePickupLogClicked() {
        return this.saveNClosePickupLogClicked; }
    public void setSaveNClosePickupLogClicked(boolean saveNClosePickupLogClicked) {
        this.saveNClosePickupLogClicked = saveNClosePickupLogClicked; }
    boolean saveNClosePickupLogClicked;
    @JsonSetter("saveNCloseDeliveryLogClicked")
    public boolean getSaveNCloseDeliveryLogClicked() {
        return this.saveNCloseDeliveryLogClicked; }
    public void setSaveNCloseDeliveryLogClicked(boolean saveNCloseDeliveryLogClicked) {
        this.saveNCloseDeliveryLogClicked = saveNCloseDeliveryLogClicked; }
    boolean saveNCloseDeliveryLogClicked;
    @JsonSetter("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("authorized")
    public boolean getAuthorized() {
        return this.authorized; }
    public void setAuthorized(boolean authorized) {
        this.authorized = authorized; }
    boolean authorized;
    @JsonSetter("brokerage")
    public boolean getBrokerage() {
        return this.brokerage; }
    public void setBrokerage(boolean brokerage) {
        this.brokerage = brokerage; }
    boolean brokerage;
    @JsonSetter("carrierContact")
    public Object getCarrierContact() {
        return this.carrierContact; }
    public void setCarrierContact(Object carrierContact) {
        this.carrierContact = carrierContact; }
    Object carrierContact;
    @JsonSetter("carrierEmail")
    public Object getCarrierEmail() {
        return this.carrierEmail; }
    public void setCarrierEmail(Object carrierEmail) {
        this.carrierEmail = carrierEmail; }
    Object carrierEmail;
    @JsonSetter("carrierPhone")
    public Object getCarrierPhone() {
        return this.carrierPhone; }
    public void setCarrierPhone(Object carrierPhone) {
        this.carrierPhone = carrierPhone; }
    Object carrierPhone;
    @JsonSetter("carrierTractor")
    public Object getCarrierTractor() {
        return this.carrierTractor; }
    public void setCarrierTractor(Object carrierTractor) {
        this.carrierTractor = carrierTractor; }
    Object carrierTractor;
    @JsonSetter("carrierTrailer")
    public Object getCarrierTrailer() {
        return this.carrierTrailer; }
    public void setCarrierTrailer(Object carrierTrailer) {
        this.carrierTrailer = carrierTrailer; }
    Object carrierTrailer;
    @JsonSetter("driverId")
    public Object getDriverId() {
        return this.driverId; }
    public void setDriverId(Object driverId) {
        this.driverId = driverId; }
    Object driverId;
    @JsonSetter("driverName")
    public Object getDriverName() {
        return this.driverName; }
    public void setDriverName(Object driverName) {
        this.driverName = driverName; }
    Object driverName;
    @JsonSetter("driverCell")
    public Object getDriverCell() {
        return this.driverCell; }
    public void setDriverCell(Object driverCell) {
        this.driverCell = driverCell; }
    Object driverCell;
    @JsonSetter("driverEmail")
    public Object getDriverEmail() {
        return this.driverEmail; }
    public void setDriverEmail(Object driverEmail) {
        this.driverEmail = driverEmail; }
    Object driverEmail;
    @JsonSetter("destStopId")
    public String getDestStopId() {
        return this.destStopId; }
    public void setDestStopId(String destStopId) {
        this.destStopId = destStopId; }
    String destStopId;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("originStopId")
    public String getOriginStopId() {
        return this.originStopId; }
    public void setOriginStopId(String originStopId) {
        this.originStopId = originStopId; }
    String originStopId;
    @JsonSetter("overridePayeeId")
    public Object getOverridePayeeId() {
        return this.overridePayeeId; }
    public void setOverridePayeeId(Object overridePayeeId) {
        this.overridePayeeId = overridePayeeId; }
    Object overridePayeeId;
    @JsonSetter("status")
    public String getStatus() {
        return this.status; }
    public void setStatus(String status) {
        this.status = status; }
    String status;
    @JsonSetter("statusDescr")
    public String getStatusDescr() {
        return this.statusDescr; }
    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr; }
    String statusDescr;
    @JsonSetter("carrierRate")
    public Object getCarrierRate() {
        return this.carrierRate; }
    public void setCarrierRate(Object carrierRate) {
        this.carrierRate = carrierRate; }
    Object carrierRate;
    @JsonSetter("carrierUnit")
    public Object getCarrierUnit() {
        return this.carrierUnit; }
    public void setCarrierUnit(Object carrierUnit) {
        this.carrierUnit = carrierUnit; }
    Object carrierUnit;
    @JsonSetter("carrierAmt")
    public Object getCarrierAmt() {
        return this.carrierAmt; }
    public void setCarrierAmt(Object carrierAmt) {
        this.carrierAmt = carrierAmt; }
    Object carrierAmt;
    @JsonSetter("carrierPayType")
    public Object getCarrierPayType() {
        return this.carrierPayType; }
    public void setCarrierPayType(Object carrierPayType) {
        this.carrierPayType = carrierPayType; }
    Object carrierPayType;
    @JsonSetter("brokerageStatus")
    public String getBrokerageStatus() {
        return this.brokerageStatus; }
    public void setBrokerageStatus(String brokerageStatus) {
        this.brokerageStatus = brokerageStatus; }
    String brokerageStatus;
    @JsonSetter("brokerageStatusInt")
    public int getBrokerageStatusInt() {
        return this.brokerageStatusInt; }
    public void setBrokerageStatusInt(int brokerageStatusInt) {
        this.brokerageStatusInt = brokerageStatusInt; }
    int brokerageStatusInt;
    @JsonSetter("carrier")
    public Object getCarrier() {
        return this.carrier; }
    public void setCarrier(Object carrier) {
        this.carrier = carrier; }
    Object carrier;
    @JsonSetter("saveCloseArrivalDeparture")
    public boolean getSaveCloseArrivalDeparture() {
        return this.saveCloseArrivalDeparture; }
    public void setSaveCloseArrivalDeparture(boolean saveCloseArrivalDeparture) {
        this.saveCloseArrivalDeparture = saveCloseArrivalDeparture; }
    boolean saveCloseArrivalDeparture;
    @JsonSetter("rateConfirmationStatus")
    public Object getRateConfirmationStatus() {
        return this.rateConfirmationStatus; }
    public void setRateConfirmationStatus(Object rateConfirmationStatus) {
        this.rateConfirmationStatus = rateConfirmationStatus; }
    Object rateConfirmationStatus;
    @JsonSetter("saveCompleteCarrierRating")
    public boolean getSaveCompleteCarrierRating() {
        return this.saveCompleteCarrierRating; }
    public void setSaveCompleteCarrierRating(boolean saveCompleteCarrierRating) {
        this.saveCompleteCarrierRating = saveCompleteCarrierRating; }
    boolean saveCompleteCarrierRating;
    @JsonSetter("brokerageStatusStr")
    public Object getBrokerageStatusStr() {
        return this.brokerageStatusStr; }
    public void setBrokerageStatusStr(Object brokerageStatusStr) {
        this.brokerageStatusStr = brokerageStatusStr; }
    Object brokerageStatusStr;
    @JsonSetter("capacityUserId")
    public Object getCapacityUserId() {
        return this.capacityUserId; }
    public void setCapacityUserId(Object capacityUserId) {
        this.capacityUserId = capacityUserId; }
    Object capacityUserId;
    @JsonSetter("dispatcherUserId")
    public Object getDispatcherUserId() {
        return this.dispatcherUserId; }
    public void setDispatcherUserId(Object dispatcherUserId) {
        this.dispatcherUserId = dispatcherUserId; }
    Object dispatcherUserId;
    @JsonSetter("capacityUserStrDate")
    public Object getCapacityUserStrDate() {
        return this.capacityUserStrDate; }
    public void setCapacityUserStrDate(Object capacityUserStrDate) {
        this.capacityUserStrDate = capacityUserStrDate; }
    Object capacityUserStrDate;
    @JsonSetter("dispatcherUserStrDate")
    public Object getDispatcherUserStrDate() {
        return this.dispatcherUserStrDate; }
    public void setDispatcherUserStrDate(Object dispatcherUserStrDate) {
        this.dispatcherUserStrDate = dispatcherUserStrDate; }
    Object dispatcherUserStrDate;
    @JsonSetter("capacityUserDate")
    public Object getCapacityUserDate() {
        return this.capacityUserDate; }
    public void setCapacityUserDate(Object capacityUserDate) {
        this.capacityUserDate = capacityUserDate; }
    Object capacityUserDate;
    @JsonSetter("dispatcherUserDate")
    public Object getDispatcherUserDate() {
        return this.dispatcherUserDate; }
    public void setDispatcherUserDate(Object dispatcherUserDate) {
        this.dispatcherUserDate = dispatcherUserDate; }
    Object dispatcherUserDate;
    @JsonSetter("moveDistance")
    public String getMoveDistance() {
        return this.moveDistance; }
    public void setMoveDistance(String moveDistance) {
        this.moveDistance = moveDistance; }
    String moveDistance;
    @JsonSetter("originLocation")
    public String getOriginLocation() {
        return this.originLocation; }
    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation; }
    String originLocation;
    @JsonSetter("destLocation")
    public String getDestLocation() {
        return this.destLocation; }
    public void setDestLocation(String destLocation) {
        this.destLocation = destLocation; }
    String destLocation;
    @JsonSetter("confirmCarrierRating")
    public boolean getConfirmCarrierRating() {
        return this.confirmCarrierRating; }
    public void setConfirmCarrierRating(boolean confirmCarrierRating) {
        this.confirmCarrierRating = confirmCarrierRating; }
    boolean confirmCarrierRating;
    @JsonSetter("proNbr")
    public Object getProNbr() {
        return this.proNbr; }
    public void setProNbr(Object proNbr) {
        this.proNbr = proNbr; }
    Object proNbr;
    @JsonSetter("segAllocCode")
    public Object getSegAllocCode() {
        return this.segAllocCode; }
    public void setSegAllocCode(Object segAllocCode) {
        this.segAllocCode = segAllocCode; }
    Object segAllocCode;
    @JsonSetter("fromCarrierOfferFlag")
    public boolean getFromCarrierOfferFlag() {
        return this.fromCarrierOfferFlag; }
    public void setFromCarrierOfferFlag(boolean fromCarrierOfferFlag) {
        this.fromCarrierOfferFlag = fromCarrierOfferFlag; }
    boolean fromCarrierOfferFlag;
    @JsonSetter("ediStatusFlag")
    public boolean getEdiStatusFlag() {
        return this.ediStatusFlag; }
    public void setEdiStatusFlag(boolean ediStatusFlag) {
        this.ediStatusFlag = ediStatusFlag; }
    boolean ediStatusFlag;
    @JsonSetter("brokerageLoadStatus")
    public Object getBrokerageLoadStatus() {
        return this.brokerageLoadStatus; }
    public void setBrokerageLoadStatus(Object brokerageLoadStatus) {
        this.brokerageLoadStatus = brokerageLoadStatus; }
    Object brokerageLoadStatus;
    @JsonSetter("crstEquipmentDivision")
    public Object getCrstEquipmentDivision() {
        return this.crstEquipmentDivision; }
    public void setCrstEquipmentDivision(Object crstEquipmentDivision) {
        this.crstEquipmentDivision = crstEquipmentDivision; }
    Object crstEquipmentDivision;
    @JsonSetter("carrierRatingDone")
    public String getCarrierRatingDone() {
        return this.carrierRatingDone; }
    public void setCarrierRatingDone(String carrierRatingDone) {
        this.carrierRatingDone = carrierRatingDone; }
    String carrierRatingDone;
    @JsonSetter("tractorId")
    public Object getTractorId() {
        return this.tractorId; }
    public void setTractorId(Object tractorId) {
        this.tractorId = tractorId; }
    Object tractorId;
    @JsonSetter("trailerId")
    public Object getTrailerId() {
        return this.trailerId; }
    public void setTrailerId(Object trailerId) {
        this.trailerId = trailerId; }
    Object trailerId;
    @JsonSetter("orderMovementExecutionDivision")
    public String getOrderMovementExecutionDivision() {
        return this.orderMovementExecutionDivision; }
    public void setOrderMovementExecutionDivision(String orderMovementExecutionDivision) {
        this.orderMovementExecutionDivision = orderMovementExecutionDivision; }
    String orderMovementExecutionDivision;
}
