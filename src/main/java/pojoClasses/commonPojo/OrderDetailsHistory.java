package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailsHistory {
    @JsonSetter("orderId")
    public String getOrderId() {
        return this.orderId; }
    public void setOrderId(String orderId) {
        this.orderId = orderId; }
    String orderId;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("agentPortalUserId")
    public String getAgentPortalUserId() {
        return this.agentPortalUserId; }
    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId; }
    String agentPortalUserId;
    @JsonSetter("mcleodUserId")
    public String getMcleodUserId() {
        return this.mcleodUserId; }
    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId; }
    String mcleodUserId;
    @JsonSetter("historyData")
    public ArrayList<Object> getHistoryData() {
        return this.historyData; }
    public void setHistoryData(ArrayList<Object> historyData) {
        this.historyData = historyData; }
    ArrayList<Object> historyData;
    @JsonSetter("customerId")
    public String getCustomerId() {
        return this.customerId; }
    public void setCustomerId(String customerId) {
        this.customerId = customerId; }
    String customerId;
    @JsonSetter("agencyPayeeId")
    public String getAgencyPayeeId() {
        return this.agencyPayeeId; }
    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId; }
    String agencyPayeeId;
}
