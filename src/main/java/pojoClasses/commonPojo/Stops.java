package pojoClasses.commonPojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stops{
    //Todo check from where this is value comes from
    @JsonSetter("agentPortalUserId")
    public String getAgentPortalUserId() {
        return this.agentPortalUserId; }
    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId; }
    String agentPortalUserId;
    //Todo check from where this is value comes from
    @JsonSetter("mcleodUserId")
    public String getMcleodUserId() {
        return this.mcleodUserId; }
    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId; }
    String mcleodUserId;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("orderId")
    public String getOrderId() {
        return this.orderId; }
    public void setOrderId(String orderId) {
        this.orderId = orderId; }
    String orderId;
    @JsonSetter("cityId")
    public int getCityId() {
        return this.cityId; }
    public void setCityId(int cityId) {
        this.cityId = cityId; }
    int cityId;
    @JsonSetter("cityName")
    public String getCityName() {
        return this.cityName; }
    public void setCityName(String cityName) {
        this.cityName = cityName; }
    String cityName;
    @JsonSetter("state")
    public String getState() {
        return this.state; }
    public void setState(String state) {
        this.state = state; }
    String state;
    @JsonSetter("zipCode")
    public String getZipCode() {
        return this.zipCode; }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode; }
    String zipCode;
    @JsonSetter("locationId")
    public String getLocationId() {
        return this.locationId; }
    public void setLocationId(String locationId) {
        this.locationId = locationId; }
    String locationId;
    @JsonSetter("locationName")
    public String getLocationName() {
        return this.locationName; }
    public void setLocationName(String locationName) {
        this.locationName = locationName; }
    String locationName;
    @JsonSetter("locationType")
    public Object getLocationType() {
        return this.locationType; }
    public void setLocationType(Object locationType) {
        this.locationType = locationType; }
    Object locationType;
    @JsonSetter("address1")
    public String getAddress1() {
        return this.address1; }
    public void setAddress1(String address1) {
        this.address1 = address1; }
    String address1;
    @JsonSetter("address2")
    public String getAddress2() {
        return this.address2; }
    public void setAddress2(String address2) {
        this.address2 = address2; }
    String address2;
    @JsonSetter("stopType")
    public String getStopType() {
        return this.stopType; }
    public void setStopType(String stopType) {
        this.stopType = stopType; }
    String stopType;
    @JsonSetter("comments")
    public ArrayList<Comment> getComments() {
        return this.comments; }
    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments; }
    ArrayList<Comment> comments;

    //Todo - get order api returns zone id for country. get this fixed by dev
    @JsonSetter("country")
    public String getCountry() {
        return this.country; }
    public void setCountry(String country) {
        this.country = country; }
    String country;
    @JsonSetter("contactName")
    public String getContactName() {
        return this.contactName; }
    public void setContactName(String contactName) {
        this.contactName = contactName; }
    String contactName;
    @JsonSetter("contactNumber")
    public String getContactNumber() {
        return this.contactNumber; }
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber; }
    String contactNumber;
    @JsonSetter("email")
    public Object getEmail() {
        return this.email; }
    public void setEmail(Object email) {
        this.email = email; }
    Object email;
    @JsonSetter("schedDepartDate")
    public String getSchedDepartDate() {
        return this.schedDepartDate; }
    public void setSchedDepartDate(String schedDepartDate) {
        this.schedDepartDate = schedDepartDate; }
    String schedDepartDate;
    @JsonSetter("schedArriveDate")
    public String getSchedArriveDate() {
        return this.schedArriveDate; }
    public void setSchedArriveDate(String schedArriveDate) {
        this.schedArriveDate = schedArriveDate; }
    String schedArriveDate;
    @JsonSetter("apptRequired")
    public boolean getApptRequired() {
        return this.apptRequired; }
    public void setApptRequired(boolean apptRequired) {
        this.apptRequired = apptRequired; }
    boolean apptRequired;
    @JsonSetter("confirmed")
    public boolean getConfirmed() {
        return this.confirmed; }
    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed; }
    boolean confirmed;
    @JsonSetter("references")
    public ArrayList<Object> getReferences() {
        return this.references; }
    public void setReferences(ArrayList<Object> references) {
        this.references = references; }
    ArrayList<Object> references;
    @JsonSetter("deleteFlagStop")
    public boolean getDeleteFlagStop() {
        return this.deleteFlagStop; }
    public void setDeleteFlagStop(boolean deleteFlagStop) {
        this.deleteFlagStop = deleteFlagStop; }
    boolean deleteFlagStop;
    //Todo check from where this is value comes from
    @JsonSetter("movement_sequence")
    public int getMovement_sequence() {
        return this.movement_sequence; }
    public void setMovement_sequence(int movement_sequence) {
        this.movement_sequence = movement_sequence; }
    int movement_sequence;
    //Todo check from where this is value comes from
    @JsonSetter("order_sequence")
    public int getOrder_sequence() {
        return this.order_sequence; }
    public void setOrder_sequence(int order_sequence) {
        this.order_sequence = order_sequence; }
    int order_sequence;
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("driverLoadId")
    public String getDriverLoadId() {
        return this.driverLoadId; }
    public void setDriverLoadId(String driverLoadId) {
        this.driverLoadId = driverLoadId; }
    String driverLoadId;
    @JsonSetter("driverUnloadId")
    public String getDriverUnloadId() {
        return this.driverUnloadId; }
    public void setDriverUnloadId(String driverUnloadId) {
        this.driverUnloadId = driverUnloadId; }
    String driverUnloadId;
}
