package pojoClasses.requestPojo.DispatchMicroservice.Assignment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssignmentDetails {
    @JsonSetter("movementID")
    public String getMovementID() {
        return this.movementID; }
    public void setMovementID(String movementID) {
        this.movementID = movementID; }
    String movementID;
    @JsonSetter("driver1ID")
    public String getDriver1ID() {
        return this.driver1ID; }
    public void setDriver1ID(String driver1ID) {
        this.driver1ID = driver1ID; }
    String driver1ID;
    @JsonSetter("driver2ID")
    public String getDriver2ID() {
        return this.driver2ID; }
    public void setDriver2ID(String driver2ID) {
        this.driver2ID = driver2ID; }
    String driver2ID;
    @JsonSetter("tractorID")
    public String getTractorID() {
        return this.tractorID; }
    public void setTractorID(String tractorID) {
        this.tractorID = tractorID; }
    String tractorID;
    @JsonSetter("trailer1ID")
    public String getTrailer1ID() {
        return this.trailer1ID; }
    public void setTrailer1ID(String trailer1ID) {
        this.trailer1ID = trailer1ID; }
    String trailer1ID;
    @JsonSetter("trailer3ID")
    public String getTrailer3ID() {
        return this.trailer3ID; }
    public void setTrailer3ID(String trailer3ID) {
        this.trailer3ID = trailer3ID; }
    String trailer3ID;
    @JsonSetter("overrideExistingPreassingment")
    public boolean getOverrideExistingPreassingment() {
        return this.overrideExistingPreassingment; }
    public void setOverrideExistingPreassingment(boolean overrideExistingPreassingment) {
        this.overrideExistingPreassingment = overrideExistingPreassingment; }
    boolean overrideExistingPreassingment;
    @JsonSetter("overrideLinkedMoves")
    public boolean getOverrideLinkedMoves() {
        return this.overrideLinkedMoves; }
    public void setOverrideLinkedMoves(boolean overrideLinkedMoves) {
        this.overrideLinkedMoves = overrideLinkedMoves; }
    boolean overrideLinkedMoves;
}
