package pojoClasses.requestPojo.UpdateOrderMicroservice.UpdateOrder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import pojoClasses.commonPojo.*;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {
    @JsonSetter("id")
    public String getId() {
        return this.id; }
    public void setId(String id) {
        this.id = id; }
    String id;
    @JsonSetter("companyId")
    public String getCompanyId() {
        return this.companyId; }
    public void setCompanyId(String companyId) {
        this.companyId = companyId; }
    String companyId;
    @JsonSetter("customerId")
    public String getCustomerId() {
        return this.customerId; }
    public void setCustomerId(String customerId) {
        this.customerId = customerId; }
    String customerId;
    @JsonSetter("customerName")
    public String getCustomerName() {
        return this.customerName; }
    public void setCustomerName(String customerName) {
        this.customerName = customerName; }
    String customerName;
    @JsonSetter("customerLocation")
    public Object getCustomerLocation() {
        return this.customerLocation; }
    public void setCustomerLocation(Object customerLocation) {
        this.customerLocation = customerLocation; }
    Object customerLocation;
    @JsonSetter("customerLocationAddress")
    public String getCustomerLocationAddress() {
        return this.customerLocationAddress; }
    public void setCustomerLocationAddress(String customerLocationAddress) {
        this.customerLocationAddress = customerLocationAddress; }
    String customerLocationAddress;
    @JsonSetter("customerLocationState")
    public String getCustomerLocationState() {
        return this.customerLocationState; }
    public void setCustomerLocationState(String customerLocationState) {
        this.customerLocationState = customerLocationState; }
    String customerLocationState;
    @JsonSetter("customerLocationCity")
    public String getCustomerLocationCity() {
        return this.customerLocationCity; }
    public void setCustomerLocationCity(String customerLocationCity) {
        this.customerLocationCity = customerLocationCity; }
    String customerLocationCity;
    @JsonSetter("customerLocationZip")
    public String getCustomerLocationZip() {
        return this.customerLocationZip; }
    public void setCustomerLocationZip(String customerLocationZip) {
        this.customerLocationZip = customerLocationZip; }
    String customerLocationZip;
    @JsonSetter("agentPortalUserId")
    public String getAgentPortalUserId() {
        return this.agentPortalUserId; }
    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId; }
    String agentPortalUserId;
    @JsonSetter("mcleodUserId")
    public String getMcleodUserId() {
        return this.mcleodUserId; }
    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId; }
    String mcleodUserId;
    @JsonSetter("agencyId")
    public String getAgencyId() {
        return this.agencyId; }
    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId; }
    String agencyId;
    @JsonSetter("agencyPayeeId")
    public String getAgencyPayeeId() {
        return this.agencyPayeeId; }
    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId; }
    String agencyPayeeId;
    @JsonSetter("equipmentTypeId")
    public String getEquipmentTypeId() {
        return this.equipmentTypeId; }
    public void setEquipmentTypeId(String equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId; }
    String equipmentTypeId;
    @JsonSetter("equipmentTypeDescr")
    public String getEquipmentTypeDescr() {
        return this.equipmentTypeDescr; }
    public void setEquipmentTypeDescr(String equipmentTypeDescr) {
        this.equipmentTypeDescr = equipmentTypeDescr; }
    String equipmentTypeDescr;
    @JsonSetter("orderValue")
    public Integer getOrderValue() {
        return this.orderValue; }
    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue; }
    Integer orderValue;
    @JsonSetter("highValue")
    public boolean getHighValue() {
        return this.highValue; }
    public void setHighValue(boolean highValue) {
        this.highValue = highValue; }
    boolean highValue;
    @JsonSetter("totalWeight")
    public Integer getTotalWeight() {
        return this.totalWeight; }
    public void setTotalWeight(Integer totalWeight) {
        this.totalWeight = totalWeight; }
    Integer totalWeight;
    @JsonSetter("totalPieces")
    public Integer getTotalPieces() {
        return this.totalPieces; }
    public void setTotalPieces(Integer totalPieces) {
        this.totalPieces = totalPieces; }
    Integer totalPieces;
    @JsonSetter("temperatureMin")
    public Integer getTemperatureMin() {
        return this.temperatureMin; }
    public void setTemperatureMin(Integer temperatureMin) {
        this.temperatureMin = temperatureMin; }
    Integer temperatureMin;
    @JsonSetter("temperatureMax")
    public Integer getTemperatureMax() {
        return this.temperatureMax; }
    public void setTemperatureMax(Integer temperatureMax) {
        this.temperatureMax = temperatureMax; }
    Integer temperatureMax;
    @JsonSetter("temperatureUOM")
    public Object getTemperatureUOM() {
        return this.temperatureUOM; }
    public void setTemperatureUOM(Object temperatureUOM) {
        this.temperatureUOM = temperatureUOM; }
    Object temperatureUOM;
    @JsonSetter("billNumber")
    public String getBillNumber() {
        return this.billNumber; }
    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber; }
    String billNumber;
    @JsonSetter("consigneeRefNumber")
    public String getConsigneeRefNumber() {
        return this.consigneeRefNumber; }
    public void setConsigneeRefNumber(String consigneeRefNumber) {
        this.consigneeRefNumber = consigneeRefNumber; }
    String consigneeRefNumber;
    @JsonSetter("planningComments")
    public String getPlanningComments() {
        return this.planningComments; }
    public void setPlanningComments(String planningComments) {
        this.planningComments = planningComments; }
    String planningComments;
    @JsonSetter("operationsUser")
    public String getOperationsUser() {
        return this.operationsUser; }
    public void setOperationsUser(String operationsUser) {
        this.operationsUser = operationsUser; }
    String operationsUser;
    @JsonSetter("operationsStrUserDate")
    public String getOperationsStrUserDate() {
        return this.operationsStrUserDate; }
    public void setOperationsStrUserDate(String operationsStrUserDate) {
        this.operationsStrUserDate = operationsStrUserDate; }
    String operationsStrUserDate;
    @JsonSetter("enteredStrUserDate")
    public String getEnteredStrUserDate() {
        return this.enteredStrUserDate; }
    public void setEnteredStrUserDate(String enteredStrUserDate) {
        this.enteredStrUserDate = enteredStrUserDate; }
    String enteredStrUserDate;
    @JsonSetter("operationsUserId")
    public String getOperationsUserId() {
        return this.operationsUserId; }
    public void setOperationsUserId(String operationsUserId) {
        this.operationsUserId = operationsUserId; }
    String operationsUserId;
    @JsonSetter("enteredUserId")
    public String getEnteredUserId() {
        return this.enteredUserId; }
    public void setEnteredUserId(String enteredUserId) {
        this.enteredUserId = enteredUserId; }
    String enteredUserId;
    @JsonSetter("enteredUserDate")
    public String getEnteredUserDate() {
        return this.enteredUserDate; }
    public void setEnteredUserDate(String enteredUserDate) {
        this.enteredUserDate = enteredUserDate; }
    String enteredUserDate;
    @JsonSetter("movementId")
    public String getMovementId() {
        return this.movementId; }
    public void setMovementId(String movementId) {
        this.movementId = movementId; }
    String movementId;
    @JsonSetter("commodities")
    public Commodities getCommodities() {
        return this.commodities; }
    public void setCommodities(Commodities commodities) {
        this.commodities = commodities; }
    Commodities commodities;
    @JsonSetter("stops")
    public ArrayList<Stops> getStops() {
        return this.stops; }
    public void setStops(ArrayList<Stops> stops) {
        this.stops = stops; }
    ArrayList<Stops> stops;
    @JsonSetter("deleteStops")
    public ArrayList<Object> getDeleteStops() {
        return this.deleteStops; }
    public void setDeleteStops(ArrayList<Object> deleteStops) {
        this.deleteStops = deleteStops; }
    ArrayList<Object> deleteStops;
    @JsonSetter("orderDetailsHistory")
    public OrderDetailsHistory getOrderDetailsHistory() {
        return this.orderDetailsHistory; }
    public void setOrderDetailsHistory(OrderDetailsHistory orderDetailsHistory) {
        this.orderDetailsHistory = orderDetailsHistory; }
    OrderDetailsHistory orderDetailsHistory;
    @JsonSetter("orderType")
    public String getOrderType() {
        return this.orderType; }
    public void setOrderType(String orderType) {
        this.orderType = orderType; }
    String orderType;
    @JsonSetter("billingMethod")
    public String getBillingMethod() {
        return this.billingMethod; }
    public void setBillingMethod(String billingMethod) {
        this.billingMethod = billingMethod; }
    String billingMethod;
    @JsonSetter("operationsUserDate")
    public String getOperationsUserDate() {
        return this.operationsUserDate; }
    public void setOperationsUserDate(String operationsUserDate) {
        this.operationsUserDate = operationsUserDate; }
    String operationsUserDate;
    @JsonSetter("movements")
    public ArrayList<Movements> getMovements() {
        return this.movements; }
    public void setMovements(ArrayList<Movements> movements) {
        this.movements = movements; }
    ArrayList<Movements> movements;
    @JsonSetter("additionalEquipments")
    public ArrayList<AdditionalEquipment> getAdditionalEquipments() {
        return this.additionalEquipments; }
    public void setAdditionalEquipments(ArrayList<AdditionalEquipment> additionalEquipments) {
        this.additionalEquipments = additionalEquipments; }
    ArrayList<AdditionalEquipment> additionalEquipments;
    @JsonSetter("deletedAdditionalEquips")
    public ArrayList<Object> getDeletedAdditionalEquips() {
        return this.deletedAdditionalEquips; }
    public void setDeletedAdditionalEquips(ArrayList<Object> deletedAdditionalEquips) {
        this.deletedAdditionalEquips = deletedAdditionalEquips; }
    ArrayList<Object> deletedAdditionalEquips;
}
