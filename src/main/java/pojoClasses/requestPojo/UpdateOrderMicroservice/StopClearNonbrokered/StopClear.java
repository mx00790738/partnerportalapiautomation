package pojoClasses.requestPojo.UpdateOrderMicroservice.StopClearNonbrokered;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StopClear {
    @JsonSetter("stopId")
    public String getStopId() {
        return this.stopId; }
    public void setStopId(String stopId) {
        this.stopId = stopId; }
    String stopId;
    @JsonSetter("arrivalDate")
    public String getArrivalDate() {
        return this.arrivalDate; }
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate; }
    String arrivalDate;
    @JsonSetter("departureDate")
    public String getDepartureDate() {
        return this.departureDate; }
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate; }
    String departureDate;
    @JsonSetter("delayCode")
    public String getDelayCode() {
        return this.delayCode; }
    public void setDelayCode(String delayCode) {
        this.delayCode = delayCode; }
    String delayCode;
    @JsonSetter("delayComments")
    public String getDelayComments() {
        return this.delayComments; }
    public void setDelayComments(String delayComments) {
        this.delayComments = delayComments; }
    String delayComments;
    @JsonSetter("hub")
    public int getHub() {
        return this.hub; }
    public void setHub(int hub) {
        this.hub = hub; }
    int hub;
}
