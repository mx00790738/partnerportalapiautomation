package pojoClasses.requestPojo.NeedRevisit;

import java.util.ArrayList;
import java.util.List;

public class Order {
	private String agencyResponsiblityCode;
	private String agentCaptiveOrder;//t100
	private String userType;
	private String division;//t100
	private Object directCustomerInd;
	private String billingMethod;
	private Object segAllocCode;
	private String companyId;
	private String agencyId;
	private String agencyPayeeId;
	private String customerId;
	private String customerName;
	private Object customerLocation;
	private String customerLocationAddress;
	private String customerLocationState;
	private String customerLocationCity;
	private String customerLocationZip;
	private String agentPortalUserId;
	private String mcleodUserId;
	private String equipmentTypeId;
	private String orderValue;
	private Boolean highValue;
	private String totalWeight;
	private String totalPieces;
	private String temperatureMin;
	private String temperatureMax;
	private String temperatureUOM;
	private String billNumber;
	private String consigneeRefNumber;
	private String planningComments;
	private String operationsUser;
	private String operationsUserId;
	private String enteredUserId;
	private String enteredUserDate;
	private String enteredStrUserDate;
	private String operationsStrUserDate;
	private String operationsUserDate;
	private Integer noOfLoads;
	private String revenueCode;
	private Commodities commodities;
	private List<Stop> stops = new ArrayList<Stop>();
	private String orderType;
	private String orderMode;
	private String orderTypeId;
	//private Integer rate;
	//private String rateType;
	//private Integer rateUnit;
	//private String rateCurrencyType;
	//private Integer lineHaulCharges;
	//private List<OtherCharge> otherCharges = new ArrayList<OtherCharge>();
	//private Integer otherChargeTotal;
	//private Integer totalCharge;
	//private List<CarrierOtherPay> carrierOtherPay;
	//private List<Movement> movements;
	private List<AdditionalEquipment> additionalEquipments = new ArrayList<AdditionalEquipment>();
	private String agentOrderReRatingStatus;

	public String getAgencyResponsiblityCode() {
		return agencyResponsiblityCode;
	}

	public void setAgencyResponsiblityCode(String agencyResponsiblityCode) {
		this.agencyResponsiblityCode = agencyResponsiblityCode;
	}

	public String getAgentCaptiveOrder() {
		return agentCaptiveOrder;
	}

	public void setAgentCaptiveOrder(String agentCaptiveOrder) {
		this.agentCaptiveOrder = agentCaptiveOrder;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Object getDirectCustomerInd() {
		return directCustomerInd;
	}

	public void setDirectCustomerInd(Object directCustomerInd) {
		this.directCustomerInd = directCustomerInd;
	}

	public String getBillingMethod() {
		return billingMethod;
	}

	public void setBillingMethod(String billingMethod) {
		this.billingMethod = billingMethod;
	}

	public Object getSegAllocCode() {
		return segAllocCode;
	}

	public void setSegAllocCode(Object segAllocCode) {
		this.segAllocCode = segAllocCode;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getAgencyPayeeId() {
		return agencyPayeeId;
	}

	public void setAgencyPayeeId(String agencyPayeeId) {
		this.agencyPayeeId = agencyPayeeId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Object getCustomerLocation() {
		return customerLocation;
	}

	public void setCustomerLocation(Object customerLocation) {
		this.customerLocation = customerLocation;
	}

	public String getCustomerLocationAddress() {
		return customerLocationAddress;
	}

	public void setCustomerLocationAddress(String customerLocationAddress) {
		this.customerLocationAddress = customerLocationAddress;
	}

	public String getCustomerLocationState() {
		return customerLocationState;
	}

	public void setCustomerLocationState(String customerLocationState) {
		this.customerLocationState = customerLocationState;
	}

	public String getCustomerLocationCity() {
		return customerLocationCity;
	}

	public void setCustomerLocationCity(String customerLocationCity) {
		this.customerLocationCity = customerLocationCity;
	}

	public String getCustomerLocationZip() {
		return customerLocationZip;
	}

	public void setCustomerLocationZip(String customerLocationZip) {
		this.customerLocationZip = customerLocationZip;
	}

	public String getAgentPortalUserId() {
		return agentPortalUserId;
	}

	public void setAgentPortalUserId(String agentPortalUserId) {
		this.agentPortalUserId = agentPortalUserId;
	}

	public String getMcleodUserId() {
		return mcleodUserId;
	}

	public void setMcleodUserId(String mcleodUserId) {
		this.mcleodUserId = mcleodUserId;
	}

	public String getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(String equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}

	public String getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(String orderValue) {
		this.orderValue = orderValue;
	}

	public Boolean getHighValue() {
		return highValue;
	}

	public void setHighValue(Boolean highValue) {
		this.highValue = highValue;
	}

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getTotalPieces() {
		return totalPieces;
	}

	public void setTotalPieces(String totalPieces) {
		this.totalPieces = totalPieces;
	}

	public String getTemperatureMin() {
		return temperatureMin;
	}

	public void setTemperatureMin(String temperatureMin) {
		this.temperatureMin = temperatureMin;
	}

	public String getTemperatureMax() {
		return temperatureMax;
	}

	public void setTemperatureMax(String temperatureMax) {
		this.temperatureMax = temperatureMax;
	}

	public String getTemperatureUOM() {
		return temperatureUOM;
	}

	public void setTemperatureUOM(String temperatureUOM) {
		this.temperatureUOM = temperatureUOM;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getConsigneeRefNumber() {
		return consigneeRefNumber;
	}

	public void setConsigneeRefNumber(String consigneeRefNumber) {
		this.consigneeRefNumber = consigneeRefNumber;
	}

	public String getPlanningComments() {
		return planningComments;
	}

	public void setPlanningComments(String planningComments) {
		this.planningComments = planningComments;
	}

	public String getOperationsUser() {
		return operationsUser;
	}

	public void setOperationsUser(String operationsUser) {
		this.operationsUser = operationsUser;
	}

	public String getOperationsUserId() {
		return operationsUserId;
	}

	public void setOperationsUserId(String operationsUserId) {
		this.operationsUserId = operationsUserId;
	}

	public String getEnteredUserId() {
		return enteredUserId;
	}

	public void setEnteredUserId(String enteredUserId) {
		this.enteredUserId = enteredUserId;
	}

	public String getEnteredUserDate() {
		return enteredUserDate;
	}

	public void setEnteredUserDate(String enteredUserDate) {
		this.enteredUserDate = enteredUserDate;
	}

	public String getEnteredStrUserDate() {
		return enteredStrUserDate;
	}

	public void setEnteredStrUserDate(String enteredStrUserDate) {
		this.enteredStrUserDate = enteredStrUserDate;
	}

	public String getOperationsStrUserDate() {
		return operationsStrUserDate;
	}

	public void setOperationsStrUserDate(String operationsStrUserDate) {
		this.operationsStrUserDate = operationsStrUserDate;
	}

	public String getOperationsUserDate() {
		return operationsUserDate;
	}

	public void setOperationsUserDate(String operationsUserDate) {
		this.operationsUserDate = operationsUserDate;
	}

	public Integer getNoOfLoads() {
		return noOfLoads;
	}

	public void setNoOfLoads(Integer noOfLoads) {
		this.noOfLoads = noOfLoads;
	}

	public String getRevenueCode() {
		return revenueCode;
	}

	public void setRevenueCode(String revenueCode) {
		this.revenueCode = revenueCode;
	}

	public Commodities getCommodities() {
		return commodities;
	}

	public void setCommodities(Commodities commodities) {
		this.commodities = commodities;
	}

	public List<Stop> getStops() {
		return stops;
	}

	public void setStops(List<Stop> stops) {
		this.stops = stops;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(String orderMode) {
		this.orderMode = orderMode;
	}

	public String getOrderTypeId() {
		return orderTypeId;
	}

	public void setOrderTypeId(String orderTypeId) {
		this.orderTypeId = orderTypeId;
	}
/*
	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public Integer getRateUnit() {
		return rateUnit;
	}

	public void setRateUnit(Integer rateUnit) {
		this.rateUnit = rateUnit;
	}

	public String getRateCurrencyType() {
		return rateCurrencyType;
	}

	public void setRateCurrencyType(String rateCurrencyType) {
		this.rateCurrencyType = rateCurrencyType;
	}

	public Integer getLineHaulCharges() {
		return lineHaulCharges;
	}

	public void setLineHaulCharges(Integer lineHaulCharges) {
		this.lineHaulCharges = lineHaulCharges;
	}

	public List<OtherCharge> getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(List<OtherCharge> otherCharges) {
		this.otherCharges = otherCharges;
	}

	public Integer getOtherChargeTotal() {
		return otherChargeTotal;
	}

	public void setOtherChargeTotal(Integer otherChargeTotal) {
		this.otherChargeTotal = otherChargeTotal;
	}

	public Integer getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Integer totalCharge) {
		this.totalCharge = totalCharge;
	}

	public List<CarrierOtherPay> getCarrierOtherPay() {
		return carrierOtherPay;
	}

	public void setCarrierOtherPay(List<CarrierOtherPay> carrierOtherPay) {
		this.carrierOtherPay = carrierOtherPay;
	}

	public List<Movement> getMovements() {
		return movements;
	}

	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}
*/
	public List<AdditionalEquipment> getAdditionalEquipments() {
		return additionalEquipments;
	}

	public void setAdditionalEquipments(List<AdditionalEquipment> additionalEquipments) {
		this.additionalEquipments = additionalEquipments;
	}

	public String getAgentOrderReRatingStatus() {
		return agentOrderReRatingStatus;
	}

	public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
		this.agentOrderReRatingStatus = agentOrderReRatingStatus;
	}
}
