package pojoClasses.requestPojo.NeedRevisit;

public class Reference {
	private String referenceNumber;
	private String referenceType;
	private String weight;
	private String pieces;
	private Boolean sendToDriver;
	private String partnerId;

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getPieces() {
		return pieces;
	}

	public void setPieces(String pieces) {
		this.pieces = pieces;
	}

	public Boolean getSendToDriver() {
		return sendToDriver;
	}

	public void setSendToDriver(Boolean sendToDriver) {
		this.sendToDriver = sendToDriver;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
}
