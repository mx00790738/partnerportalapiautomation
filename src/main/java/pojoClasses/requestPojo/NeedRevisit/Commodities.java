package pojoClasses.requestPojo.NeedRevisit;

import java.util.ArrayList;
import java.util.List;

public class Commodities {
	private List<Commodity> commodity = new ArrayList<Commodity>();
	
    private List<ServiceEntry> services = new ArrayList<ServiceEntry>();
    
    public List<Commodity> getCommodity() {
        return commodity;
    }
    public void setCommodity(List<Commodity> commodity) {
        this.commodity = commodity;
    }
    public List<ServiceEntry> getServices() {
        return services;
    }
    public void setServices(List<ServiceEntry> services) {
        this.services = services;
    }
}
