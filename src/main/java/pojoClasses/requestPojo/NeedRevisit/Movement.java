package pojoClasses.requestPojo.NeedRevisit;

public class Movement {
	private Integer carrierRate;
    private Integer carrierUnit;
    private Integer carrierAmt;
    private String carrierPayType;
    private Boolean brokerage;
    private String overridePayeeId;
    private Boolean saveCompleteCarrierRating;
    private String capacityUserId;
    private String capacityUserStrDate;
    private String dispatcherUserStrDate;
    private String capacityUserDate;
    private String dispatcherUserDate;
    private Boolean ediStatusFlag;
    private String brokerageLoadStatus;
    public Integer getCarrierRate() {
        return carrierRate;
    }
    public void setCarrierRate(Integer carrierRate) {
        this.carrierRate = carrierRate;
    }
    public Integer getCarrierUnit() {
        return carrierUnit;
    }
    public void setCarrierUnit(Integer carrierUnit) {
        this.carrierUnit = carrierUnit;
    }
    public Integer getCarrierAmt() {
        return carrierAmt;
    }
    public void setCarrierAmt(Integer carrierAmt) {
        this.carrierAmt = carrierAmt;
    }
    public String getCarrierPayType() {
        return carrierPayType;
    }
    public void setCarrierPayType(String carrierPayType) {
        this.carrierPayType = carrierPayType;
    }
    public Boolean getBrokerage() {
        return brokerage;
    }
    public void setBrokerage(Boolean brokerage) {
        this.brokerage = brokerage;
    }
    public String getOverridePayeeId() {
        return overridePayeeId;
    }
    public void setOverridePayeeId(String overridePayeeId) {
        this.overridePayeeId = overridePayeeId;
    }
    public Boolean getSaveCompleteCarrierRating() {
        return saveCompleteCarrierRating;
    }
    public void setSaveCompleteCarrierRating(Boolean saveCompleteCarrierRating) {
        this.saveCompleteCarrierRating = saveCompleteCarrierRating;
    }
    public String getCapacityUserId() {
        return capacityUserId;
    }
    public void setCapacityUserId(String capacityUserId) {
        this.capacityUserId = capacityUserId;
    }
    public String getCapacityUserStrDate() {
        return capacityUserStrDate;
    }
    public void setCapacityUserStrDate(String capacityUserStrDate) {
        this.capacityUserStrDate = capacityUserStrDate;
    }
    public String getDispatcherUserStrDate() {
        return dispatcherUserStrDate;
    }
    public void setDispatcherUserStrDate(String dispatcherUserStrDate) {
        this.dispatcherUserStrDate = dispatcherUserStrDate;
    }
    public String getCapacityUserDate() {
        return capacityUserDate;
    }
    public void setCapacityUserDate(String capacityUserDate) {
        this.capacityUserDate = capacityUserDate;
    }
    public String getDispatcherUserDate() {
        return dispatcherUserDate;
    }
    public void setDispatcherUserDate(String dispatcherUserDate) {
        this.dispatcherUserDate = dispatcherUserDate;
    }
    public Boolean getEdiStatusFlag() {
        return ediStatusFlag;
    }
    public void setEdiStatusFlag(Boolean ediStatusFlag) {
        this.ediStatusFlag = ediStatusFlag;
    }
    public String getBrokerageLoadStatus() {
        return brokerageLoadStatus;
    }
    public void setBrokerageLoadStatus(String brokerageLoadStatus) {
        this.brokerageLoadStatus = brokerageLoadStatus;
    }
}
