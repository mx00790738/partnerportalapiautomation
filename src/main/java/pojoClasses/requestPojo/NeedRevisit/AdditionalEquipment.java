package pojoClasses.requestPojo.NeedRevisit;

public class AdditionalEquipment {
	private String companyId;
    private String appliesTo;
    private String equipReq;
    private String equipTypeId;
    private String active;
    private String shipperConsignee;
    private String quantity;
    private Object zmit;
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getAppliesTo() {
        return appliesTo;
    }
    public void setAppliesTo(String appliesTo) {
        this.appliesTo = appliesTo;
    }
    public String getEquipReq() {
        return equipReq;
    }
    public void setEquipReq(String equipReq) {
        this.equipReq = equipReq;
    }
    public String getEquipTypeId() {
        return equipTypeId;
    }
    public void setEquipTypeId(String equipTypeId) {
        this.equipTypeId = equipTypeId;
    }
    public String getActive() {
        return active;
    }
    public void setActive(String active) {
        this.active = active;
    }
    public String getShipperConsignee() {
        return shipperConsignee;
    }
    public void setShipperConsignee(String shipperConsignee) {
        this.shipperConsignee = shipperConsignee;
    }
    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public Object getZmit() {
        return zmit;
    }
    public void setZmit(Object zmit) {
        this.zmit = zmit;
    }
}
