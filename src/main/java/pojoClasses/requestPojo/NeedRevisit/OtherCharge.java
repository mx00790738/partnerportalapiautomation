package pojoClasses.requestPojo.NeedRevisit;

public class OtherCharge {
	 private String charge_id;
	    private Integer rate;
	    private String rateType;
	    private String description;
	    private Integer amount;
	    private Integer units;
	    private String id;
	    private Object customer;
	    private Object customer_id;
	    private String calc_method_descr;
	    private String sequence;
	    public String getCharge_id() {
	        return charge_id;
	    }
	    public void setCharge_id(String chargeId) {
	        this.charge_id = chargeId;
	    }
	    public Integer getRate() {
	        return rate;
	    }
	    public void setRate(Integer rate) {
	        this.rate = rate;
	    }
	    public String getRateType() {
	        return rateType;
	    }
	    public void setRateType(String rateType) {
	        this.rateType = rateType;
	    }
	    public String getDescription() {
	        return description;
	    }
	    public void setDescription(String description) {
	        this.description = description;
	    }
	    public Integer getAmount() {
	        return amount;
	    }
	    public void setAmount(Integer amount) {
	        this.amount = amount;
	    }
	    public Integer getUnits() {
	        return units;
	    }
	    public void setUnits(Integer units) {
	        this.units = units;
	    }
	    public String getId() {
	        return id;
	    }
	    public void setId(String id) {
	        this.id = id;
	    }
	    public Object getCustomer() {
	        return customer;
	    }
	    public void setCustomer(Object customer) {
	        this.customer = customer;
	    }
	    public Object getCustomer_id() {
	        return customer_id;
	    }
	    public void setCustomerId(Object customer_id) {
	        this.customer_id = customer_id;
	    }
	    public String getCalc_method_descr() {
	        return calc_method_descr;
	    }
	    public void setCalc_Method_Descr(String calc_method_descr) {
	        this.calc_method_descr = calc_method_descr;
	    }
	    public String getSequence() {
	        return sequence;
	    }
	    public void setSequence(String sequence) {
	        this.sequence = sequence;
	    }
}
