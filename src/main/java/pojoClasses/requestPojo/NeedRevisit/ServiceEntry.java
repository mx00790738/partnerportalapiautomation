package pojoClasses.requestPojo.NeedRevisit;

public class ServiceEntry {
	private Boolean delete;
	private Service service;

	public Boolean getDelete() {
		return delete;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}
