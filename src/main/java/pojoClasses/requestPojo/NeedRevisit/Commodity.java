package pojoClasses.requestPojo.NeedRevisit;

public class Commodity {
	private String id;
    private String description;
    private String freightClass;
    private String quantity;
    private String packaging;
    private String length;
    private String width;
    private String height;
    private String density;
    private String weight;
    private String weightUOM;
    private String handlingUnits;
    private String reqSpots;
    private String value;
    private String nmfcCode;
    private Boolean hazmat;
    private String hazmatPackageGroup;
    private String hazmatClassCode;
    private String hazmatUNNbr;
    private String hazmatShipname;
    private String hazmatContactName;
    private String hazmatContactNumber;
    private String hazmatDescription;
    private String linearUnits;
    private String cubicUnits;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getFreightClass() {
        return freightClass;
    }
    public void setFreightClass(String freightClass) {
        this.freightClass = freightClass;
    }
    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public String getPackaging() {
        return packaging;
    }
    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }
    public String getLength() {
        return length;
    }
    public void setLength(String length) {
        this.length = length;
    }
    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getHeight() {
        return height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    public String getDensity() {
        return density;
    }
    public void setDensity(String density) {
        this.density = density;
    }
    public String getWeight() {
        return weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getWeightUOM() {
        return weightUOM;
    }
    public void setWeightUOM(String weightUOM) {
        this.weightUOM = weightUOM;
    }
    public String getHandlingUnits() {
        return handlingUnits;
    }
    public void setHandlingUnits(String handlingUnits) {
        this.handlingUnits = handlingUnits;
    }
    public String getReqSpots() {
        return reqSpots;
    }
    public void setReqSpots(String reqSpots) {
        this.reqSpots = reqSpots;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getNmfcCode() {
        return nmfcCode;
    }
    public void setNmfcCode(String nmfcCode) {
        this.nmfcCode = nmfcCode;
    }
    public Boolean getHazmat() {
        return hazmat;
    }
    public void setHazmat(Boolean hazmat) {
        this.hazmat = hazmat;
    }
    public String getHazmatPackageGroup() {
        return hazmatPackageGroup;
    }
    public void setHazmatPackageGroup(String hazmatPackageGroup) {
        this.hazmatPackageGroup = hazmatPackageGroup;
    }
    public String getHazmatClassCode() {
        return hazmatClassCode;
    }
    public void setHazmatClassCode(String hazmatClassCode) {
        this.hazmatClassCode = hazmatClassCode;
    }
    public String getHazmatUNNbr() {
        return hazmatUNNbr;
    }
    public void setHazmatUNNbr(String hazmatUNNbr) {
        this.hazmatUNNbr = hazmatUNNbr;
    }
    public String getHazmatShipname() {
        return hazmatShipname;
    }
    public void setHazmatShipname(String hazmatShipname) {
        this.hazmatShipname = hazmatShipname;
    }
    public String getHazmatContactName() {
        return hazmatContactName;
    }
    public void setHazmatContactName(String hazmatContactName) {
        this.hazmatContactName = hazmatContactName;
    }
    public String getHazmatContactNumber() {
        return hazmatContactNumber;
    }
    public void setHazmatContactNumber(String hazmatContactNumber) {
        this.hazmatContactNumber = hazmatContactNumber;
    }
    public String getHazmatDescription() {
        return hazmatDescription;
    }
    public void setHazmatDescription(String hazmatDescription) {
        this.hazmatDescription = hazmatDescription;
    }
    public String getLinearUnits() {
        return linearUnits;
    }
    public void setLinearUnits(String linearUnits) {
        this.linearUnits = linearUnits;
    }
    public String getCubicUnits() {
        return cubicUnits;
    }
    public void setCubicUnits(String cubicUnits) {
        this.cubicUnits = cubicUnits;
    }
}
