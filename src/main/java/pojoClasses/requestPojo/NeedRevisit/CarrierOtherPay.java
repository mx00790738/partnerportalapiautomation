package pojoClasses.requestPojo.NeedRevisit;

public class CarrierOtherPay {
	private String deductCodeId;
    private Integer rate;
    private String sequence;
    private Integer amount;
    private String description;
    private String payeeId;
    private Integer units;
    private String otherPayId;
    private String memo;
    private String transactionDate;
    
    public String getDeductCodeId() {
        return deductCodeId;
    }
    public void setDeductCodeId(String deductCodeId) {
        this.deductCodeId = deductCodeId;
    }
    public Integer getRate() {
        return rate;
    }
    public void setRate(Integer rate) {
        this.rate = rate;
    }
    public String getSequence() {
        return sequence;
    }
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    public Integer getAmount() {
        return amount;
    }
    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPayeeId() {
        return payeeId;
    }
    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }
    public Integer getUnits() {
        return units;
    }
    public void setUnits(Integer units) {
        this.units = units;
    }
    public String getOtherPayId() {
        return otherPayId;
    }
    public void setOtherPayId(String otherPayId) {
        this.otherPayId = otherPayId;
    }
    public String getMemo() {
        return memo;
    }
    public void setMemo(String memo) {
        this.memo = memo;
    }
    public String getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
