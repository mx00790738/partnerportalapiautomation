package pojoClasses.requestPojo.NeedRevisit;

import java.util.ArrayList;
import java.util.List;

public class Opportunity {
	private String agencyResponsiblityCode;
    private String userType;
    private Object directCustomerInd;
    private Object segAllocCode;
    private String companyId;
    private String agencyId;
    private String agencyPayeeId;
    private String subjectOrderStatus;
    private String subjectOrderVoidDate;
    private String customerId;
    private String customerName;
    private Object customerLocation;
    private String customerLocationAddress;
    private String customerLocationState;
    private String customerLocationCity;
    private String customerLocationZip;
    private String agentPortalUserId;
    private String mcleodUserId;
    private String equipmentTypeId;
    private String orderValue;
    private String planningComments;
    private String operationsUser;
    private String operationsUserId;
    private String enteredUserId;
    private String enteredUserDate;
    private String enteredStrUserDate;
    private String operationsStrUserDate;
    private String operationsUserDate;
    private String revenueCode;
    private Commodities commodities;
    private List<Stop> stops = new ArrayList<Stop>();
    private String orderType;
    private String orderMode;
    private String orderTypeId;
    private String billingMethod;
    private String rateType;
    private Integer rateUnit;
    private Integer rate;
    private Integer lineHaulCharges;
    private String agentOrderReRatingStatus;
    public String getAgencyResponsiblityCode() {
        return agencyResponsiblityCode;
    }
    public void setAgencyResponsiblityCode(String agencyResponsiblityCode) {
        this.agencyResponsiblityCode = agencyResponsiblityCode;
    }
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    public Object getDirectCustomerInd() {
        return directCustomerInd;
    }
    public void setDirectCustomerInd(Object directCustomerInd) {
        this.directCustomerInd = directCustomerInd;
    }
    public Object getSegAllocCode() {
        return segAllocCode;
    }
    public void setSegAllocCode(Object segAllocCode) {
        this.segAllocCode = segAllocCode;
    }
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getAgencyId() {
        return agencyId;
    }
    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }
    public String getAgencyPayeeId() {
        return agencyPayeeId;
    }
    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId;
    }
    public String getSubjectOrderStatus() {
        return subjectOrderStatus;
    }
    public void setSubjectOrderStatus(String subjectOrderStatus) {
        this.subjectOrderStatus = subjectOrderStatus;
    }
    public String getSubjectOrderVoidDate() {
        return subjectOrderVoidDate;
    }
    public void setSubjectOrderVoidDate(String subjectOrderVoidDate) {
        this.subjectOrderVoidDate = subjectOrderVoidDate;
    }
    public String getCustomerId() {
        return customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public Object getCustomerLocation() {
        return customerLocation;
    }
    public void setCustomerLocation(Object customerLocation) {
        this.customerLocation = customerLocation;
    }
    public String getCustomerLocationAddress() {
        return customerLocationAddress;
    }
    public void setCustomerLocationAddress(String customerLocationAddress) {
        this.customerLocationAddress = customerLocationAddress;
    }
    public String getCustomerLocationState() {
        return customerLocationState;
    }
    public void setCustomerLocationState(String customerLocationState) {
        this.customerLocationState = customerLocationState;
    }
    public String getCustomerLocationCity() {
        return customerLocationCity;
    }
    public void setCustomerLocationCity(String customerLocationCity) {
        this.customerLocationCity = customerLocationCity;
    }
    public String getCustomerLocationZip() {
        return customerLocationZip;
    }
    public void setCustomerLocationZip(String customerLocationZip) {
        this.customerLocationZip = customerLocationZip;
    }
    public String getAgentPortalUserId() {
        return agentPortalUserId;
    }
    public void setAgentPortalUserId(String agentPortalUserId) {
        this.agentPortalUserId = agentPortalUserId;
    }
    public String getMcleodUserId() {
        return mcleodUserId;
    }
    public void setMcleodUserId(String mcleodUserId) {
        this.mcleodUserId = mcleodUserId;
    }
    public String getEquipmentTypeId() {
        return equipmentTypeId;
    }
    public void setEquipmentTypeId(String equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }
    public String getOrderValue() {
        return orderValue;
    }
    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }
    public String getPlanningComments() {
        return planningComments;
    }
    public void setPlanningComments(String planningComments) {
        this.planningComments = planningComments;
    }
    public String getOperationsUser() {
        return operationsUser;
    }
    public void setOperationsUser(String operationsUser) {
        this.operationsUser = operationsUser;
    }
    public String getOperationsUserId() {
        return operationsUserId;
    }
    public void setOperationsUserId(String operationsUserId) {
        this.operationsUserId = operationsUserId;
    }
    public String getEnteredUserId() {
        return enteredUserId;
    }
    public void setEnteredUserId(String enteredUserId) {
        this.enteredUserId = enteredUserId;
    }
    public String getEnteredUserDate() {
        return enteredUserDate;
    }
    public void setEnteredUserDate(String enteredUserDate) {
        this.enteredUserDate = enteredUserDate;
    }
    public String getEnteredStrUserDate() {
        return enteredStrUserDate;
    }
    public void setEnteredStrUserDate(String enteredStrUserDate) {
        this.enteredStrUserDate = enteredStrUserDate;
    }
    public String getOperationsStrUserDate() {
        return operationsStrUserDate;
    }
    public void setOperationsStrUserDate(String operationsStrUserDate) {
        this.operationsStrUserDate = operationsStrUserDate;
    }
    public String getOperationsUserDate() {
        return operationsUserDate;
    }
    public void setOperationsUserDate(String operationsUserDate) {
        this.operationsUserDate = operationsUserDate;
    }
    public String getRevenueCode() {
        return revenueCode;
    }
    public void setRevenueCode(String revenueCode) {
        this.revenueCode = revenueCode;
    }
    public Commodities getCommodities() {
        return commodities;
    }
    public void setCommodities(Commodities commodities) {
        this.commodities = commodities;
    }
    public List<Stop> getStops() {
        return stops;
    }
    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }
    public String getOrderType() {
        return orderType;
    }
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    public String getOrderMode() {
        return orderMode;
    }
    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode;
    }
    public String getOrderTypeId() {
        return orderTypeId;
    }
    public void setOrderTypeId(String orderTypeId) {
        this.orderTypeId = orderTypeId;
    }
    public String getBillingMethod() {
        return billingMethod;
    }
    public void setBillingMethod(String billingMethod) {
        this.billingMethod = billingMethod;
    }
    public String getRateType() {
        return rateType;
    }
    public void setRateType(String rateType) {
        this.rateType = rateType;
    }
    public Integer getRateUnit() {
        return rateUnit;
    }
    public void setRateUnit(Integer rateUnit) {
        this.rateUnit = rateUnit;
    }
    public Integer getRate() {
        return rate;
    }
    public void setRate(Integer rate) {
        this.rate = rate;
    }
    public Integer getLineHaulCharges() {
        return lineHaulCharges;
    }
    public void setLineHaulCharges(Integer lineHaulCharges) {
        this.lineHaulCharges = lineHaulCharges;
    }
    public String getAgentOrderReRatingStatus() {
        return agentOrderReRatingStatus;
    }
    public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
        this.agentOrderReRatingStatus = agentOrderReRatingStatus;
    }
}
