package pojoClasses.requestPojo.NeedRevisit;

import java.util.ArrayList;
import java.util.List;

public class Stop {
	 private Integer cityId;
	    private String cityName;
	    private String state;
	    private Integer zipCode;
	    private String locationId;
	    private String locationName;
	    private String locationType;
	    private String address1;
	    private Object address2;
	    private String stopType;
	    private List<Comment> comments = new ArrayList<Comment>();
	    private String country;
	    private Object contactName;
	    private Object contactNumber;
	    private String schedDepartDate;
	    private String schedArriveDate;
	    private Boolean apptRequired;
	    private Boolean confirmed;
	    private List<Reference> references = new ArrayList<Reference>();
	    private String driverLoadId;
	    private Object email;
	    private String driverUnloadId;
	    public Integer getCityId() {
	        return cityId;
	    }
	    public void setCityId(Integer cityId) {
	        this.cityId = cityId;
	    }
	    public String getCityName() {
	        return cityName;
	    }
	    public void setCityName(String cityName) {
	        this.cityName = cityName;
	    }
	    public String getState() {
	        return state;
	    }
	    public void setState(String state) {
	        this.state = state;
	    }
	    public Integer getZipCode() {
	        return zipCode;
	    }
	    public void setZipCode(Integer i) {
	        this.zipCode = i;
	    }
	    public String getLocationId() {
	        return locationId;
	    }
	    public void setLocationId(String locationId) {
	        this.locationId = locationId;
	    }
	    public String getLocationName() {
	        return locationName;
	    }
	    public void setLocationName(String locationName) {
	        this.locationName = locationName;
	    }
	    public String getLocationType() {
	        return locationType;
	    }
	    public void setLocationType(String locationType) {
	        this.locationType = locationType;
	    }
	    public String getAddress1() {
	        return address1;
	    }
	    public void setAddress1(String address1) {
	        this.address1 = address1;
	    }
	    public Object getAddress2() {
	        return address2;
	    }
	    public void setAddress2(Object address2) {
	        this.address2 = address2;
	    }
	    public String getStopType() {
	        return stopType;
	    }
	    public void setStopType(String stopType) {
	        this.stopType = stopType;
	    }
	    public List<Comment> getComments() {
	        return comments;
	    }
	    public void setComments(List<Comment> comments) {
	        this.comments = comments;
	    }
	    public String getCountry() {
	        return country;
	    }
	    public void setCountry(String country) {
	        this.country = country;
	    }
	    public Object getContactName() {
	        return contactName;
	    }
	    public void setContactName(Object contactName) {
	        this.contactName = contactName;
	    }
	    public Object getContactNumber() {
	        return contactNumber;
	    }
	    public void setContactNumber(Object contactNumber) {
	        this.contactNumber = contactNumber;
	    }
	    public String getSchedDepartDate() {
	        return schedDepartDate;
	    }
	    public void setSchedDepartDate(String schedDepartDate) {
	        this.schedDepartDate = schedDepartDate;
	    }
	    public String getSchedArriveDate() {
	        return schedArriveDate;
	    }
	    public void setSchedArriveDate(String schedArriveDate) {
	        this.schedArriveDate = schedArriveDate;
	    }
	    public Boolean getApptRequired() {
	        return apptRequired;
	    }
	    public void setApptRequired(Boolean apptRequired) {
	        this.apptRequired = apptRequired;
	    }
	    public Boolean getConfirmed() {
	        return confirmed;
	    }
	    public void setConfirmed(Boolean confirmed) {
	        this.confirmed = confirmed;
	    }
	    public List<Reference> getReferences() {
	        return references;
	    }
	    public void setReferences(List<Reference> references) {
	        this.references = references;
	    }
	    public String getDriverLoadId() {
	        return driverLoadId;
	    }
	    public void setDriverLoadId(String driverLoadId) {
	        this.driverLoadId = driverLoadId;
	    }
	    public Object getEmail() {
	        return email;
	    }
	    public void setEmail(Object email) {
	        this.email = email;
	    }
	    public String getDriverUnloadId() {
	        return driverUnloadId;
	    }
	    public void setDriverUnloadId(String driverUnloadId) {
	        this.driverUnloadId = driverUnloadId;
	    }
}
