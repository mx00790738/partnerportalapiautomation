package pojoClasses.requestPojo.NeedRevisit;

public class Comment {
	private String commentType;
	private String comment;
	private String commentSectionId;
	private Boolean deleteFlagStopNotes;
	private Object id;

	public String getCommentType() {
		return commentType;
	}

	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentSectionId() {
		return commentSectionId;
	}

	public void setCommentSectionId(String commentSectionId) {
		this.commentSectionId = commentSectionId;
	}

	public Boolean getDeleteFlagStopNotes() {
		return deleteFlagStopNotes;
	}

	public void setDeleteFlagStopNotes(Boolean deleteFlagStopNotes) {
		this.deleteFlagStopNotes = deleteFlagStopNotes;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}
}
