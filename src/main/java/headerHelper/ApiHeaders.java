package headerHelper;

import io.restassured.http.Header;
import io.restassured.http.Headers;

public class ApiHeaders {

	public static Headers getLoginRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage, 
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS);
		return headers;		
	}
	
	public static Headers getUserRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage, 
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS);
		return headers;		
	}
	
	public static Headers getCommentTypesRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage, 
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS);
		return headers;		
	}
	
	public static Headers getResponsibilityCodeCheckRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage,
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS);
		return headers;		
	}
	
	public static Headers getServiceFailureReasonListRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage,
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS);
		return headers;		
	}
	
	public static Headers getEquipmentTypeRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Header X_crst_CompanyID = new Header("X-crst-CompanyID","TMS");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage,
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS, X_crst_CompanyID);
		return headers;		
	}

	public static Headers getRequestHeaders() {
		Header Accept = new Header("Accept","application/json, text/plain, */*");
		Header AcceptEncoding = new Header("Accept-Encoding"," gzip, deflate");
		Header AcceptLanguage = new Header("Accept-Language"," en-US,en;q=0.9");
		Header CacheControl = new Header("Cache-Control"," no-cache");
		Header Connection = new Header("Connection"," keep-alive");
		//Header ContentLength = new Header("Content-Length","102");
		Header ContentType = new Header("Content-Type","application/json");
		Header Pragma = new Header("Pragma"," no-cache");
		Header UserAgent = new Header("User-Agent"," Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
		Header X_ACCESS = new Header("X-ACCESS"," N4jqPMj3XvMZwnVAkx6F0KKCwzWR5tBW");
		Header X_crst_CompanyID = new Header("X-crst-CompanyID","T100");
		Headers headers = new Headers(Accept, AcceptEncoding, AcceptLanguage,
				CacheControl, Connection, ContentType,
				Pragma, UserAgent, X_ACCESS, X_crst_CompanyID);
		return headers;
	}
}
