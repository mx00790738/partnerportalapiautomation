package headerHelper;

import static io.restassured.RestAssured.given;

import testConfiguration.PATH;
import testConfiguration.TestConfig;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AuthorizationHeader {
	private static Header authHeader = null;
	
	public static Header getAuthorizationHeader(String userName, String password) throws Exception {
		if (authHeader == null) {
			RequestSpecification requestSpecification = RestAssured.
					given().
						baseUri(TestConfig.get("HOST")).
						basePath(TestConfig.get("LOGIN_NEW"))
					.headers(ApiHeaders.getLoginRequestHeaders());
			String loginBody = "{\"loginId\":\"" + userName + "\",\"password\":\"" + password + "\"}";
			Response response = 
					given().
						spec(requestSpecification).
						body(loginBody).
					when().
					post();
			String bearerToken = response.getHeader("APIAuthorization");
			authHeader = new Header("APIAuthorization", bearerToken);
			return authHeader;
		}
		return authHeader;
	}
}
