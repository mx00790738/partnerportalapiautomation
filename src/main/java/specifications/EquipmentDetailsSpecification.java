package specifications;

import java.util.ArrayList;
import java.util.List;

import pojoClasses.requestPojo.NeedRevisit.AdditionalEquipment;

public class EquipmentDetailsSpecification {
	public EquipmentDetailsSpecification() {
		equipmentTypeId = "F";
		additionalEquipList = new ArrayList<AdditionalEquipment>();//need to add a default equip
		//add OPTIONS.CUSTOM
	}
String equipmentTypeId;
List<AdditionalEquipment> additionalEquipList = new ArrayList<AdditionalEquipment>();

public String getEquipmentTypeId() {
	return equipmentTypeId;
}

public void setEquipmentTypeId(String equipmentTypeId) {
	this.equipmentTypeId = equipmentTypeId;
}

public List<AdditionalEquipment> getAdditionalEquipList() {
	return additionalEquipList;
}

public void addToAdditionalEquipList(AdditionalEquipment additionalEquipment) {
	this.additionalEquipList.add(additionalEquipment);
}
}
