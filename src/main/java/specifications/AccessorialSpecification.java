package specifications;

import java.util.ArrayList;
import java.util.List;

import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.Service;
import pojoClasses.requestPojo.NeedRevisit.ServiceEntry;

public class AccessorialSpecification {
	public AccessorialSpecification(OPTION option) {
		switch (option) {
		case DEFAULT:
			accessorialList = new ArrayList<ServiceEntry>();
			// Default service
			ServiceEntry serviceEntry = new ServiceEntry();
			Service service = new Service();
			service.setId("PL");
			service.setUid(9);
			service.setDescription("Pickup Liftgate");
			service.setTitle("Pickup Liftgate");
			serviceEntry.setDelete(false);
			serviceEntry.setService(service);
			accessorialList.add(serviceEntry);
			break;
		case CUSTOM:
			accessorialList = new ArrayList<ServiceEntry>();
			break;
		}
	}

	private List<ServiceEntry> accessorialList;

	public List<ServiceEntry> getServiceList() {
		return accessorialList;
	}

	public void setServiceList(List<ServiceEntry> accessorialList) {
		this.accessorialList = accessorialList;
	}
}
