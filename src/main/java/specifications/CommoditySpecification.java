package specifications;

import java.util.ArrayList;
import java.util.List;

import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.Commodity;

public class CommoditySpecification {
	public CommoditySpecification(OPTION option) {
		switch (option) {
		case DEFAULT:
			isCommodityTotalTrue = true;
			totalWeight = "2000";
			totalPieces = "10";
			commodityList = new ArrayList<Commodity>();// need to set default value
			break;
		case CUSTOM:
			isCommodityTotalTrue = true;
			totalWeight = null;
			totalPieces = null;
			commodityList = new ArrayList<Commodity>();// need to set default value
			break;
		}
	}

	private boolean isCommodityTotalTrue;
	private String totalWeight;
	private String totalPieces;
	private List<Commodity> commodityList;

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getTotalPieces() {
		return totalPieces;
	}

	public void setTotalPieces(String totalPieces) {
		this.totalPieces = totalPieces;
	}

	public boolean getIsCommodityTotalTrue() {
		return isCommodityTotalTrue;
	}

	public void setIsCommodityTotalTrue(boolean isCommodityTotalTrue) {
		this.isCommodityTotalTrue = isCommodityTotalTrue;
	}

	public List<Commodity> getCommodityList() {
		return commodityList;
	}

	public void setCommodityList(List<Commodity> commodityList) {
		this.commodityList = commodityList;
	}
}
