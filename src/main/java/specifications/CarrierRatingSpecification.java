package specifications;

import java.util.ArrayList;
import java.util.List;

import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.CarrierOtherPay;
import pojoClasses.requestPojo.NeedRevisit.Movement;
import utilities.DateTimeUtils;

public class CarrierRatingSpecification {

	public CarrierRatingSpecification(OPTION option) {
		switch (option) {
		case DEFAULT:
		Movement defaultMovement = new Movement();
		defaultMovement.setCarrierRate(6000);
		defaultMovement.setCarrierUnit(1);
		defaultMovement.setCarrierAmt(6000);
		defaultMovement.setCarrierPayType("F");
		defaultMovement.setBrokerage(true);
		defaultMovement.setOverridePayeeId("GARDONCA");
		defaultMovement.setSaveCompleteCarrierRating(true);
		defaultMovement.setCapacityUserId("1001mgil  ");
		defaultMovement.setCapacityUserStrDate(DateTimeUtils.getDateTimeAfterFormat3(0));
		defaultMovement.setDispatcherUserStrDate(DateTimeUtils.getDateTimeAfterFormat3(0));
		defaultMovement.setCapacityUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
		defaultMovement.setDispatcherUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
		defaultMovement.setEdiStatusFlag(false);
		defaultMovement.setBrokerageLoadStatus("yes");
		movements.add(defaultMovement);
		CarrierOtherPay carrierOtherPay = new CarrierOtherPay();
		carrierOtherPay.setDeductCodeId("CRD");
		carrierOtherPay.setRate(10);
		carrierOtherPay.setSequence("1");
		carrierOtherPay.setAmount(9);
		carrierOtherPay.setDescription("CHECK RECEIVED FROM DRIVER");
		carrierOtherPay.setPayeeId("GARDONCA");
		carrierOtherPay.setUnits(1);
		carrierOtherPay.setOtherPayId("");
		carrierOtherPay.setMemo("1234");
		carrierOtherPay.setTransactionDate(DateTimeUtils.getDateTimeAfterFormat3(0));
		carrierOtherPayList.add(carrierOtherPay);
		break;
		case CUSTOM:
			carrierOtherPayList = new ArrayList<CarrierOtherPay>();
			movements = new ArrayList<Movement>();
			break;
		}
	}

	private List<CarrierOtherPay> carrierOtherPayList=new ArrayList<CarrierOtherPay>();
	private List<Movement> movements=new ArrayList<Movement>();

	public List<CarrierOtherPay> getCarrierOtherPay() {
		return carrierOtherPayList;
	}

	public void setServiceList(List<CarrierOtherPay> carrierOtherPayList) {
		this.carrierOtherPayList = carrierOtherPayList;
	}

	public List<Movement> getMovement() {
		return movements;
	}

	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}
}
