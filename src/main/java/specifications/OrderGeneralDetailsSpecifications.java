package specifications;

public class OrderGeneralDetailsSpecifications {
	public OrderGeneralDetailsSpecifications() {
		orderType = "T";
		orderMode = "T";
		orderTypeId = "HRISK";
		orderValue = "10000";
		highValue = true;
		temperatureMin = "50";
		temperatureMax = "50";//need to change to proper temp range
		temperatureUOM = "Fahrenheit";
		agentOrderReRatingStatus = "FTL-I";
	}
	private String orderType;
	private String orderMode;
	private String orderTypeId;
	private String orderValue;
	private Boolean highValue;
	private String temperatureMin;
	private String temperatureMax;
	private String temperatureUOM;
	private String agentOrderReRatingStatus;
	
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(String orderMode) {
		this.orderMode = orderMode;
	}

	public String getOrderTypeId() {
		return orderTypeId;
	}

	public void setOrderTypeId(String orderTypeId) {
		this.orderTypeId = orderTypeId;
	}
	
	public String getOrderValue() {
		return orderValue;
	}
	
	public void setOrderValue(String orderValue) {
		this.orderValue = orderValue;
	}

	public Boolean getHighValue() {
		return highValue;
	}

	public void setHighValue(Boolean highValue) {
		this.highValue = highValue;
	}
	
	public String getTemperatureMin() {
		return temperatureMin;
	}

	public void setTemperatureMin(String temperatureMin) {
		this.temperatureMin = temperatureMin;
	}

	public String getTemperatureMax() {
		return temperatureMax;
	}

	public void setTemperatureMax(String temperatureMax) {
		this.temperatureMax = temperatureMax;
	}

	public String getTemperatureUOM() {
		return temperatureUOM;
	}

	public void setTemperatureUOM(String temperatureUOM) {
		this.temperatureUOM = temperatureUOM;
	}

	public String getAgentOrderReRatingStatus() {
		return agentOrderReRatingStatus;
	}

	public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
		this.agentOrderReRatingStatus = agentOrderReRatingStatus;
	}
}
