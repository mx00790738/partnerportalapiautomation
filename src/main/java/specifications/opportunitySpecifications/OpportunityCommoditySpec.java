/**
 * 
 */
package specifications.opportunitySpecifications;

/**
 * @author Manoj
 *
 */
public class OpportunityCommoditySpec {
	public OpportunityCommoditySpec() {
           id = "BOXES    ";
           desc = "Corrugated Boxes";
	}
	
	private String id;
	private String desc;
	
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
