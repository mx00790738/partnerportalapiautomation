/**
 * 
 */
package specifications.opportunitySpecifications;

/**
 * @author MX0C92049
 *
 */
public class OpportunityCustomerRatingSpec {
	public OpportunityCustomerRatingSpec() {
		rate=50;
		rateType="F";
		rateUnit=1;
		lineHaulCharges=50;
		agentOrderReRatingStatus="FTL-I";
	}
	
	private Integer rate;
	private String rateType;
	private Integer rateUnit;
	private Integer lineHaulCharges;
	private String agentOrderReRatingStatus;
	
	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public Integer getRateUnit() {
		return rateUnit;
	}

	public void setRateUnit(Integer rateUnit) {
		this.rateUnit = rateUnit;
	}
	
	public Integer getLineHaulCharges() {
		return lineHaulCharges;
	}

	public void setLineHaulCharges(Integer lineHaulCharges) {
		this.lineHaulCharges = lineHaulCharges;
	}
	
	public String getAgentOrderReRatingStatus() {
		return agentOrderReRatingStatus;
	}

	public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
		this.agentOrderReRatingStatus = agentOrderReRatingStatus;
	}
}
