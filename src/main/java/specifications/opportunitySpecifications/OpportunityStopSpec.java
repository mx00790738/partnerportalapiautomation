/**
 * 
 */
package specifications.opportunitySpecifications;

import java.util.ArrayList;
import java.util.List;

import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.Stop;
import utilities.DateTimeUtils;

/**
 * @author MX0C92049
 *
 */
public class OpportunityStopSpec {

	/**
	 * 
	 */
	public OpportunityStopSpec(OPTION option) {
		switch (option) {
		case DEFAULT:
			Stop stop1 = new Stop();
			stop1.setCityId(8867);
			stop1.setCityName("TEXARKANA");
			stop1.setState("AR");
			stop1.setZipCode(Integer.parseInt("71854"));
			stop1.setLocationId(null);
			stop1.setLocationName(null);
			stop1.setAddress1(null);
			stop1.setStopType("PU");
			stop1.setCountry("USA");
			stop1.setContactName("bob");
			stop1.setContactNumber("998764678");
			stop1.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(1));
			stop1.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(1));

			Stop stop2 = new Stop();
			stop2.setCityId(74532);
			stop2.setCityName("FLOODWOOD");
			stop2.setState("MN");
			stop2.setZipCode(Integer.parseInt("55736"));
			stop2.setLocationId(null);
			stop2.setLocationName(null);
			stop2.setAddress1(null);
			stop2.setStopType("SO");
			stop2.setCountry("USA");
			stop2.setContactName("JOE");
			stop2.setContactNumber("678566");
			stop2.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(2));
			stop2.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(2));
			
			stopListDetails = new ArrayList<Stop>();
			stopListDetails.add(stop1);
			stopListDetails.add(stop2);
			break;
		case CUSTOM:
			stopListDetails = new ArrayList<Stop>();
			break;
		}
	}

	private List<Stop> stopListDetails;

	public List<Stop> getStopListDetails() {
		return stopListDetails;
	}

	public void addStop(Stop stop) {
		stopListDetails.add(stop);
	}
}
