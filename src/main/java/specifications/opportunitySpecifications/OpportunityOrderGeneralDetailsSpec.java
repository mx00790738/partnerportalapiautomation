package specifications.opportunitySpecifications;

import utilities.DateTimeUtils;

public class OpportunityOrderGeneralDetailsSpec {
	public OpportunityOrderGeneralDetailsSpec() {
		orderType = "T";
		orderMode = "T";
		orderTypeId = "HRISK";
		orderValue = "10000";		
		agentOrderReRatingStatus = "FTL-I";
		subjectOrderStatus = "S";
		subjectOrderVoidDate = DateTimeUtils.getDateTimeAfterFormat1(1);
	}
	private String orderType;
	private String orderMode;
	private String orderTypeId;
	private String orderValue;
	private String agentOrderReRatingStatus;
	private String subjectOrderStatus;
	private String subjectOrderVoidDate;
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(String orderMode) {
		this.orderMode = orderMode;
	}

	public String getOrderTypeId() {
		return orderTypeId;
	}

	public void setOrderTypeId(String orderTypeId) {
		this.orderTypeId = orderTypeId;
	}
	
	public String getOrderValue() {
		return orderValue;
	}
	
	public void setOrderValue(String orderValue) {
		this.orderValue = orderValue;
	}
	
	public String getAgentOrderReRatingStatus() {
		return agentOrderReRatingStatus;
	}

	public void setAgentOrderReRatingStatus(String agentOrderReRatingStatus) {
		this.agentOrderReRatingStatus = agentOrderReRatingStatus;
	}
	
	public void setSubjectOrderStatus(String subjectOrderStatus) {
		this.subjectOrderStatus = subjectOrderStatus;
	}

	public String getSubjectOrderStatus() {
		return subjectOrderStatus;
	}
	
	public void setSubjectOrderVoidDate(String subjectOrderVoidDate) {
		this.subjectOrderVoidDate = subjectOrderVoidDate;
	}

	public String getSubjectOrderVoidDate() {
		return subjectOrderVoidDate;
	}
}
