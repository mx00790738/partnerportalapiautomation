package specifications.opportunitySpecifications;

public class OpportunityEquipmentDetailsSpec {
	public OpportunityEquipmentDetailsSpec() {
		equipmentTypeId = "V";
	}

	String equipmentTypeId;

	public String getEquipmentTypeId() {
		return equipmentTypeId;
	}

	public void setEquipmentTypeId(String equipmentTypeId) {
		this.equipmentTypeId = equipmentTypeId;
	}
}
