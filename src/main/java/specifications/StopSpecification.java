package specifications;

import java.util.ArrayList;
import java.util.List;

import apiHelper.MasterDataLocationAPI;
import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.Comment;
import pojoClasses.requestPojo.NeedRevisit.Reference;
import pojoClasses.requestPojo.NeedRevisit.Stop;
import pojoClasses.responsePojo.MasterData.Location.LocationDetails;
import utilities.DateTimeUtils;

public class StopSpecification {

	public StopSpecification(OPTION option) throws Exception {
		switch (option) {
		case DEFAULT:
			stopListDetails = new ArrayList<Stop>();
			// Comment for pickup
			List<Comment> commentsListPickup = new ArrayList<Comment>();
			Comment pickupComment1 = new Comment();
			pickupComment1.setCommentType("DC");
			pickupComment1.setComment("test dispatch comment");
			commentsListPickup.add(pickupComment1);
			// Reference for pickup
			List<Reference> referenceListPickup = new ArrayList<Reference>();
			Reference pickupReference1 = new Reference();
			pickupReference1.setReferenceNumber("2345");
			pickupReference1.setReferenceType("AO");
			pickupReference1.setWeight("678");
			pickupReference1.setPieces("7");
			pickupReference1.setSendToDriver(false);
			pickupReference1.setPartnerId("TMS");
			referenceListPickup.add(pickupReference1);
			//Pickup
			addPickup("0028FOWA", DateTimeUtils.getDateTimeAfterFormat1(0), DateTimeUtils.getDateTimeAfterFormat1(0),
					referenceListPickup, commentsListPickup);
			
			// Comment for Drop
			List<Comment> commentsListDrop = new ArrayList<Comment>();
			Comment dropComment1 = new Comment();
			dropComment1.setCommentType("BC");
			dropComment1.setComment("test billing comment");
			commentsListDrop.add(dropComment1);
			// Reference for Drop
			List<Reference> referenceListDrop = new ArrayList<Reference>();
			Reference dropReference1 = new Reference();
			dropReference1.setReferenceNumber("765");
			dropReference1.setReferenceType("OI");
			dropReference1.setWeight("67");
			dropReference1.setPieces("2");
			dropReference1.setSendToDriver(false);
			dropReference1.setPartnerId("T100");
			referenceListDrop.add(dropReference1);
			//Drop
			addDrop("1013ASVA", DateTimeUtils.getDateTimeAfterFormat1(2), DateTimeUtils.getDateTimeAfterFormat1(2),
					referenceListDrop, commentsListDrop);
			break;
		case CUSTOM:
			stopListDetails = new ArrayList<Stop>();
			break;
		}
	}

	private List<Stop> stopListDetails;
	private MasterDataLocationAPI masterDataLocationAPI = new MasterDataLocationAPI();

	public void addPickup(String locationCode, String schedArriveDate, String schedDepartDate,
			List<Reference> referenceList, List<Comment> commentsList) throws Exception {
		LocationDetails locDetails = masterDataLocationAPI.getLocation(locationCode);
		Stop stop = new Stop();
		stop.setCityId(locDetails.getCityId());
		stop.setCityName(locDetails.getCity());
		stop.setState(locDetails.getState());
		stop.setZipCode(Integer.parseInt(locDetails.getPostalCode()));
		stop.setLocationId(locDetails.getId());
		stop.setLocationName(locDetails.getName());
		stop.setLocationType("Un-defined");
		stop.setAddress1(locDetails.getAddressLine1());
		stop.setAddress2(locDetails.getAddressLine2());
		stop.setStopType("PU");
		stop.setComments(commentsList);
		stop.setCountry(locDetails.getCountry());
		stop.setContactName(null);
		stop.setContactNumber(null);
		stop.setSchedDepartDate(schedDepartDate);
		stop.setSchedArriveDate(schedArriveDate);
		stop.setApptRequired(locDetails.getApptRequired());
		stop.setConfirmed(false);
		stop.setReferences(referenceList);
		stop.setDriverLoadId(locDetails.getDriverLoadId());
		stopListDetails.add(stop);
	}

	public void addDrop(String locationCode, String schedArriveDate, String schedDepartDate,
			List<Reference> referenceList, List<Comment> commentsList) throws Exception {
		LocationDetails locDetails = masterDataLocationAPI.getLocation(locationCode);
		Stop stop = new Stop();
		stop.setCityId(locDetails.getCityId());
		stop.setCityName(locDetails.getCity());
		stop.setState(locDetails.getState());
		stop.setZipCode(Integer.parseInt(locDetails.getPostalCode()));
		stop.setLocationId(locDetails.getId());
		stop.setLocationName(locDetails.getName());
		stop.setLocationType("Un-defined");
		stop.setAddress1(locDetails.getAddressLine1());
		stop.setAddress2(locDetails.getAddressLine2());
		stop.setStopType("SO");
		stop.setComments(commentsList);
		stop.setCountry(locDetails.getCountry());
		stop.setContactName(null);
		stop.setContactNumber(null);
		stop.setSchedDepartDate(schedDepartDate);
		stop.setSchedArriveDate(schedArriveDate);
		stop.setApptRequired(locDetails.getApptRequired());
		stop.setConfirmed(false);
		stop.setReferences(referenceList);
		stop.setDriverUnloadId(locDetails.getDriverUnloadId());
		stopListDetails.add(stop);
	}

	public List<Stop> getStopListDetails() {
		return stopListDetails;
	}
}
