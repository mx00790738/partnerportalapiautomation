package specifications;

public class SpecialReferenceSpecification {
	public SpecialReferenceSpecification() {
		billNumber = "56738";
		consigneeRefNumber = "2060";
		planningComments = "This is test planning comment";
		enteredUserDate = null;
		enteredStrUserDate = null;
		operationsStrUserDate = null;
		operationsUserDate = null;
		noOfLoads = 1;
	}
	private String billNumber;
	private String consigneeRefNumber;
	private String planningComments;
	private String enteredUserDate;
	private String enteredStrUserDate;
	private String operationsStrUserDate;
	private String operationsUserDate;
	private Integer noOfLoads;
	
	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getConsigneeRefNumber() {
		return consigneeRefNumber;
	}

	public void setConsigneeRefNumber(String consigneeRefNumber) {
		this.consigneeRefNumber = consigneeRefNumber;
	}

	public String getPlanningComments() {
		return planningComments;
	}

	public void setPlanningComments(String planningComments) {
		this.planningComments = planningComments;
	}
	
	public String getEnteredUserDate() {
	return enteredUserDate;
	}

	public void setEnteredUserDate(String enteredUserDate) {
		this.enteredUserDate = enteredUserDate;
	}

	public String getEnteredStrUserDate() {
		return enteredStrUserDate;
	}

	public void setEnteredStrUserDate(String enteredStrUserDate) {
		this.enteredStrUserDate = enteredStrUserDate;
	}

	public String getOperationsStrUserDate() {
		return operationsStrUserDate;
	}

	public void setOperationsStrUserDate(String operationsStrUserDate) {
		this.operationsStrUserDate = operationsStrUserDate;
	}

	public String getOperationsUserDate() {
		return operationsUserDate;
	}

	public void setOperationsUserDate(String operationsUserDate) {
		this.operationsUserDate = operationsUserDate;
	}

	public Integer getNoOfLoads() {
		return noOfLoads;
	}

	public void setNoOfLoads(Integer noOfLoads) {
		this.noOfLoads = noOfLoads;
	}
}
