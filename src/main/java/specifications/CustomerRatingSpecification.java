package specifications;

import java.util.ArrayList;
import java.util.List;

import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.OtherCharge;

public class CustomerRatingSpecification {

	public CustomerRatingSpecification(OPTION option) {
		switch (option) {
		case DEFAULT:
			rate = 2500;
			rateType = "F";
			rateUnit = 1;
			rateCurrencyType = "USD";
			lineHaulCharges = rate;
			otherCharges = new ArrayList<OtherCharge>();
			OtherCharge otherCharge1 = new OtherCharge();
			otherCharge1.setCharge_id("CHR");
			otherCharge1.setRate(20);
			otherCharge1.setRateType("F");
			otherCharge1.setDescription("CHASSIS REPAIR");
			otherCharge1.setAmount(20);
			otherCharge1.setUnits(1);
			otherCharge1.setId("");
			otherCharge1.setCustomer(null);

			OtherCharge otherCharge2 = new OtherCharge();
			otherCharge2.setCharge_id("DUL");
			otherCharge2.setRate(10);
			otherCharge2.setRateType("F");
			otherCharge2.setDescription("ASSIST IN UNLOADING");
			otherCharge2.setAmount(10);
			otherCharge2.setUnits(1);
			otherCharge2.setId("");
			otherCharge2.setCustomer(null);

			OtherCharge otherCharge3 = new OtherCharge();
			otherCharge3.setCharge_id("DFS");
			otherCharge3.setRate(80);
			otherCharge3.setRateType("F");
			otherCharge3.setDescription("Fuel Surcharge - Distance");
			otherCharge3.setCalc_Method_Descr("Flat");
			otherCharge3.setSequence("1");
			otherCharge3.setAmount(80);
			otherCharge3.setUnits(1);

			otherCharges.add(otherCharge1);
			otherCharges.add(otherCharge2);
			otherCharges.add(otherCharge3);
//System.out.println(otherCharge2.getChargeId());
			System.out.println("**********");
			System.out.println(otherCharges.size());
			System.out.println("**********");
			otherChargeTotal = 0;
			int total = 0;
			for (OtherCharge otherCharge : otherCharges) {
				total = total + otherCharge.getRate();
			}
			totalCharge = lineHaulCharges + total;
			break;
		case CUSTOM:
			rate = null;
			rateType = "F";
			rateUnit = 1;
			rateCurrencyType = "USD";
			lineHaulCharges = null;
			otherCharges = new ArrayList<OtherCharge>();
			otherChargeTotal = 0;
			totalCharge = 0;
			break;
		}
	}

	private Integer rate;
	private String rateType;
	private Integer rateUnit;
	private String rateCurrencyType;
	private Integer lineHaulCharges;
	private List<OtherCharge> otherCharges;
	private Integer otherChargeTotal;
	private Integer totalCharge;

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public Integer getRateUnit() {
		return rateUnit;
	}

	public void setRateUnit(Integer rateUnit) {
		this.rateUnit = rateUnit;
	}

	public String getRateCurrencyType() {
		return rateCurrencyType;
	}

	public void setRateCurrencyType(String rateCurrencyType) {
		this.rateCurrencyType = rateCurrencyType;
	}

	public Integer getLineHaulCharges() {
		return lineHaulCharges;
	}

	public void setLineHaulCharges(Integer lineHaulCharges) {
		this.lineHaulCharges = lineHaulCharges;
	}

	public Integer getOtherChargeTotalt() {
		return otherChargeTotal;
	}

	public void setOtherChargeTotal(Integer otherChargeTotal) {
		this.otherChargeTotal = otherChargeTotal;
	}

	public Integer getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Integer totalCharge) {
		this.totalCharge = totalCharge;
	}

	public List<OtherCharge> getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(List<OtherCharge> otherCharges) {
		this.otherCharges = otherCharges;
	}
}
