package requestPayloadProvider.DispatchMicroservice;

import apiManager.APIManager;
import deserializer.Deserialize;
import io.restassured.response.Response;
import pojoClasses.requestPojo.DispatchMicroservice.Assignment.AssignmentDetails;
import pojoClasses.responsePojo.AgentPortalService.GetOrder.OrderDetails;

public class AssignmentRequestPayloadProvider {
    public AssignmentRequestPayloadProvider(){}
    public AssignmentDetails getAssignmentRequestPayloadForOrder(String orderID,String driver1,String tractor1, String trailer1) throws Exception {
        Response getOrderAPIResponse = APIManager.getGetOrderAPIApiInstance().getOrderDetails(orderID);
        OrderDetails getOrderAPIResponseDeserialized = Deserialize.deserializeJsonToObject(getOrderAPIResponse, OrderDetails.class);

        AssignmentDetails assignmentDetails = new AssignmentDetails();
        assignmentDetails.setDriver1ID(driver1);
        assignmentDetails.setDriver2ID("");
        assignmentDetails.setMovementID(getOrderAPIResponseDeserialized.getMovementId());
        assignmentDetails.setOverrideExistingPreassingment(false);
        assignmentDetails.setOverrideLinkedMoves(false);
        assignmentDetails.setTractorID(tractor1);
        assignmentDetails.setTrailer1ID(trailer1);
        assignmentDetails.setTrailer3ID(null);
        return assignmentDetails;
    }
}
