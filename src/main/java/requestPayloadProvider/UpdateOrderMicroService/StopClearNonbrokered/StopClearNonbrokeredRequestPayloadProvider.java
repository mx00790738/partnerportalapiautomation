package requestPayloadProvider.UpdateOrderMicroService.StopClearNonbrokered;

import apiManager.APIManager;
import deserializer.Deserialize;
import io.restassured.response.Response;
import pojoClasses.commonPojo.Stops;
import pojoClasses.requestPojo.UpdateOrderMicroservice.StopClearNonbrokered.StopClear;
import pojoClasses.responsePojo.AgentPortalService.GetOrder.OrderDetails;
import utilities.DateTimeUtils;

public class StopClearNonbrokeredRequestPayloadProvider {
    public StopClearNonbrokeredRequestPayloadProvider(){}
    public StopClear getNonbrokeredStopClearPayloadForStop(String orderID, int stopNo) throws Exception {
        Response getOrderAPIResponse = APIManager.getGetOrderAPIApiInstance().getOrderDetails(orderID);
        OrderDetails getOrderAPIResponseDeserialized = Deserialize.deserializeJsonToObject(getOrderAPIResponse, OrderDetails.class);

        Stops stop = getOrderAPIResponseDeserialized.getStops().get(stopNo);
        StopClear stopClear = new StopClear();
        stopClear.setArrivalDate(DateTimeUtils.incrementGivenTimeBy(stop.getSchedArriveDate(),1));
        stopClear.setDepartureDate(DateTimeUtils.incrementGivenTimeBy(stop.getSchedDepartDate(),1));
        stopClear.setHub(0);
        stopClear.setStopId(stop.getId());

        return stopClear;
    }

    public StopClear getNonbrokeredStopClearPayloadForStopWithServiceFailure(String orderID, int stopNo) throws Exception {
        Response getOrderAPIResponse = APIManager.getGetOrderAPIApiInstance().getOrderDetails(orderID);
        OrderDetails getOrderAPIResponseDeserialized = Deserialize.deserializeJsonToObject(getOrderAPIResponse, OrderDetails.class);

        Stops stop = getOrderAPIResponseDeserialized.getStops().get(stopNo);
        StopClear stopClear = new StopClear();
        stopClear.setArrivalDate(DateTimeUtils.incrementGivenTimeBy(stop.getSchedArriveDate(),1));
        stopClear.setDelayCode("A2");
        stopClear.setDelayComments("test delay");
        stopClear.setDepartureDate(DateTimeUtils.incrementGivenTimeBy(stop.getSchedDepartDate(),1));
        stopClear.setHub(0);
        stopClear.setStopId(stop.getId());

        return stopClear;
    }
}
