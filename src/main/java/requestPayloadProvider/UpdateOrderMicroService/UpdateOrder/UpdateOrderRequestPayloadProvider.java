package requestPayloadProvider.UpdateOrderMicroService.UpdateOrder;


import apiManager.APIManager;
import deserializer.Deserialize;
import io.restassured.response.Response;
import pojoClasses.requestPojo.UpdateOrderMicroservice.UpdateOrder.Order;
import pojoClasses.responsePojo.AgentPortalService.GetOrder.OrderDetails;

public class UpdateOrderRequestPayloadProvider {
    public UpdateOrderRequestPayloadProvider() {
    }

    public Order getUpdateOrderRequestPayloadForOrderId(String orderID) throws Exception {
        Response getOrderAPIResponse = APIManager.getGetOrderAPIApiInstance().getOrderDetails(orderID);
        OrderDetails getOrderAPIResponseDeserialized = Deserialize.deserializeJsonToObject(getOrderAPIResponse, OrderDetails.class);
        Order order = new Order();
        order.setId(getOrderAPIResponseDeserialized.getId());
        order.setCompanyId(getOrderAPIResponseDeserialized.getCompanyId());
        order.setCustomerId(getOrderAPIResponseDeserialized.getCustomerId());
        order.setCustomerName(getOrderAPIResponseDeserialized.getCustomerName());
        order.setCustomerLocation(getOrderAPIResponseDeserialized.getCustomerLocation());
        order.setCustomerLocationAddress(getOrderAPIResponseDeserialized.getCustomerLocationAddress());
        order.setCustomerLocationState(getOrderAPIResponseDeserialized.getCustomerState());
        order.setCustomerLocationCity(getOrderAPIResponseDeserialized.getCustomerCity());
        order.setCustomerLocationZip(getOrderAPIResponseDeserialized.getCustomerZip());
        //Todo check from where this is value comes from
        order.setAgentPortalUserId(getOrderAPIResponseDeserialized.getAgentPortalUserId());
        //Todo check from where this is value comes from
        order.setMcleodUserId(getOrderAPIResponseDeserialized.getMcleodUserId());
        order.setAgencyId(getOrderAPIResponseDeserialized.getAgencyId());
        order.setAgencyPayeeId(getOrderAPIResponseDeserialized.getAgencyPayeeId());
        order.setEquipmentTypeId(getOrderAPIResponseDeserialized.getEquipmentTypeId());
        order.setEquipmentTypeDescr(getOrderAPIResponseDeserialized.getEquipmentTypeDescr());
        //Todo not sure why it set to 0 when get order return it as null. May be Integer to int conversion
        order.setOrderValue(getOrderAPIResponseDeserialized.getOrderValue());
        order.setHighValue(getOrderAPIResponseDeserialized.getHighValue());
        order.setTotalWeight(getOrderAPIResponseDeserialized.getTotalWeight());
        order.setTotalPieces(getOrderAPIResponseDeserialized.getTotalPieces());
        //Todo not sure why it set to 0 when get order return it as null. May be Integer to int conversion
        order.setTemperatureMin(getOrderAPIResponseDeserialized.getTemperatureMin());
        //Todo not sure why it set to 0 when get order return it as null. May be Integer to int conversion
        order.setTemperatureMax(getOrderAPIResponseDeserialized.getTemperatureMax());
        order.setTemperatureUOM(getOrderAPIResponseDeserialized.getTemperatureUOM());
        order.setBillNumber(getOrderAPIResponseDeserialized.getBillNumber());
        order.setConsigneeRefNumber(getOrderAPIResponseDeserialized.getConsigneeRefNumber());
        order.setPlanningComments(getOrderAPIResponseDeserialized.getPlanningComments());
        order.setOperationsUser(getOrderAPIResponseDeserialized.getOperationsUser());
        //todo change date format from get order to one used by update order api req payload
        order.setOperationsStrUserDate(getOrderAPIResponseDeserialized.getOperationsStrUserDate());
        //todo change date format from get order to one used by update order api req payload
        order.setEnteredStrUserDate(getOrderAPIResponseDeserialized.getEnteredStrUserDate());
        order.setOperationsUserId(getOrderAPIResponseDeserialized.getOperationsUserId());
        order.setEnteredUserId(getOrderAPIResponseDeserialized.getEnteredUserId());
        order.setEnteredUserDate(getOrderAPIResponseDeserialized.getEnteredUserDate());
        order.setMovementId(getOrderAPIResponseDeserialized.getMovementId());
        order.setCommodities(getOrderAPIResponseDeserialized.getCommodities());
        //todo - check stops pojo multiple field need attention
        order.setStops(getOrderAPIResponseDeserialized.getStops());
        order.setDeleteStops(getOrderAPIResponseDeserialized.getDeleteStops());
        //Todo check from where this is value comes from
        order.setOrderDetailsHistory(getOrderAPIResponseDeserialized.getOrderDetailsHistory());
        order.setOrderType(getOrderAPIResponseDeserialized.getOrderType());
        order.setBillingMethod(getOrderAPIResponseDeserialized.getBillingMethod());
        order.setOperationsUserDate(getOrderAPIResponseDeserialized.getOperationsUserDate());
        //todo - check movements pojo multiple field need attention
        order.setMovements(getOrderAPIResponseDeserialized.getMovements());
        order.setAdditionalEquipments(getOrderAPIResponseDeserialized.getAdditionalEquipments());
        order.setDeletedAdditionalEquips(getOrderAPIResponseDeserialized.getDeletedAdditionalEquips());

        return order;
    }
}
