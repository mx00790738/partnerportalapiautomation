package requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo;

import com.fasterxml.jackson.annotation.JsonSetter;

public class FunctionAccessRule {
    @JsonSetter("functionId")
    public String getFunctionId() {
        return this.functionId; }
    public void setFunctionId(String functionId) {
        this.functionId = functionId; }
    String functionId;
    @JsonSetter("functionDescription")
    public String getFunctionDescription() {
        return this.functionDescription; }
    public void setFunctionDescription(String functionDescription) {
        this.functionDescription = functionDescription; }
    String functionDescription;
    @JsonSetter("functionScreenLabel")
    public String getFunctionScreenLabel() {
        return this.functionScreenLabel; }
    public void setFunctionScreenLabel(String functionScreenLabel) {
        this.functionScreenLabel = functionScreenLabel; }
    String functionScreenLabel;
    @JsonSetter("functionAccess")
    public String getFunctionAccess() {
        return this.functionAccess; }
    public void setFunctionAccess(String functionAccess) {
        this.functionAccess = functionAccess; }
    String functionAccess;
    @JsonSetter("nextMandatoryFunId")
    public String getNextMandatoryFunId() {
        return this.nextMandatoryFunId; }
    public void setNextMandatoryFunId(String nextMandatoryFunId) {
        this.nextMandatoryFunId = nextMandatoryFunId; }
    String nextMandatoryFunId;
    @JsonSetter("nextMandatoryFunDesc")
    public String getNextMandatoryFunDesc() {
        return this.nextMandatoryFunDesc; }
    public void setNextMandatoryFunDesc(String nextMandatoryFunDesc) {
        this.nextMandatoryFunDesc = nextMandatoryFunDesc; }
    String nextMandatoryFunDesc;
    @JsonSetter("nextMandatoryFunLabel")
    public String getNextMandatoryFunLabel() {
        return this.nextMandatoryFunLabel; }
    public void setNextMandatoryFunLabel(String nextMandatoryFunLabel) {
        this.nextMandatoryFunLabel = nextMandatoryFunLabel; }
    String nextMandatoryFunLabel;
    @JsonSetter("previousBrokerageStatus")
    public String getPreviousBrokerageStatus() {
        return this.previousBrokerageStatus; }
    public void setPreviousBrokerageStatus(String previousBrokerageStatus) {
        this.previousBrokerageStatus = previousBrokerageStatus; }
    String previousBrokerageStatus;
    @JsonSetter("functionIdOpen")
    public String getFunctionIdOpen() {
        return this.functionIdOpen; }
    public void setFunctionIdOpen(String functionIdOpen) {
        this.functionIdOpen = functionIdOpen; }
    String functionIdOpen;
    @JsonSetter("functionAccessEditOrView")
    public String getFunctionAccessEditOrView() {
        return this.functionAccessEditOrView; }
    public void setFunctionAccessEditOrView(String functionAccessEditOrView) {
        this.functionAccessEditOrView = functionAccessEditOrView; }
    String functionAccessEditOrView;
    @JsonSetter("functionGroupDescription")
    public String getFunctionGroupDescription() {
        return this.functionGroupDescription; }
    public void setFunctionGroupDescription(String functionGroupDescription) {
        this.functionGroupDescription = functionGroupDescription; }
    String functionGroupDescription;
    @JsonSetter("nextMandatoryFlag")
    public String getNextMandatoryFlag() {
        return this.nextMandatoryFlag; }
    public void setNextMandatoryFlag(String nextMandatoryFlag) {
        this.nextMandatoryFlag = nextMandatoryFlag; }
    String nextMandatoryFlag;
    @JsonSetter("brokerageStatusRangeforAction")
    public String getBrokerageStatusRangeforAction() {
        return this.brokerageStatusRangeforAction; }
    public void setBrokerageStatusRangeforAction(String brokerageStatusRangeforAction) {
        this.brokerageStatusRangeforAction = brokerageStatusRangeforAction; }
    String brokerageStatusRangeforAction;
    @JsonSetter("hasTemplate")
    public String getHasTemplate() {
        return this.hasTemplate; }
    public void setHasTemplate(String hasTemplate) {
        this.hasTemplate = hasTemplate; }
    String hasTemplate;
    @JsonSetter("uiFunctionIdType")
    public String getUiFunctionIdType() {
        return this.uiFunctionIdType; }
    public void setUiFunctionIdType(String uiFunctionIdType) {
        this.uiFunctionIdType = uiFunctionIdType; }
    String uiFunctionIdType;
    @JsonSetter("templateFunctionIdType")
    public String getTemplateFunctionIdType() {
        return this.templateFunctionIdType; }
    public void setTemplateFunctionIdType(String templateFunctionIdType) {
        this.templateFunctionIdType = templateFunctionIdType; }
    String templateFunctionIdType;
}
