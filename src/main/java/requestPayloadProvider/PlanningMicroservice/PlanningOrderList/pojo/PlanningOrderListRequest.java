package requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

public class PlanningOrderListRequest {
    @JsonSetter("ignoreReadyToBill")
    public boolean getIgnoreReadyToBill() {
        return this.ignoreReadyToBill; }
    public void setIgnoreReadyToBill(boolean ignoreReadyToBill) {
        this.ignoreReadyToBill = ignoreReadyToBill; }
    boolean ignoreReadyToBill;
    @JsonSetter("isDefaultCriteria")
    public boolean getIsDefaultCriteria() {
        return this.isDefaultCriteria; }
    public void setIsDefaultCriteria(boolean isDefaultCriteria) {
        this.isDefaultCriteria = isDefaultCriteria; }
    boolean isDefaultCriteria;
    @JsonSetter("pickupFromCompare")
    public String getPickupFromCompare() {
        return this.pickupFromCompare; }
    public void setPickupFromCompare(String pickupFromCompare) {
        this.pickupFromCompare = pickupFromCompare; }
    String pickupFromCompare;
    @JsonSetter("pickUpDateStr")
    public String getPickUpDateStr() {
        return this.pickUpDateStr; }
    public void setPickUpDateStr(String pickUpDateStr) {
        this.pickUpDateStr = pickUpDateStr; }
    String pickUpDateStr;
    @JsonSetter("pickupDateFrom")
    public String getPickupDateFrom() {
        return this.pickupDateFrom; }
    public void setPickupDateFrom(String pickupDateFrom) {
        this.pickupDateFrom = pickupDateFrom; }
    String pickupDateFrom;
    @JsonSetter("pickupToCompare")
    public String getPickupToCompare() {
        return this.pickupToCompare; }
    public void setPickupToCompare(String pickupToCompare) {
        this.pickupToCompare = pickupToCompare; }
    String pickupToCompare;
    @JsonSetter("pickupDateToStr")
    public String getPickupDateToStr() {
        return this.pickupDateToStr; }
    public void setPickupDateToStr(String pickupDateToStr) {
        this.pickupDateToStr = pickupDateToStr; }
    String pickupDateToStr;
    @JsonSetter("pickupDateTo")
    public String getPickupDateTo() {
        return this.pickupDateTo; }
    public void setPickupDateTo(String pickupDateTo) {
        this.pickupDateTo = pickupDateTo; }
    String pickupDateTo;
    @JsonSetter("orderStatusCompare")
    public String getOrderStatusCompare() {
        return this.orderStatusCompare; }
    public void setOrderStatusCompare(String orderStatusCompare) {
        this.orderStatusCompare = orderStatusCompare; }
    String orderStatusCompare;
    @JsonSetter("agencyPayeeId")
    public String getAgencyPayeeId() {
        return this.agencyPayeeId; }
    public void setAgencyPayeeId(String agencyPayeeId) {
        this.agencyPayeeId = agencyPayeeId; }
    String agencyPayeeId;

    @JsonSetter("tabAccessRules")
    public List<TabAccessRule> getTabAccessRules() {
        return this.tabAccessRules; }
    public void setTabAccessRules(List<TabAccessRule> tabAccessRules) {
        this.tabAccessRules = tabAccessRules; }
    List<TabAccessRule> tabAccessRules;

    @JsonSetter("userType")
    public String getUserType() {
        return this.userType; }
    public void setUserType(String userType) {
        this.userType = userType; }
    String userType;
    @JsonSetter("loginId")
    public String getLoginId() {
        return this.loginId; }
    public void setLoginId(String loginId) {
        this.loginId = loginId; }
    String loginId;
    @JsonSetter("responsibilityFactor")
    public List<ResponsibilityFactor> getResponsibilityFactor() {
        return this.responsibilityFactor; }
    public void setResponsibilityFactor(List<ResponsibilityFactor> responsibilityFactor) {
        this.responsibilityFactor = responsibilityFactor; }
    List<ResponsibilityFactor> responsibilityFactor;
    @JsonSetter("currentUserGroup")
    public String getCurrentUserGroup() {
        return this.currentUserGroup; }
    public void setCurrentUserGroup(String currentUserGroup) {
        this.currentUserGroup = currentUserGroup; }
    String currentUserGroup;
    @JsonSetter("customerID")
    public String getCustomerID() {
        return this.customerID; }
    public void setCustomerID(String customerID) {
        this.customerID = customerID; }
    String customerID;
    @JsonSetter("customerName")
    public String getCustomerName() {
        return this.customerName; }
    public void setCustomerName(String customerName) {
        this.customerName = customerName; }
    String customerName;
    @JsonSetter("pageNumber")
    public int getPageNumber() {
        return this.pageNumber; }
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber; }
    int pageNumber;
}
