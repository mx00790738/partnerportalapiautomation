package requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

public class TabAccessRule {
    @JsonSetter("tabId")
    public String getTabId() {
        return this.tabId; }
    public void setTabId(String tabId) {
        this.tabId = tabId; }
    String tabId;
    @JsonSetter("tabDescription")
    public String getTabDescription() {
        return this.tabDescription; }
    public void setTabDescription(String tabDescription) {
        this.tabDescription = tabDescription; }
    String tabDescription;
    @JsonSetter("tabScreenLabel")
    public String getTabScreenLabel() {
        return this.tabScreenLabel; }
    public void setTabScreenLabel(String tabScreenLabel) {
        this.tabScreenLabel = tabScreenLabel; }
    String tabScreenLabel;
    @JsonSetter("tabAccess")
    public String getTabAccess() {
        return this.tabAccess; }
    public void setTabAccess(String tabAccess) {
        this.tabAccess = tabAccess; }
    String tabAccess;
    @JsonSetter("functionIdOpen")
    public Object getFunctionIdOpen() {
        return this.functionIdOpen; }
    public void setFunctionIdOpen(Object functionIdOpen) {
        this.functionIdOpen = functionIdOpen; }
    Object functionIdOpen;
    @JsonSetter("functionGroupDescription")
    public Object getFunctionGroupDescription() {
        return this.functionGroupDescription; }
    public void setFunctionGroupDescription(Object functionGroupDescription) {
        this.functionGroupDescription = functionGroupDescription; }
    Object functionGroupDescription;
    @JsonSetter("nextMandatoryFlag")
    public Object getNextMandatoryFlag() {
        return this.nextMandatoryFlag; }
    public void setNextMandatoryFlag(Object nextMandatoryFlag) {
        this.nextMandatoryFlag = nextMandatoryFlag; }
    Object nextMandatoryFlag;
    @JsonSetter("brokerageStatusRangeforAction")
    public Object getBrokerageStatusRangeforAction() {
        return this.brokerageStatusRangeforAction; }
    public void setBrokerageStatusRangeforAction(Object brokerageStatusRangeforAction) {
        this.brokerageStatusRangeforAction = brokerageStatusRangeforAction; }
    Object brokerageStatusRangeforAction;
    @JsonSetter("functionAccessRules")
    public List<FunctionAccessRule> getFunctionAccessRules() {
        return this.functionAccessRules; }
    public void setFunctionAccessRules(List<FunctionAccessRule> functionAccessRules) {
        this.functionAccessRules = functionAccessRules; }
    List<FunctionAccessRule> functionAccessRules;
    @JsonSetter("hasTemplate")
    public Object getHasTemplate() {
        return this.hasTemplate; }
    public void setHasTemplate(Object hasTemplate) {
        this.hasTemplate = hasTemplate; }
    Object hasTemplate;
    @JsonSetter("uiFunctionIdType")
    public Object getUiFunctionIdType() {
        return this.uiFunctionIdType; }
    public void setUiFunctionIdType(Object uiFunctionIdType) {
        this.uiFunctionIdType = uiFunctionIdType; }
    Object uiFunctionIdType;
    @JsonSetter("templateFunctionIdType")
    public Object getTemplateFunctionIdType() {
        return this.templateFunctionIdType; }
    public void setTemplateFunctionIdType(Object templateFunctionIdType) {
        this.templateFunctionIdType = templateFunctionIdType; }
    Object templateFunctionIdType;
}
