package requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo;

import com.fasterxml.jackson.annotation.JsonSetter;

public class ResponsibilityFactor {
    @JsonSetter("value")
    public String getValue() {
        return this.value; }
    public void setValue(String value) {
        this.value = value; }
    String value;
    @JsonSetter("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
}
