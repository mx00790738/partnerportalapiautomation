package requestPayloadProvider.PlanningMicroservice.PlanningOrderList;

import requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo.PlanningOrderListRequest;
import requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo.ResponsibilityFactor;
import utilities.DateTimeUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PlanningOrderListRequestPayloadProvider {
    public PlanningOrderListRequestPayloadProvider(){ }

    public PlanningOrderListRequest getPlanningOrderListRequestPayload(){
        PlanningOrderListRequest planningOrderListRequest = new PlanningOrderListRequest();

        //*******************************************************************************************
        //Todo- get this no. and respCodeList from /partner-portal-micro-service-qa/crst/agent/responsiblityCodeCheck
        int NoOfResponsibility = 2;
        LinkedList<String> responsibilityCodeList = new LinkedList<String>(){{
            add("1000");
            add("1001");
        }};

        ResponsibilityFactor[] responsibilityFactorArray = new ResponsibilityFactor[NoOfResponsibility];
        for (int i = 0; i < NoOfResponsibility; i++) {
            responsibilityFactorArray[i] = new ResponsibilityFactor();
            if(i==0)
                responsibilityFactorArray[i].setType("Primary");
            else
                responsibilityFactorArray[i].setType("Secondary");

            responsibilityFactorArray[i].setValue(responsibilityCodeList.get(i));
        }
        //*******************************************************************************************

        planningOrderListRequest.setIgnoreReadyToBill(true);
        planningOrderListRequest.setIsDefaultCriteria(true);
        planningOrderListRequest.setPickupFromCompare(">=");
        planningOrderListRequest.setPickUpDateStr(DateTimeUtils.getDateTimeAfterFormat4(-14));
        planningOrderListRequest.setPickupDateFrom(DateTimeUtils.getDateTimeAfterFormat1(-13));
        planningOrderListRequest.setPickupToCompare("<=");
        planningOrderListRequest.setPickupDateToStr(DateTimeUtils.getDateTimeAfterFormat4(14));
        planningOrderListRequest.setPickupDateTo(DateTimeUtils.getDateTimeAfterFormat1(13));
        planningOrderListRequest.setOrderStatusCompare("IN");
        TabAccessRuleProvider1 tabAccessRuleProvider1 = new TabAccessRuleProvider1();
        planningOrderListRequest.setTabAccessRules(new ArrayList<>());

        //TODO - Get below from API getUser and Responsibility code check
        planningOrderListRequest.setAgencyPayeeId("DEBBCEIA");
        planningOrderListRequest.setUserType("AGFT");
        planningOrderListRequest.setLoginId("1001mgil");
        planningOrderListRequest.setCurrentUserGroup("FLAT");

        List<ResponsibilityFactor> list = Arrays.asList(responsibilityFactorArray);
        planningOrderListRequest.setResponsibilityFactor(list);
        planningOrderListRequest.setCustomerID("");
        planningOrderListRequest.setCustomerName("");
        planningOrderListRequest.setPageNumber(0);

        return planningOrderListRequest;
    }
}
