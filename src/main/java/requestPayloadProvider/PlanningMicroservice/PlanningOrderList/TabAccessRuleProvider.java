package requestPayloadProvider.PlanningMicroservice.PlanningOrderList;

import requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo.FunctionAccessRule;
import requestPayloadProvider.PlanningMicroservice.PlanningOrderList.pojo.TabAccessRule;

import java.util.ArrayList;
import java.util.List;

public class TabAccessRuleProvider {
    public TabAccessRuleProvider(){

    }

    public TabAccessRuleProvider[] getTabAccessRuleArray(){
        //tabId1 ****************************************
        TabAccessRule tabId1 = new TabAccessRule();
        tabId1.setTabId("");
        tabId1.setTabDescription("");
        tabId1.setTabScreenLabel("");
        tabId1.setTabAccess("");
        tabId1.setFunctionIdOpen(null);
        tabId1.setFunctionGroupDescription(null);
        tabId1.setNextMandatoryFlag(null);
        tabId1.setBrokerageStatusRangeforAction(null);
        tabId1.setHasTemplate(null);
        tabId1.setUiFunctionIdType(null);
        tabId1.setTemplateFunctionIdType(null);

        List<FunctionAccessRule> functionAccessRuleListTabId1 = new ArrayList<FunctionAccessRule>();
        FunctionAccessRule functionAccessRuleTab1 = new FunctionAccessRule();
        functionAccessRuleTab1.setFunctionId("");
        functionAccessRuleTab1.setFunctionDescription("");
        functionAccessRuleTab1.setFunctionScreenLabel("");
        functionAccessRuleTab1.setFunctionAccess("");
        functionAccessRuleTab1.setNextMandatoryFunId("");
        functionAccessRuleTab1.setNextMandatoryFunDesc("");
        functionAccessRuleTab1.setNextMandatoryFunLabel("");
        functionAccessRuleTab1.setPreviousBrokerageStatus(null);
        functionAccessRuleTab1.setFunctionAccessEditOrView("");
        functionAccessRuleTab1.setFunctionGroupDescription("");
        functionAccessRuleTab1.setNextMandatoryFlag("");
        functionAccessRuleTab1.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab1.setHasTemplate("");
        functionAccessRuleTab1.setUiFunctionIdType("");
        functionAccessRuleTab1.setTemplateFunctionIdType(null);
        functionAccessRuleListTabId1.add(functionAccessRuleTab1);

        tabId1.setFunctionAccessRules(functionAccessRuleListTabId1);
        //************************************************

        //tabId6 ****************************************
        TabAccessRule tabId6 = new TabAccessRule();
        tabId6.setTabId("");
        tabId6.setTabDescription("");
        tabId6.setTabScreenLabel("");
        tabId6.setTabAccess("");
        tabId6.setFunctionIdOpen(null);
        tabId6.setFunctionGroupDescription(null);
        tabId6.setNextMandatoryFlag(null);
        tabId6.setBrokerageStatusRangeforAction(null);
        tabId6.setHasTemplate(null);
        tabId6.setUiFunctionIdType(null);
        tabId6.setTemplateFunctionIdType(null);

        List<FunctionAccessRule> functionAccessRuleListTabId6 = new ArrayList<FunctionAccessRule>();
        FunctionAccessRule functionAccessRuleTab6 = new FunctionAccessRule();
        functionAccessRuleTab6.setFunctionId("");
        functionAccessRuleTab6.setFunctionDescription("");
        functionAccessRuleTab6.setFunctionScreenLabel("");
        functionAccessRuleTab6.setFunctionAccess("");
        functionAccessRuleTab6.setNextMandatoryFunId("");
        functionAccessRuleTab6.setNextMandatoryFunDesc("");
        functionAccessRuleTab6.setNextMandatoryFunLabel("");
        functionAccessRuleTab6.setPreviousBrokerageStatus(null);
        functionAccessRuleTab6.setFunctionAccessEditOrView("");
        functionAccessRuleTab6.setFunctionGroupDescription("");
        functionAccessRuleTab6.setNextMandatoryFlag("");
        functionAccessRuleTab6.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab6.setHasTemplate("");
        functionAccessRuleTab6.setUiFunctionIdType("");
        functionAccessRuleTab6.setTemplateFunctionIdType(null);
        functionAccessRuleListTabId6.add(functionAccessRuleTab6);

        tabId6.setFunctionAccessRules(functionAccessRuleListTabId6);
        //************************************************

        //tabId5 ****************************************
        TabAccessRule tabId5 = new TabAccessRule();
        tabId5.setTabId("");
        tabId5.setTabDescription("");
        tabId5.setTabScreenLabel("");
        tabId5.setTabAccess("");
        tabId5.setFunctionIdOpen(null);
        tabId5.setFunctionGroupDescription(null);
        tabId5.setNextMandatoryFlag(null);
        tabId5.setBrokerageStatusRangeforAction(null);
        tabId5.setHasTemplate(null);
        tabId5.setUiFunctionIdType(null);
        tabId5.setTemplateFunctionIdType(null);

        List<FunctionAccessRule> functionAccessRuleListTabId5 = new ArrayList<FunctionAccessRule>();
        FunctionAccessRule functionAccessRuleTab5 = new FunctionAccessRule();
        functionAccessRuleTab5.setFunctionId("");
        functionAccessRuleTab5.setFunctionDescription("");
        functionAccessRuleTab5.setFunctionScreenLabel("");
        functionAccessRuleTab5.setFunctionAccess("");
        functionAccessRuleTab5.setNextMandatoryFunId("");
        functionAccessRuleTab5.setNextMandatoryFunDesc("");
        functionAccessRuleTab5.setNextMandatoryFunLabel("");
        functionAccessRuleTab5.setPreviousBrokerageStatus(null);
        functionAccessRuleTab5.setFunctionAccessEditOrView("");
        functionAccessRuleTab5.setFunctionGroupDescription("");
        functionAccessRuleTab5.setNextMandatoryFlag("");
        functionAccessRuleTab5.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab5.setHasTemplate("");
        functionAccessRuleTab5.setUiFunctionIdType("");
        functionAccessRuleTab5.setTemplateFunctionIdType(null);
        FunctionAccessRule functionAccessRuleTab5_37 = new FunctionAccessRule();
        functionAccessRuleTab5_37.setFunctionId("");
        functionAccessRuleTab5_37.setFunctionDescription("");
        functionAccessRuleTab5_37.setFunctionScreenLabel("");
        functionAccessRuleTab5_37.setFunctionAccess("");
        functionAccessRuleTab5_37.setNextMandatoryFunId("");
        functionAccessRuleTab5_37.setNextMandatoryFunDesc("");
        functionAccessRuleTab5_37.setNextMandatoryFunLabel("");
        functionAccessRuleTab5_37.setPreviousBrokerageStatus(null);
        functionAccessRuleTab5_37.setFunctionAccessEditOrView("");
        functionAccessRuleTab5_37.setFunctionGroupDescription("");
        functionAccessRuleTab5_37.setNextMandatoryFlag("");
        functionAccessRuleTab5_37.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab5_37.setHasTemplate("");
        functionAccessRuleTab5_37.setUiFunctionIdType("");
        functionAccessRuleTab5_37.setTemplateFunctionIdType(null);
        functionAccessRuleListTabId5.add(functionAccessRuleTab5);
        functionAccessRuleListTabId5.add(functionAccessRuleTab5_37);

        tabId5.setFunctionAccessRules(functionAccessRuleListTabId5);
        //************************************************
//tabId3 ****************************************
        TabAccessRule tabId3 = new TabAccessRule();
        tabId3.setTabId("");
        tabId3.setTabDescription("");
        tabId3.setTabScreenLabel("");
        tabId3.setTabAccess("");
        tabId3.setFunctionIdOpen(null);
        tabId3.setFunctionGroupDescription(null);
        tabId3.setNextMandatoryFlag(null);
        tabId3.setBrokerageStatusRangeforAction(null);
        tabId3.setHasTemplate(null);
        tabId3.setUiFunctionIdType(null);
        tabId3.setTemplateFunctionIdType(null);

        List<FunctionAccessRule> functionAccessRuleListTabId3 = new ArrayList<FunctionAccessRule>();
        FunctionAccessRule functionAccessRuleTab3 = new FunctionAccessRule();
        functionAccessRuleTab3.setFunctionId("");
        functionAccessRuleTab3.setFunctionDescription("");
        functionAccessRuleTab3.setFunctionScreenLabel("");
        functionAccessRuleTab3.setFunctionAccess("");
        functionAccessRuleTab3.setNextMandatoryFunId("");
        functionAccessRuleTab3.setNextMandatoryFunDesc("");
        functionAccessRuleTab3.setNextMandatoryFunLabel("");
        functionAccessRuleTab3.setPreviousBrokerageStatus(null);
        functionAccessRuleTab3.setFunctionAccessEditOrView("");
        functionAccessRuleTab3.setFunctionGroupDescription("");
        functionAccessRuleTab3.setNextMandatoryFlag("");
        functionAccessRuleTab3.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab3.setHasTemplate("");
        functionAccessRuleTab3.setUiFunctionIdType("");
        functionAccessRuleTab3.setTemplateFunctionIdType(null);
        functionAccessRuleListTabId3.add(functionAccessRuleTab3);

        tabId3.setFunctionAccessRules(functionAccessRuleListTabId3);
        //************************************************

        //tabId11 ****************************************
        TabAccessRule tabId11 = new TabAccessRule();
        tabId11.setTabId("");
        tabId11.setTabDescription("");
        tabId11.setTabScreenLabel("");
        tabId11.setTabAccess("");
        tabId11.setFunctionIdOpen(null);
        tabId11.setFunctionGroupDescription(null);
        tabId11.setNextMandatoryFlag(null);
        tabId11.setBrokerageStatusRangeforAction(null);
        tabId11.setHasTemplate(null);
        tabId11.setUiFunctionIdType(null);
        tabId11.setTemplateFunctionIdType(null);

        List<FunctionAccessRule> functionAccessRuleListTabId11 = new ArrayList<FunctionAccessRule>();
        FunctionAccessRule functionAccessRuleTab11 = new FunctionAccessRule();
        functionAccessRuleTab11.setFunctionId("");
        functionAccessRuleTab11.setFunctionDescription("");
        functionAccessRuleTab11.setFunctionScreenLabel("");
        functionAccessRuleTab11.setFunctionAccess("");
        functionAccessRuleTab11.setNextMandatoryFunId("");
        functionAccessRuleTab11.setNextMandatoryFunDesc("");
        functionAccessRuleTab11.setNextMandatoryFunLabel("");
        functionAccessRuleTab11.setPreviousBrokerageStatus(null);
        functionAccessRuleTab11.setFunctionAccessEditOrView("");
        functionAccessRuleTab11.setFunctionGroupDescription("");
        functionAccessRuleTab11.setNextMandatoryFlag("");
        functionAccessRuleTab11.setBrokerageStatusRangeforAction(null);
        functionAccessRuleTab11.setHasTemplate("");
        functionAccessRuleTab11.setUiFunctionIdType("");
        functionAccessRuleTab11.setTemplateFunctionIdType(null);
        functionAccessRuleListTabId11.add(functionAccessRuleTab11);

        tabId11.setFunctionAccessRules(functionAccessRuleListTabId11);
        //************************************************

        return new TabAccessRuleProvider[1];
    }
}
