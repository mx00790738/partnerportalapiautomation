package requestPayloadProvider.CreateOrderMicroservice.NewOrder;

import apiHelper.MasterDataLocationDetailsAPI;
import javafx.util.Pair;
import pojoClasses.requestPojo.NeedRevisit.*;
import pojoClasses.responsePojo.MasterData.Location.LocationDetails;
import utilities.DateTimeUtils;

import java.util.*;

public class NewOrderRequestPayloadProvider {
    public NewOrderRequestPayloadProvider(){}

    public Order getRandomNewOrderRequestPayloadFTL() throws Exception {
        Order order = new Order();
        order.setAgencyResponsiblityCode("1001");
        order.setAgentCaptiveOrder("Y");
        order.setUserType("AGFT");
        order.setDivision("FLAT");
        List<String> billingMethodList = Arrays.asList("P", "C", "T");
        Random billingMethodRand = new Random();
        String billingMethodRandom = billingMethodList.get(billingMethodRand.nextInt(billingMethodList.size()));
        order.setBillingMethod(billingMethodRandom);
        order.setCompanyId("T100"); //***Change
        order.setAgencyId("1001    ");
        order.setAgencyPayeeId("DEBBCEIA");

        // ####### Customer Details ######
        HashMap<String, String> customerDetails = getRandomCustomerFLAT(); //###Random
        order.setCustomerId(customerDetails.get("customerID"));
        order.setCustomerName(customerDetails.get("customerName"));
        order.setCustomerLocation(customerDetails.get("customerLocation"));
        order.setCustomerLocationAddress(customerDetails.get("customerAddress"));
        order.setCustomerLocationState(customerDetails.get("customerState"));
        order.setCustomerLocationCity(customerDetails.get("customerCity"));
        order.setCustomerLocationZip(customerDetails.get("customerZip"));
        order.setRevenueCode("");

        order.setAgentPortalUserId("1001mgil  ");
        order.setMcleodUserId("1001mgil  ");
        List<String> equipList = Arrays.asList("AC", "F", "C", "DT", "FR", "MX", "LO", "3RAM", "3PA");
        Random equipRand = new Random();
        String equipRandom = equipList.get(equipRand.nextInt(equipList.size()));
        order.setEquipmentTypeId(equipRandom); //###Random
        List<String> orderValueList = Arrays.asList("500", "1000", "2500", "5000", "7300", "8100", "9000", "11000");
        Random orderValueRand = new Random();
        String orderValueRandom = orderValueList.get(orderValueRand.nextInt(orderValueList.size()));
        order.setOrderValue(orderValueRandom);//###Random
        List<Boolean> highValueList = Arrays.asList(false, true);
        Random highValueRand = new Random();
        Boolean highValueRandom = highValueList.get(highValueRand.nextInt(highValueList.size()));
        order.setHighValue(highValueRandom);//###Random
        List<String> totalWeightList = Arrays.asList("9000", "1000", "2000", "2500", "3000", "3200", "4000", "5000");
        Random totalWeightRand = new Random();
        String totalWeightRandom = totalWeightList.get(totalWeightRand.nextInt(totalWeightList.size()));
        order.setTotalWeight(totalWeightRandom);//###Random
        List<String> totalPiecesList = Arrays.asList("3", "2", "5", "9", "4", "7", "6", "8", "10");
        Random totalPieceRand = new Random();
        String totalPieceRandom = totalPiecesList.get(totalPieceRand.nextInt(totalPiecesList.size()));
        order.setTotalPieces(totalPieceRandom);//###Random
        order.setTemperatureMin("50");
        order.setTemperatureMax("90");
        order.setTemperatureUOM("Fahrenheit");
        List<String> billNumberList = Arrays.asList("30899", "21234", "59645", "989295", "40006", "72211",
                "633443", "89000", "103233");
        Random billNumberRand = new Random();
        String billNumberRandom = billNumberList.get(billNumberRand.nextInt(billNumberList.size()));
        order.setBillNumber(billNumberRandom);
        List<String> consigneeRefNumberList = Arrays.asList("39800", "20220", "523450", "908765",
                "4110089", "722211", "633442", "8000768", "100034");
        Random consigneeRefNumberRand = new Random();
        String consigneeRefNumberRandom = consigneeRefNumberList.get(consigneeRefNumberRand.nextInt(consigneeRefNumberList.size()));
        order.setConsigneeRefNumber(consigneeRefNumberRandom);
        order.setPlanningComments("ORDER IS CREATED BY AUTOMATION TEST");
        order.setOperationsUser("1001mgil  ");
        order.setOperationsUserId("1001mgil  ");
        order.setEnteredUserId("1001mgil  ");
        order.setEnteredUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
        order.setEnteredStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
        order.setOperationsStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
        order.setOperationsUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
        order.setNoOfLoads(1);
        order.setRevenueCode("");

        Commodities commodities = new Commodities();
        commodities.setCommodity(new ArrayList<Commodity>());
        commodities.setServices(new ArrayList<ServiceEntry>());
        order.setCommodities(commodities);

        ArrayList<Stop> stopListDetails = new ArrayList<Stop>();
        //PICKUP
        // Comment for pickup
        List<Comment> commentsListPickup = new ArrayList<Comment>();
        Comment pickupComment1 = new Comment();
        List<String> commentTypeList_P = Arrays.asList("DC", "HC", "OC", "BC");
        Random commentTypeRand_P = new Random();
        String commentTypeRandom_P = commentTypeList_P.get(commentTypeRand_P.nextInt(commentTypeList_P.size()));
        pickupComment1.setCommentType(commentTypeRandom_P);
        pickupComment1.setComment(commentTypeRandom_P + ": *****DUMMY COMMENT******");
        commentsListPickup.add(pickupComment1);
        // Reference for pickup
        List<Reference> referenceListPickup = new ArrayList<Reference>();
        Reference pickupReference1 = new Reference();
        List<String> refNoList_P = Arrays.asList("23145", "10087", "90876", "516754", "199866", "00231", "5433467", "425357");
        Random refNoRand_P = new Random();
        String refNoRandom_P = refNoList_P.get(refNoRand_P.nextInt(refNoList_P.size()));
        pickupReference1.setReferenceNumber(refNoRandom_P);
        List<String> refTypeList_P = Arrays.asList("PU", "OR", "PS", "QL", "BM", "CG", "MB", "OI", "PO", "ZZ");
        Random refTypeRand_P = new Random();
        String refTypeRandom_P = refTypeList_P.get(refTypeRand_P.nextInt(refTypeList_P.size()));
        pickupReference1.setReferenceType(refTypeRandom_P);
        List<String> refWeightList_P = Arrays.asList("100", "220", "300", "310", "600", "768", "210");
        Random refWeightRand_P = new Random();
        String refWeightRandom_P = refWeightList_P.get(refWeightRand_P.nextInt(refWeightList_P.size()));
        pickupReference1.setWeight(refWeightRandom_P);
        List<String> refPiecesList_P = Arrays.asList("2", "5", "7", "8", "9", "10", "12", "15");
        Random refPiecesRand_P = new Random();
        String refPiecesRandom_P = refPiecesList_P.get(refPiecesRand_P.nextInt(refPiecesList_P.size()));
        pickupReference1.setPieces(refPiecesRandom_P);
        pickupReference1.setSendToDriver(false);
        pickupReference1.setPartnerId("T100"); //***change
        referenceListPickup.add(pickupReference1);

        MasterDataLocationDetailsAPI masterDataLocationDetailsAPI = new MasterDataLocationDetailsAPI();
        List<String> locCodeList_P = Arrays.asList("0004FOMO", "0015FOTX", "100JPANJ", "1015LATX", "101RMATX", "101SLESC",
                "1021TUMS", "1024COTX", "1030OVKS", "1042SHLA", "106SSHNC", "1075GRSC", "108WHIGA"); //***Change
        Random locCodeRand_P = new Random();
        String locCodeRandom_P = locCodeList_P.get(locCodeRand_P.nextInt(locCodeList_P.size()));
        LocationDetails locDetails = masterDataLocationDetailsAPI.getLocationDetails(locCodeRandom_P);//###Random
        Stop stop = new Stop();//Change to stopP
        stop.setCityId(locDetails.getCityId());
        stop.setCityName(locDetails.getCity());
        stop.setState(locDetails.getState());
        stop.setZipCode(Integer.parseInt(locDetails.getPostalCode()));
        stop.setLocationId(locDetails.getId());
        stop.setLocationName(locDetails.getName());
        stop.setLocationType("Un-defined");
        stop.setAddress1(locDetails.getAddressLine1());
        stop.setAddress2(locDetails.getAddressLine2());
        stop.setStopType("PU");
        stop.setComments(commentsListPickup);
        stop.setCountry(locDetails.getCountry());
        stop.setContactName(null);
        stop.setContactNumber(null);

        Pair<Integer, Integer> pair1 = new Pair<>(5, 7);
        Pair<Integer, Integer> pair2 = new Pair<>(9, 11);
        Pair<Integer, Integer> pair3 = new Pair<>(6, 10);
        Pair<Integer, Integer> pair4 = new Pair<>(10, 13);
        Pair<Integer, Integer> pair5 = new Pair<>(2, 4);
        Pair<Integer, Integer> pair6 = new Pair<>(5, 7);
        Pair<Integer, Integer> pair7 = new Pair<>(10, 14);
        Pair<Integer, Integer> pair8 = new Pair<>(14, 16);
        Pair<Integer, Integer> pair9 = new Pair<>(15, 16);
        Pair<Integer, Integer> pair10 = new Pair<>(13, 16);
        Pair<Integer, Integer> pair11 = new Pair<>(19, 22);
        Pair<Integer, Integer> pair12 = new Pair<>(16, 20);
        Pair<Integer, Integer> pair13 = new Pair<>(19, 25);
        Pair<Integer, Integer> pair14 = new Pair<>(25, 30);
        Pair<Integer, Integer> pair15 = new Pair<>(20, 21);
        List<Pair<Integer, Integer>> pickDropDatePairList = Arrays.asList(pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, pair9,
                pair10, pair11, pair12, pair13, pair14, pair15); //***Change
        Random pickDropDatePairRand = new Random();
        Pair<Integer, Integer> pickUpDateRandom = pickDropDatePairList.get(pickDropDatePairRand.nextInt(pickDropDatePairList.size()));
        stop.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(pickUpDateRandom.getKey()));
        stop.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(pickUpDateRandom.getKey()));
        stop.setApptRequired(locDetails.getApptRequired());
        List<Boolean> confirmedValueList = Arrays.asList(false, true);
        Random confirmedValueRand = new Random();
        Boolean confirmedValueRandom = confirmedValueList.get(highValueRand.nextInt(confirmedValueList.size()));
        stop.setConfirmed(confirmedValueRandom);
        stop.setReferences(referenceListPickup);
        stop.setDriverLoadId(locDetails.getDriverLoadId());
        stopListDetails.add(stop);

        //DROP
        // Comment for Drop
        List<Comment> commentsListDrop = new ArrayList<Comment>();
        Comment dropComment1 = new Comment();
        List<String> commentTypeList_D = Arrays.asList("DC", "HC", "OC", "BC");
        Random commentTypeRand_D = new Random();
        String commentTypeRandom_D = commentTypeList_D.get(commentTypeRand_D.nextInt(commentTypeList_D.size()));
        dropComment1.setCommentType(commentTypeRandom_D);
        dropComment1.setComment(commentTypeRandom_D + ": ************DUMMY COMMENT**********");
        commentsListDrop.add(dropComment1);
        // Reference for Drop
        List<Reference> referenceListDrop = new ArrayList<Reference>();
        Reference dropReference1 = new Reference();
        List<String> refNoList_D = Arrays.asList("23145", "10087", "90876", "516754", "199866", "00231", "5433467", "425357");
        Random refNoRand_D = new Random();
        String refNoRandom_D = refNoList_D.get(refNoRand_D.nextInt(refNoList_D.size()));
        dropReference1.setReferenceNumber(refNoRandom_D);
        List<String> refTypeList_D = Arrays.asList("PU", "OR", "PS", "QL", "BM", "CG", "MB", "OI", "PO", "ZZ");
        Random refTypeRand_D = new Random();
        String refTypeRandom_D = refTypeList_D.get(refTypeRand_D.nextInt(refTypeList_D.size()));
        dropReference1.setReferenceType(refTypeRandom_D);
        List<String> refWeightList_D = Arrays.asList("100", "220", "300", "310", "600", "768", "210");
        Random refWeightRand_D = new Random();
        String refWeightRandom_D = refWeightList_D.get(refWeightRand_D.nextInt(refWeightList_D.size()));
        dropReference1.setWeight(refWeightRandom_D);
        List<String> refPiecesList_D = Arrays.asList("2", "5", "7", "8", "9", "10", "12", "15");
        Random refPiecesRand_D = new Random();
        String refPiecesRandom_D = refPiecesList_D.get(refPiecesRand_D.nextInt(refPiecesList_D.size()));
        dropReference1.setPieces(refPiecesRandom_D);
        dropReference1.setSendToDriver(false);
        dropReference1.setPartnerId("T100");
        referenceListDrop.add(dropReference1);

        List<String> locCodeList_D = Arrays.asList("1115PAWA", "1122DECO", "115CGLKY", "1160ALTN", "1165CAPA", "1165TOON",
                "1170NOAR", "1200PIPA", "1208COIA", "1213ELTX", "1220SAO2", "1225COFL", "1255FAOH"); //***Change
        Random locCodeRand_D = new Random();
        String locCodeRandom_D = locCodeList_D.get(locCodeRand_D.nextInt(locCodeList_D.size()));
        LocationDetails locDetailsD = masterDataLocationDetailsAPI.getLocationDetails(locCodeRandom_D);
        Stop stopD = new Stop();
        stopD.setCityId(locDetailsD.getCityId());
        stopD.setCityName(locDetailsD.getCity());
        stopD.setState(locDetailsD.getState());
        stopD.setZipCode(Integer.parseInt(locDetailsD.getPostalCode()));
        stopD.setLocationId(locDetailsD.getId());
        stopD.setLocationName(locDetailsD.getName());
        stopD.setLocationType("Un-defined");
        stopD.setAddress1(locDetailsD.getAddressLine1());
        stopD.setAddress2(locDetailsD.getAddressLine2());
        stopD.setStopType("SO");
        stopD.setComments(commentsListDrop);
        stopD.setCountry(locDetails.getCountry());
        stopD.setContactName(null);
        stopD.setContactNumber(null);
        stopD.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(pickUpDateRandom.getValue()));//###Random
        stopD.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(pickUpDateRandom.getValue()));//###Random
        stopD.setApptRequired(locDetails.getApptRequired());
        List<Boolean> confirmedValueListD = Arrays.asList(false, true);
        Random confirmedValueRandD = new Random();
        Boolean confirmedValueRandomD = confirmedValueListD.get(confirmedValueRandD.nextInt(confirmedValueListD.size()));
        stopD.setConfirmed(confirmedValueRandomD);
        stopD.setReferences(referenceListDrop);
        stopD.setDriverUnloadId(locDetails.getDriverUnloadId());
        stopListDetails.add(stopD);

        order.setStops(stopListDetails);

        order.setOrderType("T");
        order.setOrderMode("T");
        List<String> orderTypeIDList = Arrays.asList("HVHR", "HRISK", "NEWB", "UHV"); //##Change
        Random orderTypeIDRand = new Random();
        String orderTypeIDRandom = orderTypeIDList.get(orderTypeIDRand.nextInt(orderTypeIDList.size()));
        order.setOrderTypeId(orderTypeIDRandom);

        order.setAdditionalEquipments(new ArrayList<AdditionalEquipment>());
        order.setAgentOrderReRatingStatus("FTL-I");

        return order;
    }

    private HashMap<String, String> getRandomCustomerFLAT(){
        HashMap customer1 = new HashMap<String, String>();
        customer1.put("customerID","1000003 ");
        customer1.put("customerName","1220 EXHIBITS INC");
        customer1.put("customerLocation",null);
        customer1.put("customerAddress","3801 VULCAN DRIVE");
        customer1.put("customerState","TN");
        customer1.put("customerCity","NASHVILLE                                         ");
        customer1.put("customerZip","37211     ");

        HashMap customer2 = new HashMap<String, String>();
        customer2.put("customerID","1000036 ");
        customer2.put("customerName","7078 UNIVERSITY LOGISTICS SVCS");
        customer2.put("customerLocation",null);
        customer2.put("customerAddress","618 32ND STREET SE");
        customer2.put("customerState","MI");
        customer2.put("customerCity","GRAND RAPIDS                                      ");
        customer2.put("customerZip","49548     ");

        HashMap customer3 = new HashMap<String, String>();
        customer3.put("customerID","1000054 ");
        customer3.put("customerName","7425 RAINBOW MOVERS INC");
        customer3.put("customerLocation",null);
        customer3.put("customerAddress","19 NATIONAL DR");
        customer3.put("customerState","MA");
        customer3.put("customerCity","FRANKLIN                                          ");
        customer3.put("customerZip","02038     ");

        List<HashMap<String, String>> customerList =  new ArrayList<HashMap<String, String>>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);

        Random custRand = new Random();
        HashMap<String, String> customer = customerList.get(custRand.nextInt(customerList.size()));

        return customer;
    }

    public Order getNewOrderRequestPayloadFTLForFLAT() throws Exception {
        Order order = new Order();
        order.setAgencyResponsiblityCode("1001");
        order.setAgentCaptiveOrder("Y");
        order.setUserType("AGFT");
        order.setDivision("FLAT");
        order.setBillingMethod("P");
        order.setCompanyId("T100");
        order.setAgencyId("1001    ");
        order.setAgencyPayeeId("DEBBCEIA");

        // ####### Customer Details ######
        order.setCustomerId("1000054 ");
        order.setCustomerName("7425 RAINBOW MOVERS INC");
        order.setCustomerLocation(null);
        order.setCustomerLocationAddress("19 NATIONAL DR");
        order.setCustomerLocationState("MA");
        order.setCustomerLocationCity("FRANKLIN                                          ");
        order.setCustomerLocationZip("02038     ");
        order.setRevenueCode("");

        order.setAgentPortalUserId("1001mgil  ");
        order.setMcleodUserId("1001mgil  ");
        order.setEquipmentTypeId("F");
        order.setOrderValue("5000");
        order.setHighValue(true);
        order.setTotalWeight("1000");
        order.setTotalPieces("3");
        order.setTemperatureMin("50");
        order.setTemperatureMax("90");
        order.setTemperatureUOM("Fahrenheit");
        order.setBillNumber("30899");
        order.setConsigneeRefNumber("4110089");
        order.setPlanningComments("ORDER IS CREATED BY AUTOMATION TEST");
        order.setOperationsUser("1001mgil  ");
        order.setOperationsUserId("1001mgil  ");
        order.setEnteredUserId("1001mgil  ");
        order.setEnteredUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
        order.setEnteredStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
        order.setOperationsStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
        order.setOperationsUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
        order.setNoOfLoads(1);
        order.setRevenueCode("");

        Commodities commodities = new Commodities();
        commodities.setCommodity(new ArrayList<Commodity>());
        commodities.setServices(new ArrayList<ServiceEntry>());
        order.setCommodities(commodities);

        ArrayList<Stop> stopListDetails = new ArrayList<Stop>();
        //PICKUP
        // Comment for pickup
        List<Comment> commentsListPickup = new ArrayList<Comment>();
        Comment pickupComment1 = new Comment();
        pickupComment1.setCommentType("DC");
        pickupComment1.setComment("*****DUMMY COMMENT******");
        commentsListPickup.add(pickupComment1);
        // Reference for pickup
        List<Reference> referenceListPickup = new ArrayList<Reference>();
        Reference pickupReference1 = new Reference();
        pickupReference1.setReferenceNumber("23145");
        pickupReference1.setReferenceType("PU");
        pickupReference1.setWeight("300");
        pickupReference1.setPieces("5");
        pickupReference1.setSendToDriver(false);
        pickupReference1.setPartnerId("T100");
        referenceListPickup.add(pickupReference1);

        MasterDataLocationDetailsAPI masterDataLocationDetailsAPI = new MasterDataLocationDetailsAPI();
        LocationDetails locDetails = masterDataLocationDetailsAPI.getLocationDetails("0004FOMO");//###Random
        Stop stop = new Stop();//Change to stopP
        stop.setCityId(locDetails.getCityId());
        stop.setCityName(locDetails.getCity());
        stop.setState(locDetails.getState());
        stop.setZipCode(Integer.parseInt(locDetails.getPostalCode()));
        stop.setLocationId(locDetails.getId());
        stop.setLocationName(locDetails.getName());
        stop.setLocationType("Un-defined");
        stop.setAddress1(locDetails.getAddressLine1());
        stop.setAddress2(locDetails.getAddressLine2());
        stop.setStopType("PU");
        stop.setComments(commentsListPickup);
        stop.setCountry(locDetails.getCountry());
        stop.setContactName(null);
        stop.setContactNumber(null);
        stop.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(1));
        stop.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(1));
        stop.setApptRequired(locDetails.getApptRequired());
        stop.setConfirmed(false);
        stop.setReferences(referenceListPickup);
        stop.setDriverLoadId(locDetails.getDriverLoadId());
        stopListDetails.add(stop);

        //DROP
        // Comment for Drop
        List<Comment> commentsListDrop = new ArrayList<Comment>();
        Comment dropComment1 = new Comment();
        dropComment1.setCommentType("DC");
        dropComment1.setComment("************DUMMY COMMENT**********");
        commentsListDrop.add(dropComment1);
        // Reference for Drop
        List<Reference> referenceListDrop = new ArrayList<Reference>();
        Reference dropReference1 = new Reference();
        dropReference1.setReferenceNumber("23145");
        dropReference1.setReferenceType("PS");
        dropReference1.setWeight("600");
        dropReference1.setPieces("8");
        dropReference1.setSendToDriver(false);
        dropReference1.setPartnerId("T100");
        referenceListDrop.add(dropReference1);

        LocationDetails locDetailsD = masterDataLocationDetailsAPI.getLocationDetails("1122DECO");
        Stop stopD = new Stop();
        stopD.setCityId(locDetailsD.getCityId());
        stopD.setCityName(locDetailsD.getCity());
        stopD.setState(locDetailsD.getState());
        stopD.setZipCode(Integer.parseInt(locDetailsD.getPostalCode()));
        stopD.setLocationId(locDetailsD.getId());
        stopD.setLocationName(locDetailsD.getName());
        stopD.setLocationType("Un-defined");
        stopD.setAddress1(locDetailsD.getAddressLine1());
        stopD.setAddress2(locDetailsD.getAddressLine2());
        stopD.setStopType("SO");
        stopD.setComments(commentsListDrop);
        stopD.setCountry(locDetails.getCountry());
        stopD.setContactName(null);
        stopD.setContactNumber(null);
        stopD.setSchedDepartDate(DateTimeUtils.getDateTimeAfterFormat1(2));
        stopD.setSchedArriveDate(DateTimeUtils.getDateTimeAfterFormat1(2));
        stopD.setApptRequired(locDetails.getApptRequired());
        stopD.setConfirmed(false);
        stopD.setReferences(referenceListDrop);
        stopD.setDriverUnloadId(locDetails.getDriverUnloadId());
        stopListDetails.add(stopD);

        order.setStops(stopListDetails);

        order.setOrderType("T");
        order.setOrderMode("T");
        order.setOrderTypeId("HRISK");

        order.setAdditionalEquipments(new ArrayList<AdditionalEquipment>());
        order.setAgentOrderReRatingStatus("FTL-I");

        return order;
    }
}
