package requestPayloadProvider.opportunityPayloadProvider;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import apiHelper.AgencyDtlsForUserAPI;
import apiHelper.GetUserAPI;
import apiHelper.MasterDataCustomerAPI;
import apiHelper.OperationAgentUserAPI;
import apiHelper.ResponsibilityCodeCheckApi;
import enumClass.OPTION;
import pojoClasses.requestPojo.NeedRevisit.Commodities;
import pojoClasses.requestPojo.NeedRevisit.Commodity;
import pojoClasses.requestPojo.NeedRevisit.Opportunity;
import pojoClasses.requestPojo.NeedRevisit.ServiceEntry;
import pojoClasses.responsePojo.MasterData.Customer.Customer;
import specifications.opportunitySpecifications.OpportunityCommoditySpec;
import specifications.opportunitySpecifications.OpportunityCustomerRatingSpec;
import specifications.opportunitySpecifications.OpportunityEquipmentDetailsSpec;
import specifications.opportunitySpecifications.OpportunityOrderGeneralDetailsSpec;
import specifications.opportunitySpecifications.OpportunityStopSpec;
import utilities.DateTimeUtils;

public class NewOpportunityRequestBody {
	private ResponsibilityCodeCheckApi responsibilityCodeCheckApi;
	private GetUserAPI getUserAPI;
	private AgencyDtlsForUserAPI agencyDtlsForUserAPI;
	private OperationAgentUserAPI operationAgentUserAPI;
	private MasterDataCustomerAPI masterDataCustomerAPI;
	
	public NewOpportunityRequestBody() throws Exception {
		responsibilityCodeCheckApi = new ResponsibilityCodeCheckApi();
		getUserAPI = new GetUserAPI();
		agencyDtlsForUserAPI = new AgencyDtlsForUserAPI();
		operationAgentUserAPI = new OperationAgentUserAPI();
		masterDataCustomerAPI = new MasterDataCustomerAPI();
	}
	
	public JsonNode getOpportunityRequestBody(OpportunityOrderGeneralDetailsSpec orderGeneralDetailsSpec, String customerID,
			OpportunityCommoditySpec commoditySpec, OpportunityStopSpec stopSpec,
			OpportunityEquipmentDetailsSpec equipDetailsSpec,
			OpportunityCustomerRatingSpec opportunityCustomerRatingSpec,
			String billingMethod) throws Exception {
		Opportunity opportunity = new Opportunity();
		
		// ####### Agency/User/Company/Billing Details ######
		opportunity.setAgencyResponsiblityCode(responsibilityCodeCheckApi.getResponsibilityCode());
		opportunity.setUserType(getUserAPI.getUserType());
		opportunity.setDirectCustomerInd(null);
		opportunity.setBillingMethod(billingMethod);
		opportunity.setSegAllocCode(null);
		opportunity.setCompanyId(agencyDtlsForUserAPI.getCompanyId());
		opportunity.setAgencyId(getUserAPI.getAgencyId());
		opportunity.setAgencyPayeeId(agencyDtlsForUserAPI.getAgencyPayeeId());
		opportunity.setAgentPortalUserId("1001mgil  ");
		opportunity.setMcleodUserId(getUserAPI.getMcleodUserId());
		
		// ###### Special Reference ######
		opportunity.setOperationsUser("1001mgil  ");
		opportunity.setOperationsUserId(operationAgentUserAPI.getOperationUserList().get(0).toString());
		opportunity.setEnteredUserId("1001mgil  ");
		opportunity.setEnteredUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
		opportunity.setEnteredStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
		opportunity.setOperationsStrUserDate(DateTimeUtils.getDateTimeAfterFormat2(0));
		opportunity.setOperationsUserDate(DateTimeUtils.getDateTimeAfterFormat1(0));
		
		// ####### Order General Details ######
		opportunity.setSubjectOrderStatus("S");
		opportunity.setSubjectOrderVoidDate(DateTimeUtils.getDateTimeAfterFormat1(2));
		opportunity.setOrderType(orderGeneralDetailsSpec.getOrderType());
		opportunity.setOrderMode(orderGeneralDetailsSpec.getOrderMode());
		opportunity.setOrderTypeId(orderGeneralDetailsSpec.getOrderTypeId());
		opportunity.setOrderValue(orderGeneralDetailsSpec.getOrderValue());
		
		// ####### Customer Details ######
		Customer customerDetails = masterDataCustomerAPI.getCustomer(customerID);
		opportunity.setCustomerId(customerDetails.getId());
		opportunity.setCustomerName(customerDetails.getName());
		opportunity.setCustomerLocation(customerDetails.getLocationName());
		opportunity.setCustomerLocationAddress(customerDetails.getLocationAddress());
		opportunity.setCustomerLocationState(customerDetails.getState());
		opportunity.setCustomerLocationCity(customerDetails.getCity());
		opportunity.setCustomerLocationZip(customerDetails.getZipCode());
		opportunity.setRevenueCode("");
			
		// ###### Equipment details ######
		opportunity.setEquipmentTypeId(equipDetailsSpec.getEquipmentTypeId());
		
		// ###### COMMODITIES AND SERVICES ######
		Commodities commodities = new Commodities();
		Commodity commodity = new Commodity();
		commodity.setId(commoditySpec.getID());
		commodity.setDescription(commoditySpec.getDesc());
		List<Commodity> commodityList = new ArrayList<Commodity>();
		commodityList.add(commodity);
		commodities.setCommodity(commodityList);
		commodities.setServices(new ArrayList<ServiceEntry>());
		opportunity.setCommodities(commodities);
		
		// ###### Customer Rating Details ######
		opportunity.setRate(opportunityCustomerRatingSpec.getRate());
		opportunity.setRateType(opportunityCustomerRatingSpec.getRateType());
		opportunity.setRateUnit(opportunityCustomerRatingSpec.getRateUnit());
		opportunity.setLineHaulCharges(opportunityCustomerRatingSpec.getLineHaulCharges());
		opportunity.setAgentOrderReRatingStatus(opportunityCustomerRatingSpec.getAgentOrderReRatingStatus());
		
		// ###### STOP DETAILS ######
		opportunity.setStops(stopSpec.getStopListDetails());
		
		return removeUncessaryFields(opportunity);
	}
	
	public JsonNode getOpportunityRequestBody() throws Exception {
		OpportunityOrderGeneralDetailsSpec orderGeneralDetailsSpec = new OpportunityOrderGeneralDetailsSpec();
		String customerID = "1000001 ";
		OpportunityCommoditySpec commoditySpec = new OpportunityCommoditySpec();
		OpportunityStopSpec stopSpec = new OpportunityStopSpec(OPTION.DEFAULT);
		OpportunityEquipmentDetailsSpec equipDetailsSpec = new OpportunityEquipmentDetailsSpec();
		OpportunityCustomerRatingSpec opportunityCustomerRatingSpec = new OpportunityCustomerRatingSpec();
		String billingMethod = "P";
		
		JsonNode opportunityJsonNode = getOpportunityRequestBody(orderGeneralDetailsSpec, customerID,
				commoditySpec, stopSpec,
				equipDetailsSpec,
				opportunityCustomerRatingSpec,
				billingMethod);
		
		return opportunityJsonNode;
	}
	
	
	private JsonNode removeUncessaryFields(Opportunity obj) {	
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode node = objectMapper.convertValue(obj, JsonNode.class);
		ObjectNode commodityNode = (ObjectNode)node.get("commodities").get("commodity").get(0);
		commodityNode.remove("freightClass");
		commodityNode.remove("quantity");
		commodityNode.remove("packaging");
		commodityNode.remove("length");	
		commodityNode.remove("width");
		commodityNode.remove("height");
		commodityNode.remove("density");
		commodityNode.remove("weight");
		commodityNode.remove("weightUOM");	
		commodityNode.remove("handlingUnits");
		commodityNode.remove("reqSpots");
		commodityNode.remove("value");
		commodityNode.remove("nmfcCode");
		commodityNode.remove("hazmat");	
		commodityNode.remove("hazmatPackageGroup");
		commodityNode.remove("hazmatClassCode");
		commodityNode.remove("hazmatUNNbr");
		commodityNode.remove("hazmatShipname");	
		commodityNode.remove("hazmatContactName");
		commodityNode.remove("hazmatContactNumber");
		commodityNode.remove("hazmatDescription");
		commodityNode.remove("linearUnits");	
		commodityNode.remove("cubicUnits");
		
		int stopArraySize = node.get("stops").size();
		for(int i=0;i<stopArraySize;i++) {
			ObjectNode stopNode = (ObjectNode) node.get("stops").get(i);
			stopNode.remove("address2");
			stopNode.remove("locationType");
			stopNode.remove("apptRequired");
			stopNode.remove("comments");
			stopNode.remove("confirmed");
			stopNode.remove("references");
			stopNode.remove("driverLoadId");
			stopNode.remove("email");
			stopNode.remove("driverUnloadId");
		}
		
		return node;
					
	}
}
