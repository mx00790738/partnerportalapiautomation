/**
 * 
 */
package requestPayloadProvider.MasterData.Customer;

/**
 * @author MX0C92049
 *
 */
public class CustomerSearchAPIRequestPayload {

	/**
	 * 
	 */
	public CustomerSearchAPIRequestPayload() throws Exception {
		//needed for integration test commenting these for component test
		//setAgencyId(APIManager.getUserAPIInstance().getAgencyId());
		//setUserType(APIManager.getUserAPIInstance().getUserType());
		setAgencyId("");
		setUserType("");
		setCustomerId("");
		setCustomerName("");
		setCity("");
		setState("");
		setZip("");
		setAddress("");
	}

	private String agencyId;
	private String customerName;
	private String userType;
	private String customerId;
	private String city;
	private String state;
	private String zip;
	private String address;
	private String userGrop;
	private String isActive;

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserGrop() {
		return userGrop;
	}

	public void setUserGrop(String userGrop) {
		this.userGrop = userGrop;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String userGrop) {
		this.isActive = isActive;
	}
}
