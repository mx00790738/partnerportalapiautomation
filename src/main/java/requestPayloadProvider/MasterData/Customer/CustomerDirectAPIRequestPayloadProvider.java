/**
 * 
 */
package requestPayloadProvider.MasterData.Customer;

/**
 * @author Manoj
 *
 */
public class CustomerDirectAPIRequestPayloadProvider {

	public CustomerDirectAPIRequestPayloadProvider() {
		setAgencyId("");
		setCustomerId("");
		setCustomerName("");
		setAddress("");
		setDivision("");
	}
	
	private String address;
	private String agencyId;
	private String customerId;
	private String customerName;
	private String division;
	
	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
}
