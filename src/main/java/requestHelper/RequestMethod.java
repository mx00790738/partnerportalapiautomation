/**
 * 
 */
package requestHelper;

import io.restassured.response.Response;
import io.restassured.specification.QueryableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.SpecificationQuerier;
import pojoClasses.responsePojo.BusinessResponse;

import static io.restassured.RestAssured.given;

/**
 * @author Manoj
 *
 */
public class RequestMethod {
	public static Response GET(RequestSpecification requestSpecification) throws Exception {
		Response response = 
				given(requestSpecification).
				when().
					get();
		QueryableRequestSpecification queryRequest = SpecificationQuerier.query(requestSpecification);

		int statusCode = response.getStatusCode();
		if(statusCode < 200 || statusCode > 299) {
			throw new Exception("Failure response!! Status code: "+statusCode
					+" from API: "+ queryRequest.getBasePath()+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		BusinessResponse businessResponse;
		try {
			businessResponse = response.as(BusinessResponse.class);
		} catch (Exception e) {
			System.out.println(e.toString());
		    //TODO Add a log line to say response structure is changed..In all the methods
			throw new Exception("Unexpected response!! from API: "+ queryRequest.getBasePath()
					+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		String businessResponseStatus = businessResponse.getStatus();
		boolean status;
		try{
			status = businessResponseStatus.equals("SUCCESS");
		}catch (NullPointerException e) {
			status = true;
		}
		if(!status) {
			throw new Exception("API: "+queryRequest.getBasePath()+"\tFailure business response : \n"
					+ response.getBody().asPrettyString());
		}
		return response;
	}
	
	public static Response GET_DEBUG(RequestSpecification requestSpecification) throws Exception {
		Response response = 
				given(requestSpecification).
					log().all().
				when().
					get().prettyPeek();
		QueryableRequestSpecification queryRequest = SpecificationQuerier.query(requestSpecification);

		int statusCode = response.getStatusCode();
		if(statusCode < 200 || statusCode > 299) {
			throw new Exception("Failure response!! Status code: "+statusCode
					+" from API: "+ queryRequest.getBasePath() + "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		BusinessResponse businessResponse;
		try {
			businessResponse = response.as(BusinessResponse.class);
		} catch (Exception e) {
			throw new Exception("Unexpected response!! from API: "+ queryRequest.getBasePath()
					+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		String businessResponseStatus = businessResponse.getStatus();
		boolean status;
		try{
			status = businessResponseStatus.equals("SUCCESS");
		}catch (Exception e) {
			status = true;
		}
		if(!status) {
			throw new Exception("API: "+queryRequest.getBasePath()+"\tFailure business response : \n"
					+ response.getBody().asPrettyString());
		}
		return response;
	}
	
	public static Response POST(RequestSpecification requestSpecification) throws Exception {
		Response response = 
				given(requestSpecification).
				when().
					post();
		QueryableRequestSpecification queryRequest = SpecificationQuerier.query(requestSpecification);
		int statusCode = response.getStatusCode();
		if(statusCode < 200 || statusCode > 299) {
			throw new Exception("Failure response!! Status code: "+statusCode
					+" from API: "+ queryRequest.getBasePath()+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		BusinessResponse businessResponse;
		try {
			businessResponse = response.as(BusinessResponse.class);
		} catch (Exception e) {
			throw new Exception("Unexpected response!! from API: "+ queryRequest.getBasePath()
					+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		String businessResponseStatus = businessResponse.getStatus();
		boolean status;
		try{
			status = businessResponseStatus.equals("SUCCESS");
		}catch (Exception e) {
			status = true;
		}
		if(!status) {
			throw new Exception("API: "+queryRequest.getBasePath()+"\tFailure business response : \n"
					+ response.getBody().asPrettyString());
		}
		return response;
	}
	
	public static Response POST_DEBUG(RequestSpecification requestSpecification) throws Exception {
		Response response = 
				given(requestSpecification).
					log().all().
				when().
					post().prettyPeek();
		QueryableRequestSpecification queryRequest = SpecificationQuerier.query(requestSpecification);

		int statusCode = response.getStatusCode();
		if(statusCode < 200 || statusCode > 299) {
			throw new Exception("Failure response!! Status code: "+statusCode
			+" from API: "+ queryRequest.getBasePath()+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		BusinessResponse businessResponse;
		try {
			businessResponse = response.as(BusinessResponse.class);
		} catch (Exception e) {
			throw new Exception("Unexpected response!! from API: "+ queryRequest.getBasePath()
					+ "\n Response payload \n"+ response.getBody().asPrettyString());
		}
		String businessResponseStatus = businessResponse.getStatus();
		boolean status;
		try{
			status = businessResponseStatus.equals("SUCCESS");
		}catch (Exception e) {
			status = true;
		}
		if(!status) {
			throw new Exception("API: "+queryRequest.getBasePath()+"\tFailure business response : \n"
					+ response.getBody().asPrettyString());
		}
		return response;
	}
}
