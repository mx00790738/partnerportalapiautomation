package deserializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import pojoClasses.responsePojo.BusinessResponse;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Deserialize {
    public static <T> T as(Response response, T t) {
        T responsePayload = null;
        try {
            responsePayload = response.as((Type) t.getClass());
        } catch (Exception e) {
        }
        return responsePayload;
    }

    public static <T> List<T> deserializeJsonToObjectList(Response response, Class<T> classOnWhichArrayIsDefined) throws Exception {
        BusinessResponse<List<T>> responsePayload = response.as(BusinessResponse.class);
        if(responsePayload.getResponse()==null){
            throw new Exception("Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        ObjectMapper mapper = new ObjectMapper();
        String json=null;
        try {
            json = mapper.writeValueAsString(responsePayload.getResponse());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + classOnWhichArrayIsDefined.getName() + ";");
        T[] objects = mapper.readValue(json, arrayClass);
        return Arrays.asList(objects);
    }

    public static <T> T deserializeJsonToObject(Response response, Class<T> classOnWhichObjIsDefined) throws Exception {
        BusinessResponse<T> responsePayload = response.as(BusinessResponse.class);
        if(responsePayload.getResponse()==null){
            throw new Exception("Unexpected!! response object is null \n Response payload: \n" + response.asPrettyString());
        }
        ObjectMapper mapper = new ObjectMapper();
        String json=null;
        try {
            json = mapper.writeValueAsString(responsePayload.getResponse());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        T object = mapper.readValue(json, classOnWhichObjIsDefined);
        return object;
    }

    //todo remove below method not needed for deserializer
    public static <T> T retrieveObjectFromObjectList(List<T> objList, String methodName, String arg1)
            throws Exception {
        T obj = null;
        for(T o:objList) {
            Method method = o.getClass().getMethod(methodName);
            if (method.invoke(o, null).equals(arg1)) {
                obj = o;
            }
        }
        if(obj == null){
            throw new Exception("Unexpected!! Record with "+methodName.substring(3)+": ["+ arg1 +"] not found");
        }
        return obj;
    }
}
